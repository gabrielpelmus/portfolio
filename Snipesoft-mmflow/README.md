# mmflow

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 4.0.4.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org)
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- npm install --global --production windows-build-tools
- [node-gyp](https://github.com/nodejs/node-gyp)
- [SQLite](https://www.sqlite.org/quickstart.html)

### Development
1. Pentru DB nu mai folosim XAMPP sau WAMP si ne punem doar ce se gaseste la adresa [Workbench 5.7 cu MySQL](https://dev.mysql.com/downloads/windows/installer/5.7.html)
2. Am instalat si MySQL Notifier ca sa ma pot juca cu enable / sdisable la serviciul de MySQL
3. Se face clona la proiect de pe bitbucket
4. Run `npm install` to install server dependencies.
5. Daca nu porneste proiectul direct cu "gulp serve", e nevoie de clean-up pe calculator si anume: se dezinstaleaza node, se sterg folderele de genul "node-cli" daca exita in Program files sau Program data
6. Se reinstaleaza node v4.7, apoi "npm install npm -g"
7. In Environment Variables se adauga un nod nou NODE_PATH cu %NODE_PATH;C:\Users\....\AppData\Roaming\npm\node_modules, daca nu exista
8. In proiect "npm install" si "npm rebuild"
9. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Testing

Running `npm test` will run the unit tests with karma.