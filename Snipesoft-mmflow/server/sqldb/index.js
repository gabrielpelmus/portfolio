/**
 * Sequelize initialization module
 */

'use strict';

import path from 'path';
import config from '../config/environment';
import Sequelize from 'sequelize';
import dal from "./db"

var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.database, config.sequelize.username, config.sequelize.password,
    config.sequelize.options),
  dal: dal
};

// Insert models below
db.StuliFile = db.sequelize.import('../api/stuliFile/stuliFile.model');
db.PnlFile = db.sequelize.import('../api/pnlFile/pnlFile.model');
db.MaterialGroup = db.sequelize.import('../api/materialGroup/materialGroup.model');
db.Sector = db.sequelize.import('../api/sector/sector.model');
db.CostCenter = db.sequelize.import('../api/costCenter/costCenter.model');
db.BudgetLine = db.sequelize.import('../api/budgetLine/budgetLine.model');
db.BudgetLineAccount = db.sequelize.import('../api/budgetLineAccount/budgetLineAccount.model');
db.BudgetYearly = db.sequelize.import('../api/budgetYearly/budgetYearly.model');
db.SellVolume = db.sequelize.import('../api/sellVolume/sellVolume.model');
db.StuliFile = db.sequelize.import('../api/stuliFile/stuliFile.model');
db.Stuli = db.sequelize.import('../sqldb/models/stuli.model');
db.Pnl = db.sequelize.import('../sqldb/models/pnl.model');
db.ReportProfitLoss = db.sequelize.import('../api/reportProfitLoss/reportProfitLoss.model');
db.AchievementFile = db.sequelize.import('../api/achievementFile/achievementFile.model');
db.Achievement = db.sequelize.import('../sqldb/models/achievement.model');
// db.Thing = db.sequelize.import('../api/thing/thing.model');
db.User = db.sequelize.import('../api/user/user.model');

db.MaterialGroup.belongsTo(db.Sector, { foreignKey: 'sector_id' });
db.Sector.hasMany(db.MaterialGroup, { foreignKey: 'sector_id' });

db.StuliFile.hasMany(db.Stuli, { foreignKey: 'file_id', onDelete: 'CASCADE' });
db.StuliFile.belongsTo(db.User, { foreignKey: 'created_by' });
db.Stuli.belongsTo(db.StuliFile, { foreignKey: 'file_id' });

// db.MaterialGroup.hasMany(db.Stuli, { foreignKey: 'material_group_id' })
// db.Stuli.belongsTo(db.MaterialGroup, { foreignKey: 'material_group_id' });

db.AchievementFile.hasMany(db.Achievement, { foreignKey: 'file_id', onDelete: 'CASCADE' });
db.AchievementFile.belongsTo(db.User, { foreignKey: 'created_by' });
db.Achievement.belongsTo(db.AchievementFile, { foreignKey: 'file_id' });

// db.MaterialGroup.hasMany(db.Achievement, { foreignKey: 'material_group_id' });
// db.Achievement.belongsTo(db.MaterialGroup, { foreignKey: 'material_group_id' });

db.BudgetLineAccount.belongsTo(db.BudgetLine, {foreignKey: 'budget_line_id' } );
db.BudgetLine.hasMany(db.BudgetLineAccount, {foreignKey: 'budget_line_id' });

db.BudgetYearly.belongsTo(db.BudgetLine, {foreignKey: 'budget_line_id' });
db.BudgetLine.hasMany(db.BudgetYearly, {foreignKey: 'budget_line_id' });

db.PnlFile.hasMany(db.Pnl, { foreignKey: 'file_id', onDelete: 'CASCADE' });
db.PnlFile.belongsTo(db.User, { foreignKey: 'created_by' });
db.Pnl.belongsTo(db.PnlFile, { foreignKey: 'file_id' });

module.exports = db;
