'use strict';

export default function (sequelize, DataTypes) {
  return sequelize.define('Stuli', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    logisticUnit: {
      type: DataTypes.STRING,
      field: 'logistic_unit'
    },
    duration: DataTypes.DECIMAL(10,2),
    materialCode: {
      type: DataTypes.STRING,
      field: 'material_code'
    },
    materialGroupCode: {
      type: DataTypes.STRING,
      field: 'material_group_code'
    },
    materialDescription: {
      type: DataTypes.STRING, 
      field: 'material_description'
    }
  }, {
      name: {
        singular: 'stuli',
        plural: 'stulis',
      },
      timestamps: false,
      paranoid: false,
      underscored: true,
      tableName: 'stulis'
    }
  );
}
