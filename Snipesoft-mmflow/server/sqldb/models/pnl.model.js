'use strict';

export default function (sequelize, DataTypes) {
  return sequelize.define('Pnl', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    fileId: {
      type: DataTypes.INTEGER,
      field: 'file_id'
    },
    account: DataTypes.STRING,
    postingDateTime: {
      type: DataTypes.DATE,
      field: 'posting_date'
    },
    amount: DataTypes.INTEGER,
    currency: DataTypes.STRING,
    costCenter: {
      type: DataTypes.INTEGER, 
      field: 'cost_center_code'
    },
    createdAt: {
      type: DataTypes.DATE, 
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.DATE, 
      field: 'updated_at'
    },
    deletedAt: {
      type: DataTypes.DATE, 
      field: 'deleted_at'
    },
    createdBy: {
      type: DataTypes.INTEGER, 
      field: 'created_by'
    },
    updatedBy: {
      type: DataTypes.INTEGER, 
      field: 'updated_by'
    },
    deletedBy: {
      type: DataTypes.INTEGER, 
      field: 'deleted_by'
    }
  }, {
      name: {
        singular: 'pnl',
        plural: 'pnls',
      },
      timestamps: false,
      paranoid: false,
      underscored: true,
      tableName: 'pnl'
    }
  );
}
