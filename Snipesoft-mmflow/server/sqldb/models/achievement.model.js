'use strict';

export default function (sequelize, DataTypes) {
    return sequelize.define('Achievement', {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        recordedDateTime: {
            type: DataTypes.DATE,
            field: 'recorded_date_time'
        },
        logisticUnit: {
            type: DataTypes.STRING,
            field: 'logistic_unit'
        },
        storageUnit: {
            type: DataTypes.STRING,
            field: 'storage_unit'
        },
        quantity: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        materialCode: {
            type: DataTypes.STRING,
            field: 'material_code'
        },
        materialDescription: {
            type: DataTypes.STRING, 
            field: 'material_description'
        },
        materialGroupCode: {
            type: DataTypes.STRING,
            field: 'material_group_code'
        },
        movementType: {
            type: DataTypes.STRING,
            field: 'movement_type'
        },
        profitCenter: {
            type: DataTypes.STRING,
            field: 'profit_center'
        },
    }, {
            name: {
                singular: 'achievement',
                plural: 'achievements',
            },
            timestamps: false,
            paranoid: false,
            underscored: true,
            tableName: 'achievements'
        }
    );
}
