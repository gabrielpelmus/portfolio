"use strict";

import mysql from 'mysql';

module.exports = function () {
    var internals = {};
    var externals = {};

    externals.callback = function (err, data, reply, query, statusCode) {

        if (err) {
            var error = {
                dbMessage: err,
                details: {
                    warning: "[DISABLE IN PRODUCTION!] - to be replaced with app-wide db error log ",
                    query: query
                },
                //operationCode: 999
            };
            statusCode = statusCode || 500;
            reply.status(statusCode).send(error);
            // return;
        } else {
            statusCode = statusCode || 200;
            reply.status(statusCode).json(data);
        }
    }

    var options = {
        multipleStatements: true,
        host: "localhost",
        user: "root",
        password: "BvBzDbPh4",
        database: "mmflow",
        timezone: 'utc'
    };

    var pool = mysql.createPool(options);

    internals.connect = function (connectHandler) {
        pool.getConnection(function (err, connection) {
            if (err) return connectHandler(err, null);
            return connectHandler(null, connection);
        });
    };

    externals.query = function (params) {
        var sql = params.sql;
        var values = params.values;
        var queryHandler = params.callback;
        internals.connect(function (err, connection) {
            if (err) return queryHandler(err, null);
            console.log(sql);
            connection.query(sql, values, function (err, rows, fields) {                
                queryHandler(err, rows);
                connection.release();
            });
        });
    };

    externals.transaction = function (params) {

        var queries = params.queries;
        var queryHandler = params.callback;
        var results = [];

        var callTransactionQuery = function (connection, index, result) {
            var query = queries[index];

            if (query) {
                var sql = query.statement;
                var vals = query.values;

                /* replace !# with the existing result at the specified index */
                var matches = sql.match(/!\d+\.\w+/g);
                if (matches) {
                    matches.forEach(function (match) {
                        let separatorIndex = match.indexOf('.');
                        let qIndex = match.substr(1, separatorIndex - 1);
                        let propertyName = match.substr(separatorIndex + 1, match.length - separatorIndex - 1);
                        if (results[qIndex].length > 0) {
                            sql = sql.replace(match, results[qIndex][0][propertyName]);
                        };
                    });
                };
                /* replace "!" wildcard with previous result */
                if (result) {
                    sql = sql.replace("!", result.insertId);
                }
console.log(sql);
                connection.query(sql, vals, function (err, rows, fields) {
                    if (err) {
                        connection.rollback(function () { throw err; });
                        connection.release();
                        return queryHandler(err, null);
                    };

                    results.push(rows);
                    callTransactionQuery(connection, ++index, rows);
                });
            } else {
                connection.commit(function (err) {
                    if (err) {
                        connection.rollback(function () { throw err; });
                    }
                    queryHandler(err, results);
                    connection.release();
                });
            }
        }

        internals.connect(function (err, connection) {
            if (err) return queryHandler(err, null);

            connection.beginTransaction(function (err) {
                if (err) {
                    connection.release();
                    return queryHandler(err, null);
                }

                callTransactionQuery(connection, 0, null);
            });
        });
    }

    return externals;
} ();