'use strict';
/*eslint no-process-env:0*/

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP
  || process.env.ip
  || undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT
  || process.env.port
  || 3022,

  sequelize: {
    uri: process.env.SEQUELIZE_URI
    || 'sqlite://',
    database: 'mmflow',
    username: 'root',
    password: 'BvBzDbPh4',
    options: {
      logging: false,
      storage: 'dist.sqlite',
      define: {
        timestamps: false
      },
      host: '127.0.0.1',
      dialect: 'mysql',
      multipleStatements: true
    }
  }
};
