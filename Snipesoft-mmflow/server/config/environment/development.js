'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  sequelize: {
    database: 'mmflow',
    username: 'root',
    password: 'BvBzDbPh4',
    options: {
      host: '127.0.0.1',
      dialect: 'mysql',
      multipleStatements: true,
      define: {
        timestamps: false
      }
    }
  },

  // Seed database on startup
  seedDB: false   
  
};
