/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import sqldb from '../sqldb';
// var Thing = sqldb.Thing;

var User = sqldb.User;
var Sector = sqldb.Sector;
var MaterialGroup = sqldb.MaterialGroup;
var StuliFile = sqldb.StuliFile;
var Stuli = sqldb.Stuli;
var PnlFile = sqldb.PnlFile;
var Pnl = sqldb.Pnl;
var AchievementFile = sqldb.AchievementFile;
var Achievement = sqldb.Achievement; 

// Thing.sync()
//   .then(() => {
//     return Thing.destroy({ where: {} });
//   })
//   .then(() => {
//     Thing.bulkCreate([{
//       name: 'Development Tools',
//       info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
//             + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
//             + 'Stylus, Sass, and Less.'
//     }, {
//       name: 'Server and Client integration',
//       info: 'Built with a powerful and fun stack: MongoDB, Express, '
//             + 'AngularJS, and Node.'
//     }, {
//       name: 'Smart Build System',
//       info: 'Build system ignores `spec` files, allowing you to keep '
//             + 'tests alongside code. Automatic injection of scripts and '
//             + 'styles into your index.html'
//     }, {
//       name: 'Modular Structure',
//       info: 'Best practice client and server structures allow for more '
//             + 'code reusability and maximum scalability'
//     }, {
//       name: 'Optimized Build',
//       info: 'Build process packs up your templates as a single JavaScript '
//             + 'payload, minifies your scripts/css/images, and rewrites asset '
//             + 'names for caching.'
//     }, {
//       name: 'Deployment Ready',
//       info: 'Easily deploy your app to Heroku or Openshift with the heroku '
//             + 'and openshift subgenerators'
//     }]);
//   });

User.sync()
 .then(() => User.destroy({ where: {} }))
  .then(() => {
    User.bulkCreate([{
      provider: 'local',
      name: 'User obisnuit',
      userName: 'test',
      password: 'test',
      active: false
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Cont de admin',
      userName: 'admin',
      password: 'admin',
      active: true
    }])
    .then(() => {
      console.log('finished populating users');
    });
  }); 

  // Sector.sync()
  //.then(() => Sector.destroy({ where: {} }))
  // .then(() => {
  //   Sector.bulkCreate([{
  //     name: 'Sector1',
  //     description: 'descrierea sectorului 1',
  //     details: 'detalii sector 1',
  //     active: true
  //   }, {
  //     name: 'Sector2',
  //     description: 'descrierea sectorului 2',
  //     details: 'detalii sector 2',
  //     active: true
  //   }])
  //   .then(() => {
  //     console.log('finished populating users');
  //   });
  // })
  // ;

  //    MaterialGroup.sync()
  // .then(() => MaterialGroup.destroy({ where: {} }))
  // .then(() => {
  //   MaterialGroup.bulkCreate([{
  //     name: 'MaterialGroup1',
  //     description: 'descrierea grupei 1',
  //     details: 'detalii grupa 1',
  //     active: true
  //   }, {
  //     name: 'MaterialGroup2',
  //     description: 'descrierea grupei 2',
  //     details: 'detalii grupa 2',
  //     active: true  
  //   }])
  //   .then(() => {
  //     console.log('finished populating users');
  //   });
  // });

StuliFile.sync();

Stuli.sync();

PnlFile.sync();

Pnl.sync();

AchievementFile.sync();

Achievement.sync();