import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';

function localAuthenticate(User, userName, password, done) {
  console.log('caut user');
  User.find({
    where: {
      userName: userName.toLowerCase()
    }
  })
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'Utilizatorul ca nu exista.'
        });
      } else {
        if (!user.active) {
          return done(null, false, {
            message: 'Utilizatorul este inactiv.'
          });
        }
      }
      user.authenticate(password, function(authError, authenticated) {
        if(authError) {
          return done(authError);
        }
        if(!authenticated) {
          return done(null, false, { message: 'Parola gresita' });
        } else {
          return done(null, user);
        }
      });
    })
    .catch(err => done(err));
}

export function setup(User/*, config*/) {
  passport.use(new LocalStrategy({
    usernameField: 'userName',
    passwordField: 'password' // this is the virtual field on the model
  }, function(userName, password, done) {
    return localAuthenticate(User, userName, password, done);
  }));
}
