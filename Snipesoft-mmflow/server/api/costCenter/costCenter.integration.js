'use strict';

var app = require('../..');
import request from 'supertest';

var newCostCenter;

describe('CostCenter API:', function() {
  describe('GET /api/costCenters', function() {
    var CostCenters;

    beforeEach(function(done) {
      request(app)
        .get('/api/costCenters')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          CostCenters = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(CostCenters).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/costCenters', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/costCenters')
        .send({
          name: 'New CostCenter',
          info: 'This is the brand new CostCenter!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newCostCenter = res.body;
          done();
        });
    });

    it('should respond with the newly created CostCenter', function() {
      expect(newCostCenter.name).to.equal('New CostCenter');
      expect(newCostCenter.info).to.equal('This is the brand new CostCenter!!!');
    });
  });

  describe('GET /api/costCenters/:id', function() {
    var CostCenter;

    beforeEach(function(done) {
      request(app)
        .get(`/api/costCenters/${newCostCenter._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          CostCenter = res.body;
          done();
        });
    });

    afterEach(function() {
      CostCenter = {};
    });

    it('should respond with the requested CostCenter', function() {
      expect(CostCenter.name).to.equal('New CostCenter');
      expect(CostCenter.info).to.equal('This is the brand new CostCenter!!!');
    });
  });

  describe('PUT /api/costCenters/:id', function() {
    var updatedCostCenter;

    beforeEach(function(done) {
      request(app)
        .put(`/api/costCenters/${newCostCenter._id}`)
        .send({
          name: 'Updated CostCenter',
          info: 'This is the updated CostCenter!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedCostCenter = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCostCenter = {};
    });

    it('should respond with the original CostCenter', function() {
      expect(updatedCostCenter.name).to.equal('New CostCenter');
      expect(updatedCostCenter.info).to.equal('This is the brand new CostCenter!!!');
    });

    it('should respond with the updated CostCenter on a subsequent GET', function(done) {
      request(app)
        .get(`/api/costCenters/${newCostCenter._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let CostCenter = res.body;

          expect(CostCenter.name).to.equal('Updated CostCenter');
          expect(CostCenter.info).to.equal('This is the updated CostCenter!!!');

          done();
        });
    });
  });

  describe('PATCH /api/costCenters/:id', function() {
    var patchedCostCenter;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/costCenters/${newCostCenter._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched CostCenter' },
          { op: 'replace', path: '/info', value: 'This is the patched CostCenter!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedCostCenter = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedCostCenter = {};
    });

    it('should respond with the patched CostCenter', function() {
      expect(patchedCostCenter.name).to.equal('Patched CostCenter');
      expect(patchedCostCenter.info).to.equal('This is the patched CostCenter!!!');
    });
  });

  describe('DELETE /api/costCenters/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/costCenters/${newCostCenter._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when CostCenter does not exist', function(done) {
      request(app)
        .delete(`/api/costCenters/${newCostCenter._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
