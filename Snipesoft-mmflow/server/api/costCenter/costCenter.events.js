/**
 * CostCenter model events
 */

'use strict';

import {EventEmitter} from 'events';
var CostCenter = require('../../sqldb').CostCenter;
var CostCenterEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CostCenterEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  CostCenter.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    CostCenterEvents.emit(event + ':' + doc._id, doc);
    CostCenterEvents.emit(event, doc);
    done(null);
  };
}

export default CostCenterEvents;
