'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('CostCenter', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    description: DataTypes.STRING,
    isMfa: {
      field: "is_mfa",
      type: DataTypes.BOOLEAN
    },
    isActive: {
      field: "is_active",
      type: DataTypes.BOOLEAN
    },
    createdAt:
    {
      field: "created_at",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.STRING
    },
    updatedBy: {
      field: "updated_by",
      type: DataTypes.STRING
    },
    deletedBy: {
      field: "deleted_by",
      type: DataTypes.STRING
    }
  }, {
      name: {
        singular: 'costCenter',
        plural: 'costCenters',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'cost_centers'
    }
  );
}
