'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var costCenterCtrlStub = {
  index: 'costCenterCtrl.index',
  show: 'costCenterCtrl.show',
  create: 'costCenterCtrl.create',
  upsert: 'costCenterCtrl.upsert',
  patch: 'costCenterCtrl.patch',
  destroy: 'costCenterCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var costCenterIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './costCenter.controller': costCenterCtrlStub
});

describe('costCenter API Router:', function() {
  it('should return an express router instance', function() {
    expect(costCenterIndex).to.equal(routerStub);
  });

  describe('GET /api/costCenters', function() {
    it('should route to costCenter.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'costCenterCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/costCenters/:id', function() {
    it('should route to costCenter.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'costCenterCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/costCenters', function() {
    it('should route to costCenter.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'costCenterCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/costCenters/:id', function() {
    it('should route to costCenter.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'costCenterCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/costCenters/:id', function() {
    it('should route to costCenter.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'costCenterCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/costCenters/:id', function() {
    it('should route to costCenter.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'costCenterCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
