'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var budgetLineAccountCtrlStub = {
  index: 'budgetLineAccountCtrl.index',
  show: 'budgetLineAccountCtrl.show',
  create: 'budgetLineAccountCtrl.create',
  upsert: 'budgetLineAccountCtrl.upsert',
  patch: 'budgetLineAccountCtrl.patch',
  destroy: 'budgetLineAccountCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var budgetLineAccountIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './budgetLineAccount.controller': budgetLineAccountCtrlStub
});

describe('budgetLineAccount API Router:', function() {
  it('should return an express router instance', function() {
    expect(budgetLineAccountIndex).to.equal(routerStub);
  });

  describe('GET /api/budgetLineAccounts', function() {
    it('should route to budgetLineAccount.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'budgetLineAccountCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/budgetLineAccounts/:id', function() {
    it('should route to budgetLineAccount.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'budgetLineAccountCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/budgetLineAccounts', function() {
    it('should route to budgetLineAccount.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'budgetLineAccountCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/budgetLineAccounts/:id', function() {
    it('should route to budgetLineAccount.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'budgetLineAccountCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/budgetLineAccounts/:id', function() {
    it('should route to budgetLineAccount.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'budgetLineAccountCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/budgetLineAccounts/:id', function() {
    it('should route to budgetLineAccount.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'budgetLineAccountCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
