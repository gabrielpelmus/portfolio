/**
 * BudgetLineAccount model events
 */

'use strict';

import {EventEmitter} from 'events';
var BudgetLineAccount = require('../../sqldb').BudgetLineAccount;
var BudgetLineAccountEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BudgetLineAccountEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  BudgetLineAccount.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    BudgetLineAccountEvents.emit(event + ':' + doc._id, doc);
    BudgetLineAccountEvents.emit(event, doc);
    done(null);
  };
}

export default BudgetLineAccountEvents;
