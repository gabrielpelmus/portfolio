'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('BudgetLineAccount', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    budgetLineId: {
      type: DataTypes.INTEGER,
      field: "budget_line_id",
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    validFrom: {
      field: "valid_from",
      type: DataTypes.DATE
    },
    createdAt:
    {
      field: "created_at",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.INTEGER
    },
    updatedBy: {
      field: "updated_by",
      type: DataTypes.INTEGER
    },
    deletedBy: {
      field: "deleted_by",
      type: DataTypes.INTEGER
    }
  }, {
      name: {
        singular: 'account',
        plural: 'accounts',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'budget_accounts'
    }
  );
}
