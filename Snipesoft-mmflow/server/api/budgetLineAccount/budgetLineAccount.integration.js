'use strict';

var app = require('../..');
import request from 'supertest';

var newBudgetLineAccount;

describe('BudgetLineAccount API:', function() {
  describe('GET /api/budgetLineAccounts', function() {
    var BudgetLineAccounts;

    beforeEach(function(done) {
      request(app)
        .get('/api/budgetLineAccounts')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetLineAccounts = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(BudgetLineAccounts).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/budgetLineAccounts', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/budgetLineAccounts')
        .send({
          name: 'New BudgetLineAccount',
          info: 'This is the brand new BudgetLineAccount!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newBudgetLineAccount = res.body;
          done();
        });
    });

    it('should respond with the newly created BudgetLineAccount', function() {
      expect(newBudgetLineAccount.name).to.equal('New BudgetLineAccount');
      expect(newBudgetLineAccount.info).to.equal('This is the brand new BudgetLineAccount!!!');
    });
  });

  describe('GET /api/budgetLineAccounts/:id', function() {
    var BudgetLineAccount;

    beforeEach(function(done) {
      request(app)
        .get(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetLineAccount = res.body;
          done();
        });
    });

    afterEach(function() {
      BudgetLineAccount = {};
    });

    it('should respond with the requested BudgetLineAccount', function() {
      expect(BudgetLineAccount.name).to.equal('New BudgetLineAccount');
      expect(BudgetLineAccount.info).to.equal('This is the brand new BudgetLineAccount!!!');
    });
  });

  describe('PUT /api/budgetLineAccounts/:id', function() {
    var updatedBudgetLineAccount;

    beforeEach(function(done) {
      request(app)
        .put(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .send({
          name: 'Updated BudgetLineAccount',
          info: 'This is the updated BudgetLineAccount!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedBudgetLineAccount = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBudgetLineAccount = {};
    });

    it('should respond with the original BudgetLineAccount', function() {
      expect(updatedBudgetLineAccount.name).to.equal('New BudgetLineAccount');
      expect(updatedBudgetLineAccount.info).to.equal('This is the brand new BudgetLineAccount!!!');
    });

    it('should respond with the updated BudgetLineAccount on a subsequent GET', function(done) {
      request(app)
        .get(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let BudgetLineAccount = res.body;

          expect(BudgetLineAccount.name).to.equal('Updated BudgetLineAccount');
          expect(BudgetLineAccount.info).to.equal('This is the updated BudgetLineAccount!!!');

          done();
        });
    });
  });

  describe('PATCH /api/budgetLineAccounts/:id', function() {
    var patchedBudgetLineAccount;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched BudgetLineAccount' },
          { op: 'replace', path: '/info', value: 'This is the patched BudgetLineAccount!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedBudgetLineAccount = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedBudgetLineAccount = {};
    });

    it('should respond with the patched BudgetLineAccount', function() {
      expect(patchedBudgetLineAccount.name).to.equal('Patched BudgetLineAccount');
      expect(patchedBudgetLineAccount.info).to.equal('This is the patched BudgetLineAccount!!!');
    });
  });

  describe('DELETE /api/budgetLineAccounts/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when BudgetLineAccount does not exist', function(done) {
      request(app)
        .delete(`/api/budgetLineAccounts/${newBudgetLineAccount._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
