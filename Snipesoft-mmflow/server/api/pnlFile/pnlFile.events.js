/**
 * PnlFiles model events
 */

'use strict';

import {EventEmitter} from 'events';
var PnlFiles = require('../../sqldb').PnlFiles;
var PnlFilesEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PnlFilesEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  PnlFiles.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    PnlFilesEvents.emit(event + ':' + doc._id, doc);
    PnlFilesEvents.emit(event, doc);
    done(null);
  };
}

export default PnlFilesEvents;
