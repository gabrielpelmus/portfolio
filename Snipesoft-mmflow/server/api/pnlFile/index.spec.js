'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var pnlFileCtrlStub = {
  index: 'pnlFileCtrl.index',
  show: 'pnlFileCtrl.show',
  create: 'pnlFileCtrl.create',
  upsert: 'pnlFileCtrl.upsert',
  patch: 'pnlFileCtrl.patch',
  destroy: 'pnlFileCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var pnlFileIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './pnlFile.controller': pnlFileCtrlStub
});

describe('PnlFile API Router:', function() {
  it('should return an express router instance', function() {
    expect(pnlFileIndex).to.equal(routerStub);
  });

  describe('GET /api/pnlFiles', function() {
    it('should route to pnlFile.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'pnlFileCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/pnlFiles/:id', function() {
    it('should route to pnlFile.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'pnlFileCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/pnlFiles', function() {
    it('should route to pnlFile.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'pnlFileCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/pnlFiles/:id', function() {
    it('should route to pnlFile.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'pnlFileCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/pnlFiles/:id', function() {
    it('should route to pnlFile.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'pnlFileCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/pnlFiles/:id', function() {
    it('should route to pnlFile.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'pnlFileCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
