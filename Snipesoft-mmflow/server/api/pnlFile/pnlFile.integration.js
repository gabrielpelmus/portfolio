'use strict';

var app = require('../..');
import request from 'supertest';

var newPnlFiles;

describe('PnlFiles API:', function() {
  describe('GET /api/pnlFiles', function() {
    var pnlFiles;

    beforeEach(function(done) {
      request(app)
        .get('/api/pnlFiles')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          pnlFiles = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(pnlFiles).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/pnlFiles', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/pnlFiles')
        .send({
          name: 'New PnlFiles',
          info: 'This is the brand new pnlFile!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPnlFiles = res.body;
          done();
        });
    });

    it('should respond with the newly created pnlFile', function() {
      expect(newPnlFiles.name).to.equal('New PnlFiles');
      expect(newPnlFiles.info).to.equal('This is the brand new pnlFile!!!');
    });
  });

  describe('GET /api/pnlFiles/:id', function() {
    var pnlFile;

    beforeEach(function(done) {
      request(app)
        .get(`/api/pnlFiles/${newPnlFiles._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          pnlFile = res.body;
          done();
        });
    });

    afterEach(function() {
      pnlFile = {};
    });

    it('should respond with the requested pnlFile', function() {
      expect(pnlFile.name).to.equal('New PnlFiles');
      expect(pnlFile.info).to.equal('This is the brand new pnlFile!!!');
    });
  });

  describe('PUT /api/pnlFiles/:id', function() {
    var updatedPnlFiles;

    beforeEach(function(done) {
      request(app)
        .put(`/api/pnlFiles/${newPnlFiles._id}`)
        .send({
          name: 'Updated PnlFiles',
          info: 'This is the updated pnlFile!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPnlFiles = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPnlFiles = {};
    });

    it('should respond with the original pnlFile', function() {
      expect(updatedPnlFiles.name).to.equal('New PnlFiles');
      expect(updatedPnlFiles.info).to.equal('This is the brand new pnlFile!!!');
    });

    it('should respond with the updated pnlFile on a subsequent GET', function(done) {
      request(app)
        .get(`/api/pnlFiles/${newPnlFiles._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let pnlFile = res.body;

          expect(pnlFile.name).to.equal('Updated PnlFiles');
          expect(pnlFile.info).to.equal('This is the updated pnlFile!!!');

          done();
        });
    });
  });

  describe('PATCH /api/pnlFiles/:id', function() {
    var patchedPnlFiles;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/pnlFiles/${newPnlFiles._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched PnlFiles' },
          { op: 'replace', path: '/info', value: 'This is the patched pnlFile!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPnlFiles = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPnlFiles = {};
    });

    it('should respond with the patched pnlFile', function() {
      expect(patchedPnlFiles.name).to.equal('Patched PnlFiles');
      expect(patchedPnlFiles.info).to.equal('This is the patched pnlFile!!!');
    });
  });

  describe('DELETE /api/pnlFiles/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/pnlFiles/${newPnlFiles._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when pnlFile does not exist', function(done) {
      request(app)
        .delete(`/api/pnlFiles/${newPnlFiles._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
