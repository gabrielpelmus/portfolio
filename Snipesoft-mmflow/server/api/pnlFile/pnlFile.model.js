'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('PnlFile', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    importDate: {
      type: DataTypes.DATE,
      field: "import_date"
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      field: "created_at"
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: "updated_at"
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: "deleted_at"
    },
    createdBy: {
      type: DataTypes.INTEGER,
      field: "created_by"
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      field: "updated_by"
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      field: "deleted_by"
    }
  }, {
      /**
       * Table options
       */
      timestamps: true,
      underscored: true,
      tableName: 'pnl_files'
    }
  );
}