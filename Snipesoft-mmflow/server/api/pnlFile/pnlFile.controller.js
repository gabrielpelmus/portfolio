/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pnl-files              ->  index
 * POST    /api/pnl-files              ->  create
 * GET     /api/pnl-files/:id          ->  show
 * PUT     /api/pnl-files/:id          ->  upsert
 * PATCH   /api/pnl-files/:id          ->  patch
 * DELETE  /api/pnl-files/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { PnlFile } from '../../sqldb';
import { Pnl } from '../../sqldb';
import { User } from '../../sqldb';
import sqldb from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of PnlFiles
export function index(req, res) {
  return PnlFile.findAll({
    include: [{
      model: User,
      required: true
    }]
  })
    .then(function(entity) {
      entity.forEach(pnlFile => {
        pnlFile.dataValues.User = pnlFile.dataValues.User.profile
      });
      return entity;
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single PnlFile from the DB
export function show(req, res) {
  return PnlFile.find({
    include: [{
      model: Pnl,
      required: false
    }, {
      model: User,
      required: true
    }],
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(function(entity) {
      entity.dataValues.User = entity.dataValues.User.profile;
      return entity;
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new PnlFile in the DB
export function create(req, res) {
  let pnlFile = req.body;
  return sqldb.sequelize.transaction({
    isolationLevel: sqldb.sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
  }, function(t) {
    return PnlFile.create({
      name: pnlFile.name,
      importDate: pnlFile.importDate,
      created_by: req.user.id
    }, { transaction: t }).then(function(instance) {
      pnlFile.newId = instance.id;
      let newPnls = new Array();
      pnlFile.pnls.forEach(pnl => {
        newPnls.push({
          account: pnl.account,
          postingDateTime: pnl.postingDateTime,
          amount: pnl.amount,
          currency: pnl.currency,
          costCenter: pnl.costCenter,
          file_id: instance.id
        });
      })
      return Pnl.bulkCreate(newPnls, { transaction: t });
    });
  })
    .then(function(entity) {
      return PnlFile.find({
        include: [{
          model: Pnl,
          required: false
        }, {
          model: User,
          required: true
        }],
        where: {
          id: pnlFile.newId
        }
      })
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Upserts the given PnlFile in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return PnlFile.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing PnlFile in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return PnlFile.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a PnlFile from the DB
export function destroy(req, res) {
  return PnlFile.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
