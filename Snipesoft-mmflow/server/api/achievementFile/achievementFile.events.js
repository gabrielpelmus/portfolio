/**
 * AchievementFile model events
 */

'use strict';

import {EventEmitter} from 'events';
var AchievementFile = require('../../sqldb').AchievementFile;
var AchievementFileEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AchievementFileEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  AchievementFile.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    AchievementFileEvents.emit(event + ':' + doc._id, doc);
    AchievementFileEvents.emit(event, doc);
    done(null);
  };
}

export default AchievementFileEvents;
