/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/achievement-files              ->  index
 * POST    /api/achievement-files              ->  create
 * GET     /api/achievement-files/:id          ->  show
 * PUT     /api/achievement-files/:id          ->  upsert
 * PATCH   /api/achievement-files/:id          ->  patch
 * DELETE  /api/achievement-files/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { AchievementFile } from '../../sqldb';
import { Achievement } from '../../sqldb';
import { User } from '../../sqldb';
import sqldb from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of AchievementFiles
export function index(req, res) {
  return AchievementFile.findAll({
    include: [{
      model: User,
      required: true
    }]
  })
    .then(function(entity) {
      entity.forEach(achievementFile => {
        achievementFile.dataValues.User = achievementFile.dataValues.User.profile
      });
      return entity;
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single AchievementFile from the DB
export function show(req, res) {
  var processData = function(res) {
    return function(entity) {
      if(entity) {
        var achievementFile = entity;
        var calculateProductionTime = function(res) {
          return function(entity) {
            if(entity) {
              var stulis = {};
              for(var i = 0; i < entity.length; i++) {
                if(!stulis[entity[i].material_code]) {
                  stulis[entity[i].material_code] = entity[i].duration;
                }
              }
              for(var i = 0; i < achievementFile.dataValues.achievements.length; i++) {
                var fMaterialProductionTime = achievementFile.dataValues.achievements[i].dataValues.quantity * stulis[achievementFile.dataValues.achievements[i].dataValues.materialCode];
                achievementFile.dataValues.achievements[i].dataValues.materialProductionTime = !isNaN(fMaterialProductionTime) ? parseFloat(fMaterialProductionTime.toFixed(2)) : null;
              }
              return res.status(200).json(achievementFile);
            }
            return null;
          }
        }
        var sql = 'SELECT material_code, duration FROM stuli_validities WHERE validity <= "' + entity.workingDayDateTime.toISOString() + '"';
        return sqldb.sequelize.query(sql, { type: sqldb.sequelize.QueryTypes.SELECT })
          .then(calculateProductionTime(res));
      }
      return null;
    }
  }

  return AchievementFile.find({
    include: [{
      model: Achievement,
      required: false
    }, {
      model: User,
      required: true
    }],
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(function(entity) {
      entity.dataValues.User = entity.dataValues.User.profile;
      return entity;
    })
    .then(processData(res))
    .catch(handleError(res));
}

// Creates a new AchievementFile in the DB
export function create(req, res) {
  let achievementFile = req.body;
  return sqldb.sequelize.transaction({
    isolationLevel: sqldb.sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
  }, function(t) {
    return AchievementFile.create({
      name: achievementFile.name,
      description: achievementFile.description,
      workingDayDateTime: achievementFile.workingDayDateTime,
      created_by: req.user.id
    }, { transaction: t }).then(function(instance) {
      achievementFile.newId = instance.id;
      let newAchievements = new Array();
      achievementFile.achievements.forEach(achievement => {
        newAchievements.push({
          recordedDateTime: achievement.recordedDateTime,
          logisticUnit: achievement.logisticUnit,
          storageUnit: achievement.storageUnit,
          quantity: achievement.quantity,
          materialCode: achievement.materialCode,
          materialDescription: achievement.materialDescription,
          materialGroupCode: achievement.materialGroupCode,
          movementType: achievement.movementType,
          profitCenter: achievement.profitCenter,
          file_id: instance.id
        });
      })
      return Achievement.bulkCreate(newAchievements, { transaction: t });
    });
  })
    .then(function(entity) {
      return AchievementFile.find({
        include: [{
          model: Achievement,
          required: false
        }, {
          model: User,
          required: true
        }],
        where: {
          id: achievementFile.newId
        }
      })
    })
    .then(respondWithResult(res)).catch(handleError(res));
}

// Upserts the given AchievementFile in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return AchievementFile.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing AchievementFile in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return AchievementFile.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a AchievementFile from the DB
export function destroy(req, res) {
  return AchievementFile.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
