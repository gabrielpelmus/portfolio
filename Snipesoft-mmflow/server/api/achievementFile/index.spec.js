'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var achievementFileCtrlStub = {
  index: 'achievementFileCtrl.index',
  show: 'achievementFileCtrl.show',
  create: 'achievementFileCtrl.create',
  upsert: 'achievementFileCtrl.upsert',
  patch: 'achievementFileCtrl.patch',
  destroy: 'achievementFileCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var achievementFileIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './achievementFile.controller': achievementFileCtrlStub
});

describe('AchievementFile API Router:', function() {
  it('should return an express router instance', function() {
    expect(achievementFileIndex).to.equal(routerStub);
  });

  describe('GET /api/achievement-files', function() {
    it('should route to achievementFile.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'achievementFileCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/achievement-files/:id', function() {
    it('should route to achievementFile.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'achievementFileCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/achievement-files', function() {
    it('should route to achievementFile.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'achievementFileCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/achievement-files/:id', function() {
    it('should route to achievementFile.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'achievementFileCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/achievement-files/:id', function() {
    it('should route to achievementFile.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'achievementFileCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/achievement-files/:id', function() {
    it('should route to achievementFile.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'achievementFileCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
