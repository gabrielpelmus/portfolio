'use strict';

export default function (sequelize, DataTypes) {
  return sequelize.define('AchievementFile', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    description: DataTypes.STRING,
    workingDayDateTime: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'working_day_date_time'
    },
  }, {
      /**
       * Table options
       */
      timestamps: true,
      paranoid: false,
      underscored: true,
      tableName: 'achievement_files'
    }
  );
}