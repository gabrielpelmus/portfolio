'use strict';

var app = require('../..');
import request from 'supertest';

var newAchievementFile;

describe('AchievementFile API:', function() {
  describe('GET /api/achievement-files', function() {
    var achievementFiles;

    beforeEach(function(done) {
      request(app)
        .get('/api/achievement-files')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          achievementFiles = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(achievementFiles).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/achievement-files', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/achievement-files')
        .send({
          name: 'New AchievementFile',
          info: 'This is the brand new achievementFile!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newAchievementFile = res.body;
          done();
        });
    });

    it('should respond with the newly created achievementFile', function() {
      expect(newAchievementFile.name).to.equal('New AchievementFile');
      expect(newAchievementFile.info).to.equal('This is the brand new achievementFile!!!');
    });
  });

  describe('GET /api/achievement-files/:id', function() {
    var achievementFile;

    beforeEach(function(done) {
      request(app)
        .get(`/api/achievement-files/${newAchievementFile._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          achievementFile = res.body;
          done();
        });
    });

    afterEach(function() {
      achievementFile = {};
    });

    it('should respond with the requested achievementFile', function() {
      expect(achievementFile.name).to.equal('New AchievementFile');
      expect(achievementFile.info).to.equal('This is the brand new achievementFile!!!');
    });
  });

  describe('PUT /api/achievement-files/:id', function() {
    var updatedAchievementFile;

    beforeEach(function(done) {
      request(app)
        .put(`/api/achievement-files/${newAchievementFile._id}`)
        .send({
          name: 'Updated AchievementFile',
          info: 'This is the updated achievementFile!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedAchievementFile = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAchievementFile = {};
    });

    it('should respond with the original achievementFile', function() {
      expect(updatedAchievementFile.name).to.equal('New AchievementFile');
      expect(updatedAchievementFile.info).to.equal('This is the brand new achievementFile!!!');
    });

    it('should respond with the updated achievementFile on a subsequent GET', function(done) {
      request(app)
        .get(`/api/achievement-files/${newAchievementFile._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let achievementFile = res.body;

          expect(achievementFile.name).to.equal('Updated AchievementFile');
          expect(achievementFile.info).to.equal('This is the updated achievementFile!!!');

          done();
        });
    });
  });

  describe('PATCH /api/achievement-files/:id', function() {
    var patchedAchievementFile;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/achievement-files/${newAchievementFile._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched AchievementFile' },
          { op: 'replace', path: '/info', value: 'This is the patched achievementFile!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedAchievementFile = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedAchievementFile = {};
    });

    it('should respond with the patched achievementFile', function() {
      expect(patchedAchievementFile.name).to.equal('Patched AchievementFile');
      expect(patchedAchievementFile.info).to.equal('This is the patched achievementFile!!!');
    });
  });

  describe('DELETE /api/achievement-files/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/achievement-files/${newAchievementFile._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when achievementFile does not exist', function(done) {
      request(app)
        .delete(`/api/achievement-files/${newAchievementFile._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
