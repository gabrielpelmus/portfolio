/**
 * BudgetYearly model events
 */

'use strict';

import {EventEmitter} from 'events';
var BudgetYearly = require('../../sqldb').BudgetYearly;
var BudgetYearlyEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BudgetYearlyEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  BudgetYearly.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    BudgetYearlyEvents.emit(event + ':' + doc._id, doc);
    BudgetYearlyEvents.emit(event, doc);
    done(null);
  };
}

export default BudgetYearlyEvents;
