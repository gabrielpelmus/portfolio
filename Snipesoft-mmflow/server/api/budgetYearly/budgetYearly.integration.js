'use strict';

var app = require('../..');
import request from 'supertest';

var newBudgetYearly;

describe('BudgetYearly API:', function() {
  describe('GET /api/budgetYearlys', function() {
    var BudgetYearlys;

    beforeEach(function(done) {
      request(app)
        .get('/api/budgetYearlys')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetYearlys = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(BudgetYearlys).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/budgetYearlys', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/budgetYearlys')
        .send({
          name: 'New BudgetYearly',
          info: 'This is the brand new BudgetYearly!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newBudgetYearly = res.body;
          done();
        });
    });

    it('should respond with the newly created BudgetYearly', function() {
      expect(newBudgetYearly.name).to.equal('New BudgetYearly');
      expect(newBudgetYearly.info).to.equal('This is the brand new BudgetYearly!!!');
    });
  });

  describe('GET /api/budgetYearlys/:id', function() {
    var BudgetYearly;

    beforeEach(function(done) {
      request(app)
        .get(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetYearly = res.body;
          done();
        });
    });

    afterEach(function() {
      BudgetYearly = {};
    });

    it('should respond with the requested BudgetYearly', function() {
      expect(BudgetYearly.name).to.equal('New BudgetYearly');
      expect(BudgetYearly.info).to.equal('This is the brand new BudgetYearly!!!');
    });
  });

  describe('PUT /api/budgetYearlys/:id', function() {
    var updatedBudgetYearly;

    beforeEach(function(done) {
      request(app)
        .put(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .send({
          name: 'Updated BudgetYearly',
          info: 'This is the updated BudgetYearly!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedBudgetYearly = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBudgetYearly = {};
    });

    it('should respond with the original BudgetYearly', function() {
      expect(updatedBudgetYearly.name).to.equal('New BudgetYearly');
      expect(updatedBudgetYearly.info).to.equal('This is the brand new BudgetYearly!!!');
    });

    it('should respond with the updated BudgetYearly on a subsequent GET', function(done) {
      request(app)
        .get(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let BudgetYearly = res.body;

          expect(BudgetYearly.name).to.equal('Updated BudgetYearly');
          expect(BudgetYearly.info).to.equal('This is the updated BudgetYearly!!!');

          done();
        });
    });
  });

  describe('PATCH /api/budgetYearlys/:id', function() {
    var patchedBudgetYearly;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched BudgetYearly' },
          { op: 'replace', path: '/info', value: 'This is the patched BudgetYearly!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedBudgetYearly = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedBudgetYearly = {};
    });

    it('should respond with the patched BudgetYearly', function() {
      expect(patchedBudgetYearly.name).to.equal('Patched BudgetYearly');
      expect(patchedBudgetYearly.info).to.equal('This is the patched BudgetYearly!!!');
    });
  });

  describe('DELETE /api/budgetYearlys/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when BudgetYearly does not exist', function(done) {
      request(app)
        .delete(`/api/budgetYearlys/${newBudgetYearly._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
