'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('BudgetYearly', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    budgetYear: {
      field: "budget_year",
      type: DataTypes.DATE
    },
    ian: {
      field: "ian",
      type: DataTypes.DOUBLE
    },
    feb: {
      field: "feb",
      type: DataTypes.DOUBLE
    },
    mar: {
      field: "mar",
      type: DataTypes.DOUBLE
    },
    apr: {
      field: "apr",
      type: DataTypes.DOUBLE
    },
    mai: {
      field: "mai",
      type: DataTypes.DOUBLE
    },
    iun: {
      field: "iun",
      type: DataTypes.DOUBLE
    },
    iul: {
      field: "iul",
      type: DataTypes.DOUBLE
    },
    aug: {
      field: "aug",
      type: DataTypes.DOUBLE
    },
    sep: {
      field: "sep",
      type: DataTypes.DOUBLE
    },
    oct: {
      field: "oct",
      type: DataTypes.DOUBLE
    },
    nov: {
      field: "nov",
      type: DataTypes.DOUBLE
    },
    dcbr: {
      field: "dcbr",
      type: DataTypes.DOUBLE
    },
    budgetLineId: {
      type: DataTypes.INTEGER,
      field: "budget_line_id",
      allowNull: false
    },
    createdAt:
    {
      field: "created_at",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.INTEGER
    },
    updatedBy: {
      field: "updated_by",
      type: DataTypes.INTEGER
    },
    deletedBy: {
      field: "deleted_by",
      type: DataTypes.INTEGER
    }
  }, {
      name: {
        singular: 'budget',
        plural: 'budgets',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'budget_yearly'
    }
  );
}
