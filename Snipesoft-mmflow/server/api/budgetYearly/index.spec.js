'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var budgetYearlyCtrlStub = {
  index: 'budgetYearlyCtrl.index',
  show: 'budgetYearlyCtrl.show',
  create: 'budgetYearlyCtrl.create',
  upsert: 'budgetYearlyCtrl.upsert',
  patch: 'budgetYearlyCtrl.patch',
  destroy: 'budgetYearlyCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var budgetYearlyIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './budgetYearly.controller': budgetYearlyCtrlStub
});

describe('budgetYearly API Router:', function() {
  it('should return an express router instance', function() {
    expect(budgetYearlyIndex).to.equal(routerStub);
  });

  describe('GET /api/budgetYearlys', function() {
    it('should route to budgetYearly.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'budgetYearlyCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/budgetYearlys/:id', function() {
    it('should route to budgetYearly.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'budgetYearlyCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/budgetYearlys', function() {
    it('should route to budgetYearly.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'budgetYearlyCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/budgetYearlys/:id', function() {
    it('should route to budgetYearly.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'budgetYearlyCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/budgetYearlys/:id', function() {
    it('should route to budgetYearly.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'budgetYearlyCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/budgetYearlys/:id', function() {
    it('should route to budgetYearly.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'budgetYearlyCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
