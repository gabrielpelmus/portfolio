/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/ReportProfitLoss              ->  index
 * POST    /api/ReportProfitLoss              ->  create
 * GET     /api/ReportProfitLoss/:id          ->  show
 * PUT     /api/ReportProfitLoss/:id          ->  upsert
 * PATCH   /api/ReportProfitLoss/:id          ->  patch
 * DELETE  /api/ReportProfitLoss/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { ReportProfitLoss } from '../../sqldb';
import sqldb from '../../sqldb';
import _ from 'lodash';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      return entity.updateAttributes(updates)
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of ReportProfitLosses
export function index(req, res) {

  var sql = 'SELECT GET_MONTHLY_TOTAL(v.id,1, 2017) as monthly_total, v.* FROM v_budget_line_dtl v';
  return sqldb.sequelize.query(sql, { type: sqldb.sequelize.QueryTypes.SELECT })
  .then(respondWithResult(res))
  .catch(handleError(res));
}

// get a list of months/years where data exists
export function getMonthsYears(req, res) {

  var sql = 'SELECT distinct month(posting_date) as sel_mon, year(posting_date) as sel_year FROM pnl order by sel_year desc, sel_mon desc;';
  return sqldb.sequelize.query(sql, { type: sqldb.sequelize.QueryTypes.SELECT })
  .then(respondWithResult(res))
  .catch(handleError(res));
}
