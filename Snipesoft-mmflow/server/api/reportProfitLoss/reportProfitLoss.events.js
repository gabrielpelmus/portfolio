/**
 * ReportProfitLoss model events
 */

'use strict';

import {EventEmitter} from 'events';
var ReportProfitLoss = require('../../sqldb').ReportProfitLoss;
var ReportProfitLoss = new EventEmitter();

// Set max event listeners (0 == unlimited)
ReportProfitLossEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  ReportProfitLoss.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    ReportProfitLossEvents.emit(event + ':' + doc._id, doc);
    ReportProfitLossEvents.emit(event, doc);
    done(null);
  };
}

export default ReportProfitLossEvents;
