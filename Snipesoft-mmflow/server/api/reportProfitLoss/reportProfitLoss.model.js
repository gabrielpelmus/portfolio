'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('ReportProfitLoss', {
    code: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
        unique: true
      }
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
      name: {
        singular: 'reportProfitLoss',
        plural: 'reportProfitLosses',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'v_budget_line_dtl'
    }
  );
}
