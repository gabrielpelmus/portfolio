'use strict';

var app = require('../..');
import request from 'supertest';

var newReportProfitLoss;

describe('ReportProfitLoss API:', function() {
  describe('GET /api/reportProfitLosses', function() {
    var ReportProfitLosses;

    beforeEach(function(done) {
      request(app)
        .get('/api/reportProfitLosses')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          ReportProfitLosses = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(ReportProfitLosses).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/reportProfitLosses', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/reportProfitLosses')
        .send({
          name: 'New ReportProfitLoss',
          info: 'This is the brand new ReportProfitLoss!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newReportProfitLoss = res.body;
          done();
        });
    });

    it('should respond with the newly created ReportProfitLoss', function() {
      expect(newReportProfitLoss.name).to.equal('New ReportProfitLoss');
      expect(newReportProfitLoss.info).to.equal('This is the brand new ReportProfitLoss!!!');
    });
  });

});
