'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var reportProfitLossCtrlStub = {
  index: 'reportProfitLossCtrl.index'
};

var routerStub = {
  get: sinon.spy()
};

// require the index with our stubbed out modules
var reportProfitLossIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './reportProfitLoss.controller': reportProfitLossCtrlStub
});

describe('reportProfitLoss API Router:', function() {
  it('should return an express router instance', function() {
    expect(reportProfitLossIndex).to.equal(routerStub);
  });

  describe('GET /api/reportProfitLosss', function() {
    it('should route to reportProfitLoss.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'reportProfitLossCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/reportProfitLosss/:id', function() {
    it('should route to reportProfitLoss.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'reportProfitLossCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

});
