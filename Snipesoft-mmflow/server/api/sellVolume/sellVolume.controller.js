/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/sellVolume              ->  index
 * POST    /api/sellVolume              ->  create
 * GET     /api/sellVolume/:id          ->  show
 * PUT     /api/sellVolume/:id          ->  upsert
 * PATCH   /api/sellVolume/:id          ->  patch
 * DELETE  /api/sellVolume/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { SellVolume } from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity){
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of SellVolumes
export function index(req, res) {
  var sellVolumeWhere = {
  };

  if(req.query.filter) {
    sellVolumeWhere.$or = [
      {
        id: {
          $like: req.query.filter ? '%' + req.query.filter + '%' : '%%',
        }
      }
    ];
  }

  if(req.query.active === "true" || req.query.active === "false") {
    sellVolumeWhere.active = {
      $eq: req.query.active == "true" ? true : false
    }
  }

  return SellVolume.findAll({
    where: sellVolumeWhere
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single SellVolume from the DB
export function show(req, res) {
  return SellVolume.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function checkDataIntegrity(res, req) {
  return function(entity) {
    if(req.params.id) {
      //update
      if(entity) {
        if(entity.id != req.params.id) {
          res.status(400).send({ message: "Codul introdus exista deja!" }).end();
          return null;
        }
      }
      else {
        return "OK_TO_SEARCH_BY_ID";
      }
    }
    else {
      //create
      if(entity && entity.id === req.body.id) {
        res.status(400).send({ message: "Codul introdus exista deja!" }).end();
      }
    }

    return entity;
  };
}


// Creates a new SellVolume in the DB
export function create(req, res) {
  req.body.createdBy = req.user.id;
  req.body.updatedBy = req.user.id; 

  return SellVolume.find({
    where: {
      id: req.body.id
    }
  }).then(checkDataIntegrity(res, req))
    .then(function(entity) {
      return !entity ? SellVolume.create(req.body) : null
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given SellVolume in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return SellVolume.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing SellVolume in the DB
export function patch(req, res) {
  if(req.body.id) {
    delete req.body.id;
  }

  req.body.updatedBy = req.user.id; 

  return SellVolume.find({
    where: {
      id: req.body.id
    }
  })
    .then(checkDataIntegrity(res, req))
    .then(function(entity) {
      if(entity != "OK_TO_SEARCH_BY_ID") {
        return entity;
      }
      else {
        return SellVolume.find({
          where: {
            id: req.params.id
          }
        })
      }
    })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a SellVolume from the DB
export function destroy(req, res) {
  req.body.deletedBy = req.user.id; 

  return SellVolume.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
