'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('SellVolume', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    year: {
      type: DataTypes.DATE,
      unique: {msg: "Inregistrarea exista!"},
      validate: {
        notEmpty: true,
        //unique: {msg: "Inregistrarea exista!"}
      }
    },
    soldBudget: {
      field: "sold_budget",
      type: DataTypes.DOUBLE
    },
    prodBudget: {
      field: "prod_budget",
      type: DataTypes.DOUBLE
    },
    prodMins: {
      field: "prod_mins",
      type: DataTypes.INTEGER
    },
    soldMins: {
      field: "sold_mins",
      type: DataTypes.INTEGER
    },
    createdAt:
    {
      field: "created_at",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.INTEGER
    },
    updatedBy: {
      field: "updated_by",
      type: DataTypes.INTEGER
    },
    deletedBy: {
      field: "deleted_by",
      type: DataTypes.INTEGER
    }
  }, {
      name: {
        singular: 'sellVolume',
        plural: 'sellVolumes',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'sell_volume'
    }
  );
}
