'use strict';

var app = require('../..');
import request from 'supertest';

var newSellVolume;

describe('SellVolume API:', function() {
  describe('GET /api/sellVolumes', function() {
    var SellVolumes;

    beforeEach(function(done) {
      request(app)
        .get('/api/sellVolumes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          SellVolumes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(SellVolumes).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/sellVolumes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/sellVolumes')
        .send({
          name: 'New SellVolume',
          info: 'This is the brand new SellVolume!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newSellVolume = res.body;
          done();
        });
    });

    it('should respond with the newly created SellVolume', function() {
      expect(newSellVolume.name).to.equal('New SellVolume');
      expect(newSellVolume.info).to.equal('This is the brand new SellVolume!!!');
    });
  });

  describe('GET /api/sellVolumes/:id', function() {
    var SellVolume;

    beforeEach(function(done) {
      request(app)
        .get(`/api/sellVolumes/${newSellVolume._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          SellVolume = res.body;
          done();
        });
    });

    afterEach(function() {
      SellVolume = {};
    });

    it('should respond with the requested SellVolume', function() {
      expect(SellVolume.name).to.equal('New SellVolume');
      expect(SellVolume.info).to.equal('This is the brand new SellVolume!!!');
    });
  });

  describe('PUT /api/sellVolumes/:id', function() {
    var updatedSellVolume;

    beforeEach(function(done) {
      request(app)
        .put(`/api/sellVolumes/${newSellVolume._id}`)
        .send({
          name: 'Updated SellVolume',
          info: 'This is the updated SellVolume!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedSellVolume = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSellVolume = {};
    });

    it('should respond with the original SellVolume', function() {
      expect(updatedSellVolume.name).to.equal('New SellVolume');
      expect(updatedSellVolume.info).to.equal('This is the brand new SellVolume!!!');
    });

    it('should respond with the updated SellVolume on a subsequent GET', function(done) {
      request(app)
        .get(`/api/sellVolumes/${newSellVolume._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let SellVolume = res.body;

          expect(SellVolume.name).to.equal('Updated SellVolume');
          expect(SellVolume.info).to.equal('This is the updated SellVolume!!!');

          done();
        });
    });
  });

  describe('PATCH /api/sellVolumes/:id', function() {
    var patchedSellVolume;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/sellVolumes/${newSellVolume._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched SellVolume' },
          { op: 'replace', path: '/info', value: 'This is the patched SellVolume!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedSellVolume = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedSellVolume = {};
    });

    it('should respond with the patched SellVolume', function() {
      expect(patchedSellVolume.name).to.equal('Patched SellVolume');
      expect(patchedSellVolume.info).to.equal('This is the patched SellVolume!!!');
    });
  });

  describe('DELETE /api/sellVolumes/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/sellVolumes/${newSellVolume._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when SellVolume does not exist', function(done) {
      request(app)
        .delete(`/api/sellVolumes/${newSellVolume._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
