/**
 * SellVolume model events
 */

'use strict';

import {EventEmitter} from 'events';
var SellVolume = require('../../sqldb').SellVolume;
var SellVolumeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
SellVolumeEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  SellVolume.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    SellVolumeEvents.emit(event + ':' + doc._id, doc);
    SellVolumeEvents.emit(event, doc);
    done(null);
  };
}

export default SellVolumeEvents;
