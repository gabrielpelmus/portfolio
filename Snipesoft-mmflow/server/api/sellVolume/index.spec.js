'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var sellVolumeCtrlStub = {
  index: 'sellVolumeCtrl.index',
  show: 'sellVolumeCtrl.show',
  create: 'sellVolumeCtrl.create',
  upsert: 'sellVolumeCtrl.upsert',
  patch: 'sellVolumeCtrl.patch',
  destroy: 'sellVolumeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var sellVolumeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './sellVolume.controller': sellVolumeCtrlStub
});

describe('sellVolume API Router:', function() {
  it('should return an express router instance', function() {
    expect(sellVolumeIndex).to.equal(routerStub);
  });

  describe('GET /api/sellVolumes', function() {
    it('should route to sellVolume.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'sellVolumeCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/sellVolumes/:id', function() {
    it('should route to sellVolume.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'sellVolumeCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/sellVolumes', function() {
    it('should route to sellVolume.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'sellVolumeCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/sellVolumes/:id', function() {
    it('should route to sellVolume.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'sellVolumeCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/sellVolumes/:id', function() {
    it('should route to sellVolume.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'sellVolumeCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/sellVolumes/:id', function() {
    it('should route to sellVolume.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'sellVolumeCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
