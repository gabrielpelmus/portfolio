/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/reports              ->  index
 * POST    /api/reports              ->  create
 * GET     /api/reports/:id          ->  show
 * PUT     /api/reports/:id          ->  upsert
 * PATCH   /api/reports/:id          ->  patch
 * DELETE  /api/reports/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import {Achievement} from '../../sqldb';
import sqldb from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

export function productionBySector(req, res) {

  var processAchievements = function (res) {
    return function(entity) {
      if (entity) {
        var achievements = entity;

        var processStulis = function(res) {
          return function(entity) {
            if(entity) {
              var stulis = entity;

              /* parcurge paralel lista de achievements si cea de stuli cu cei doi cursori - i si index intrucat tabelele sunt ordonate descrescator dupa material si data*/
              var index = 0;
              var grouping = {};
              
              for (var i = 0; i < achievements.length; i++) {
                var materialCode = achievements[i].material_code;
                var materialDate = new Date(achievements[i].recorded_date_time);

                /* cauta materialul */
                while (materialCode != stulis[index].material_code && index < stulis.length) {
                  index++;
                }

                /* cauta stuli-ul corespunzator */
                while (materialDate <= new Date(stulis[index].validity) && index < stulis.length) {
                  index++;
                }

                /* se presupune ca nu exista achivement fara stuli valid */
                if (index == stulis.length) {
                  console.log("cacat!");
                  break;
                }

                achievements[i].duration = parseFloat((stulis[index].duration * achievements[i].quantity).toFixed(2));

                /* ziua oficiala de lucru incepe la 4:50 | un achievement inregistrat la 2 AM spre exemplu se inregistreaza zilei calendaristice precedente */
                var workDayStart = new Date(materialDate.getFullYear(),materialDate.getMonth(), materialDate.getDate(), 4, 50, 0);
                var previousDay = new Date(materialDate.getFullYear(),materialDate.getMonth(), materialDate.getDate() - 1, 4, 50, 0);                
                var achievementDate = materialDate < workDayStart ? previousDay.toJSON().split("T")[0] : workDayStart.toJSON().split("T")[0];

                /* grupeaza dupa gama, zi de lucru si grupa material */
                var key = achievements[i].profit_center + '#$'+ achievementDate + '#$' + achievements[i].material_group_code;
                if (grouping[key] != null) {
                  grouping[key].duration += achievements[i].duration;
                  grouping[key].quantity += achievements[i].quantity;
                }
                else {
                  grouping[key] = {
                    duration: achievements[i].duration,
                    quantity: achievements[i].quantity,
                    materialGroupCode: achievements[i].material_group_code,
                    profitCenter: achievements[i].profit_center,
                    workDay: achievementDate,
                    sectorId: achievements[i].sector_id,
                    sectorName: achievements[i].sector_name
                  }
                }
              }

              /* grupeaza dupa zi de lucru, gama si sector */
              var results = {};
              for (var property in grouping) {
                var entry = grouping[property];
                var key = entry.workDay + '#$' + entry.profitCenter + '#$' + entry.sectorId;

                if (results[key] != null) {
                  results[key].duration += entry.duration;
                  results[key].quantity += entry.quantity;
                  if (results[key].materialGroups.indexOf(entry.materialGroupCode) == -1) {
                    results[key].materialGroups.push(entry.materialGroupCode);
                  }
                }
                else {
                  results[key] = {
                    date: entry.workDay,
                    profitCenter: entry.profitCenter,
                    sectorId: entry.sectorId,
                    sectorName: entry.sectorName,
                    duration: entry.duration,
                    quantity: entry.quantity,
                    materialGroups: [entry.materialGroupCode]
                  }
                }
              }

              /* formatare rezultat */
              var output = [];
              for (var property in results) {
                results[property].duration = Math.round(results[property].duration);
                output.push(results[property]);
              }
              
              return res.status(200).json(output);
            }
            else return null;
            };
        } 

        var stuliSql =  'SELECT material_code, duration, validity FROM stuli_validities ORDER BY material_code DESC, validity DESC ';// WHERE validity <= "' + entity.workingDayDateTime.toISOString() + '"';
        return sqldb.sequelize.query(stuliSql, { type: sqldb.sequelize.QueryTypes.SELECT })
          .then(processStulis(res))
      }
      else return null;
    }
  }

  var startDate = new Date(req.body.fromDate);
  var endDate = new Date(req.body.toDate);

  var profitCenters = '';
  req.body.profitCenters.forEach(element => {
    profitCenters += '"' + element + '",'; 
  });
  profitCenters = profitCenters.slice(0, -1);

  var achievementsSql = 'SELECT ach.recorded_date_time, ach.quantity, ach.material_code, ach.material_group_code, ach.profit_center, mg.sector_id, sct.name AS sector_name ' + 
    'FROM achievements ach ' +
    'JOIN material_groups mg ON ach.material_group_code = mg.code AND mg.deleted_at is NULL ' +
    'JOIN sectors sct ON mg.sector_id = sct.id AND sct.deleted_at is NULL ' +
    'WHERE ach.recorded_date_time >= :startDate ' +
    'AND ach.recorded_date_time <= :endDate ' +
    'AND ach.profit_center IN (' + profitCenters + ') ' + // RISC DE SQL INJECTION!!!
    'ORDER BY ach.material_code DESC, ach.recorded_date_time DESC';
  return sqldb.sequelize.query(achievementsSql,{
            replacements: { 
              startDate: startDate.toISOString(),
              endDate: endDate.toISOString()
            }, 
            type: sqldb.sequelize.QueryTypes.SELECT 
          })
          .then(processAchievements(res))
          .catch(handleError(res));
}

// Gets a list of ProfitCenters
export function profitCenterIndex(req, res) {

  function respondWithProfitCenters(res) {
    return function(entity) {
      if(entity) {
        return res.status(200).json(entity.map(function(value) {return value.profitCenter }));
      }
      return null;
    };
  }

  return Achievement.findAll({
    attributes: [[sqldb.sequelize.fn('DISTINCT', sqldb.sequelize.col('profit_center')), 'profitCenter']]
  })
    .then(respondWithProfitCenters(res))
    .catch(handleError(res));
}

// Gets a single Report from the DB
export function show(req, res) {
  return Report.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Report in the DB
export function create(req, res) {
  return Report.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Report in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return Report.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Report in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Report.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Report from the DB
export function destroy(req, res) {
  return Report.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
