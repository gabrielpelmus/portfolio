/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/sectors              ->  index
 * POST    /api/sectors              ->  create
 * GET     /api/sectors/:id          ->  show
 * PUT     /api/sectors/:id          ->  upsert
 * PATCH   /api/sectors/:id          ->  patch
 * DELETE  /api/sectors/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { Sector } from '../../sqldb';
import { MaterialGroup } from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      return entity.updateAttributes(updates)
        .then(updated => {
          return updated;
        });
    }
    return null;
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Sectors
export function index(req, res) {
  var sectorWhere = {
  };

  if(req.query.filter) {
    sectorWhere.$or = [
      {
        code: {
          $like: req.query.filter ? '%' + req.query.filter + '%' : '%%',
        }
      },
      {
        name: {
          $like: req.query.filter ? '%' + req.query.filter + '%' : '%%',
        }
      }
    ];
  }

  if(req.query.active === "true" || req.query.active === "false") {
    sectorWhere.active = {
      $eq: req.query.active == "true" ? true : false
    }
  }

  return Sector.findAll({
    where: sectorWhere,
    include: [{
      model: MaterialGroup,
      required: false
    }]
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Sector from the DB
export function show(req, res) {
  return Sector.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function checkDataIntegrity(res, req) {
  return function(entity) {
    if(req.params.id) {
      //update
      if(entity) {
        if(entity.id != req.params.id) {
          res.status(400).send({ message: "Codul introdus exista deja!" }).end();
          return null;
        }
      }
      else {
        return "OK_TO_SEARCH_BY_ID";
      }
    }
    else {
      //create
      if(entity && entity.code === req.body.code) {
        res.status(400).send({ message: "Codul introdus exista deja!" }).end();
      }
    }

    return entity;
  };
}

// Creates a new Sector in the DB
export function create(req, res) {
  return Sector.find({
    where: {
      code: req.body.code
    }
  }).then(checkDataIntegrity(res, req))
    .then(function(entity) {
      return !entity ? Sector.create(req.body) : null
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Sector in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return Sector.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Sector in the DB
export function patch(req, res) {
  if(req.body.id) {
    delete req.body.id;
  }
  return Sector.find({
    where: {
      code: req.body.code
    }
  })
    .then(checkDataIntegrity(res, req))
    .then(function(entity) {
      if(entity != "OK_TO_SEARCH_BY_ID") {
        return entity;
      }
      else {
        return Sector.find({
          where: {
            id: req.params.id
          }
        })
      }
    })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Sector from the DB
export function destroy(req, res) {
  return Sector.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
