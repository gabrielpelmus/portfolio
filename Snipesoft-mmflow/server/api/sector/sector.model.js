'use strict';

export default function (sequelize, DataTypes) {
  return sequelize.define('Sector', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    name: {
      type: DataTypes.STRING
    },
    description: DataTypes.STRING,
    details: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {
      name: {
        singular: 'sector',
        plural: 'sectors',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'sectors'
    }
  );
}
