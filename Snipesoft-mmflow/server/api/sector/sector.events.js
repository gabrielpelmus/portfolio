/**
 * Sector model events
 */

'use strict';

import {EventEmitter} from 'events';
var Sector = require('../../sqldb').Sector;
var SectorEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
SectorEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Sector.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    SectorEvents.emit(event + ':' + doc._id, doc);
    SectorEvents.emit(event, doc);
    done(null);
  };
}

export default SectorEvents;
