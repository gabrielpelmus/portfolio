'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var budgetLineCtrlStub = {
  index: 'budgetLineCtrl.index',
  show: 'budgetLineCtrl.show',
  create: 'budgetLineCtrl.create',
  upsert: 'budgetLineCtrl.upsert',
  patch: 'budgetLineCtrl.patch',
  destroy: 'budgetLineCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var budgetLineIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './budgetLine.controller': budgetLineCtrlStub
});

describe('budgetLine API Router:', function() {
  it('should return an express router instance', function() {
    expect(budgetLineIndex).to.equal(routerStub);
  });

  describe('GET /api/budgetLines', function() {
    it('should route to budgetLine.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'budgetLineCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/budgetLines/:id', function() {
    it('should route to budgetLine.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'budgetLineCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/budgetLines', function() {
    it('should route to budgetLine.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'budgetLineCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/budgetLines/:id', function() {
    it('should route to budgetLine.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'budgetLineCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/budgetLines/:id', function() {
    it('should route to budgetLine.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'budgetLineCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/budgetLines/:id', function() {
    it('should route to budgetLine.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'budgetLineCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
