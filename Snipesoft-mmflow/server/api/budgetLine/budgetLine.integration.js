'use strict';

var app = require('../..');
import request from 'supertest';

var newBudgetLine;

describe('BudgetLine API:', function() {
  describe('GET /api/budgetLines', function() {
    var BudgetLines;

    beforeEach(function(done) {
      request(app)
        .get('/api/budgetLines')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetLines = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(BudgetLines).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/budgetLines', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/budgetLines')
        .send({
          name: 'New BudgetLine',
          info: 'This is the brand new BudgetLine!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newBudgetLine = res.body;
          done();
        });
    });

    it('should respond with the newly created BudgetLine', function() {
      expect(newBudgetLine.name).to.equal('New BudgetLine');
      expect(newBudgetLine.info).to.equal('This is the brand new BudgetLine!!!');
    });
  });

  describe('GET /api/budgetLines/:id', function() {
    var BudgetLine;

    beforeEach(function(done) {
      request(app)
        .get(`/api/budgetLines/${newBudgetLine._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          BudgetLine = res.body;
          done();
        });
    });

    afterEach(function() {
      BudgetLine = {};
    });

    it('should respond with the requested BudgetLine', function() {
      expect(BudgetLine.name).to.equal('New BudgetLine');
      expect(BudgetLine.info).to.equal('This is the brand new BudgetLine!!!');
    });
  });

  describe('PUT /api/budgetLines/:id', function() {
    var updatedBudgetLine;

    beforeEach(function(done) {
      request(app)
        .put(`/api/budgetLines/${newBudgetLine._id}`)
        .send({
          name: 'Updated BudgetLine',
          info: 'This is the updated BudgetLine!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedBudgetLine = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBudgetLine = {};
    });

    it('should respond with the original BudgetLine', function() {
      expect(updatedBudgetLine.name).to.equal('New BudgetLine');
      expect(updatedBudgetLine.info).to.equal('This is the brand new BudgetLine!!!');
    });

    it('should respond with the updated BudgetLine on a subsequent GET', function(done) {
      request(app)
        .get(`/api/budgetLines/${newBudgetLine._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let BudgetLine = res.body;

          expect(BudgetLine.name).to.equal('Updated BudgetLine');
          expect(BudgetLine.info).to.equal('This is the updated BudgetLine!!!');

          done();
        });
    });
  });

  describe('PATCH /api/budgetLines/:id', function() {
    var patchedBudgetLine;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/budgetLines/${newBudgetLine._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched BudgetLine' },
          { op: 'replace', path: '/info', value: 'This is the patched BudgetLine!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedBudgetLine = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedBudgetLine = {};
    });

    it('should respond with the patched BudgetLine', function() {
      expect(patchedBudgetLine.name).to.equal('Patched BudgetLine');
      expect(patchedBudgetLine.info).to.equal('This is the patched BudgetLine!!!');
    });
  });

  describe('DELETE /api/budgetLines/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/budgetLines/${newBudgetLine._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when BudgetLine does not exist', function(done) {
      request(app)
        .delete(`/api/budgetLines/${newBudgetLine._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
