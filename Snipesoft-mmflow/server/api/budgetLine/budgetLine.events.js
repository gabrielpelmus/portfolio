/**
 * BudgetLine model events
 */

'use strict';

import {EventEmitter} from 'events';
var BudgetLine = require('../../sqldb').BudgetLine;
var BudgetLineEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BudgetLineEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  BudgetLine.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    BudgetLineEvents.emit(event + ':' + doc._id, doc);
    BudgetLineEvents.emit(event, doc);
    done(null);
  };
}

export default BudgetLineEvents;
