/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/BudgetLine              ->  index
 * POST    /api/BudgetLine              ->  create
 * GET     /api/BudgetLine/:id          ->  show
 * PUT     /api/BudgetLine/:id          ->  upsert
 * PATCH   /api/BudgetLine/:id          ->  patch
 * DELETE  /api/BudgetLine/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { BudgetLine } from '../../sqldb';
import { BudgetLineAccount } from '../../sqldb';
import { BudgetYearly } from '../../sqldb';
import sqldb from '../../sqldb';
import _ from 'lodash';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      return entity.updateAttributes(updates)
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of BudgetLines
export function index(req, res) {
  var BudgetLineWhere = {
  };

  if(req.query.filter) {
    BudgetLineWhere.$or = [
      {
        code: {
          $like: req.query.filter ? '%' + req.query.filter + '%' : '%%',
        }
      }
    ];
  }

  if(req.query.active === "true" || req.query.active === "false") {
    BudgetLineWhere.active = {
      $eq: req.query.active == "true" ? true : false
    }
  }

  return BudgetLine.findAll({
    // where: BudgetLineWhere,
    //include: {all: true}
    include: [
      {
      model: BudgetLineAccount,
      //as: 'budgetLineIdFK',
      required: false
    },
    {
      model: BudgetYearly,
      //as: 'budgetLineIdY',
      required: false
    }],

  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single BudgetLine from the DB
export function show(req, res) {
  return BudgetLine.find({
    where: {
      id: req.params.id
    },
    include: [{
      model: BudgetLineAccount,
      required: false
    },
    {
      model: BudgetYearly,
      required: false
    }]
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function checkDataIntegrity(res, req) {
  return function(entity) {
    if(req.params.id) {
      //update
      if(entity) {
        if(entity.id != req.params.id) {
          res.status(400).send({ message: "Codul introdus exista deja!" }).end();
          return null;
        }
      }
      else {
        return "OK_TO_SEARCH_BY_ID";
      }
    }
    else {
      //create
      if(entity && entity.code === req.body.code) {
        res.status(400).send({ message: "Codul introdus exista deja!" }).end();
      }
    }

    return entity;
  };
}


// Creates a new BudgetLine in the DB
export function create(req, res) {
  // req.body.createdBy = req.user.id;
  // req.body.updatedBy = req.user.id;

  return BudgetLine.find({
    where: {
      code: req.body.code,
      deleted_at: null// ai deletedAT sa fie null
    }
  }).then(checkDataIntegrity(res, req))
    .then(function(entity) {
      // return !entity ? BudgetLine.create(req.body) : null
      var queries = [];

      queries.push({
        statement: "CALL `mmflow`.`iu_budget_line`(@id := ?, @code := ?, @name := ?, @description := ?, @is_active := ?, \
          @parent_id := ?, @position := ?, @created_by := ?, @updated_by := ?, @budget_line_insert_id);",
        values: [
          req.params.id,
          req.body.code,
          req.body.name,
          req.body.description,
          req.body.isActive ? 1 : 0,
          req.body.parent_id,
          req.body.position,
          req.user.id,
          req.user.id
        ]
      });

      queries.push({
        statement: "SELECT @budget_line_insert_id as budget_line_insert_id;"
      });

      _.forEach(req.body.accounts, function(account) {
        queries.push({
          statement: "CALL `mmflow`.`iu_budget_accounts`(@id := ?, @budget_line_id := @budget_line_insert_id, \
            @name := ?, @valid_from := ?, \
            @created_by := ?, @updated_by := ?, @account_id);",
          values: [
            account.id,
            account.name,
            account.validFrom,
            req.user.id,
            req.user.id
          ]
        });
      });

      return sqldb.dal.transaction({
        queries: queries,
        callback: function(err, data) {
          // res.status(201).send({ data: data}).end();
          // return;

          if(!err) {
            BudgetLine.find({
              where: {
                id: data[1][0].budget_line_insert_id
              },
              include: [{
                model: BudgetLineAccount,
                required: false
              }]
            })
              .then(handleEntityNotFound(res))
              .then(respondWithResult(res))
              .catch(handleError(res));
          }
          else {
            sqldb.dal.callback(err, data, res, queries);
          }
        }
      });

    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given BudgetLine in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return BudgetLine.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing BudgetLine in the DB
export function patch(req, res) {
  if(req.body.id) {
    delete req.body.id;
  }

  req.body.updatedBy = req.user.id;

  return BudgetLine.find({
    where: {
      code: req.body.code
    }
  })
    .then(checkDataIntegrity(res, req))
    .then(function(entity) {
      // if(entity != "OK_TO_SEARCH_BY_ID") {
      //return entity;
      var queries = [];

      queries.push({
        statement: "CALL `mmflow`.`iu_budget_line`(@id := ?, @code := ?, @name := ?, @description := ?, @is_active := ?, \
          @parent_id := ?, @position := ?, @created_by := ?, @updated_by := ?, @budget_line_insert_id);",
        values: [
          req.params.id,
          req.body.code,
          req.body.name,
          req.body.description,
          req.body.isActive ? 1 : 0,
          req.body.parent_id,
          req.body.position,
          req.user.id,
          req.user.id
        ]
      });

      queries.push({
        statement: "SELECT @budget_line_insert_id as budget_line_insert_id;"
      });

      _.forEach(req.body.accounts, function(account) {
        queries.push({
          statement: "CALL `mmflow`.`iu_budget_accounts`(@id := ?, @budget_line_id := @budget_line_insert_id, \
            @name := ?, @valid_from := ?, \
            @created_by := ?, @updated_by := ?, @account_id);",
          values: [
            account.id,
            account.name,
            account.validFrom,
            req.user.id,
            req.user.id
          ]
        });
      });

      _.forEach(req.body.budgets, function(budget) {
        if(budget.updated == "true") {
          queries.push({
            statement: "CALL `mmflow`.`iu_budget_yearly`(@id := ?, @budget_line_id := @budget_line_insert_id, \
            @budget_from := ?, @ian := ?, @feb := ?, @mar := ?, @apr := ?, @mai := ?, @iun := ?, @iul := ?, @aug := ?, \
            @sep := ?, @oct := ?, @nov := ?, @dcbr := ?, \
            @created_by := ?, @updated_by := ?, @budget_id);",
            values: [
              budget.id,
              budget.budgetYear,
              budget.ian,
              budget.feb,
              budget.mar,
              budget.apr,
              budget.mai,
              budget.iun,
              budget.iul,
              budget.aug,
              budget.sep,
              budget.oct,
              budget.nov,
              budget.dcbr,
              req.user.id,
              req.user.id
            ]
          });
        }

      });

      _.forEach(req.body.budgetsTBD, function(budget) {
        queries.push({
          statement: "CALL `mmflow`.`d_budget_yearly`(@id := ?, @deleted_by := ?, @budget_id);",
          values: [
            budget.id,
            req.user.id
          ]
        });
      });

      return sqldb.dal.transaction({
        queries: queries,
        callback: function(err, data) {
          // res.status(201).send({ data: data}).end();
          // return;
          if(!err) {

            BudgetLine.find({
              where: {
                id: data[1][0].budget_line_insert_id
              },
              include: [
                {
                model: BudgetLineAccount,
                required: false
              },
                {
                model: BudgetYearly,
                required: false
              }]
            })
              .then(handleEntityNotFound(res))
              .then(respondWithResult(res))
              .catch(handleError(res));
          }
          else {
            sqldb.dal.callback(err, data, res, queries);
          }
        }
      });
    })
    .catch(handleError(res));
}

// Deletes a BudgetLine from the DB
export function destroy(req, res) {
  req.body.deletedBy = req.user.id;

  return BudgetLine.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    //.then(removeEntity(res))
    .then(function(entity) {
      var queries = [];

      queries.push({
        statement: "CALL `mmflow`.`d_budget_accounts`(@id := ?, @deleted_by := ?, @account_id);",
        values: [
          req.params.id,
          req.user.id
        ]
      });

      queries.push({
        statement: "CALL `mmflow`.`d_budget_line`(@id := ?, @deleted_by := ?, @budget_line_delete_id);",
        values: [
          req.params.id,
          req.user.id
        ]
      });

      return sqldb.dal.transaction({
        queries: queries,
        callback: function(err, data) {
           res.status(201).send({ data: data}).end();
           return;

          // if(!err) {
          //   console.log(data);
            // BudgetLine.find({
            //   //where: {
            //     //id: data[1][0].budget_line_delete_id
            //   //}
            // })
              //  .then(handleEntityNotFound(res))
              //  .then(respondWithResult(res))
              //  .catch(handleError(res));
          // }
          // else {
          //   sqldb.dal.callback(err, data, res, queries);
          // }
        }
      });
    })
    .catch(handleError(res));
}
