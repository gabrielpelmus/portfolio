'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('BudgetLine', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
        unique: true
      }
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    description: DataTypes.STRING,
    isActive: {
      field: "is_active",
      type: DataTypes.BOOLEAN
    },
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    position: {
      type: DataTypes.STRING
    },
    createdAt:
    {
      field: "created_at",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.STRING
    },
    updatedBy: {
      field: "updated_by",
      type: DataTypes.STRING
    },
    deletedBy: {
      field: "deleted_by",
      type: DataTypes.STRING
    }
  }, {
      name: {
        singular: 'budgetLine',
        plural: 'budgetLines',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'budget_line'
    }
  );
}
