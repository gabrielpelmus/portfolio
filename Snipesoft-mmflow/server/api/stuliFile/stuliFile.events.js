/**
 * StuliFile model events
 */

'use strict';

import {EventEmitter} from 'events';
var StuliFile = require('../../sqldb').StuliFile;
var StuliFileEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
StuliFileEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  StuliFile.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    StuliFileEvents.emit(event + ':' + doc._id, doc);
    StuliFileEvents.emit(event, doc);
    done(null);
  };
}

export default StuliFileEvents;
