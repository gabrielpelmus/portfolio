'use strict';

var app = require('../..');
import request from 'supertest';

var newStuliFile;

describe('StuliFile API:', function() {
  describe('GET /api/stuliFiles', function() {
    var stuliFiles;

    beforeEach(function(done) {
      request(app)
        .get('/api/stuliFiles')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          stuliFiles = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(stuliFiles).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/stuliFiles', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/stuliFiles')
        .send({
          name: 'New StuliFile',
          info: 'This is the brand new stuliFile!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newStuliFile = res.body;
          done();
        });
    });

    it('should respond with the newly created stuliFile', function() {
      expect(newStuliFile.name).to.equal('New StuliFile');
      expect(newStuliFile.info).to.equal('This is the brand new stuliFile!!!');
    });
  });

  describe('GET /api/stuliFiles/:id', function() {
    var stuliFile;

    beforeEach(function(done) {
      request(app)
        .get(`/api/stuliFiles/${newStuliFile._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          stuliFile = res.body;
          done();
        });
    });

    afterEach(function() {
      stuliFile = {};
    });

    it('should respond with the requested stuliFile', function() {
      expect(stuliFile.name).to.equal('New StuliFile');
      expect(stuliFile.info).to.equal('This is the brand new stuliFile!!!');
    });
  });

  describe('PUT /api/stuliFiles/:id', function() {
    var updatedStuliFile;

    beforeEach(function(done) {
      request(app)
        .put(`/api/stuliFiles/${newStuliFile._id}`)
        .send({
          name: 'Updated StuliFile',
          info: 'This is the updated stuliFile!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedStuliFile = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedStuliFile = {};
    });

    it('should respond with the original stuliFile', function() {
      expect(updatedStuliFile.name).to.equal('New StuliFile');
      expect(updatedStuliFile.info).to.equal('This is the brand new stuliFile!!!');
    });

    it('should respond with the updated stuliFile on a subsequent GET', function(done) {
      request(app)
        .get(`/api/stuliFiles/${newStuliFile._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let stuliFile = res.body;

          expect(stuliFile.name).to.equal('Updated StuliFile');
          expect(stuliFile.info).to.equal('This is the updated stuliFile!!!');

          done();
        });
    });
  });

  describe('PATCH /api/stuliFiles/:id', function() {
    var patchedStuliFile;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/stuliFiles/${newStuliFile._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched StuliFile' },
          { op: 'replace', path: '/info', value: 'This is the patched stuliFile!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedStuliFile = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedStuliFile = {};
    });

    it('should respond with the patched stuliFile', function() {
      expect(patchedStuliFile.name).to.equal('Patched StuliFile');
      expect(patchedStuliFile.info).to.equal('This is the patched stuliFile!!!');
    });
  });

  describe('DELETE /api/stuliFiles/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/stuliFiles/${newStuliFile._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when stuliFile does not exist', function(done) {
      request(app)
        .delete(`/api/stuliFiles/${newStuliFile._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
