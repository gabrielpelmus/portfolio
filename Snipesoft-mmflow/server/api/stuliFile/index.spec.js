'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var stuliFileCtrlStub = {
  index: 'stuliFileCtrl.index',
  show: 'stuliFileCtrl.show',
  create: 'stuliFileCtrl.create',
  upsert: 'stuliFileCtrl.upsert',
  patch: 'stuliFileCtrl.patch',
  destroy: 'stuliFileCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var stuliFileIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './stuliFile.controller': stuliFileCtrlStub
});

describe('StuliFile API Router:', function() {
  it('should return an express router instance', function() {
    expect(stuliFileIndex).to.equal(routerStub);
  });

  describe('GET /api/stuliFiles', function() {
    it('should route to stuliFile.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'stuliFileCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/stuliFiles/:id', function() {
    it('should route to stuliFile.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'stuliFileCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/stuliFiles', function() {
    it('should route to stuliFile.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'stuliFileCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/stuliFiles/:id', function() {
    it('should route to stuliFile.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'stuliFileCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/stuliFiles/:id', function() {
    it('should route to stuliFile.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'stuliFileCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/stuliFiles/:id', function() {
    it('should route to stuliFile.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'stuliFileCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
