/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/stuli-files              ->  index
 * POST    /api/stuli-files              ->  create
 * GET     /api/stuli-files/:id          ->  show
 * PUT     /api/stuli-files/:id          ->  upsert
 * PATCH   /api/stuli-files/:id          ->  patch
 * DELETE  /api/stuli-files/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { StuliFile } from '../../sqldb';
import { Stuli } from '../../sqldb';
import { User } from '../../sqldb';
import sqldb from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of StuliFiles
export function index(req, res) {
  return StuliFile.findAll({
    include: [{
      model: User,
      required: true
    }]
  })
    .then(function(entity) {
      entity.forEach(stuliFile => {
        stuliFile.dataValues.User = stuliFile.dataValues.User.profile
      });
      return entity;
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Stuli
export function stulisByMaterial(req, res) {

  var date = req.query.date;
  if(!date) date = (new Date()).toISOString();

  var sql = 'SELECT material_code, duration FROM stuli_validities WHERE validity <= "' + date + '"';

  var processData = function(res) {
    return function(entity) {
      if(entity) {
        var results = {};
        for(var i = 0; i < entity.length; i++) {
          if(!results[entity[i].material_code]) {
            results[entity[i].material_code] = entity[i].duration;
          }
        }
        return res.status(200).json(results);
      }
      return null;
    };
  }
  return sqldb.sequelize.query(sql, { type: sqldb.sequelize.QueryTypes.SELECT })
    .then(processData(res))
    .catch(handleError(res));
}

// Gets a single StuliFile from the DB
export function show(req, res) {
  return StuliFile.find({
    include: [{
      model: Stuli,
      required: false
    }, {
      model: User,
      required: true
    }],
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(function(entity) {
      entity.dataValues.User = entity.dataValues.User.profile;
      return entity;
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new StuliFile in the DB
export function create(req, res) {

  /* Below implementation returns indexes for all added entities */
  /*
  let stuliFile = req.body;
  return sqldb.sequelize.transaction({
    isolationLevel: sqldb.sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
  }, function (t) {
    return StuliFile.create({
      name: stuliFile.name,
      description: stuliFile.description,
      validity: stuliFile.name
    }, { transaction: t }).then(function (instance) {
      let stuliPromises = new Array();
      stuliFile.Stulis.forEach(stuli => {
        stuliPromises.push(Stuli.create({
          logisticUnit: stuli.logisticUnit,
          duration: stuli.duration,
          materialCode: stuli.materialCode,
          materialDescription: stuli.materialDescription,

          file_id: instance.id,
          material_group_id: stuli.materialGroupId
        }, { transaction: t }));
      });
      return sqldb.sequelize.Promise.all(stuliPromises);
    });
  }).then(respondWithResult(res)).catch(handleError(res));
*/

  let stuliFile = req.body;
  return sqldb.sequelize.transaction({
    isolationLevel: sqldb.sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
  }, function(t) {
    return StuliFile.create({
      name: stuliFile.name,
      description: stuliFile.description,
      validity: stuliFile.validity,
      created_by: req.user.id
    }, { transaction: t }).then(function(instance) {
      stuliFile.newId = instance.id;
      let newStulis = new Array();
      stuliFile.stulis.forEach(stuli => {
        newStulis.push({
          logisticUnit: stuli.logisticUnit,
          duration: stuli.duration,
          materialCode: stuli.materialCode,
          materialGroupCode: stuli.materialGroupCode,
          materialDescription: stuli.materialDescription,

          file_id: instance.id,
          material_group_id: stuli.materialGroupId
        });
      })
      return Stuli.bulkCreate(newStulis, { transaction: t });
    });
  })
    .then(function(entity) {
      return StuliFile.find({
        include: [{
          model: Stuli,
          required: false
        }, {
          model: User,
          required: true
        }],
        where: {
          id: stuliFile.newId
        }
      })
    })
    .then(respondWithResult(res)).catch(handleError(res));
}

// Upserts the given StuliFile in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return StuliFile.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing StuliFile in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return StuliFile.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a StuliFile from the DB
export function destroy(req, res) {
  return StuliFile.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
