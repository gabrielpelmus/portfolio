'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('StuliFile', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    description: DataTypes.STRING,
    validity: DataTypes.DATE
  }, {
      /**
       * Table options
       */
      timestamps: true,
      underscored: true,
      tableName: 'stuli_files'
    }
  );
}