/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/material-groups              ->  index
 * POST    /api/material-groups              ->  create
 * GET     /api/material-groups/:id          ->  show
 * PUT     /api/material-groups/:id          ->  upsert
 * PATCH   /api/material-groups/:id          ->  patch
 * DELETE  /api/material-groups/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import { MaterialGroup } from '../../sqldb';
import { Sector } from '../../sqldb';
import { Stuli } from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity){
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of MaterialGroups
export function index(req, res) {
  var setHasStuli = function(res, codes) {
    codes = codes.map(function(code) { return code.DISTINCT });
    return function(entity) {
      if(entity) {
        entity.forEach(materialGroup => {
          materialGroup.dataValues.hasStuli = codes.indexOf(materialGroup.dataValues.code) > -1;
        })

        return res.status(200).json(entity);
      }
      return null;
    };
  };

  return Stuli.aggregate('material_group_code', 'DISTINCT', { plain: false })
    .then(function(codesList) {
      let codes = codesList;
      return MaterialGroup.findAll({
        include: [{
          model: Sector,
          required: false
        }]
      })
        .then(setHasStuli(res, codes))
        .catch(handleError(res));
    });
}

// Gets a single MaterialGroup from the DB
export function show(req, res) {
  return MaterialGroup.find({
    include: [{
      model: Sector,
      required: false
    }],
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function checkDataIntegrity(res, req) {
  return function(entity) {
    if(req.params.id) {
      //update
      if(entity) {
        if(entity.id != req.params.id) {
          res.status(400).send({ message: "Codul introdus exista deja!" }).end();
          return null;
        }
      }
      else {
        return "OK_TO_SEARCH_BY_ID";
      }
    }
    else {
      //create
      if(entity && entity.code === req.body.code) {
        res.status(400).send({ message: "Codul introdus exista deja!" }).end();
      }
    }

    return entity;
  };
}


// Creates a new MaterialGroup in the DB
export function create(req, res) {
  return MaterialGroup.find({
    where: {
      code: req.body.code
    }
  }).then(checkDataIntegrity(res, req))
    .then(function(entity) {
      return !entity ? MaterialGroup.create(req.body) : null
    })
    .then(function(entity) {
      if(!entity) return;
      return MaterialGroup.find({
        include: [{
          model: Sector,
          required: false
        }],
        where: {
          id: entity.id
        }
      })
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

  // return MaterialGroup.create(req.body)
  //   .then(function (entity) {
  //     return MaterialGroup.find({
  //       include: [{
  //         model: Sector,
  //         required: false
  //       }],
  //       where: {
  //         id: entity.id
  //       }
  //     })
  //   })
  //   .then(respondWithResult(res, 201))
  //   .catch(handleError(res));
}

// Upserts the given MaterialGroup in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return MaterialGroup.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing MaterialGroup in the DB
export function patch(req, res) {
  if(req.body.id) {
    delete req.body.id;
  }
  return MaterialGroup.find({
    where: {
      code: req.body.code
    }
  })
    .then(checkDataIntegrity(res, req))
    .then(function(entity) {
      if(entity != "OK_TO_SEARCH_BY_ID") {
        return entity;
      }
      else {
        return MaterialGroup.find({
          where: {
            id: req.params.id
          }
        })
      }
    })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(function(entity) {
      if(!entity) return;
      return MaterialGroup.find({
        include: [{
          model: Sector,
          required: false
        }],
        where: {
          id: entity.id
        }
      })
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MaterialGroup from the DB
export function destroy(req, res) {
  return MaterialGroup.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
