'use strict';

export default function (sequelize, DataTypes) {
  return sequelize.define('MaterialGroup', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    name: {
      type: DataTypes.STRING
    },
    description: DataTypes.STRING,
    details: DataTypes.STRING,
    position: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {
      name: {
        singular: 'materialGroup',
        plural: 'materialGroups',
      },
      timestamps: true,
      paranoid: true,
      underscored: true,
      tableName: 'material_groups'
    }
  );
}
