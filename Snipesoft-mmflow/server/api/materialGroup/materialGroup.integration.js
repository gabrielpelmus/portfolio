'use strict';

var app = require('../..');
import request from 'supertest';

var newMaterialGroup;

describe('MaterialGroup API:', function() {
  describe('GET /api/material-groups', function() {
    var materialGroups;

    beforeEach(function(done) {
      request(app)
        .get('/api/material-groups')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          materialGroups = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(materialGroups).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/material-groups', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/material-groups')
        .send({
          name: 'New MaterialGroup',
          info: 'This is the brand new materialGroup!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newMaterialGroup = res.body;
          done();
        });
    });

    it('should respond with the newly created materialGroup', function() {
      expect(newMaterialGroup.name).to.equal('New MaterialGroup');
      expect(newMaterialGroup.info).to.equal('This is the brand new materialGroup!!!');
    });
  });

  describe('GET /api/material-groups/:id', function() {
    var materialGroup;

    beforeEach(function(done) {
      request(app)
        .get(`/api/material-groups/${newMaterialGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          materialGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      materialGroup = {};
    });

    it('should respond with the requested materialGroup', function() {
      expect(materialGroup.name).to.equal('New MaterialGroup');
      expect(materialGroup.info).to.equal('This is the brand new materialGroup!!!');
    });
  });

  describe('PUT /api/material-groups/:id', function() {
    var updatedMaterialGroup;

    beforeEach(function(done) {
      request(app)
        .put(`/api/material-groups/${newMaterialGroup._id}`)
        .send({
          name: 'Updated MaterialGroup',
          info: 'This is the updated materialGroup!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedMaterialGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMaterialGroup = {};
    });

    it('should respond with the original materialGroup', function() {
      expect(updatedMaterialGroup.name).to.equal('New MaterialGroup');
      expect(updatedMaterialGroup.info).to.equal('This is the brand new materialGroup!!!');
    });

    it('should respond with the updated materialGroup on a subsequent GET', function(done) {
      request(app)
        .get(`/api/material-groups/${newMaterialGroup._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let materialGroup = res.body;

          expect(materialGroup.name).to.equal('Updated MaterialGroup');
          expect(materialGroup.info).to.equal('This is the updated materialGroup!!!');

          done();
        });
    });
  });

  describe('PATCH /api/material-groups/:id', function() {
    var patchedMaterialGroup;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/material-groups/${newMaterialGroup._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched MaterialGroup' },
          { op: 'replace', path: '/info', value: 'This is the patched materialGroup!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedMaterialGroup = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedMaterialGroup = {};
    });

    it('should respond with the patched materialGroup', function() {
      expect(patchedMaterialGroup.name).to.equal('Patched MaterialGroup');
      expect(patchedMaterialGroup.info).to.equal('This is the patched materialGroup!!!');
    });
  });

  describe('DELETE /api/material-groups/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/material-groups/${newMaterialGroup._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when materialGroup does not exist', function(done) {
      request(app)
        .delete(`/api/material-groups/${newMaterialGroup._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
