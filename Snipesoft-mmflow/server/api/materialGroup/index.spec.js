'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var materialGroupCtrlStub = {
  index: 'materialGroupCtrl.index',
  show: 'materialGroupCtrl.show',
  create: 'materialGroupCtrl.create',
  upsert: 'materialGroupCtrl.upsert',
  patch: 'materialGroupCtrl.patch',
  destroy: 'materialGroupCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var materialGroupIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './materialGroup.controller': materialGroupCtrlStub
});

describe('MaterialGroup API Router:', function() {
  it('should return an express router instance', function() {
    expect(materialGroupIndex).to.equal(routerStub);
  });

  describe('GET /api/material-groups', function() {
    it('should route to materialGroup.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'materialGroupCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/material-groups/:id', function() {
    it('should route to materialGroup.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'materialGroupCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/material-groups', function() {
    it('should route to materialGroup.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'materialGroupCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/material-groups/:id', function() {
    it('should route to materialGroup.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'materialGroupCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/material-groups/:id', function() {
    it('should route to materialGroup.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'materialGroupCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/material-groups/:id', function() {
    it('should route to materialGroup.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'materialGroupCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
