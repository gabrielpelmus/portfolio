/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function (app) {
  // Insert routes below
  app.use('/api/reports', require('./api/report'));
  app.use('/api/stulis', require('./api/stuliFile/stuliFile.controller').stulisByMaterial);
  app.use('/api/achievement-files', require('./api/achievementFile'));
  app.use('/api/costCenters', require('./api/costCenter'));
  app.use('/api/budgetLines', require('./api/budgetLine'));
  app.use('/api/reportProfitLosses', require('./api/reportProfitLoss'));
  app.use('/api/budgetLineAccount', require('./api/budgetLineAccount'));
  app.use('/api/sellVolumes', require('./api/sellVolume'));
  app.use('/api/stuli-files', require('./api/stuliFile'));
  app.use('/api/material-groups', require('./api/materialGroup'));
  app.use('/api/sectors', require('./api/sector'));
  app.use('/api/pnl-files', require('./api/pnlFile'));
  // app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));
  app.use('/auth', require('./auth').default);
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
}
