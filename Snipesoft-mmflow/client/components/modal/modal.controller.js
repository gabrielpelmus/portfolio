'use strict';

export default class ModalController {

    /*@ngInject*/
    constructor($scope, $uibModalInstance, title, item, showDelete) {

        $scope.title = title;
        $scope.item = item;
        $scope.isDirtyConfirmationPopup = {
            yes: function() {
                $uibModalInstance.dismiss('modal closed ok');
            },
            no: function() {
                $scope.isDirtyConfirmationPopup.visible = false;
            },
            visible: false
        };

        $scope.delete = {};

        $scope.delete.visible = showDelete;
        if($scope.delete.visible) {
            $scope.delete.confirm = function() {
                $uibModalInstance.close({
                    action: "delete",
                    item: $scope.item
                });
            }
        }

        $scope.save = function() {
            $uibModalInstance.close({
                action: "save",
                item: $scope.item
            });
        };

        $scope.cancel = function(isDirty) {
            if(isDirty) {
                $scope.isDirtyConfirmationPopup.visible = true;
            }
            else {
                $uibModalInstance.dismiss('modal closed ok');
            }
        };


    }
}