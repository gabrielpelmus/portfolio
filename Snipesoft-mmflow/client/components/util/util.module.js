'use strict';

import angular from 'angular';
import {
  UtilService
} from './util.service';

export default angular.module('mmflowApp.util', [])
  .factory('Util', UtilService)
  .name;

window.snpHelper = {};

window.snpHelper.getPropByString = function(obj, propString) {
  if(!propString) return null;

  var prop,
    props = propString.split('.');

  for(var i = 0, iLen = props.length - 1; i < iLen; i++) {
    prop = props[i];

    var candidate = obj[prop];
    if(candidate !== undefined) {
      obj = candidate;
    } else {
      break;
    }
  }
  return obj[props[i]];
};

window.snpHelper.guid = function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

window.snpHelper.getUTCDateMidnight = function(sValue) {
  var date = sValue ? new Date(new Date(sValue).setHours(0, 0, 0, 0)) : new Date(new Date().setHours(0, 0, 0, 0));
  var offset = moment(date).utcOffset();
  var date = new Date(moment(date).add(offset, 'm'));
  return date;
}

require('./filterByMatchingFields/filterByMatchingFields.filter');
require('./snpValidators/snpValidators.directive');
require('./snpDelete/snpDelete.directive');
require('./snpEsc/snpEsc.directive');
require('./snpLoading/snpLoading.directive');
require('./snp-confirm/snp-confirm.directive');