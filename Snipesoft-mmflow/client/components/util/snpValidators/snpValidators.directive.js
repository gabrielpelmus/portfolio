'use strict';
const angular = require('angular');

export default angular.module('mmflowApp.util')
  .directive('snpIsObject', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        var validate = function (value) {
          if (!value || value.length == 0) return;

          ngModel.$setValidity('snpIsObject', typeof value === "object");
          return value;
        };

        ngModel.$parsers.unshift(validate);
        ngModel.$formatters.push(validate);
      }
    }
  })
  .name;
