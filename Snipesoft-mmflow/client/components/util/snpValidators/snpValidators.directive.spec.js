'use strict';

describe('Directive: snpValidators', function() {
  // load the directive's module
  beforeEach(module('mmflowApp.util.snpValidators'));

  var element,
    scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<snp-validators></snp-validators>');
    element = $compile(element)(scope);
    expect(element.text()).to.equal('this is the snpValidators directive');
  }));
});
