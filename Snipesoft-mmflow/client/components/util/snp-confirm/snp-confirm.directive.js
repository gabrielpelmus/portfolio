'use strict';
const angular = require('angular');

export default angular.module('mmflowApp.util')
    .directive('snpConfirm', function () {
        return {
            template: require("./snp-confirm.html"),
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                confirmDisabled: "=",
                size: "@",
                confirm: '&',
                cancel: '&'
            },
            controller: ['$scope', function ($scope) {
                if (!$scope.size) $scope.size = "btn-lg";
            }]
        };
    })
    .name;