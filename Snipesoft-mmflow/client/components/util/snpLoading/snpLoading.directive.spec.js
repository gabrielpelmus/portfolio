'use strict';

describe('Directive: snpLoading', function() {
  // load the directive's module
  beforeEach(module('mmflowApp.util'));

  var element,
    scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<snp-loading></snp-loading>');
    element = $compile(element)(scope);
    expect(element.text()).to.equal('this is the snpLoading directive');
  }));
});
