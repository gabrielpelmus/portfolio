'use strict';
const angular = require('angular');

/*@ngInject*/
export default angular.module('mmflowApp.util')
  .directive('snpLoading', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (v) {
          elm.toggleClass("hidden", !v);
        });
      }
    };
  })
  .name;
