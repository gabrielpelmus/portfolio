'use strict';

describe('Directive: snpDelete', function() {
  // load the directive's module and view
  beforeEach(module('mmflowApp.util'));
  beforeEach(module('components/util/snpDelete/snpDelete.html'));

  var element, scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<snp-delete></snp-delete>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).to.equal('this is the snpDelete directive');
  }));
});
