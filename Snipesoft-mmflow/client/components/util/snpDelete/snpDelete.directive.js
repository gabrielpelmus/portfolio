'use strict';
const angular = require('angular');

export default angular.module('mmflowApp.util')
  .directive('snpDelete', function() {
    return {
            template: require("./snpDelete.html"),
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                confirm: '&',
                size: "@size"
            },
            controller: ['$scope', function($scope) {
                $scope.confirmationGroupVisible = false;
                if (!$scope.size) $scope.size = "btn-lg";
            }]
        };
  })
  .name;
