'use strict';
const angular = require('angular');

/*@ngInject*/
export function filterByMatchingFieldsFilter() {
  return function (inputArray, filterValue) {
    if (!filterValue || filterValue == "") {
      return inputArray;
    }
    var outputArray = [],
      fieldsToMatch = [];
    for (var i = 2; i <= arguments.length - 1; i++) {
      fieldsToMatch.push(arguments[i]);
    }

    angular.forEach(inputArray, function (item) {
      for (var i = 0; i <= fieldsToMatch.length - 1; i++) {
        if (snpHelper.getPropByString(item, fieldsToMatch[i]) != null && snpHelper.getPropByString(item, fieldsToMatch[i]).toString().toLowerCase().indexOf(filterValue.toString().toLowerCase()) != -1) {
          outputArray.push(item);
          break;
        }
      };
    });
    return outputArray;
  };
}


export default angular.module('mmflowApp.util')
  .filter('filterByMatchingFields', filterByMatchingFieldsFilter)
  .name;
