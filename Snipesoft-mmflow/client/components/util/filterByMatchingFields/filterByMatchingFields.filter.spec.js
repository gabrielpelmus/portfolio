'use strict';

describe('Filter: filterByMatchingFields', function() {
  // load the filter's module
  beforeEach(module('mmflowApp.util'));

  // initialize a new instance of the filter before each test
  var filterByMatchingFields;
  beforeEach(inject(function($filter) {
    filterByMatchingFields = $filter('filterByMatchingFields');
  }));

  it('should return the input prefixed with "filterByMatchingFields filter:"', function() {
    var text = 'angularjs';
    expect(filterByMatchingFields(text)).to.equal('filterByMatchingFields filter: ' + text);
  });
});
