'use strict';
const angular = require('angular');

require("../../other-libs/exportTable/jquery.base64.js");
require("../../other-libs/exportTable/tableExport.js");
// require("../../other-libs/exportTable/jspdf/libs/sprintf.js");

// require("../../other-libs/exportTable/jspdf/jspdf.js");
// require("../../other-libs/exportTable/jspdf/libs/base64.js");
// require("../../other-libs/exportTable/jspdf.plugin.autotable.js");




export default angular.module('mmFlowApp.excel')
  .directive('exportTable', function () {
    return {
    restrict: 'AC',
    link: function ($scope, elm, attr) {
      $scope.$on('export-pdf', function (e, d) {
        $scope.fontSize = d.fontSize;
        $scope.theme = d.theme;
        $scope.filename = d.filename;
        $scope.reportName = d.reportName;
        elm.tableExport({
          type: 'pdf',
          pdfFontSize: $scope.fontSize,
          pdfTheme: $scope.theme,
          escape: 'false',
          filename: $scope.filename,
          reportName: $scope.reportName
        });
      });

      $scope.$on('export-excel', function (e, d) {
        // $scope.fontSize = d.fontSize;
        // $scope.theme = d.theme;
        debugger;
        $scope.filename = d.filename;
        $scope.reportName = d.reportName;
        elm.tableExport({
          type: 'excel',
          escape: false,
          filename: $scope.filename,
          reportName: $scope.reportName
        });
      });

    }
  }
  })
  .name;
