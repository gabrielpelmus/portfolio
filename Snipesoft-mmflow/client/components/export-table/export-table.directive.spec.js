'use strict';

describe('Directive: snpExcel', function() {
  // load the directive's module
  beforeEach(module('mmFlowApp.excel'));

  var element,
    scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<snp-excel></snp-excel>');
    element = $compile(element)(scope);
    expect(element.text()).to.equal('this is the snpExcel directive');
  }));
});
