'use strict';
const angular = require('angular');
const XLS = require('xlsx');

// https://github.com/SheetJS/js-xlsx/issues/61#issuecomment-55886087
// pentru fisiere mari, va trebui sa folosim XLS files (sunt extrem de mici, datele ar trebui sa ramana acolo)

export default angular.module('mmFlowApp.excel', [])
  .directive('snpExcel', function() {
    return {
      restrict: 'E',
      template: require("./snp-excel.html"),
      replace: true,
      scope: {
        processData: "&",
        showError: "&",
        cssClasses: "@",
        text: "@"
      },
      link: function(scope, element, attrs) {
        if(!scope.cssClasses) scope.cssClasses = "btn-default btn-md";
        if(!scope.text) scope.text = "Incarcare fisier";

        function handleSelect() {

          var files = this.files;
          for(var i = 0, f = files[i]; i != files.length; ++i) {
            var reader = new FileReader();
            reader.fileName = f.name;
            reader.onload = function(e) {
              if(!e) {
                var data = reader.content;
              } else {
                var data = e.target.result;
              }

              /* if binary string, read with type 'binary' */
              try {
                var workbook = XLS.read(data, { type: 'binary' });
                workbook.fileName = this.fileName;
                scope.data = workbook;

                // http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
                scope.$apply(function() {
                  scope.processData({ data: workbook });
                })

              } catch(e) {
                scope.$apply(function() {
                  scope.showError({ error: e })
                })
              }

              // Clear input file
              element.find("input").val('');
            };

            //extend FileReader
            if(!FileReader.prototype.readAsBinaryString) {
              FileReader.prototype.readAsBinaryString = function(fileData) {
                var binary = "";
                var pt = this;
                var reader = new FileReader();
                reader.onload = function(e) {
                  var bytes = new Uint8Array(reader.result);
                  var length = bytes.byteLength;
                  for(var i = 0; i < length; i++) {
                    binary += String.fromCharCode(bytes[i]);
                  }
                  //pt.result  - readonly so assign binary
                  pt.content = binary;
                  $(pt).trigger('onload');
                }
                reader.readAsArrayBuffer(fileData);
              }
            }

            reader.readAsBinaryString(f);

          }
        }

        element.find("input").on('change', handleSelect);
        scope.onButtonClick = function() {
          element.find("input").trigger("click");
        };

      }
    };
  })
  .name;

//incarcam si libraria de export in acest modul
require('../export-table/export-table.directive');
