'use strict';

export function UserResource($resource) {
  'ngInject';

  return $resource('/api/users/:id/:controller', {
    id: '@id'
  }, {
      changePassword: {
        method: 'PUT',
        params: {
          controller: 'password'
        }
      },
      update: {
        method: 'PATCH'
      },
      get: {
        method: 'GET',
        params: {
          id: 'me'
        }
      }
    });
}
