'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
const XLS = require('xlsx');
import routes from './achievement.routes';

export class AchievementComponent {
  /*@ngInject*/
  constructor(AchievementFile, Stuli, toastr, $scope) {
    var that = this;
    this.AchievementFile = AchievementFile;
    this.Stuli = Stuli;
    this.$scope = $scope;
    this.toastr = toastr;
    this.achievementFile = null;

    this.sortOptionsFile = {
      key: "workingDayDateTime",
      reverse: true,
      valueType: "D"
    }

    this.sortFile = function(keyname, type) {
      that.sortOptionsFile.key = keyname;
      that.sortOptionsFile.reverse = !that.sortOptionsFile.reverse;
      that.sortOptionsFile.valueType = type;
    }

    this.customSorterFile = function(obj) {
      switch(that.sortOptionsFile.valueType) {
        case "":
          return obj[that.sortOptionsFile.key];
        case "N":
          return parseInt(obj[that.sortOptionsFile.key]);
        case "D":
          return new Date(obj[that.sortOptionsFile.key]);
        default:
          return obj[that.sortOptionsFile.key];
      }
    };

    this.sortOptionsAchievement = {
      key: "recordedDateTime",
      reverse: false,
      valueType: ""
    }

    this.sortAchievement = function(keyname, type) {
      that.sortOptionsAchievement.key = keyname;
      that.sortOptionsAchievement.reverse = !that.sortOptionsAchievement.reverse;
      that.sortOptionsAchievement.valueType = type;
    }

    this.customSorterAchievement = function(obj) {
      switch(that.sortOptionsAchievement.valueType) {
        case "":
          return obj[that.sortOptionsAchievement.key];
        case "N":
          return parseFloat(obj[that.sortOptionsAchievement.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptionsAchievement.key];
        default:
          return obj[that.sortOptionsAchievement.key];
      }
    };


    this.loadAchievementFiles();
  }

  loadAchievementFiles() {
    this.achievementFiles = [];
    var that = this;

    this.achievementFilesPromise = this.AchievementFile.query({}, function(achievementFiles) {
      for(var i = 0; i < achievementFiles.length; i++) {
        achievementFiles[i].extended = {
          isValidForSaving: false,
          showOnlyInvalidAchievements: false
        }
      }
      that.achievementFiles = achievementFiles;
    }).$promise;
  }

  selectFile(selectedFile) {
    var that = this;
    this.achievementFile = selectedFile;
    var achievementFilePromise = this.AchievementFile.get({ id: selectedFile.id }, function(data) {
      that.achievementFile.achievements = data.achievements;
      that.validateAchievement();
    }).$promise;
  }

  save() {
    var that = this;
    this.savingInProgress = true;
    that.achievementFile.$save(
      function(result) {
        result.achievements = [];
        //incarcam achievement-ul cu duration cu tot dupa ID
        that.AchievementFile.get({ id: result.id }, function(data) {
          result.achievements = data.achievements;
          
          result.extended = {
            isValidForSaving: false,
            showOnlyInvalidAchievements: false
          }
          that.achievementFiles.push(result);

          that.toastr.success('Fisierul a fost adaugat cu success!', { timeOut: 2000 });
          that.validateAchievement();
        })
        that.savingInProgress = false;;
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi adaugat!', { closeButton: true, tapToDismiss: true });
        that.savingInProgress = false;
      });
  }

  processData(data) {
    var that = this;

    that.achievementFile = new that.AchievementFile(
      {
        name: data.fileName,
        achievements: [],
        workingDayDateTime: null
      });

    that.achievementFile.extended = {
      isValidForSaving: false,
      showOnlyInvalidAchievements: false
    }


    var achievementData = XLS.utils.sheet_to_row_object_array(data.Sheets["Sheet1"]);
    var aAchievements = [];

    if(achievementData.length > 0) {
      var lWorkingDayDateTime = new Date(achievementData[0]["Dată introducere"] + ", " + achievementData[0]["Oră intrare"]);

      if(isFinite(lWorkingDayDateTime)) {
        lWorkingDayDateTime = new Date(lWorkingDayDateTime.getFullYear(), lWorkingDayDateTime.getMonth(), lWorkingDayDateTime.getDate(), 4, 50, 0);

        that.achievementFile.workingDayDateTime = lWorkingDayDateTime;

        this.stuliPromise = this.Stuli.query({ "date": lWorkingDayDateTime },
          function(stulis) {
            for(var i = 0; i < achievementData.length; i++) {
              aAchievements.push({
                recordedDateTime: new Date(achievementData[i]["Dată introducere"] + ", " + achievementData[i]["Oră intrare"]).toJSON(),
                logisticUnit: achievementData[i]["Unitate logistică"],
                storageUnit: achievementData[i]["Loc de depozitare"],
                quantity: achievementData[i]["Cant.în unit.intrare"],
                materialCode: achievementData[i]["Material"],
                materialDescription: achievementData[i]["Descriere material"],
                movementType: achievementData[i]["Tip mişcare"],
                profitCenter: achievementData[i]["Centru de profit"],
                materialGroupCode: achievementData[i]["Grup materiale"]
              });
              //calculare timp executie
              var fMaterialProductionTime = aAchievements[i].quantity * stulis[aAchievements[i].materialCode];
              aAchievements[i].materialProductionTime = !isNaN(fMaterialProductionTime) ? parseFloat(fMaterialProductionTime.toFixed(2)) : null;
            }

            that.achievementFile.achievements = aAchievements;
            that.validateAchievement();
          }).$promise;
      }
      else {
        that.validateAchievement();
      }
    }
  }

  showError(error) {
    this.toastr.error('A aparut o eroare in timpul procesarii fisierului!', { closeButton: true, tapToDismiss: true });
  }

  deleteAchievementFile(achievementFile) {
    var that = this;
    achievementFile.$delete(
      function(oResponse) {
        that.toastr.success('Fisierul realizari productie a fost sters!', { timeOut: 2000 });
        if(that.achievementFile == achievementFile) {
          that.achievementFile = null;
        }
        var indexOf = that.achievementFiles.indexOf(achievementFile);
        if(indexOf > -1) {
          that.achievementFiles.splice(indexOf, 1);
        }
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi sters!', { closeButton: true, tapToDismiss: true });
      });
  }

  validateAchievement() {
    // debugger;
    var messageMandt = "";
    var messageOptional = "";

    if(this.achievementFile) {
      var invalidDate = false;

      if(!this.achievementFile.workingDayDateTime) {
        invalidDate = true;
      }

      if(invalidDate) {
        if(messageMandt) {
          messageMandt += "; ";
        }
        messageMandt += "Fisier invalid (nu s-a putut calcula data intrarilor)";
      }

      if(!invalidDate) {
        if(!this.achievementFile.id) {
          //doar pt fisier nou
          for(var i = 0; i < this.achievementFiles.length; i++) {
            if(new Date(this.achievementFiles[i].workingDayDateTime).getTime() === this.achievementFile.workingDayDateTime.getTime()) {
              invalidDate = true;
            }
          }
          if(invalidDate) {
            if(messageMandt) {
              messageMandt += "; ";
            }
            messageMandt += "Exista deja un fisier de realizari definit pentru aceasta data";
          }
        }

        var invalidAchievementsDate = false;
        var minDate = new Date(this.achievementFile.workingDayDateTime);
        var maxDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate() + 1, 4, 49, 59);
        var invalidAchievementMaterialProductionTime = false;
        var invalidAchievementMaterialCode = false;

        for(var i = 0; i < this.achievementFile.achievements.length; i++) {
          this.achievementFile.achievements[i].extended = {
            isDateInvalid: false,
            isMaterialProductionTimeInvalid: false,
            isMaterialCodeInvalid: false,
            message: ""
          };

          var recordedDateTime = new Date(this.achievementFile.achievements[i].recordedDateTime);

          if(!isFinite(recordedDateTime) ||
            (recordedDateTime.getTime() < minDate.getTime() || recordedDateTime.getTime() > maxDate.getTime())
          ) {
            invalidAchievementsDate = true;
            this.achievementFile.achievements[i].extended.isDateInvalid = true;
            this.achievementFile.achievements[i].extended.message = "Data invalida!"
          }

          if(this.achievementFile.achievements[i].materialProductionTime == null) {
            invalidAchievementMaterialProductionTime = true;
            this.achievementFile.achievements[i].extended.isMaterialProductionTimeInvalid = true;
            this.achievementFile.achievements[i].extended.message += this.achievementFile.achievements[i].extended.message ? "; " + "Timp executie nul" : "Timp executie nul";
          }

          if(this.achievementFile.achievements[i].materialCode == null) {
            invalidAchievementMaterialCode = true;
            this.achievementFile.achievements[i].extended.isMaterialCodeInvalid = true;
            this.achievementFile.achievements[i].extended.message += this.achievementFile.achievements[i].extended.message ? "; " + "Cod material nul" : "Cod material nul";
          }
        }

        if(invalidAchievementsDate) {
          if(messageMandt) {
            messageMandt += "; ";
          }
          messageMandt += "Exista realizari in fisier cu data invalida";
        }

        if(invalidAchievementMaterialProductionTime) {
          if(messageOptional) {
            messageOptional += "; ";
          }
          messageOptional += "Exista realizari in fisier pentru care nu se poate calcula timpul de productie";
        }

        if(invalidAchievementMaterialCode) {
          if(messageMandt) {
            messageMandt += "; ";
          }
          messageMandt += "Exista realizari in fisier pentru care nu este definit codul de material";
        }
      }
    }

    if(messageMandt || messageMandt) {
      this.toastr.error(messageMandt ? messageMandt + "; " + messageOptional : messageOptional, { closeButton: true, tapToDismiss: true });
    }
    // lasam sa salveze realizari chiar daca are intrari invalide pentru care nu am putut calcula timpul
    this.achievementFile.extended.isValidForSaving = messageMandt.length === 0;
    this.achievementFile.extended.hasInvalidAchievements = messageMandt.length > 0 || messageOptional.length > 0;
  }

}

export default angular.module('mmflowApp.achievement', [uiRouter])
  .config(routes)
  .component('achievement', {
    template: require('./achievement.html'),
    controller: AchievementComponent,
    controllerAs: '$ctrl'
  })
  .name;
