'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.achievement', {
      url: '/realizari-productie',
      template: '<achievement></achievement>'
    });
}
