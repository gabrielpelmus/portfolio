'use strict';

describe('Component: Pnl', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.pnl'));

  var PnlComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    PnlComponent = $componentController('pnl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
