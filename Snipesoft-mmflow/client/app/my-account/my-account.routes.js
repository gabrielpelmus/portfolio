'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('my-account', {
      url: '/contul-meu',
      template: '<my-account></my-account>'
    });
}
