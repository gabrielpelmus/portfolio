'use strict';

describe('Component: MyAccountComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.my-account'));

  var MyAccountComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    MyAccountComponent = $componentController('my-account', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
