'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './my-account.routes';

export class MyAccountComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('mmflowApp.my-account', [uiRouter])
  .config(routes)
  .component('myAccount', {
    template: require('./my-account.html'),
    controller: MyAccountComponent,
    controllerAs: 'myAccountCtrl'
  })
  .name;
