'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './admin.routes';

export default class AdminController {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, User, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.toastr = toastr; 
    this.User = User;

    this.sortOptions = {
      key: "name",
      reverse: false,
      valueType: ""
    }

    this.sort = function (keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function (obj) {
      switch (that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };
    this.loadUsers();
  }

  loadUsers() {
    this.users = this.User.query();
    var that = this;
    this.usersPromise = this.User.query({}, function (users) {
      that.users = users;// Use the User $resource to fetch all users
    }).$promise;
  }

  openModal(action, user) {
    var that = this;
    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'sm',
      resolve: {
        title: function () {
          switch (action) {
            case "add":
              return "Adaugati un utilizator";
            case "edit":
              return "Editati utilizatorul";
          }
          return "user: unknown action";
        },
        showDelete: function () {
          switch (action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function () {
          var item = {};

          switch (action) {
            case "add":
              item = new that.User();
              item.active = true;
              break;
            case "edit":
              item = angular.copy(user);
              break;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function (result) {
      switch (result.action) {
        case "save":
          result.item.password = result.item.initialPassword;
          if (result.item.id) {
            result.item.$update(
              function (oResponse) {
                that.toastr.success('Utilizatorul a fost editat cu success!', {timeOut: 2000});
                var indexOf = that.users.indexOf(user);
                if (indexOf > -1) {
                  that.users[indexOf] = result.item;
                }
              },
              function () {
                that.toastr.error('"Utilizatorul nu a putut fi editat!', {closeButton: true, tapToDismiss: true});
              });
          } else {
            result.item.$save(
              function () {
                that.toastr.success('Utilizatorul a fost adaugat cu success!', {timeOut: 2000});
                that.users.push(angular.copy(result.item));
              },
              function () {
                that.toastr.error('"Utilizatorul nu a putut fi adaugat!', {closeButton: true, tapToDismiss: true});
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function (oResponse) {
              that.toastr.success('Utilizatorul a fost sters cu success!', {timeOut: 2000});
              var indexOf = that.users.indexOf(user);
              if (indexOf > -1) {
                that.users.splice(indexOf, 1);
              }
            },
            function () {
              that.toastr.error('"Utilizatorul nu a putut fi sters!', {closeButton: true, tapToDismiss: true});
            });
          break;
      }
    }, function (reason) {
      //alert(reason);
    });
  }
}

// export default angular.module('mmflowApp.admin', [uiRouter])
//   .config(routes)
//   .component('admin', {
//     template: require('./admin.html'),
//     controller: AdminController,
//     controllerAs: '$ctrl'
//   })
//   .name;
