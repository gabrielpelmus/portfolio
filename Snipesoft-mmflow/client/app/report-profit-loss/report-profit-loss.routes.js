'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.report-profit-loss', {
      url: '/raport-profit-pierderi',
      template: '<report-profit-loss></report-profit-loss>'
    });
}
