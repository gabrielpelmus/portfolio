'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './report-profit-loss.routes';

export class ReportProfitLossComponent {
  /*@ngInject*/
  constructor($rootScope, $uibModal, toastr, ReportProfitLoss) {
    'ngInject'
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.toastr = toastr;
    this.ReportProfitLoss = ReportProfitLoss;
    this.reportData = [];

    this.e75 = 4487879;
    this.e72 = this.e75 - ((this.e75*15)/100);
    this.h75 = 0;
    this.h72 = 0;
    this.k75 = this.e75 - this.h75;
    this.k72 = this.e72 - this.h72;
    this.DI75 = 4879272; // hardcodam sau de unde il luam 'BPL17_PL Internal'!C70;
    this.DI72 = 4202557;  // hardcodam sau de unde il luam 'BPL17_PL Internal'!C66;
    this.ES75 = this.e75 - this.DI75;
    this.ES72 = this.e72 - this.DI72;

    this.minutes = [
                    {
                      name: 'Sold minutes',
                      values: [this.e72,this.h72,this.k72,this.DI72,this.ES72]
                    },
                    {name: 'Cost per minutes-actual'},
                    {name: 'Cost/min produced with ist scrap rate'},
                    {
                      name: 'Actual minutes worked (nr.of FG produced) SAP',
                      values: [this.e75,this.h75,this.k75,this.DI75,this.ES75]
                    }
                  ];

    this.generateReport();
    this.getMonthsYears();

    this.sortOptions = {
      key: "code",
      reverse: true,
      valueType: ""
    }

    this.sort = function (keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function (obj) {
      switch (that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          return new Date(obj[that.sortOptions.key]);
        default:
          return obj[that.sortOptions.key];
      }
    };

  }

  generateReport() {
    var that = this;

    this.setReportTitle();

    this.reportProfitLossesPromise = this.ReportProfitLoss.query({}, function(reportProfitLosses) {
      that.reportData = reportProfitLosses;
    }).$promise;
  }

  getMonthsYears() {
    this.yearsMonths = [];
    var that = this;
    this.yearsMonthsPromise = this.ReportProfitLoss.query({}, function(my) {
      that.yearsMonths = my;
      console.log(my);
    }).$promise;
  }

  resetReport() {
    this.reportTitle = "";
  }

  setReportTitle(){
    this.reportTitle = "Raport P&L";
    // this.reportTitle += this.selectionOptions.title ? this.selectionOptions.title : "Raport";
    // this.reportTitle += " de la " + moment(this.selectionOptions.fromDate.date).format("DD-MM-YYYY")
    // this.reportTitle += " pana la " + moment(this.selectionOptions.toDate.date).format("DD-MM-YYYY")
  }

  exportAction(action) {

    switch (action) {
      case 'pdf':
        this.$rootScope.$broadcast('export-pdf', {
          filename: this.reportTitle,
          reportName: this.reportTitle
        });
        break;
      case 'excel':
        this.$rootScope.$broadcast('export-excel', {
          filename: this.reportTitle,
          reportName: this.reportTitle
        });
        break;
      default:
      // console.log('no event caught');
    }
  };

}

export default angular.module('mmflowApp.reportProfitLoss', [uiRouter])
  .config(routes)
  .component('reportProfitLoss', {
    template: require('./report-profit-loss.html'),
    controller: ReportProfitLossComponent,
    controllerAs: '$ctrl'
  })
  .name;
