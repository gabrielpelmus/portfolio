'use strict';

describe('Component: ReportProfitLossComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.reportProfitLoss'));

  var ReportProfitLossComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    ReportProfitLossComponent = $componentController('reportProfitLoss', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
