'use strict';

describe('Component: CustomizingComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.customizing'));

  var CustomizingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    CustomizingComponent = $componentController('customizing', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
