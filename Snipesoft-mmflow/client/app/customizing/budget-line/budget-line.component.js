'use strict';
const angular = require('angular');
import routes from './budget-line.routes';

export class BudgetLineComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, BudgetLine, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.BudgetLine = BudgetLine;
    this.toastr = toastr;

    this.loadBudgetLines();



    this.sortOptions = {
      key: "code",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };

  }

  loadBudgetLines() {
    this.budgetLines = [];
    var that = this;
    this.budgetLinesPromise = this.BudgetLine.query({}, function(budgetLines) {
      that.budgetLines = budgetLines;
    }).$promise;
  }

  openModal(action, budgetLine) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'md',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati buget anual";
            case "edit":
              return "Editati bugete anuale";
          }
          return "Linie de buget: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};

          switch(action) {
            case "add":
              item = new that.BudgetLine();
              item.isActive = true;
              item.accounts = [];
              break;
            case "edit":
              item = angular.copy(budgetLine);
              // debugger;
              if(!item.accounts)
                item.accounts = [];
              angular.forEach(item.accounts, function(account) {
                account.pickerValidFrom = {
                  dateOptions: angular.copy(that.$rootScope.extended.dateOptions),
                  date: new Date(account.validFrom),
                  isOpen: false,
                  open: function(oAccount) {
                    oAccount.pickerValidFrom.isOpen = true;
                  },
                  changed: function(oAccount) {
                    oAccount.validFrom = oAccount.pickerValidFrom.date;
                  }
                };
              })
              break;
          }

          item.accountsTBD = [];

          item.onDeleteAccount = function(oAccount) {
            var indexOf = item.accounts.indexOf(oAccount);
            if(indexOf > -1) {
              item.accounts.splice(indexOf, 1);
            }
            if(!oAccount.tempId) {
              item.accountsTBD.push({ id: oAccount.id });
            }
          }

          item.newAccount = {
            data: {
              id: null,
              tempId: true,
              name: null,
              validFrom: null
            },
            pickerValidFrom: {
              dateOptions: angular.copy(that.$rootScope.extended.dateOptions),
              date: null,
              isOpen: false,
              open: function(oAccount) {
                oAccount.pickerValidFrom.isOpen = true;
              },
              changed: function(oAccount) {
                item.newAccount.data.validFrom = item.newAccount.pickerValidFrom.date;
              }
            },
            isAddOpen: false,
            onAdd: function() {
              item.newAccount.isAddOpen = true;
              item.newAccount.data.id = window.snpHelper.guid();
              item.newAccount.data.tempId = true;
              item.newAccount.data.validFrom = new Date();
              item.newAccount.pickerValidFrom.date = item.newAccount.data.validFrom;
            },
            onConfirm: function() {
              item.newAccount.isAddOpen = false;
              item.accounts.push({
                id: item.newAccount.data.id,
                tempId: item.newAccount.data.tempId,
                name: item.newAccount.data.name,
                validFrom: item.newAccount.data.validFrom,
                pickerValidFrom: {
                  dateOptions: angular.copy(that.$rootScope.extended.dateOptions),
                  date: item.newAccount.data.validFrom,
                  isOpen: false,
                  open: function(oAccount) {
                    oAccount.pickerValidFrom.isOpen = true;
                  },
                  changed: function(oAccount) {
                    oAccount.validFrom = oAccount.pickerValidFrom.date;
                  }
                }
              });
              item.newAccount.resetData();
            },
            onCancel: function() {
              item.newAccount.isAddOpen = false;
              item.newAccount.resetData();
            },
            resetData: function() {
              item.newAccount.data.id = null;
              item.newAccount.data.tempId = true;
              item.newAccount.data.name = null;
              item.newAccount.data.validFrom = null;

            }
          }

          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          angular.forEach(result.item.accounts, function(oAccount) {
            if(oAccount.tempId) {
              oAccount.id = null;
            }
            delete oAccount.pickerValidFrom;
            delete oAccount.tempId;
          });
          delete result.item.newAccount;

          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Linia de buget a fost editata cu success!', { timeOut: 2000 });
                var indexOf = that.budgetLines.indexOf(budgetLine);
                if(indexOf > -1) {
                  that.budgetLines[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Linia de buget nu a putut fi editata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Linia de buget a fost adaugata cu success!', { timeOut: 2000 });
                that.budgetLines.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Linia de buget nu a putut fi adaugata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Linia de buget a fost stersa cu success!', { timeOut: 2000 });
              var indexOf = that.budgetLines.indexOf(budgetLine);
              if(indexOf > -1) {
                that.budgetLines.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Linia de buget nu a putut fi stersa!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

  openModalYearlyBudget(action, budgetLine) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./yearly-budget.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'lg',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati buget in Euro";
            case "edit":
              return "Bugete anuale in Euro";
          }
          return "Buget: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};

          switch(action) {
            case "add":
              item = new that.BudgetLine();
              item.isActive = true;
              item.budgets = [];
              break;
            case "edit":
              item = angular.copy(budgetLine);
              if(!item.budgets)
                item.budgets = [];
              angular.forEach(item.budgets, function(budget) {
                budget.pickerFrom = {
                  dateOptions: {
                    //angular.copy(that.$rootScope.extended.dateOptions),
                    format: 'yyyy',
                    showWeeks: 'false'
                  },
                  date: new Date(budget.budgetYear),
                  isOpen: false,
                  open: function(oBudget) {
                    oBudget.pickerFrom.isOpen = true;
                  },
                  changed: function(budgets, oBudget) {
                    angular.forEach(budgets, function(budget) {
                      if(budget.budgetYear == oBudget.pickerFrom.date) {
                        oBudget.ian = budget.ian;
                        oBudget.feb = budget.feb;
                        oBudget.mar = budget.mar;
                        oBudget.apr = budget.apr;
                        oBudget.mai = budget.mai;
                        oBudget.iun = budget.iun;
                        oBudget.iul = budget.iul;
                        oBudget.aug = budget.aug;
                        oBudget.sep = budget.sep;
                        oBudget.oct = budget.oct;
                        oBudget.nov = budget.nov;
                        oBudget.dcbr = budget.dcbr;
                      }
                    });
                    //item.newBudget.onAdd();
                    // oBudget.id = null;
                    //oBudget.from = oBudget.pickerFrom.date;
                    // oBudget.ian = null;
                    // oBudget.feb = null;
                    // oBudget.mar = null;
                    // oBudget.apr = null;
                    // oBudget.mai = null;
                    // oBudget.iun = null;
                    // oBudget.iul = null;
                    // oBudget.aug = null;
                    // oBudget.sep = null;
                    // oBudget.oct = null;
                    // oBudget.nov = null;
                    // oBudget.dcbr = null;
                  }
                };
              })
              break;
          }

          item.budgetsTBD = [];
          item.loadedBudget = null;

          item.onDeleteBudget = function() {
            var indexOf = item.budgets.indexOf(item.loadedBudget);
            if(indexOf > -1) {
              item.budgets.splice(indexOf, 1);
            }
            if(!item.loadedBudget.tempId) {
              item.budgetsTBD.push({ id: item.loadedBudget.id });
              item.newBudget.resetData();
              item.newBudget.isAddOpen = false;
              item.wasDeleted = true;
            }
          }

          item.isAvail = false;
          item.wasDeleted = false;

          item.newBudget = {
            data: {
              id: null,
              tempId: true,
              name: null,
              budgetYear: null
            },
            pickerFrom: {
              dateOptions: {
                //angular.copy(that.$rootScope.extended.dateOptions),
                format: 'yyyy'
              },
              date: null,
              isOpen: false,
              open: function(oBudget) {
                oBudget.pickerFrom.isOpen = true;
              },
              changed: function(budgets) {

                item.isAvail = false;
                item.newBudget.data.budgetYear = item.newBudget.pickerFrom.date;
                item.newBudget.data.ian = null;
                item.newBudget.data.feb = null;
                item.newBudget.data.mar = null;
                item.newBudget.data.apr = null;
                item.newBudget.data.mai = null;
                item.newBudget.data.iun = null;
                item.newBudget.data.iul = null;
                item.newBudget.data.aug = null;
                item.newBudget.data.sep = null;
                item.newBudget.data.oct = null;
                item.newBudget.data.nov = null;
                item.newBudget.data.dcbr = null;

                angular.forEach(budgets, function(budget) {
                  var bYear = new Date(budget.budgetYear);
                  if(bYear.getFullYear() == item.newBudget.pickerFrom.date.getFullYear()) {
                    item.isAvail = true;
                    item.loadedBudget = budget;
                    //item.newBudget.isAddOpen = false;
                    item.newBudget.data.id = budget.id;
                    item.newBudget.data.tempId = false;
                    item.newBudget.data.ian = budget.ian;
                    item.newBudget.data.feb = budget.feb;
                    item.newBudget.data.mar = budget.mar;
                    item.newBudget.data.apr = budget.apr;
                    item.newBudget.data.mai = budget.mai;
                    item.newBudget.data.iun = budget.iun;
                    item.newBudget.data.iul = budget.iul;
                    item.newBudget.data.aug = budget.aug;
                    item.newBudget.data.sep = budget.sep;
                    item.newBudget.data.oct = budget.oct;
                    item.newBudget.data.nov = budget.nov;
                    item.newBudget.data.dcbr = budget.dcbr;
                  }
                });
              }
            },
            isAddOpen: true,
            onAdd: function() {
              item.newBudget.isAddOpen = true;
              item.newBudget.data.id = window.snpHelper.guid();
              item.newBudget.data.tempId = true;
              item.newBudget.data.budgetYear = new Date();
              item.newBudget.pickerFrom.date = item.newBudget.data.budgetYear;
            },
            onConfirm: function() {
              item.newBudget.isAddOpen = false;
              item.budgets.push({
                id: item.newBudget.data.id,
                tempId: item.newBudget.data.tempId,
                budgetYear: item.newBudget.data.budgetYear,
                ian: item.newBudget.data.ian,
                feb: item.newBudget.data.feb,
                mar: item.newBudget.data.mar,
                apr: item.newBudget.data.apr,
                mai: item.newBudget.data.mai,
                iun: item.newBudget.data.iun,
                iul: item.newBudget.data.iul,
                aug: item.newBudget.data.aug,
                sep: item.newBudget.data.sep,
                oct: item.newBudget.data.oct,
                nov: item.newBudget.data.nov,
                dcbr: item.newBudget.data.dcbr,
                updated: "true",
                pickerFrom: {
                  dateOptions: {
                    //angular.copy(that.$rootScope.extended.dateOptions),
                    format: 'yyyy'
                  },
                  date: item.newBudget.data.budgetYear,
                  isOpen: false,
                  open: function(oBudget) {
                    oBudget.pickerFrom.isOpen = true;
                  },
                  changed: function(oBudget) {
                    oBudget.validFrom = oBudget.pickerFrom.date;
                  }
                }
              });
              //item.newBudget.resetData();
            },
            onCancel: function() {
              item.newBudget.isAddOpen = false;
              item.newBudget.resetData();
            },
            resetData: function() {
              item.newBudget.data.id = null;
              item.newBudget.data.tempId = true;
              item.newBudget.data.budgetYear = null;
              item.newBudget.data.ian = null;
              item.newBudget.data.feb = null;
              item.newBudget.data.mar = null;
              item.newBudget.data.apr = null;
              item.newBudget.data.mai = null;
              item.newBudget.data.iun = null;
              item.newBudget.data.iul = null;
              item.newBudget.data.aug = null;
              item.newBudget.data.sep = null;
              item.newBudget.data.oct = null;
              item.newBudget.data.nov = null;
              item.newBudget.data.dcbr = null;

            }
          }

          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          angular.forEach(result.item.budgets, function(oBudget) {
            if(oBudget.tempId) {
              oBudget.id = null;
            }
            delete oBudget.pickerFrom;
            delete oBudget.tempId;
          });
          delete result.item.newBudget;

          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Linia de buget a fost editata cu success!', { timeOut: 2000 });
                var indexOf = that.budgetLines.indexOf(budgetLine);
                if(indexOf > -1) {
                  that.budgetLines[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Linia de buget nu a putut fi editata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Linia de buget a fost adaugata cu success!', { timeOut: 2000 });
                that.budgetLines.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Linia de buget nu a putut fi adaugata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Linia de buget a fost stersa cu success!', { timeOut: 2000 });
              var indexOf = that.budgetLines.indexOf(budgetLine);
              if(indexOf > -1) {
                that.budgetLines.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Linia de buget nu a putut fi stersa!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }
}

export default angular.module('mmflowApp.customizing')
  .config(routes)
  .component('budgetLine', {
    template: require('./budget-line.html'),
    controller: BudgetLineComponent,
    controllerAs: '$ctrl'
  })
  .name;
