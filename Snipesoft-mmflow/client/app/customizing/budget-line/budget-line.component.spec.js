'use strict';

describe('Component: BudgetLineComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.budgetLine'));

  var BudgetLineComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    BudgetLineComponent = $componentController('budgetLine', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
