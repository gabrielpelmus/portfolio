'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing.budget-line', {
      url: '/linii-de-buget',
      template: '<budget-line></budget-line>',
      authenticate: 'admin'
    });
}
