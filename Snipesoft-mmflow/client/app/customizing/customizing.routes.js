'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing', {
      url: '/customizare',
      template: '<customizing></customizing>',
      authenticate: 'admin'
    });
}
