'use strict';
const angular = require('angular');
import routes from './sector.routes';

export class SectorComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, Sector, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.Sector = Sector;
    this.toastr = toastr;

    this.loadSectors();

    this.sortOptions = {
      key: "code",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };

  }

  loadSectors() {
    this.sectors = [];
    var that = this;
    this.sectorsPromise = this.Sector.query({}, function(sectors) {
      that.sectors = sectors;
    }).$promise;
  }

  openModal(action, sector) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'md',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati un sector";
            case "edit":
              return "Editati sectorul";
          }
          return "Sector: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};

          switch(action) {
            case "add":
              item = new that.Sector();
              item.active = true;
              break;
            case "edit":
              item = angular.copy(sector);
              break;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Sectorul a fost editat cu success!', { timeOut: 2000 });
                var indexOf = that.sectors.indexOf(sector);
                if(indexOf > -1) {
                  that.sectors[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Sectorul nu a putut fi editat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Sectorul a fost adaugat cu success!', { timeOut: 2000 });
                that.sectors.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Sectorul nu a putut fi adaugat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Sectorul a fost sters cu success!', { timeOut: 2000 });
              var indexOf = that.sectors.indexOf(sector);
              if(indexOf > -1) {
                that.sectors.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Sectorul nu a putut fi sters!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

}

export default angular.module('mmflowApp.customizing')
  .config(routes)
  .component('sector', {
    template: require('./sector.html'),
    controller: SectorComponent,
    controllerAs: '$ctrl'
  })
  .name;
