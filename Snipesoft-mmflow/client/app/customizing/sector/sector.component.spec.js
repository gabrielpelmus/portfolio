'use strict';

describe('Component: SectorComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.sector'));

  var SectorComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SectorComponent = $componentController('sector', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
