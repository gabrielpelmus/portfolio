'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing.sector', {
      url: '/sector',
      template: '<sector></sector>',
      authenticate: 'admin'
    });
}
