'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing.cost-center', {
      url: '/centre-de-cost',
      template: '<cost-center></cost-center>',
      authenticate: 'admin'
    });
}
