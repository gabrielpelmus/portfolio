'use strict';
const angular = require('angular');
import routes from './cost-center.routes';

export class CostCenterComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, CostCenter, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.CostCenter = CostCenter;
    this.toastr = toastr;

    this.loadCostCenters();

    this.sortOptions = {
      key: "code",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };
  }

  loadCostCenters() {
     this.costCenters = [];
     var that = this;
     this.costCentersPromise = this.CostCenter.query({}, function(costCenters) {
       that.costCenters = costCenters;
     }).$promise;
  }

  openModal(action, costCenter) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'sm',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati un centru de cost";
            case "edit":
              return "Editati centrul de cost";
          }
          return "Centru de cost: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};
          switch(action) {
            case "add":
              item = new that.CostCenter();
              item.isActive = true;
              item.isMfa = true;
              break;
            case "edit":
              item = angular.copy(costCenter);
              break;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Centrul de cost a fost editat cu success!', { timeOut: 2000 });
                var indexOf = that.costCenters.indexOf(costCenter);
                if(indexOf > -1) {
                  that.costCenters[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Centrul de cost nu a putut fi editat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Centrul de cost a fost adaugat cu success!', { timeOut: 2000 });
                that.costCenters.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Centrul de cost nu a putut fi adaugat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Centrul de cost a fost sters cu success!', { timeOut: 2000 });
              var indexOf = that.costCenters.indexOf(costCenter);
              if(indexOf > -1) {
                that.costCenters.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Centrul de cost nu a putut fi sters!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

}

export default angular.module('mmflowApp.customizing')
  .config(routes)
  .component('costCenter', {
    template: require('./cost-center.html'),
    controller: CostCenterComponent,
    controllerAs: '$ctrl'
  })
  .name;
