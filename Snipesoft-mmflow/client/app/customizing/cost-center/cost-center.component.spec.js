'use strict';

describe('Component: CostCenterComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.cost-center'));

  var CostCenterComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    CostCenterComponent = $componentController('cost-center', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
