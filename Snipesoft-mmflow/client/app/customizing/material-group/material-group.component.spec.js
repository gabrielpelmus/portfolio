'use strict';

describe('Component: MaterialGroupComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.material-group'));

  var MaterialGroupComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    MaterialGroupComponent = $componentController('material-group', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
