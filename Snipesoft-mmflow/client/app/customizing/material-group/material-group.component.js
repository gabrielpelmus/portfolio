'use strict';
const angular = require('angular');
import routes from './material-group.routes';

export class MaterialGroupComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, MaterialGroup, toastr, Sector) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.MaterialGroup = MaterialGroup;
    this.toastr = toastr;
    this.Sector = Sector;

    this.loadMaterialGroups();

    this.sortOptions = {
      key: "code",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };

  }

  loadMaterialGroups() {
    this.materialGroups = [];
    var that = this;
    this.materialGroupsPromise = this.MaterialGroup.query({}, function(materialGroups) {
      that.materialGroups = materialGroups;
    }).$promise;
  }

  openModal(action, materialGroup) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'md',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati o grupa de materiale";
            case "edit":
              return "Editati grupa de materiale";
          }
          return "Grupa de materiale: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};

          switch(action) {
            case "add":
              item = new that.MaterialGroup();
              item.active = true;
              break;
            case "edit":
              item = angular.copy(materialGroup);
              break;
          }

          item.extended = {};
          item.extended.sector = {
            data: null,
            search: null,
            add: function() {
              // item.extended.patient.data = item.extended.patient.search;
              // item.extended.contributor.search = item.extended.patient.search.contributor;
              item.sector_id = item.extended.sector.search.id;
              item.sector = item.extended.sector.search;
              // item.extended.patient.search = null
            },
            getFiltered: function(filter) {
              return that.Sector.query({ "filter": filter, "active": true }, function(sectors) {
                return sectors;
              }).$promise;
            },

          };

          if(item.sector_id) {
            item.extended.sector.search = item.sector;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          delete result.item.extended;

          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Grupa de materiale a fost editata cu success!', { timeOut: 2000 });
                var indexOf = that.materialGroups.indexOf(materialGroup);
                if(indexOf > -1) {
                  that.materialGroups[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Grupa de materiale nu a putut fi editata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Grupa de materiale a fost adaugata cu success!', { timeOut: 2000 });
                that.materialGroups.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Grupa de materiale nu a putut fi adaugata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Grupa de materiale a fost stearsa cu success!', { timeOut: 2000 });
              var indexOf = that.materialGroups.indexOf(materialGroup);
              if(indexOf > -1) {
                that.materialGroups.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Grupa de materiale nu a putut fi stearsa!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

}

export default angular.module('mmflowApp.customizing')
  .config(routes)
  .component('materialGroup', {
    template: require('./material-group.html'),
    controller: MaterialGroupComponent,
    controllerAs: '$ctrl'
  })
  .name;
