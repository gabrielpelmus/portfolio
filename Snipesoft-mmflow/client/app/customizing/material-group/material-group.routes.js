'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing.material-group', {
      url: '/grupe-de-materiale',
      template: '<material-group></material-group>',
      authenticate: 'admin'
    });
}
