'use strict';

describe('Component: CostGroupingComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.cost-grouping'));

  var CostGroupingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    CostGroupingComponent = $componentController('costGrouping', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
