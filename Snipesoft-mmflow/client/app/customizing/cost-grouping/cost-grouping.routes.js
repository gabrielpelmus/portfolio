'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('customizing.cost-grouping', {
      url: '/grupe-de-costuri',
      template: '<cost-grouping></cost-grouping>',
      authenticate: 'admin'
    });
}
