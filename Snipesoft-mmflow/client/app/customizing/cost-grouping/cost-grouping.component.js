'use strict';
const angular = require('angular');
import routes from './cost-grouping.routes';

export class CostGroupingComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, CostGrouping, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.CostGrouping = CostGrouping;
    this.toastr = toastr;

   // this.loadCostGroupings();

    this.sortOptions = {
      key: "code",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };

  }

  // loadCostGroupings() {
  //   this.costGroupings = [];
  //   var that = this;
  //   this.costGroupingsPromise = this.CostGrouping.query({}, function(costGroupings) {
  //     that.costGroupings = costGroupings;
  //   }).$promise;
  // }

  openModal(action, costGroupings) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'lg',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati o grupare";
            case "edit":
              return "Editati asignarea";
          }
          return "Grupare de costuri: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};

          switch(action) {
            case "add":
              item = new that.CostGrouping();
              item.active = true;
              break;
            case "edit":
              item = angular.copy(costGrouping);
              break;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Gruparea a fost editata cu success!', { timeOut: 2000 });
                var indexOf = that.costGroupings.indexOf(costGrouping);
                if(indexOf > -1) {
                  that.costGroupings[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Gruparea nu a putut fi editata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Gruparea a fost adaugata cu success!', { timeOut: 2000 });
                that.costGroupings.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Gruparea nu a putut fi adaugata!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Gruparea a fost stersa cu success!', { timeOut: 2000 });
              var indexOf = that.costGroupings.indexOf(costGrouping);
              if(indexOf > -1) {
                that.costGroupings.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Gruparea nu a putut fi stersa!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

}

export default angular.module('mmflowApp.customizing')
  .config(routes)
  .component('costGrouping', {
    template: require('./cost-grouping.html'),
    controller: CostGroupingComponent,
    controllerAs: '$ctrl'
  })
  .name;
