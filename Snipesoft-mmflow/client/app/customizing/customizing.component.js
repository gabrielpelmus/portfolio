'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './customizing.routes';

export class CustomizingComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hellooooo';
  }
}

export default angular.module('mmflowApp.customizing', [uiRouter])
  .config(routes)
  .component('customizing', {
    template: require('./customizing.html'),
    controller: CustomizingComponent,
    controllerAs: 'customizingCtrl'
  })
  .name;

require('./sector/sector.component');
require('./material-group/material-group.component');
require('./budget-line/budget-line.component');
require('./cost-grouping/cost-grouping.component');
require('./cost-center/cost-center.component');

