'use strict';
const angular = require('angular');
const XLS = require('xlsx');
import routes from './stuli.routes';
import uiRouter from 'angular-ui-router';

export class StuliComponent {
  /*@ngInject*/
  constructor(StuliFile, toastr, $scope, $rootScope) {
    var that = this;
    this.toastr = toastr;
    this.StuliFile = StuliFile;
    this.$scope = $scope;

    this.validity = {
      dateOptions: angular.copy($rootScope.extended.dateOptions),
      date: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 4, 50, 0),
      isOpen: false,
      open: function() {
        that.validity.isOpen = true;
      },
      changed: function() {
        that.validity.date = new Date(that.validity.date.getFullYear(), that.validity.date.getMonth(), that.validity.date.getDate(), 4, 50, 0);
        if(that.stuliFile && !that.stuliFile.id) {
          that.stuliFile.validity = that.validity.date;
          that.newStuliFileValidation();
        }
      }
    }

    this.sortOptionsFile = {
      key: "validity",
      reverse: true,
      valueType: "D"
    }

    this.sortFile = function(keyname, type) {
      that.sortOptionsFile.key = keyname;
      that.sortOptionsFile.reverse = !that.sortOptionsFile.reverse;
      that.sortOptionsFile.valueType = type;
    }

    this.customSorterFile = function(obj) {
      switch(that.sortOptionsFile.valueType) {
        case "":
          return obj[that.sortOptionsFile.key];
        case "N":
          return parseInt(obj[that.sortOptionsFile.key]);
        case "D":
          return new Date(obj[that.sortOptionsFile.key]);
        default:
          return obj[that.sortOptionsFile.key];
      }
    };

    this.sortOptionsStuli = {
      key: "materialCode",
      reverse: false,
      valueType: ""
    }

    this.sortStuli = function(keyname, type) {
      that.sortOptionsStuli.key = keyname;
      that.sortOptionsStuli.reverse = !that.sortOptionsStuli.reverse;
      that.sortOptionsStuli.valueType = type;
    }

    this.customSorterStuli = function(obj) {
      switch(that.sortOptionsStuli.valueType) {
        case "":
          return obj[that.sortOptionsStuli.key];
        case "N":
          return parseFloat(obj[that.sortOptionsStuli.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptionsStuli.key];
        default:
          return obj[that.sortOptionsStuli.key];
      }
    };

    this.loadStuliFiles();
  }

  loadStuliFiles() {
    this.stuliFiles = [];
    var that = this;

    this.stuliFilesPromise = this.StuliFile.query({}, function(stuliFiles) {
      for(var i = 0; i < stuliFiles.length; i++) {
        stuliFiles[i].extended = {
          showOnlyInvalidStulis: false,
          hasInvalidStulis: false
        }
      }
      that.stuliFiles = stuliFiles;
    }).$promise;
  }

  selectFile(selectedFile) {
    var that = this;
    that.stuliFile = selectedFile;
    that.stuliFile.extended.hasInvalidStulis = false;
    var stuliFilePromise = this.StuliFile.get({ id: selectedFile.id }, function(oResponse) {
      for(var i = 0; i < oResponse.stulis.length; i++) {
        oResponse.stulis[i].extended = {
          isInvalid: false
        };
        if(!oResponse.stulis[i].duration || !oResponse.stulis[i].materialCode) {
          oResponse.stulis[i].extended.isInvalid = true;
          that.stuliFile.extended.hasInvalidStulis = true;
        }
      }

      that.stuliFile.stulis = oResponse.stulis;
    }).$promise;
  }

  save() {
    var that = this;
    this.savingInProgress = true;
    that.stuliFile.$save(
      function(oResponse) {
        that.toastr.success('Fisierul a fost salvat cu success!', { timeOut: 2000 });
        oResponse.extended = {
          showOnlyInvalidStulis: false,
          hasInvalidStulis: false
        }

        for(var i = 0; i < oResponse.stulis.length; i++) {
          oResponse.stulis[i].extended = {
            isInvalid: false
          };
          if(!oResponse.stulis[i].duration || !oResponse.stulis[i].materialCode) {
            oResponse.stulis[i].extended.isInvalid = true;
            that.stuliFile.extended.hasInvalidStulis = true;
          }
        }

        that.stuliFiles.push(oResponse);
        that.savingInProgress = false;
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi salvat!', { closeButton: true, tapToDismiss: true });
        that.savingInProgress = false;
      });
  }

  processData(data) {
    var that = this;

    that.stuliFile = new that.StuliFile(
      {
        name: data.fileName,
        validity: that.validity.date,
        stulis: []
      });

    that.stuliFile.extended = {
      showOnlyInvalidStulis: false,
      hasInvalidStulis: false,
      isValidForSaving: false
    }

    var stuliData = XLS.utils.sheet_to_row_object_array(data.Sheets["Sheet1"]);
    var aStulis = [];
    var atLeastOneStuliIsValid = false;
    for(var i = 0; i < stuliData.length; i++) {
      var oTempStuli = {
        logisticUnit: stuliData[i]["Werk"],
        duration: stuliData[i]["te"],
        materialCode: stuliData[i]["SAP-Nr."],
        materialGroupCode: stuliData[i]["Warengruppe"],
        materialDescription: stuliData[i]["Materialbezeichnung"],
        extended: {}
      };

      if(!oTempStuli.duration || !oTempStuli.materialCode) {
        oTempStuli.extended.isInvalid = true;
        that.stuliFile.extended.hasInvalidStulis = true;
      }
      else {
        atLeastOneStuliIsValid = true;
      }
      aStulis.push(oTempStuli);
    }

    if(!atLeastOneStuliIsValid) {
      //daca niciun rand din fisierul XLS nu este valid atunci golim lista. 
      //inseamna ca fisierul ales nu este formatat corect
      aStulis.length = 0;
    }

    that.stuliFile.stulis = aStulis;
    that.newStuliFileValidation();
  }

  showError(error) {
    this.toastr.error('A aparut o eroare in timpul procesarii fisierului', { closeButton: true, tapToDismiss: true });
  }

  deleteStuliFile(stuliFile) {
    var that = this;
    stuliFile.$delete(
      function(oResponse) {
        that.toastr.success('Fisierul a fost sters cu succes', { timeOut: 2000 });
        if(that.stuliFile == stuliFile) {
          that.stuliFile = null;
        }
        var indexOf = that.stuliFiles.indexOf(stuliFile);
        if(indexOf > -1) {
          that.stuliFiles.splice(indexOf, 1);
        }
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi sters', { closeButton: true, tapToDismiss: true });
      });
  }

  toggleShowOnlyInvalidStulis(stuliFile) {
    stuliFile.extended.showOnlyInvalidStulis = !stuliFile.extended.showOnlyInvalidStulis;
  }

  newStuliFileValidation() {
    var message = "";
    if(this.stuliFile && !this.stuliFile.id) {
      var invalidFile = false;

      if(this.stuliFile.stulis.length === 0) {
        invalidFile = true;
      }

      if(invalidFile) {
        if(message) {
          message += "; ";
        }
        message += "Fisierul ales nu contine date valide";
      }

      var invalidDate = false;
      for(var i = 0; i < this.stuliFiles.length; i++) {
        if(new Date(this.stuliFiles[i].validity).getTime() === this.stuliFile.validity.getTime()) {
          invalidDate = true;
        }
      }
      if(invalidDate) {
        if(message) {
          message += "; ";
        }
        message += "Exista deja un fisier definit pentru data selectata";
      }

      var invalidStulis = false;
      for(var i = 0; i < this.stuliFile.stulis.length; i++) {
        if(this.stuliFile.stulis[i].isInvalid) {
          invalidStulis = true;
        }
      }
      if(invalidStulis) {
        if(message) {
          message += "; ";
        }
        message += "Fisierul contine intrari invalide";
      }
    }

    if(message) {
      this.toastr.error(message, { closeButton: true, tapToDismiss: true });
    }

    this.stuliFile.extended.isValidForSaving = message.length === 0;
  }

}

export default angular.module('mmflowApp.stuli', [uiRouter])
  .config(routes)
  .component('stuli', {
    template: require('./stuli.html'),
    controller: StuliComponent,
    controllerAs: '$ctrl'
  })
  .name;
