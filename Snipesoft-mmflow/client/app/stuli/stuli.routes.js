'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.stuli', {
      url: '/stuli',
      template: '<stuli></stuli>'
    });
}