'use strict';

describe('Component: StuliComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp'));

  var StuliComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    StuliComponent = $componentController('stuli', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
