'use strict';
import angular from 'angular';

export function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, toastrConfig) {
  'ngInject';

  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
      template: '<div ui-view></div>',
      data: {
        authenticate: true
      }
    });

  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);

  angular.extend(toastrConfig, {
    allowHtml: false,
    closeButton: false,
    progressBar: false,
    tapToDismiss: true,
    timeOut: 0,
    extendedTimeOut: 0
  });
}
