'use strict';

import $ from "jquery"

window.$ = $;
window.jQuery = $;

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngMessages from 'angular-messages';
import toastr from 'angular-toastr';
import validation from 'angular-validation-match';
import dirPaginate from 'angular-utils-pagination'; //'angularUtils.directives.dirPagination';
// import ngValidationMatch from 'angular-validation-match';

import {
  routeConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import modal from '../components/modal/modal.service';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import stuli from './stuli/stuli.component';
import achievement from './achievement/achievement.component';
import pnl from './pnl/pnl.component';
import sellVolume from './sell-volume/sell-volume.component';
import report from './report/report.component';
import customizing from './customizing/customizing.component';
import myAccount from './my-account/my-account.component';
import reportProfitLoss from './report-profit-loss/report-profit-loss.component';
import services from './services/services.module';
import excel from '../components/snp-excel/snp-excel.directive';

import './app.css';

angular.module('mmflowApp', [ngCookies, ngResource, ngSanitize, uiRouter, uiBootstrap, _Auth,
  account, admin, navbar, modal, footer, main, constants, util, stuli, achievement, pnl, sellVolume, report, reportProfitLoss, customizing, myAccount, services, excel, ngAnimate, ngMessages, toastr, dirPaginate, validation
])
  .config(routeConfig)
  .run(function ($rootScope, $location, Auth, $templateCache) {
    'ngInject';

    $templateCache.put("snp-overlay.html", require("../components/modal/snp-overlay.html"));

    $rootScope.extended = {
      animationsEnabled: true,
      dateOptions: {
        startingDay: 1,
        format: "dd/MM/yyyy"
      }
    };

    $rootScope.$on('$stateChangeStart', function (event, next) {
      //close the modal if opened
      if ($rootScope.extended && $rootScope.extended.uibModalInstance) {
        $rootScope.extended.uibModalInstance.dismiss('cancel');
      }

      /* verificarea autentificarii se face direct pe state, eg. authenticate: true sau authenticate: 'admin' */
      // Auth.isLoggedIn(function (loggedIn) {
      //   if (next.authenticate && !loggedIn) {
      //     $location.path('/login');
      //   }
      //   else{ $location.path('/login');}
      // });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['mmflowApp'], {
      strictDi: true
    });
  });
