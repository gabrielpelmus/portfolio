'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.report', {
      url: '/raportare',
      template: '<report></report>'
    });
}
