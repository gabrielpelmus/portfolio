'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './report.routes';

export class ReportComponent {
  /*@ngInject*/
  constructor($rootScope, $uibModal, toastr, Report) {
    'ngInject'
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.toastr = toastr;
    this.Report = Report;
    this.reportData = [];

    this.sortOptions = {
      key: "date",
      reverse: false,
      valueType: "D"
    }

    this.sort = function (keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function (obj) {
      switch (that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          return new Date(obj[that.sortOptions.key]);
        default:
          return obj[that.sortOptions.key];
      }
    };

    this.dateOptions = angular.copy($rootScope.extended.dateOptions);

    this.selectionOptions = {
      title: "",
      fromDate: {
        date: null,
        isOpen: false,
        open: function () {
          that.selectionOptions.fromDate.isOpen = true;
        },
        changed: function () {
          that.selectionOptions.fromDate.date = new Date(that.selectionOptions.fromDate.date.getFullYear(), that.selectionOptions.fromDate.date.getMonth(), that.selectionOptions.fromDate.date.getDate(), 4, 50, 0);
          that.selectionOptions.toDate.date = new Date(that.selectionOptions.fromDate.date.getFullYear(), that.selectionOptions.fromDate.date.getMonth(), that.selectionOptions.fromDate.date.getDate() + 6, 4, 49, 59);
        }
      },
      toDate: {
        date: null,
        isOpen: false,
        open: function () {
          that.selectionOptions.toDate.isOpen = true;
        },
        changed: function () {
          that.selectionOptions.toDate.date = new Date(that.selectionOptions.toDate.date.getFullYear(), that.selectionOptions.toDate.date.getMonth(), that.selectionOptions.toDate.date.getDate(), 4, 49, 59);
        }
      },
      profitCenters: [],
      getProfitCentersSelected: function () {
        var aSelected = [];
        for (var i = 0; i < that.selectionOptions.profitCenters.length; i++) {
          if (that.selectionOptions.profitCenters[i].isSelected) {
            aSelected.push(that.selectionOptions.profitCenters[i]);
          }
        }
        return aSelected;
      }
    }

    this.getProfitCentersPromise = this.Report.getProfitCenters(this.selectionOptions).then(function (data) {
      that.selectionOptions.profitCenters = data.map(function (pc) {
        return {
          name: pc,
          isSelected: true
        }
      });
    }, function (err) { });

    that.calculateTotals();
  }

  generateReport() {
    var that = this;
    this.setReportTitle();

    this.getProfitCentersPromise = this.Report.getProductionBySectors(this.selectionOptions).then(function (data) {
      that.reportData = data;
      that.calculateTotals();
      if (that.reportData.length == 0) {
        that.toastr.error('Nu exista date pentru criteriile de selectie alese', { closeButton: true, tapToDismiss: true });
      }
    }, function (err) {
      //TODO: handle report generation errors
      that.toastr.error('Raportul nu a putut fi generat!', { closeButton: true, tapToDismiss: true });
    });
  }

  resetReport() {
    for (var i = 0; i < this.selectionOptions.profitCenters.length; i++) {
      this.selectionOptions.profitCenters[i].isSelected = true;
    }
    this.reportData.length = 0;
    this.calculateTotals();
    // this.selectionOptions.title = "";
    // this.selectionOptions.fromDate.date = null;
    // this.selectionOptions.toDate.date = null;
  }

  calculateTotals() {
    this.totalDuration = 0;
    this.totalQuantity = 0;

    for (var i = 0; i < this.reportData.length; i++) {
      this.totalDuration += this.reportData[i].duration;
      this.totalQuantity += this.reportData[i].quantity;
    }
  }

  formatDate(oDate){
    return oDate.getDate() + "-" + oDate.getMonth() + "-" + oDate.getFullYear();
  }

  setReportTitle(){
    this.reportTitle = "";
    this.reportTitle += this.selectionOptions.title ? this.selectionOptions.title : "Raport";
    this.reportTitle += " de la " + moment(this.selectionOptions.fromDate.date).format("DD-MM-YYYY")
    this.reportTitle += " pana la " + moment(this.selectionOptions.toDate.date).format("DD-MM-YYYY")
  }

  exportAction(action) {

    switch (action) {
      case 'pdf':
        this.$rootScope.$broadcast('export-pdf', {
          filename: this.reportTitle, 
          reportName: this.reportTitle
        });
        break;
      case 'excel':
        this.$rootScope.$broadcast('export-excel', {
          filename: this.reportTitle,
          reportName: this.reportTitle 
        });
        break;
      default:
      // console.log('no event caught');
    }
  };

  openProfitCentersModal() {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./select-profit-centers.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'lg',
      resolve: {
        title: function () {
          return "Selectati gamele";
        },
        showDelete: function () {
          return false;
        },
        item: function () {
          var item = {};
          item.profitCenters = angular.copy(that.selectionOptions.profitCenters);

          item.allProfitCentersSelected = true;
          for (var i = 0; i < item.profitCenters.length; i++) {
            if (!item.profitCenters[i].isSelected) {
              item.allProfitCentersSelected = false;
            }
          }

          item.profiCenterIsSelectedChange = function () {
            var allProfitCentersSelected = true;
            for (var i = 0; i < item.profitCenters.length; i++) {
              if (!item.profitCenters[i].isSelected) {
                allProfitCentersSelected = false;
              }
            }
            item.allProfitCentersSelected = allProfitCentersSelected;
          }

          item.toggleSelectAllProfitCenters = function () {
            item.allProfitCentersSelected = !item.allProfitCentersSelected;
            for (var i = 0; i < item.profitCenters.length; i++) {
              item.profitCenters[i].isSelected = item.allProfitCentersSelected;
            }
          }

          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function (result) {
      that.selectionOptions.profitCenters = angular.copy(result.item.profitCenters);
    }, function (reason) {
      //alert(reason);
    });
  }


}

export default angular.module('mmflowApp.report', [uiRouter])
  .config(routes)
  .component('report', {
    template: require('./report.html'),
    controller: ReportComponent,
    controllerAs: '$ctrl'
  })
  .name;
