'use strict';

describe('Component: AchievementComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.achievement'));

  var AchievementComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AchievementComponent = $componentController('achievement', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
