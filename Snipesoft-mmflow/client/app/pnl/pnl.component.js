'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
const XLS = require('xlsx');
import routes from './pnl.routes';

export class PnlComponent {
  /*@ngInject*/
  constructor(PnlFile, toastr, $scope, $rootScope, BudgetLineAccount) {
    var that = this;
    this.BudgetLineAccount = BudgetLineAccount;
    this.PnlFile = PnlFile;
    this.$scope = $scope;
    this.toastr = toastr;

    this.sortOptionsFile = {
      key: "workingDayDateTime",
      reverse: true,
      valueType: "D"
    }

    this.sortFile = function(keyname, type) {
      that.sortOptionsFile.key = keyname;
      that.sortOptionsFile.reverse = !that.sortOptionsFile.reverse;
      that.sortOptionsFile.valueType = type;
    }

    this.customSorterFile = function(obj) {
      switch(that.sortOptionsFile.valueType) {
        case "":
          return obj[that.sortOptionsFile.key];
        case "N":
          return parseInt(obj[that.sortOptionsFile.key]);
        case "D":
          return new Date(obj[that.sortOptionsFile.key]);
        default:
          return obj[that.sortOptionsFile.key];
      }
    };

    this.sortOptionsPnl = {
      key: "recordedDateTime",
      reverse: false,
      valueType: ""
    }

    this.sortPnl = function(keyname, type) {
      that.sortOptionsPnl.key = keyname;
      that.sortOptionsPnl.reverse = !that.sortOptionsPnl.reverse;
      that.sortOptionsPnl.valueType = type;
    }

    this.customSorterPnl = function(obj) {
      switch(that.sortOptionsPnl.valueType) {
        case "":
          return obj[that.sortOptionsPnl.key];
        case "N":
          return parseFloat(obj[that.sortOptionsPnl.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptionsPnl.key];
        default:
          return obj[that.sortOptionsPnl.key];
      }
    };

    this.loadBudgetLineAccounts();
  }

  loadPnlFiles() {
    this.pnlFiles = [];
    var that = this;

    this.pnlFilesPromise = this.PnlFile.query({}, function(pnlFiles) {
      for(var i = 0; i < pnlFiles.length; i++) {
        pnlFiles[i].extended = {
          isValidForSaving: false,
          showOnlyInvalidPnls: false
        }
      }
      that.pnlFiles = pnlFiles;
    }).$promise;
  }


  loadBudgetLineAccounts() {
    this.budgetLineAccounts = [];
    var that = this;

    this.budgetLineAccountPromise = this.BudgetLineAccount.query({}, function(accounts) {
      that.budgetLineAccounts = accounts;

      //incarcam fisierele dupa ce am citit customizarea
      that.loadPnlFiles();
    }).$promise;
  }

  selectFile(selectedFile) {
    var that = this;
    this.pnlFile = selectedFile;
    var pnlFilePromise = this.PnlFile.get({ id: selectedFile.id }, function(data) {
      that.pnlFile.pnls = data.pnls;
      that.validatePnl();
    }).$promise;
  }

  save() {
    var that = this;
    this.savingInProgress = true;
    delete that.pnlFile.extended;
    that.pnlFile.$save(
      function(result) {
        result.pnls = [];
        //incarcam pnl-ul dupa ID
        that.PnlFile.get({ id: result.id }, function(data) {
          result.pnls = data.pnls;

          result.extended = {
            isValidForSaving: false,
            showOnlyInvalidPnls: false
          }
          that.pnlFiles.push(result);

          that.toastr.success('Fisierul a fost adaugat cu success!', { timeOut: 2000 });
          that.validatePnl();
        })
        that.savingInProgress = false;;
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi adaugat!', { closeButton: true, tapToDismiss: true });
        that.savingInProgress = false;
      });
  }

  processData(data) {
    var that = this;

    that.pnlFile = new that.PnlFile(
      {
        name: data.fileName,
        pnls: [],
        importDate: new Date(),
        startDate: null,
        endDate: null
      });

    that.pnlFile.extended = {
      isValidForSaving: false,
      showOnlyInvalidPnls: false
    }

    var pnlData = XLS.utils.sheet_to_row_object_array(data.Sheets["SAPdata"]);
    var aPnls = [];

    if(pnlData.length > 0) {
      // debugger;
      for(var i = 0; i < pnlData.length; i++) {
        var oTempPnl = {
          postingDateTime: window.snpHelper.getUTCDateMidnight(pnlData[i]["Pstng Date"]), //new Date().toJSON(),
          account: pnlData[i]["Account"],
          amount: parseFloat(pnlData[i]["Amount eur"]),
          currency: 'EUR',
          costCenter: pnlData[i]["Cost Ctr"]
        };
        oTempPnl.amount = parseFloat(oTempPnl.amount.toFixed(2));
        aPnls.push(oTempPnl);
      }
    }

    that.pnlFile.pnls = aPnls;
    that.validatePnl();
  }

  showError(error) {
    this.toastr.error('A aparut o eroare in timpul procesarii fisierului!', { closeButton: true, tapToDismiss: true });
  }

  deletePnlFile(pnlFile) {
    var that = this;
    pnlFile.$delete(
      function(oResponse) {
        that.toastr.success('Fisierul P&L a fost sters!', { timeOut: 2000 });
        if(that.pnlFile == pnlFile) {
          that.pnlFile = null;
        }
        var indexOf = that.pnlFiles.indexOf(pnlFile);
        if(indexOf > -1) {
          that.pnlFiles.splice(indexOf, 1);
        }
      },
      function() {
        that.toastr.error('Fisierul nu a putut fi sters!', { closeButton: true, tapToDismiss: true });
      });
  }

  // validarea PNL row
  validatePnl() {
    // try catch here TODO
    var that = this;
    var messageMandt = "";
    var messageOptional = "";

    if(this.pnlFile) {
      // var invalidDate = false;

      // if(invalidDate) {
      //   if(messageMandt) {
      //     messageMandt += "; ";
      //   }
      //   messageMandt += "Fisier invalid (nu s-a putut calcula data intrarilor)";
      // }

      // in functie de prima intrare din fisier calculam perioada de date expected.
      // ne asteptam ca fisierul sa contina date doar pe o luna!!!
      that.pnlFile.startDate = moment(that.pnlFile.pnls[0].postingDateTime).startOf('month').toDate();
      that.pnlFile.endDate = moment(that.pnlFile.pnls[0].postingDateTime).endOf('month').toDate();

      var invalidPnlsDate = false;
      var minDate = that.pnlFile.startDate;
      var maxDate = that.pnlFile.endDate;
      var invalidPnlsAccount = false;
      var invalidPnlsAmount = false;

      for(var i = 0; i < this.pnlFile.pnls.length; i++) {
        this.pnlFile.pnls[i].extended = {
          isDateInvalid: false,
          isAccountInvalid: false,
          message: ""
        };

        var postingDateTime = new Date(this.pnlFile.pnls[i].postingDateTime);

        if(!isFinite(postingDateTime) ||
          (postingDateTime.getTime() < minDate.getTime() || postingDateTime.getTime() > maxDate.getTime())
        ) {
          invalidPnlsDate = true;
          this.pnlFile.pnls[i].extended.isDateInvalid = true;
          this.pnlFile.pnls[i].extended.message = "Data invalida!"
        }

        if(!isFinite(this.pnlFile.pnls[i].amount)) {
          invalidPnlsAmount = true;
          this.pnlFile.pnls[i].extended.isAmountInvalid = true;
          this.pnlFile.pnls[i].extended.message = "Suma invalida!"
        }

        var pnlEntryAccountFound = false;
        angular.forEach(that.budgetLineAccounts, function(budgetLineAccount) {
          if(budgetLineAccount.name === that.pnlFile.pnls[i].account &&
            window.snpHelper.getUTCDateMidnight(budgetLineAccount.validFrom).getTime() <= postingDateTime.getTime()) {
            pnlEntryAccountFound = true;
            // break;
          }
        });
        if(!pnlEntryAccountFound) {
          invalidPnlsAccount = true;
          this.pnlFile.pnls[i].extended.isAccountInvalid = true;
          if(this.pnlFile.pnls[i].extended.message) {
            this.pnlFile.pnls[i].extended.message += "; "
          }
          this.pnlFile.pnls[i].extended.message += "Cont invalid pe aceasta data!"
        }
      }

      if(invalidPnlsDate) {
        if(messageMandt) {
          messageMandt += "; ";
        }
        messageMandt += "Exista intrari in fisier cu data invalida";
      }

      if(invalidPnlsAccount) {
        if(messageMandt) {
          messageMandt += "; ";
        }
        messageMandt += "Exista intrari in fisier fara cont valabil";
      }

      if(invalidPnlsAmount) {
        if(messageMandt) {
          messageMandt += "; ";
        }
        messageMandt += "Exista intrari in fisier cu suma invalida";
      }
    }

    if(messageMandt) {
      this.toastr.error(messageMandt, { closeButton: true, tapToDismiss: true });
    }
    // lasam sa salveze 
    this.pnlFile.extended.isValidForSaving = messageMandt.length === 0;
    this.pnlFile.extended.hasInvalidPnls = messageMandt.length > 0;
  }

}

export default angular.module('mmflowApp.pnl', [uiRouter])
  .config(routes)
  .component('pnl', {
    template: require('./pnl.html'),
    controller: PnlComponent,
    controllerAs: '$ctrl'
  })
  .name;
