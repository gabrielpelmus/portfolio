'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.pnl', {
      url: '/profit-loss',
      template: '<pnl></pnl>'
    });
}
