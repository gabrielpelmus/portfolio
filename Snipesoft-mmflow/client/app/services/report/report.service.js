'use strict';
const angular = require('angular');

/*@ngInject*/
export function reportService($q, $http) {
  var server = "api/"
  var getProfitCenters = function () {
    var def = $q.defer();
    $http.get(server + "reports/profit-centers").then(function (response) {
      def.resolve(response.data);
    }, function (response) {
      console.log(JSON.stringify(response));
      def.reject(response);
    });
    return def.promise;
  };

  var getProductionBySectors = function (oSelectionCriterias) {
    var data = {};
    data.fromDate = oSelectionCriterias.fromDate.date;
    // data.toDate = oSelectionCriterias.toDate.date;
    data.toDate = new Date(oSelectionCriterias.toDate.date.getFullYear(), oSelectionCriterias.toDate.date.getMonth(), oSelectionCriterias.toDate.date.getDate() + 1, 4, 49, 59);
    data.profitCenters = oSelectionCriterias.getProfitCentersSelected().map(function (pc) {
      return pc.name;
    });
    var def = $q.defer();
    $http.post(server + "reports/production-by-sector", data).then(function (response) {
      def.resolve(response.data);
    }, function (response) {
      console.log(JSON.stringify(response));
      def.reject(response);
    });
    return def.promise;
  };

  return {
    getProfitCenters: getProfitCenters,
    getProductionBySectors: getProductionBySectors
  }
}

export default angular.module('mmflowApp.services')
  .factory('Report', reportService)
  .name;
