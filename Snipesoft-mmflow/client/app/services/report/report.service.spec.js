'use strict';

describe('Service: report', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var reportFile;
  beforeEach(inject(function(_reportFile_) {
    reportFile = _reportFile_;
  }));

  it('should do something', function() {
    expect(!!reportFile).to.be.true;
  });
});
