'use strict';
const angular = require('angular');

/*@ngInject*/
export function sellVolumeService($resource) {
  return $resource('/api/sellVolumes/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('SellVolume', sellVolumeService)
  .name;
