'use strict';

describe('Service: sellVolume', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var sellVolume;
  beforeEach(inject(function(_sellVolume_) {
    sellVolume = _sellVolume_;
  }));

  it('should do something', function() {
    expect(!!sellVolume).to.be.true;
  });
});
