'use strict';
const angular = require('angular');

/*@ngInject*/
export function budgetLineAccountService($resource) {
  return $resource('/api/budgetLineAccount/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('BudgetLineAccount', budgetLineAccountService)
  .name;
