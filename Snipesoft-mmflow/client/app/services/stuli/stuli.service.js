'use strict';
const angular = require('angular');

/*@ngInject*/
export function stuliService($resource) {
  return $resource('/api/stulis/:id', { id: '@id' }, {
    'query': { method: 'GET', isArray: false },
    // update: {
    //   method: 'PUT'
    // }
  });
}

export default angular.module('mmflowApp.services')
  .factory('Stuli', stuliService)
  .name;
