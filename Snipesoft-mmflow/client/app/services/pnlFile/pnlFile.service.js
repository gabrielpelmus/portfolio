'use strict';
const angular = require('angular');

/*@ngInject*/
export function pnlFileService($resource) {
  return $resource('/api/pnl-files/:id', { id: '@id' }, {
    // update: {
    //   method: 'PUT'
    // }
  });
}

export default angular.module('mmflowApp.services')
  .factory('PnlFile', pnlFileService)
  .name;
