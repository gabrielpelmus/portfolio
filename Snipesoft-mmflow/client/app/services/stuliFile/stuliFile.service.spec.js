'use strict';

describe('Service: stuliFile', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var stuliFile;
  beforeEach(inject(function(_stuliFile_) {
    stuliFile = _stuliFile_;
  }));

  it('should do something', function() {
    expect(!!stuliFile).to.be.true;
  });
});
