'use strict';
const angular = require('angular');

/*@ngInject*/
export function stuliFileService($resource) {
  return $resource('/api/stuli-files/:id', { id: '@id' }, {
    // update: {
    //   method: 'PUT'
    // }
  });
}

export default angular.module('mmflowApp.services')
  .factory('StuliFile', stuliFileService)
  .name;
