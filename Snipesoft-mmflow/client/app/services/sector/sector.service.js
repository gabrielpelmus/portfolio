'use strict';
const angular = require('angular');

/*@ngInject*/
export function sectorService($resource) {
  return $resource('/api/sectors/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('Sector', sectorService)
  .name;
