'use strict';
const angular = require('angular');

/*@ngInject*/
export function achievementFileService($resource) {
  return $resource('/api/achievement-files/:id', { id: '@id' }, {
    // update: {
    //   method: 'PUT'
    // }
  });
}

export default angular.module('mmflowApp.services')
  .factory('AchievementFile', achievementFileService)
  .name;
