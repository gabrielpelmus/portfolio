'use strict';

describe('Service: pnl', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var pnlFile;
  beforeEach(inject(function(_pnlFile_) {
    pnlFile = _pnlFile_;
  }));

  it('should do something', function() {
    expect(!!pnlFile).to.be.true;
  });
});
