'use strict';
const angular = require('angular');

/*@ngInject*/
export function pnlService($resource) {
  return $resource('/api/pnls/:id', { id: '@id' }, {
    // 'query': { method: 'GET', isArray: false },
    update: {
      method: 'PUT'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('Pnl', pnlService)
  .name;
