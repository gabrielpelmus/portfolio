'use strict';

describe('Service: reportProfitLoss', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var reportProfitLoss;
  beforeEach(inject(function(_reportProfitLoss_) {
    reportProfitLoss = _reportProfitLoss_;
  }));

  it('should do something', function() {
    expect(!!reportProfitLoss).to.be.true;
  });
});
