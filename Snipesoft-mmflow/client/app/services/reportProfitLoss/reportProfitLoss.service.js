'use strict';
const angular = require('angular');

/*@ngInject*/
export function reportProfitLossService($resource) {
  return $resource('/api/reportProfitLosses/:id', { id: '@id' }, {
  });
}

export default angular.module('mmflowApp.services')
  .factory('ReportProfitLoss', reportProfitLossService)
  .name;
