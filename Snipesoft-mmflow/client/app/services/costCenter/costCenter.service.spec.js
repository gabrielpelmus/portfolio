'use strict';

describe('Service: costCenter', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var costCenter;
  beforeEach(inject(function(_costCenter_) {
    costCenter = _costCenter_;
  }));

  it('should do something', function() {
    expect(!!costCenter).to.be.true;
  });
});
