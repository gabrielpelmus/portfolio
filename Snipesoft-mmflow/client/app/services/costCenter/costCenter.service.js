'use strict';
const angular = require('angular');

/*@ngInject*/
export function costCenterService($resource) {
  return $resource('/api/costCenters/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('CostCenter', costCenterService)
  .name;
