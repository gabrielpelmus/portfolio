'use strict';
const angular = require('angular');

/*@ngInject*/
export function costGroupingService($resource) {
  return $resource('/api/costGrouping/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('CostGrouping', costGroupingService)
  .name;
