'use strict';

describe('Service: costGrouping', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var costGrouping;
  beforeEach(inject(function(_costGrouping_) {
    costGrouping = _costGrouping_;
  }));

  it('should do something', function() {
    expect(!!costGrouping).to.be.true;
  });
});
