'use strict';
const angular = require('angular');

export default angular.module('mmflowApp.services', []).name;

require('./sector/sector.service');
require('./materialGroup/materialGroup.service');
require('./budgetLine/budgetLine.service');
require('./budgetLineAccount/budgetLineAccount.service');
require('./sellVolume/sellVolume.service');
require('./costGrouping/costGrouping.service');
require('./costCenter/costCenter.service');
require('./stuliFile/stuliFile.service');
require('./achievementFile/achievementFile.service');
require('./pnlFile/pnlFile.service');
require('./stuli/stuli.service');
require('./report/report.service');
require('./reportProfitLoss/reportProfitLoss.service');

/* Assets */
require("font-awesome-webpack2");
