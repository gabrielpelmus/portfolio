'use strict';
const angular = require('angular');

/*@ngInject*/
export function budgetLineService($resource) {
  return $resource('/api/budgetLines/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('BudgetLine', budgetLineService)
  .name;
