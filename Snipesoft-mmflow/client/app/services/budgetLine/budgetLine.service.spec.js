'use strict';

describe('Service: budgetLine', function() {
  // load the service's module
  beforeEach(module('mmflowApp.services'));

  // instantiate service
  var budgetLine;
  beforeEach(inject(function(_budgetLine_) {
    budgetLine = _budgetLine_;
  }));

  it('should do something', function() {
    expect(!!budgetLine).to.be.true;
  });
});
