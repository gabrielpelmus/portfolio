'use strict';
const angular = require('angular');

/*@ngInject*/
export function materialGroupService($resource) {
  return $resource('/api/material-groups/:id', { id: '@id' }, {
    update: {
      method: 'PATCH'
    }
  });
}

export default angular.module('mmflowApp.services')
  .factory('MaterialGroup', materialGroupService)
  .name;
