'use strict';

export default class LoginController {

  /*@ngInject*/
  constructor(Auth, $state) {
    this.Auth = Auth;
    this.$state = $state;
    this.errors = {};
  }

  login(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.login({
        userName: this.user.userName,
        password: this.user.password
      })
        .then(() => {
          // Logged in, redirect to home
          this.$state.go('app.pnl');
        })
        .catch(err => {
          this.errors.login = err.message;
        });
    }
  }
}
