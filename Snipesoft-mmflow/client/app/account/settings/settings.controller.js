'use strict';

export default class SettingsController {

  /*@ngInject*/
  constructor(Auth) {
    this.Auth = Auth;
    this.user = Auth.getCurrentUserSync();
    this.errors = {};
  }

  changePassword(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.changePassword(this.user.oldPassword, this.user.newPassword)
        .then(() => {
          this.message = 'Parola modificata!';
        })
        .catch(() => {
          form.password.$setValidity('mongoose', false);
          this.errors.other = 'Parola incorecta';
          this.message = '';
        });
    }
  }
}
