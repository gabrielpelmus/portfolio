'use strict';
const angular = require('angular');
import routes from './sell-volume.routes';
const uiRouter = require('angular-ui-router');

export class SellVolumeComponent {
  /*@ngInject*/
  constructor(Modal, $rootScope, $uibModal, SellVolume, toastr) {
    var that = this;
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.SellVolume = SellVolume;
    this.toastr = toastr;

    this.loadSellVolumes();

    this.sortOptions = {
      key: "id",
      reverse: false,
      valueType: ""
    }

    this.sort = function(keyname, type) {
      that.sortOptions.key = keyname;
      that.sortOptions.reverse = !that.sortOptions.reverse;
      that.sortOptions.valueType = type;
    }

    this.customSorter = function(obj) {
      switch(that.sortOptions.valueType) {
        case "":
          return obj[that.sortOptions.key];
        case "N":
          return parseInt(obj[that.sortOptions.key]);
        case "D":
          //not yet used TODO
          return obj[that.sortOptions.key];
        default:
          return obj[that.sortOptions.key];
      }
    };
  }

  loadSellVolumes() {
    this.sellVolumes = [];
    var that = this;
    this.sellVolumesPromise = this.SellVolume.query({}, function(sellVolumes) {
      that.sellVolumes = sellVolumes;
    }).$promise;
  }

  openModal(action, sellVolume) {
    var that = this;

    this.$rootScope.extended.uibModalInstance = this.$uibModal.open({
      keyboard: false,
      animation: this.$rootScope.extended.animationsEnabled,
      template: require('./edit.html'),
      controller: "ModalController",
      windowClass: 'app-modal-window',
      backdrop: 'static',
      size: 'sm',
      resolve: {
        title: function() {
          switch(action) {
            case "add":
              return "Adaugati volum vanzari";
            case "edit":
              return "Editati volum vanzari";
          }
          return "Volum vanzari: unknown action";
        },
        showDelete: function() {
          switch(action) {
            case "add":
              return false;
            case "edit":
              return true;
          }
          return true;
        },
        item: function() {
          var item = {};
          switch(action) {
            case "add":
              item = new that.SellVolume();
              item.pickerYear = {
                dateOptions: angular.copy(that.$rootScope.extended.dateOptions),
                date: null,
                isOpen: false,
                open: function(oAccount) {
                  oAccount.pickerYear.isOpen = true;
                },
                changed: function(oAccount) {
                  item.year = item.pickerYear.date;
                }
              }
              break;
            case "edit":
              item = angular.copy(sellVolume);
              // debugger;
              item.pickerYear = {
                dateOptions: angular.copy(that.$rootScope.extended.dateOptions),
                date: new Date(item.year),
                isOpen: false,
                open: function(oItem) {
                  oItem.pickerYear.isOpen = true;
                },
                changed: function(oItem) {
                  // debugger;
                  oItem.year = oItem.pickerYear.date;
                }
              };
              break;
          }
          return item;
        }
      }
    });

    this.$rootScope.extended.uibModalInstance.result.then(function(result) {
      switch(result.action) {
        case "save":
          if(result.item.id) {
            result.item.$update(
              function(oResponse) {
                that.toastr.success('Volum vanzari editat cu success!', { timeOut: 2000 });
                var indexOf = that.sellVolumes.indexOf(sellVolume);
                if(indexOf > -1) {
                  that.sellVolumes[indexOf] = result.item;
                }
              },
              function(oResponse) {
                var message = "Volum vanzari nu a putut fi editat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          } else {
            result.item.$save(
              function(oResponse) {
                that.toastr.success('Volum vanzari a fost adaugat cu success!', { timeOut: 2000 });
                that.sellVolumes.push(angular.copy(result.item));
              },
              function(oResponse) {
                var message = "Volum vanzari nu a putut fi adaugat!";
                if(oResponse.data && oResponse.data.message) {
                  message = oResponse.data.message;
                }
                that.toastr.error(message, { closeButton: true, tapToDismiss: true });
              });
          }
          break;
        case "delete":
          result.item.$delete(
            function(oResponse) {
              that.toastr.success('Volum vanzari sters cu success!', { timeOut: 2000 });
              var indexOf = that.sellVolumes.indexOf(sellVolume);
              if(indexOf > -1) {
                that.sellVolumes.splice(indexOf, 1);
              }
            },
            function() {
              that.toastr.error('Volum vanzari nu a putut fi sters!', { closeButton: true, tapToDismiss: true });
            });
          break;
      }
    }, function(reason) {
      //alert(reason);
    });
  }

}

export default angular.module('mmflowApp.sellVolume', [uiRouter])
  .config(routes)
  .component('sellVolume', {
    template: require('./sell-volume.html'),
    controller: SellVolumeComponent,
    controllerAs: '$ctrl'
  })
  .name;
