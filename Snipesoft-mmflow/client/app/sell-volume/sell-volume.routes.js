'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('app.sell-volume', {
      url: '/volum-vanzari',
      template: '<sell-volume></sell-volume>',
      authenticate: 'admin'
    });
}
