'use strict';

describe('Component: SellVolumeComponent', function() {
  // load the controller's module
  beforeEach(module('mmflowApp.sell-volume'));

  var SellVolumeComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SellVolumeComponent = $componentController('sell-volume', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
