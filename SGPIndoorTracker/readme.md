**

# IndoorTrackerGP


## Senzori de miscare in Android

	1. Senzorul de gravitatie
	2. Acceleratia lineara
	3. Vectorul de rotatie
	4. Giroscopul

Senzorul de gravitatie, cel de acceleratie lineara si vectorul de rotatie se bazeaza pe datele oferite de giroscop. Daca dispozitivul nu este prevazut cu giroscop, datele de la senzori nu vor fi disponibile.

1. **Senzorul de gravitatie**
	- ofera un vector cu 3 dimensiuni ce reprezinta directia si magnitudinea gravitatiei
	- este folosit pentru a determina orientarea relativa in spatiu a dispozitivului
	- valorile sunt masurate in m/s^2
	
2. **Acceleratia lineara**
	- ofera un vector cu 3 dimensiuni ce reprezinta acceleratia pe axe, excuzand gravitatia
	- obtine date despre acceleratie fara influenta gravitatiei
	- are mereu un decalaj ce trebuie eliminat (pas de calibrare)
	- valorile sunt masurate in m/s^2
	
3. **Vectorul de rotatie**
	- reprezinta orientarea dispozitivului ca o combinatie de unghi si axa, in care dispozitivul s-a rotit cu un unghi A in jurul unei axe
	- cele trei elemente ale vectorului de rotatie sunt:
		x*sin(A/2),	y*sin(A/2),	z*sin(A/2)
	- magnitudinea vectorului de rotatie este sin(A/2)
	- directia vectorului de rotatie este directia axei de rotatie

4. **Giroscopul**
	- masoara rata de rotatie in rad/s in jurul unei axe a dispozitivului
	- valoarea rotatiei este pozitiva in sens invers acelor de ceasornic
	- valoarea de iesire este integrata peste timp pentru a calcula rotatia ce descrie schimbarea unghiurilor peste variatia de timp
	

## Functii Android

1. **Significant motion sensor**
	- declanseaza un eveniment de fiecare data cand este detectata miscare semnificativa apoi se dezactiveaza
	- miscarea semnificativa este acea miscare ce poate conduce la o schimbare in locatia utilizatorului
	
2. **Step Counter**
	- are latenta de pana la 10 secunde
	- are acuratete mai mare decat Step Detector

3. **Step Detector**
	- are latenta de pana la 2 secunde
	

## Sensor Fusion

	- combina ceea ce observi intr-un proces cu ceea ce cunosti despre proces
	- un proces este orice evolueaza in timp
	
**Modelul de miscare**

x<sub>k</sub> = f(x<sub>k-1</sub>, q<sub>k-1</sub>), unde x<sub>k-1</sub> - starea initiala iar q- incertitudinea
x<sub>k</sub> ~ p(x<sub>k</sub> | x<sub>k-1</sub>) - distributia probabilistica

In masuratorile primite de la senzori este prezent zgomot.

**Modelul de masurare**

	- cunostinte reprezentand modul in care functioneaza senzorii ca un functie de stare
- y<sub>k</sub> = h(x<sub>k</sub>, r<sub>k</sub>), r - incertitudinea prezenta in modelul senzorului
- y<sub>k</sub> ~ p(y<sub>k</sub> | x<sub>k</sub>) - masuratoarea pe care o observam este extrasa dintr-o distributie peste masuratori posibile, conditionate de stare.

Modelele de miscare si masurare sunt formulate folosind doua ipoteze:

 1. Starea x este Markoviana. Starea curenta  x<sub>k</sub> este dependenta doar de starea precedenta x<sub>k-1</sub>
 2. Masuratoarea y<sub>k</sub> este dependenta doar de starea x<sub>k</sub> si de nici o alta masuratoare precedenta.

Sensor fusion reprezinta astfel modul de combinare al modelelor incerte de miscare si masurare pentru a afla cat mai mult posibil despre starea curenta.

 - p(x<sub>k</sub> | y<sub>k-1</sub>) - ce putem spune despre starea curenta x<sub>k</sub> luand in considerare toate masuratorile pe care le-am primit pana acum?
 -  p(x<sub>k</sub> | y<sub>k-1</sub>)  - distributia posterioara peste x<sub>k</sub>
 -  p(x<sub>k</sub> | y<sub>k-1</sub>)  - estimarea optima a starii x<sub>k</sub> la timpul k
 
**Teorema lui Bayes si marginalizare**

p(x<sub>k</sub> | y<sub>1:k</sub>) = &int;p(x<sub>k</sub> | x<sub>k-1</sub>)p(x<sub>k</sub> | y<sub>1:k-1</sub>) dx<sub>k-1</sub>
p(x<sub>k</sub> | y<sub>1:k</sub>) = p(y<sub>k</sub> | x<sub>k</sub>)p(x<sub>k</sub> | y<sub>1:k-1</sub>)/p(y<sub>k</sub> | y<sub>1:k-1</sub>)

