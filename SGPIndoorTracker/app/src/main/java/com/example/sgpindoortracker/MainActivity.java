package com.example.sgpindoortracker;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Timer;

import static com.example.sgpindoortracker.CalculateFusedOrientationTask.TIME_CONSTANT;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager = null;

    private Timer fuseTimer = new Timer();

    private TextView mTextGyroOrientation;
    private TextView mTextAccMagOrientation;
    private TextView mTextAccelerometer;
    private TextView mTextVelocity;
    private TextView mTextPosition;
    private float[] startData = new float[3];
    private static final float NS2S = 1.0f / 1000000000.0f;
    private float timestamp;
    float[] last_values = null;
    float[] velocity = new float[3];
    float[] position = new float[3];
    float[] acceleration = new float[3];
    long last_timestamp = 0;
    KalmanFilter kf = new KalmanFilter();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextGyroOrientation = findViewById(R.id.label_gyro);
        mTextAccMagOrientation = findViewById(R.id.label_accmag);
        mTextAccelerometer = findViewById(R.id.label_accelerometer);
        mTextVelocity = findViewById(R.id.label_velocity);
        mTextPosition = findViewById(R.id.label_position);

        Sensors.setGyroOrientation(0.0f, 0.0f, 0.0f);
        Sensors.initGyroMatrix();

        // get sensorManager and initialise sensor listeners
        mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        initListeners();

        fuseTimer.scheduleAtFixedRate(new CalculateFusedOrientationTask(),
                1000, TIME_CONSTANT);
    }



    @Override
    protected void onStart(){
        super.onStart();


    }

    @Override
    protected void onStop(){
        super.onStop();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        switch(event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                // copy new accelerometer data into accel array
                // then calculate new orientation

                System.arraycopy(event.values, 0, Sensors.getAccelerometerVector(), 0, 3);
                Sensors.calculateAccMagOrientation();
                mTextAccMagOrientation.setText(getResources().getString(R.string.label_accmag, Sensors.getAccMagOrientation()[0], Sensors.getAccMagOrientation()[1], Sensors.getAccMagOrientation()[2]));
                mTextAccelerometer.setText(getResources().getString(R.string.label_accelerometer, Sensors.getAccelerometerVector()[0], Sensors.getAccelerometerVector()[1], Sensors.getAccelerometerVector()[2]));

                if(startData != Sensors.getAccelerometerVector()){
                    System.out.println("SGPApp - AccMagOrientation: " + Sensors.getAccMagOrientation()[0] + ", " + Sensors.getAccMagOrientation()[1] + ", " + Sensors.getAccMagOrientation()[2]);
                    System.out.println("SGPApp - Accelerometer: " + Sensors.getAccelerometerVector()[0] + ", " + Sensors.getAccelerometerVector()[1] + ", " + Sensors.getAccelerometerVector()[2]);
                    startData = Sensors.getAccelerometerVector();
                }

                break;

            case Sensor.TYPE_GYROSCOPE:
                // process gyro data
                Sensors.gyroFunction(event);
                mTextGyroOrientation.setText(getResources().getString(R.string.label_gyro, Sensors.getGyroOrientation()[0], Sensors.getGyroOrientation()[1], Sensors.getGyroOrientation()[2]));
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:
                // copy new magnetometer data into magnet array
                System.arraycopy(event.values, 0, Sensors.getMagnetVector(), 0, 3);
                break;

            case Sensor.TYPE_LINEAR_ACCELERATION:

                if(last_values != null){
                    float dt = (event.timestamp - last_timestamp) * NS2S;

                    acceleration[0]= event.values[0] - (float) 0.0188;
                    acceleration[1]= event.values[1] - (float) 0.00217;
                    acceleration[2]= event.values[2] + (float) 0.01857;

                    for(int index = 0; index < 3;++index){
                        velocity[index] += (acceleration[index] + last_values[index])/2 * dt;
                        position[index] += velocity[index] * dt;
                    }

                    kf.calculateTimestamp(event.timestamp);

                }
                else{
                    last_values = new float[3];
                    acceleration = new float[3];
                    velocity = new float[3];
                    position = new float[3];
                    velocity[0] = velocity[1] = velocity[2] = 0f;
                    position[0] = position[1] = position[2] = 0f;

                    kf.init();
                }
                System.arraycopy(acceleration, 0, last_values, 0, 3);
                last_timestamp = event.timestamp;

                mTextVelocity.setText(getResources().getString(R.string.label_velocity, velocity[0], velocity[1], velocity[2]));
                mTextPosition.setText(getResources().getString(R.string.label_position, position[0], position[1], position[2]));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void initListeners() {
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                SensorManager.SENSOR_DELAY_FASTEST);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_FASTEST);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_FASTEST);
    }
}
