package com.example.sgpindoortracker;

import com.example.matrix.IllegalDimensionException;
import com.example.matrix.Matrix;
import com.example.matrix.MatrixMathematics;
import com.example.matrix.NoSquareException;

class KalmanFilter {

    private long prev_time = 0; // time in miliseconds
    private Matrix x = new Matrix(4, 1); // start position
    private Matrix ground_truth = new Matrix(4, 1); //initial ground truth
    Matrix rmse = new Matrix(4, 1); //root mean squared error used to calculate the algorithm's performance against ground_truth
    private Matrix P = new Matrix(4, 4); // state covariance matrix
    private Matrix A = new Matrix(4, 4); // matrix used to implement the kinematic equations of distance, speed and time
    private Matrix H = new Matrix(2, 4);
    private Matrix I = MatrixMathematics.identity(4); // identity matrix
    private Matrix z = new Matrix(2, 1); // positional readings vector - stores x and y
    private Matrix R = new Matrix(2, 2); // measurement covariance matrix
    private Matrix Q = new Matrix(4, 4); // covariance matrix for acceleration

    private long delta_time = 0;
    private long delta_time_2 = 0;
    private long delta_time_3 = 0;
    private long delta_time_4 = 0;

    //    initial state of the system
    void init(){
        P.setValueAt(0,0,1.0);
        P.setValueAt(1,1,1.0);
        P.setValueAt(2,2,1000.0);
        P.setValueAt(3,3,1000.0);

        A.setValueAt(0,0,1.0);
        A.setValueAt(0,2,1.0);
        A.setValueAt(1,1,1.0);
        A.setValueAt(1,3,1.0);
        A.setValueAt(2,2,1.0);
        A.setValueAt(3,3,1.0);

        H.setValueAt(0,0,1.0);
        H.setValueAt(1,1,1.0);

        R.setValueAt(0,0,0.0255);
        R.setValueAt(1,1,0.0255);
    }

    void calculateTimestamp(long new_timestamp){

        long current_timestamp = new_timestamp/1000000;
        delta_time = current_timestamp - prev_time;

        prev_time = current_timestamp;

        delta_time_2 = delta_time*delta_time; // time variation squared
        delta_time_3 = delta_time_2 * delta_time;
        delta_time_4 = delta_time_3 * delta_time;

        updateMatrixAWithDeltaTime();
        updateMatrixQ();
        updateSensorReadings();
        collectGroundTruth();
        predict();
        update();
    }

    private void updateMatrixAWithDeltaTime(){
        A.setValueAt(0, 2, delta_time);
        A.setValueAt(1, 3, delta_time);
    }

    private void updateMatrixQ(){
        int noise_ax = 5;
        Q.setValueAt(0, 0, delta_time_4/4* noise_ax);
        Q.setValueAt(0, 2, delta_time_3/2* noise_ax);
        int noise_ay = 5;
        Q.setValueAt(1, 1, delta_time_4/4* noise_ay);
        Q.setValueAt(1, 3, delta_time_3/2* noise_ay);
        Q.setValueAt(2, 0, delta_time_3/2* noise_ax);
        Q.setValueAt(2, 2, delta_time_2* noise_ax);
        Q.setValueAt(3, 1, delta_time_3/2* noise_ay);
        Q.setValueAt(3, 3, delta_time_2* noise_ay);
    }

    private void updateSensorReadings(){
        z.setValueAt(0, 0, Sensors.getAccelerometerVector()[0]);
        z.setValueAt(1, 0, Sensors.getAccelerometerVector()[1]);
    }

    private void collectGroundTruth(){
        ground_truth.setValueAt(0, 0, 0.0);
        ground_truth.setValueAt(1, 0, 0.0);
        ground_truth.setValueAt(2, 0, 0.0);
        ground_truth.setValueAt(3, 0, 0.0);
    }

    private void predict(){
        try {
            // Predict
            x = MatrixMathematics.multiply(A, x);
            Matrix At = MatrixMathematics.transpose(A);
            P = MatrixMathematics.add(MatrixMathematics.multiply(A, MatrixMathematics.multiply(P, At)), Q);
        } catch (IllegalDimensionException e) {
            e.printStackTrace();
        }
    }

    private void update() {
        try {
            // Measurement update
            Matrix y = MatrixMathematics.subtract(z, MatrixMathematics.multiply(H, x));
            Matrix Ht = MatrixMathematics.transpose(H);
            Matrix s = MatrixMathematics.add(MatrixMathematics.multiply(H, MatrixMathematics.multiply(P, Ht)), R);
            Matrix K = MatrixMathematics.multiply(P, Ht);
            Matrix Si = MatrixMathematics.inverse(s);
            K = MatrixMathematics.multiply(K, Si);

            // New state
            x = MatrixMathematics.add(x, MatrixMathematics.multiply(K, y));
            P = MatrixMathematics.multiply(MatrixMathematics.subtract(I, MatrixMathematics.multiply(K, H)), P);
        } catch (IllegalDimensionException e) {
            e.printStackTrace();
        } catch (NoSquareException e) {
            e.printStackTrace();
        }

    }
}
