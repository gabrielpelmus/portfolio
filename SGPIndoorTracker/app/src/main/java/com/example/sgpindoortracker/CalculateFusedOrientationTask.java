package com.example.sgpindoortracker;

import java.util.TimerTask;

public class CalculateFusedOrientationTask extends TimerTask {

    // final orientation angles from sensor fusion
    private float[] fusedOrientation = new float[3];

    public static final int TIME_CONSTANT = 30;
    public static final float FILTER_COEFFICIENT = 0.98f;

    public void run() {
        float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;
        fusedOrientation[0] =
                FILTER_COEFFICIENT * Sensors.getGyroOrientation()[0]
                        + oneMinusCoeff * Sensors.getAccMagOrientation()[0];

        fusedOrientation[1] =
                FILTER_COEFFICIENT * Sensors.getGyroOrientation()[1]
                        + oneMinusCoeff * Sensors.getAccMagOrientation()[1];

        fusedOrientation[2] =
                FILTER_COEFFICIENT * Sensors.getGyroOrientation()[2]
                        + oneMinusCoeff * Sensors.getAccMagOrientation()[2];

        // overwrite gyro matrix and orientation with fused orientation
        // to compensate gyro drift
        Sensors.setGyroMatrix(Sensors.getRotationMatrixFromOrientation(fusedOrientation));
        System.arraycopy(fusedOrientation, 0, Sensors.getGyroOrientation(), 0, 3);
    }
}
