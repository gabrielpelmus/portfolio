(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.filter('getStatusName', function() {
        return function(statusCode, statuses) {
            var item = $.Enumerable.From(statuses).Where('status => status.code == ' + statusCode).FirstOrDefault();
            return typeof item == "object" ? item.name : "NN";
        };
    });

    fps.filter('filterMatchingFields', function() {
        return function(inputArray, filterValue) {
            if (!filterValue || filterValue == "") {
                return inputArray;
            }
            var outputArray = [],
                fieldsToMatch = [];
            for (var i = 2; i <= arguments.length - 1; i++) {
                fieldsToMatch.push(arguments[i]);
            }

            angular.forEach(inputArray, function(item) {
                for (var i = 0; i <= fieldsToMatch.length - 1; i++) {
                    if (snpHelper.getPropByString(item, fieldsToMatch[i]) != null && snpHelper.getPropByString(item, fieldsToMatch[i]).toString().toLowerCase().indexOf(filterValue.toString().toLowerCase()) != -1) {
                        outputArray.push(item);
                        break;
                    }
                };
            });
            return outputArray;
        };
    });

    fps.filter('dateTimeFormat', function() {
        return function(inputDate, format) {
            if (!format) format = "D MMMM YYYY"; //"dddd, D MMMM YYYY"
            return new moment(inputDate).local().locale("ro").format(format);
        };
    });

    fps.filter('dateTimeFormatLite', function() {
        return function(inputDate, format) {
            if (!format) format = "DD MMM YYYY";
            return new moment(inputDate).local().locale("ro").format(format);
        };
    });

}())
