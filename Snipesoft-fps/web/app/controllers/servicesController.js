(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ServicesController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function($scope, $state, dataProvider, $rootScope, $modal) {
        dataProvider.loadServiceGroups().then(function(data) {
            $scope.serviceGroups = data;
        }, function() {

        });

        $scope.sortKey = "name";
        $scope.reverse = false;

        $scope.itemsPerPage = 17;

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openInfo = function() {
            alert('Functionalitate in lucru...');
        }

        $scope.openModal = function(action, service) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/settings/services/edit.html',
                controller: "ModalController",
                backdrop: 'static',
                resolve: {
                    title: function() {
                        switch (action) {
                            case "add":
                                return "Adaugati un serviciu";
                            case "edit":
                                return "Editati serviciul";
                        }
                        return "Etapa: unknown action";
                    },
                    showDelete: function() {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function() {
                        var item = {};
                        item.extended = {};

                        switch (action) {
                            case "add":


                                break;
                            case "edit":
                                item.id = service.id;
                                item.name = service.name;
                                item.description = service.description;
                                item.details = service.details;

                                break;
                        }

                        return item;
                    }
                }
            });

            $rootScope.extended.modalInstance.result.then(function(result) {
                switch (result.action) {
                    case "save":
                        dataProvider.saveServiceGroup(result.item).then(function(response) {
                            if (!(result.item.id)) {
                                result.item.id = response.data[1][0].insertServiceId;
                                $scope.serviceGroups.push(result.item);
                                //actiune de add al unui serviciu

                                    $rootScope.extended.ngToast.create({
                                        content: "Serviciul a fost adaugat cu succes!"
                                    });

                                // }, function() {

                                //     $rootScope.extended.ngToast.create({
                                //         content: "Serviciul adaugat nu a putut fi citit"
                                //     });
                                // });
                            } else {
                                //actiune de edit al unui serviciu
                                var service = $.Enumerable.From($scope.serviceGroups).Where('srv => srv.id == ' + result.item.id).FirstOrDefault();
                                if (service != null) {
                                    service.name = result.item.name;
                                    service.description = result.item.description;
                                    service.details = result.item.details;
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Serviciul a fost editat cu succes!"
                                });
                            }
                        }, function() {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Serviciul nu a putut fi adaugat!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Serviciul nu a putut fi editat!"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteService(result.item).then(function(deleted) {
                                if (deleted) {
                                    //TODO optimizare cu WhereTo, indexOf si splice apoi
                                    for (var i = 0; i < $scope.serviceGroups.length; i++) {
                                        if ($scope.serviceGroups[i].id == result.item.id) {
                                            $scope.serviceGroups.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Serviciul a fost sters cu succes!"
                                });
                            },
                            function() {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Serviciul nu a putut fi sters!"
                                });
                            });
                        break;
                }
            }, function(reason) {
                //alert(reason);
            });
        }


    }]);
}())
