(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('InvoicesController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", "$rootScope",
        function($scope, $state, dataProvider, $rootScope, $modal) {

            $scope.itemsPerPage = 17;

            $scope.setFullValue = function setFullValue(fullVal) {
                console.log(fullVal);
            }

            $scope.daysRemaining = function daysRemaining(createDT, dueDT) {
                var dateStart = new Date(createDT);
                var dateEnd = new Date(dueDT);
                var timeDiff = Math.abs(dateEnd.getTime() - dateStart.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            }

            $scope.setRowColor = function(statusId) {
                if (statusId == 15) {
                    return {
                        color: "grey"
                    }
                }
            }

            $scope.clearSearch = function() {
                $scope.searchInvoices.text = '';
            }

            dataProvider.loadInvoiceBundle().then(function(data) {
                if (data[0].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista furnizori."
                    });
                } else {
                    $scope.suppliers = data[0];
                }

                if (data[1].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista tipuri de plata."
                    });
                } else {
                    $scope.paymentTypes = data[1];
                }

                if (data[2].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista cladiri."
                    });
                } else {
                    $scope.buildings = data[2];
                }

                if (data[3].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista statusuri facturi."
                    });
                } else {
                    $scope.invoiceStatuses = data[3];
                }

                if (data[4].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista tipuri facturi."
                    });
                } else {
                    $scope.invoiceTypes = data[4];
                }

                if (data[5].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire facturi."
                    });
                } else {
                    $scope.invoices = data[5];
                }

                $scope.searchInvoices = ''; // set the default search/filter term
            });


            $scope.invoicesToPay = new Array();
            $scope.paymentsToBeDeleted = new Array();

            $scope.sendInvoiceForPayment = function(invoiceToPay, addRemoveFlag) {
                if (addRemoveFlag == true) {
                    if ($scope.invoicesToPay.length === 0) {
                        $scope.invoicesToPay.push(invoiceToPay);
                    } else {
                        var sameSupplierCheck = $.Enumerable.From($scope.invoicesToPay).Where('inv => inv.supplierId == ' + invoiceToPay.supplierId).FirstOrDefault();
                        if (sameSupplierCheck) {
                            $scope.invoicesToPay.push(invoiceToPay);
                        } else {
                            $rootScope.extended.ngToast.create({
                                className: "danger",
                                dismissButton: true,
                                content: "Nu poti selecta facturi cu furnizori diferiti pentru o plata!"
                            });
                            invoiceToPay.extended.addRemoveFlag = false;
                        }
                    }

                } else {
                    var index = $scope.invoicesToPay.indexOf(invoiceToPay);
                    $scope.invoicesToPay.splice(index, 1);
                }
            }

            $scope.sortKey = "name";
            $scope.reverse = false;

            $scope.sort = function(keyname) {
                $scope.sortKey = keyname;
                $scope.reverse = !$scope.reverse;
            }

            $scope.close = function(result) {
                dialog.close(result);
            };

            $scope.supplierClick = function(supplierName) {
                $scope.searchInvoices = {
                    text: supplierName
                };
            }

            $scope.openModal = function(action, invoice) {
                $rootScope.extended.modalInstance = $modal.open({
                    keyboard: false,
                    size: "lg",
                    animation: $scope.animationsEnabled,
                    templateUrl: 'app/views/invoices/edit.html',
                    controller: "ModalController",
                    backdrop: 'static',
                    resolve: {
                        title: function() {
                            switch (action) {
                                case "add":
                                    return "Inregistrati o factura";
                                case "edit":
                                    return "Editati factura";
                            }
                            return "Etapa: unknown action";
                        },
                        showDelete: function() {
                            switch (action) {
                                case "add":
                                    return false;
                                case "edit":
                                    return true;
                            }
                            return true;
                        },
                        item: function() {
                            var item = {};

                            switch (action) {
                                case "add":
                                    item.extended = {};
                                    item.id = "";
                                    item.supplierId = "";
                                    item.serviceGroupId = "NULL";
                                    item.buildingId = "";
                                    item.externalNumber = "";
                                    item.amount = "";
                                    item.currency = "";
                                    item.paymentTypeId = 1;
                                    item.createDT = new Date();
                                    item.dueDT = new Date();
                                    item.dueDT.setDate(item.dueDT.getDate() + 1);
                                    item.paymentDT = "";
                                    item.details = "";
                                    item.initialStatusId = 12;
                                    item.statusId = 12;
                                    item.statusName = "Noua";
                                    item.invoiceTypeId = 1;
                                    item.hasIssue = "";
                                    item.issueDescription = "";

                                    item.extended.invoiceType = $.Enumerable.From($scope.invoiceTypes).Where('invType => invType.id == ' + item.invoiceTypeId).FirstOrDefault();
                                    item.extended.invoiceStatus = $.Enumerable.From($scope.invoiceStatuses).Where('sts => sts.id == ' + item.statusId).FirstOrDefault();

                                    item.extended.suppliers = $scope.suppliers;
                                    item.extended.paymentTypes = $scope.paymentTypes;
                                    item.extended.buildings = $scope.buildings;
                                    item.extended.invoiceStatuses = $scope.invoiceStatuses;
                                    item.extended.invoiceTypes = $scope.invoiceTypes;

                                    break;
                                case "edit":

                                    var initialStatusId = invoice.statusId;

                                    // daca statusul initial este Storno atunci debifarea Storno va intoarce factura
                                    // in statusul Noua.
                                    if (initialStatusId == 15) {
                                        initialStatusId = 12;
                                    }
                                    item = angular.copy(invoice);
                                    item.extended = {};
                                    item.initialStatusId = initialStatusId;

                                    item.extended.supplier = $.Enumerable.From($scope.suppliers).Where('supl => supl.id == ' + invoice.supplierId).FirstOrDefault();
                                    item.extended.supplierServiceGroup = {};
                                    if (item.extended.supplier) {
                                        item.extended.supplierServiceGroup = $.Enumerable.From(item.extended.supplier.services).Where('srv => srv.id == ' + invoice.serviceGroupId).FirstOrDefault();
                                    } else {
                                        item.extended.supplierServiceGroup.id = null;
                                    }

                                    item.extended.building = $.Enumerable.From($scope.buildings).Where('bld => bld.id == ' + invoice.buildingId).FirstOrDefault();
                                    item.extended.invoiceType = $.Enumerable.From($scope.invoiceTypes).Where('invType => invType.id == ' + invoice.invoiceTypeId).FirstOrDefault();
                                    item.extended.invoiceStatus = $.Enumerable.From($scope.invoiceStatuses).Where('sts => sts.id == ' + invoice.statusId).FirstOrDefault();

                                    item.extended.suppliers = $scope.suppliers;
                                    item.extended.buildings = $scope.buildings;
                                    item.extended.invoiceStatuses = $scope.invoiceStatuses;
                                    item.extended.invoiceTypes = $scope.invoiceTypes;

                                    break;
                            }

                            // if (invoice) {
                            //     item.extended.supplier = $scope.suppliers.filter(function (obj) {
                            //         return obj.id == invoice.supplierId;
                            //     });
                            // }

                            item.extended.dateOptions = {
                                showWeeks: true
                            };

                            item.extended.createDTIOpen = false;
                            item.extended.dueDTIsOpen = false;

                            item.extended.openCalendar = function(e, date) {
                                switch (date) {
                                    case "createDT":
                                        item.extended.createDTIsOpen = true;
                                        break;
                                    case "dueDT":
                                        item.extended.dueDTIsOpen = true;
                                        break;
                                }
                            };

                            item.filesToBeDeleted = new Array();

                            item.extended.removeFile = function(file) {
                                var index = item.files.indexOf(file);
                                item.files.splice(index, 1);
                                item.filesToBeDeleted.push(file);
                            };

                            item.extended.createDTChanged = function(item) {
                                item.dueDT = new Date(item.createDT.toString());
                                item.dueDT.setDate(item.dueDT.getDate() + 1);
                            };

                            return item;
                        }
                    }
                });

                $rootScope.extended.modalInstance.result.then(function(result) {
                    switch (result.action) {
                        case "save":
                            angular.forEach($scope.invoicesToPay, function(itp) {
                                itp.extended.addRemoveFlag = false;
                            });
                            $scope.invoicesToPay = [];

                            dataProvider.saveInvoice(result.item).then(function(insertId) {
                                //actiune de add al unei facturi
                                if (!(result.item.id)) {
                                    dataProvider.loadInvoiceById(insertId).then(function(data) {
                                        data.extended = {};
                                        $scope.invoices.push(data);
                                    }, function() {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Factura nu a putut fi incarcata!"
                                        });
                                    });
                                    $rootScope.extended.ngToast.create("Factura a fost introdusa cu success!");
                                } else {
                                    //actiune de edit al unei facturi
                                    dataProvider.loadInvoiceById(result.item.id).then(function(data) {
                                        data.extended = {};
                                        var invoice = $.Enumerable.From($scope.invoices).Where('inv => inv.id == ' + result.item.id).FirstOrDefault();
                                        var index = $scope.invoices.indexOf(invoice);

                                        if (index > -1) {
                                            $scope.invoices[index] = data;
                                            $rootScope.extended.ngToast.create("Factura a fost editata cu succes");
                                        }
                                    }, function() {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Factura nu a putut fi incarcata!"
                                        });
                                    });
                                }
                            }, function() {
                                if (result.item.id == "") {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Factura nu a putut fi adaugata!"
                                    });
                                } else {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Factura nu a putut fi editata!"
                                    });
                                }
                            });

                            break;
                        case "delete":
                            dataProvider.deleteInvoice(result.item).then(function(deleted) {
                                    if (deleted) {
                                        for (var i = 0; i < $scope.invoices.length; i++) {
                                            if ($scope.invoices[i].id == result.item.id) {
                                                $scope.invoices.splice(i, 1);
                                                break;
                                            }
                                        }
                                    }
                                    $rootScope.extended.ngToast.create("Factura a fost stearsa cu succes!");
                                },
                                function() {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Factura nu a putut fi stearsa!"
                                    });
                                });
                            break;
                    }
                }, function(reason) {
                    //TODO
                    //alert(reason);
                });
            }

            $scope.openPayModal = function(action, invoicesToPay) {

                    $rootScope.extended.modalInstance = $modal.open({
                        keyboard: false,
                        size: "lg",
                        animation: $scope.animationsEnabled,
                        templateUrl: 'app/views/invoices/editPayment.html',
                        controller: "ModalController",
                        backdrop: 'static',
                        resolve: {
                            title: function() {

                                var title = 'Adaugati plata';
                                if ($scope.invoicesToPay.length == 1) {
                                    title = title + '';
                                } else {
                                    title = title + ' multipla';
                                }

                                switch (action) {
                                    case "add":
                                        return title;
                                }
                                return "Etapa: unknown action";
                            },
                            showDelete: function() {
                                switch (action) {
                                    case "add":
                                        return false;
                                }
                                return true;
                            },

                            item: function() {
                                var item = {
                                    extended: {}
                                };
                                item.extended.validCRT = 0;

                                item.extended.dateOptions = {
                                    showWeeks: true,
                                    startingDay: 1
                                };
                                item.extended.dateOptions = {
                                    showWeeks: true
                                };
                                item.extended.operationDT = {
                                    value: new Date(),
                                    isOpen: false
                                };

                                item.extended.totalRA = function(invoices) {
                                    var total = 0;
                                    angular.forEach(invoices, function(itp) {
                                        total += parseFloat(itp.partialAmount);
                                    });

                                    return total.toFixed(2);
                                };

                                switch (action) {
                                    case "add":
                                        item.id = "";
                                        item.invoicesToPay = new Array();
                                        item.operationDT = item.extended.operationDT.value;
                                        item.invoicesToPay = invoicesToPay;
                                        item.invoicesToPayCounter = $scope.invoicesToPay.length;

                                        break;
                                }

                                item.extended.openCalendar = function(e, date) {
                                    item.extended.isCalendarOpen = true;
                                };

                                item.extended.validateThisCRT = function(crt) {
                                    dataProvider.validateCRT(crt).then(function(tof) {

                                        if (tof[0]) {
                                            item.extended.validCRT = 0;
                                            item.message = "CRT-ul introdus (" + crt + ") exista deja in baza de date!"
                                        } else {
                                            item.extended.validCRT = 1;
                                            item.message = "";
                                        }
                                    });
                                };

                                return item;
                            }
                        }
                    });

                    $rootScope.extended.modalInstance.result.then(function(result) {

                        switch (result.action) {
                            case "savePayment":
                                dataProvider.savePayments(result.item).then(function(paymentId) {
                                    if (!(result.item.id)) {
                                        angular.forEach(result.item.invoicesToPay, function(invoiceToBeRefreshed) {
                                            dataProvider.loadInvoiceById(invoiceToBeRefreshed.id).then(function(data) {
                                                data.extended = {};
                                                var invoice = $.Enumerable.From($scope.invoices).Where('inv => inv.id == ' + invoiceToBeRefreshed.id).FirstOrDefault();
                                                var index = $scope.invoices.indexOf(invoice);

                                                if (index > -1) {
                                                    $scope.invoices[index] = data;
                                                    //$rootScope.extended.ngToast.create("Factura a fost updatata cu succes");
                                                }
                                            }, function() {
                                                $rootScope.extended.ngToast.create({
                                                    className: "danger",
                                                    dismissButton: true,
                                                    content: "Factura nu a putut fi incarcata!"
                                                });
                                            });
                                        });
                                        $scope.invoicesToPay = [];
                                        $rootScope.extended.ngToast.create("Plata a fost introdusa cu success!");
                                    }
                                }, function(response) {
                                    if (result.item.id == "") {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Plata nu a putut fi adaugata!"
                                        });
                                    }
                                    if (response && response.data && response.data.dbMessage && response.data.dbMessage.code == "ER_DUP_ENTRY") {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Numarul CRT exista deja"
                                        });
                                    }
                                });
                                break;
                        }
                    }, function(reason) {
                        //TODO
                        //alert(reason);
                    });
                } //end openPayModal

            $scope.openPayHistoryModal = function(action, invoiceId, invoiceExternalNumber, payments) {

                    $rootScope.extended.modalInstance = $modal.open({
                        keyboard: false,
                        size: "lg",
                        animation: $scope.animationsEnabled,
                        templateUrl: 'app/views/invoices/editPaymentHistory.html',
                        controller: "ModalController",
                        backdrop: 'static',
                        resolve: {
                            title: function() {

                                var title = 'Vizualizare istoric plati pe factura nr. ' + invoiceExternalNumber;

                                switch (action) {
                                    case "view":
                                        return title;
                                }
                                return "Vizualizare istoric: unknown action";
                            },
                            showDelete: function() {
                                switch (action) {
                                    case "view":
                                        return true;
                                }
                                return true;
                            },
                            item: function() {

                                var item = {
                                    extended: {}
                                };

                                switch (action) {
                                    case "view":

                                        item.invoiceId = invoiceId;
                                        item.payments = payments;

                                        item.paymentsToBeDeleted = new Array();
                                        item.paymentsToBeDeleted = $scope.paymentsToBeDeleted;

                                        break;
                                }


                                item.extended.removeThisPayment = function(payment) {

                                    var pId = payment.paymentId;
                                    var found = item.paymentsToBeDeleted.some(function(el) {
                                        return el.paymentId === pId;
                                    });
                                    if (!found && payment.selected) {
                                        item.paymentsToBeDeleted.push(payment);
                                    } else if (found && !payment.selected) {

                                        var index = item.paymentsToBeDeleted.indexOf(payment);
                                        item.paymentsToBeDeleted.splice(index, 1);
                                    }

                                    //console.log(item.paymentsToBeDeleted);                                

                                };

                                return item;
                            }
                        }
                    });

                    $rootScope.extended.modalInstance.result.then(function(result) {

                        switch (result.action) {
                            case "savePaymentHistory":
                                dataProvider.deletePayments(result.item).then(function(paymentId) {

                                        // verific facturile existente care au acelasi CRT cu cel folosit la stergere
                                        // si le refreshuiesc doar pe astea.

                                        var invoicesRefreshList = new Array();

                                        $scope.paymentsToBeDeleted.forEach(function(payment) {
                                            var pCRT = payment.paymentCRT;

                                            $scope.invoices.forEach(function(invoice) {
                                                var invPays = invoice.payments;

                                                if (invPays) {
                                                    invPays.forEach(function(ip) {
                                                        if (ip.paymentCRT == pCRT) {
                                                            invoicesRefreshList.push(invoice);
                                                        }
                                                    })
                                                }
                                            })
                                        })

                                        angular.forEach(invoicesRefreshList, function(invoiceToBeRefreshed) {

                                            dataProvider.loadInvoiceById(invoiceToBeRefreshed.id).then(function(data) {
                                                data.extended = {};
                                                var invoice = $.Enumerable.From($scope.invoices).Where('inv => inv.id == ' + invoiceToBeRefreshed.id).FirstOrDefault();
                                                var index = $scope.invoices.indexOf(invoice);

                                                if (index > -1) {
                                                    $scope.invoices[index] = data;
                                                    //$rootScope.extended.ngToast.create("Factura a fost updatata cu succes");
                                                }
                                            }, function() {
                                                $rootScope.extended.ngToast.create({
                                                    className: "danger",
                                                    dismissButton: true,
                                                    content: "Factura nu a putut fi reimprospatata!"
                                                });
                                            });
                                        });

                                        $scope.paymentsToBeDeleted = [];
                                        invoicesRefreshList = [];

                                        $rootScope.extended.ngToast.create("Modificare plati efectuata cu succes!");

                                    },
                                    function() {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Eroare la modificare plati!"
                                        });
                                    });

                                break;
                        }
                    }, function(reason) {
                        //TODO
                        //alert(reason);
                    });

                } //end openPayHistoryModal
        }
    ]);
}())
