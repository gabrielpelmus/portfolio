(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('AppController', ["$scope", "$state", "DataProvider", "$rootScope", "ngToast", "jwtHelper",
        function($scope, $state, dataProvider, $rootScope, ngToast, jwtHelper) {

            $rootScope.extended.loggedUser = jwtHelper.getUserData(localStorage.getItem("jwtToken"));

            $scope.animationsEnabled = true;

            $rootScope.extended.ngToast = ngToast;

            $scope.navigateToState = function(state) {
                $state.go(state);

            }

            $scope.setActiveClass = function(value) {
                if ($state.current.name.indexOf(value) == 0)
                    return "active";
                else return "";
            }

            $scope.logout = function() {
                localStorage.removeItem("jwtToken");
                $state.go("login");
            }

        }
    ]);
}())


/*.config(['ngToastProvider', function(ngToast) {
    ngToast.configure({
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
      maxNumber: 3*/
