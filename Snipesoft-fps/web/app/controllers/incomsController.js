(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('IncomsController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function($scope, $state, dataProvider, $rootScope, $modal) {

        $scope.itemsPerPage = 17;

        $scope.clearSearch = function() {
                $scope.searchIncoms.text = '';
            }
        
        dataProvider.loadBuildings().then(function(buildings) {
            $rootScope.extended.customizing = {
                buildings: buildings
            };

            dataProvider.loadClients().then(function(data) {
                $scope.clients = data;
                $scope.searchIncoms = ''; // set the default search/filter term

                //fill the list below with data used for typeahead (each client with one case file)
                $scope.clientCaseFileList = [];
                angular.forEach($scope.clients, function(client) {
                    angular.forEach(client.cases, function(caseFile) {
                        var c = angular.copy(client);
                        delete c.cases;
                        delete c.files;
                        c.caseFile = caseFile;
                        $scope.clientCaseFileList.push(c);
                    });
                });

                dataProvider.loadIncoms().then(function(data) {
                    angular.forEach(data, function(income) {
                        income.extended = {};
                        income.extended.client = $.Enumerable.From($scope.clientCaseFileList).Where('c => c.caseFile.clientCaseId == ' + income.clientCaseId).FirstOrDefault();
                    });

                    $scope.incoms = data;
                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare la citirea listei de intrari!"
                    });
                });
            }, function() {
                $rootScope.extended.ngToast.create({
                    className: "danger",
                    dismissButton: true,
                    content: "Eroare la citirea clientilor!"
                });
            });
        }, function() {
            $rootScope.extended.ngToast.create({
                className: "danger",
                dismissButton: true,
                content: "Eroare la citirea blocurilor!"
            });
        });


        $scope.sortKey = "client.name";
        $scope.reverse = false;

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openModal = function(action, income) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/incoms/edit.html',
                controller: "ModalController",
                backdrop: 'static',
                size: "lg",
                resolve: {
                    title: function() {
                        switch (action) {
                            case "add":
                                return "Adaugati o intrare de la client";
                            case "edit":
                                return "Editati intrarea de la client";
                        }
                        return "Etapa: unknown action";
                    },
                    showDelete: function() {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function() {
                        var item = {};
                        item.extended = {};
                        item.extended.validCRT = 0;

                        switch (action) {
                            case "add":
                                item.id = "";
                                item.crt = null;
                                item.clientId = null;
                                item.bankAccountId = "1";
                                item.file = "";
                                item.amount = "";
                                item.currency = "LEI";
                                item.type = "IN";
                                item.isTransfer = "0";
                                item.transferToBankAccountId = null;
                                item.description = "";
                                item.linkNumber = "";
                                item.operationDT = "";
                                item.operatorId = "10";
                                item.extended.client = null;
                                item.extended.file = null

                                break;
                            case "edit":
                                item = angular.copy(income);
                                //item.extended.client = $.Enumerable.From($scope.clientCaseFileList).Where('c => c.caseFile.clientCaseId == ' + income.clientCaseId).FirstOrDefault();
                                item.extended.client.caseFile.apartment.extended = {};
                                item.extended.client.caseFile.apartment.extended.building = $.Enumerable.From($rootScope.extended.customizing.buildings).Where('b => b.id == ' + item.extended.client.caseFile.apartment.buildingId).FirstOrDefault();
                                break;
                        }

                        item.extended.dateOptions = {
                            showWeeks: true,
                            startingDay: 1
                        };

                        item.filesToBeDeleted = [];

                        item.extended.removeFile = function(file) {
                            var index = item.files.indexOf(file);
                            item.files.splice(index, 1);
                            item.filesToBeDeleted.push(file);
                        };

                        item.extended.getAllClientsForTypeahead = function() {
                            return $scope.clientCaseFileList;
                        }

                        item.extended.clientSelected = function($item, item, form) {
                            item.extended.client = $item;
                            item.extended.client.caseFile.apartment.extended = {};
                            item.extended.client.caseFile.apartment.extended.building = $.Enumerable.From($rootScope.extended.customizing.buildings).Where('b => b.id == ' + item.extended.client.caseFile.apartment.buildingId).FirstOrDefault();

                            form.clientAndCase.$setValidity("snpIsObject", true);
                        };

                        item.extended.openCalendar = function(e, date) {
                            item.extended.isDateOpen = true;
                        };

                        item.extended.validateThisCRT = function(crt) {
                            dataProvider.validateCRT(crt).then(function (tof) {
                                
                                if(tof[0]) {
                                    item.extended.validCRT = 0;
                                    item.message = "CRT-ul introdus (" + crt + ") exista deja in baza de date!"
                                } else {
                                    item.extended.validCRT = 1;
                                    item.message = "";
                                }
                            });
                        };

                        return item;
                    }
                }
            });
            $rootScope.extended.modalInstance.result.then(function(result) {
                result.item.newFiles = $.Enumerable.From(result.item.extended.newFiles).Where('newFile => newFile != null').ToArray();
                result.item.clientCaseId = result.item.extended.client.caseFile.clientCaseId;
                switch (result.action) {
                    case "save":
                        dataProvider.saveIncoms(result.item).then(function(insertId) {
                            //actiune de add al unei intrari
                            if (!(result.item.id)) {
                                dataProvider.loadIncomeById(insertId).then(function(data) {
                                    data.extended = {};
                                    data.extended.client = $.Enumerable.From($scope.clientCaseFileList).Where('c => c.caseFile.clientCaseId == ' + data.clientCaseId).FirstOrDefault();
                                    $scope.incoms.push(data);
                                }, function() {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Intrarea nu a putut fi incarcata!"
                                    });
                                });
                                $rootScope.extended.ngToast.create("Intrarea a fost introdusa cu success!");
                            } else {
                                //actiune de edit al unei intrari
                                dataProvider.loadIncomeById(result.item.id).then(function(data) {
                                    data.extended = {};
                                    data.extended.client = $.Enumerable.From($scope.clientCaseFileList).Where('c => c.caseFile.clientCaseId == ' + data.clientCaseId).FirstOrDefault();

                                    var income = $.Enumerable.From($scope.incoms).Where('ins => ins.id == ' + result.item.id).FirstOrDefault();
                                    var index = $scope.incoms.indexOf(income);

                                    if (index > -1) {
                                        $scope.incoms[index] = data;
                                        $rootScope.extended.ngToast.create("Intrarea a fost editata cu succes");
                                    }
                                }, function() {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Intrarea nu a putut fi incarcata!"
                                    });
                                });
                            }
                        }, function(response) {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Intrarea nu a putut fi adaugata!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Intrarea nu a putut fi editata!"
                                });
                            }

                            if (response && response.data && response.data.dbMessage && response.data.dbMessage.code == "ER_DUP_ENTRY") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Numarul CRT exista deja"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteIncoms(result.item).then(function(deleted) {
                                if (deleted) {
                                    //TODO optimizare cu WhereTo, indexOf si splice apoi
                                    for (var i = 0; i < $scope.incoms.length; i++) {
                                        if ($scope.incoms[i].id == result.item.id) {
                                            $scope.incoms.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                                $rootScope.extended.ngToast.create("Intrarea a fost stearsa cu succes");
                            },
                            function() {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Intrarea nu a putut fi stearsa!"
                                });

                            });
                        break;
                }
            }, function(reason) {
                //TODO
                //alert(reason);
            });
        }
    }]);
}())
