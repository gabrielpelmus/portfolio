(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('BuildingsController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function($scope, $state, dataProvider, $rootScope, $modal) {
        dataProvider.loadBuildings().then(function(data) {
            $scope.buildings = data;
        }, function() {

        });

        $scope.sortKey = "name";
        $scope.reverse = false;

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openModal = function(action, building) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/settings/buildings/edit.html',
                controller: "ModalController",
                backdrop: 'static',
                resolve: {
                    title: function() {
                        switch (action) {
                            case "add":
                                return "Adaugati o cladire";
                            case "edit":
                                return "Editati cladirea";
                        }
                        return "Etapa: unknown action";
                    },
                    showDelete: function() {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function() {
                        var item = {};

                        switch (action) {
                            case "add":
                                item.id = "";
                                item.name = "";
                                item.address = "";
                                item.details = "";

                                break;
                            case "edit":
                                item.id = building.id;
                                item.name = building.name;
                                item.address = building.address;
                                item.details = building.details;
                                break;
                        }
                        return item;
                    }
                }
            });

            $rootScope.extended.modalInstance.result.then(function(result) {
                switch (result.action) {
                    case "save":
                        dataProvider.saveBuilding(result.item).then(function(buildingId) {
                            if (result.item.id == "") {
                                $scope.buildings.push({
                                    id: buildingId,
                                    name: result.item.name,
                                    address: result.item.address,
                                    details: result.item.details
                                });
                                $rootScope.extended.ngToast.create({
                                    content: "Cladirea fost introdusa cu succes!"
                                });
                            } else {
                                //actiune de edit al unei cladiri
                                var building = $.Enumerable.From($scope.buildings).Where('bld => bld.id == ' + result.item.id).FirstOrDefault();
                                if (building != null) {
                                    building.name = result.item.name;
                                    building.address = result.item.address;
                                    building.details = result.item.details;
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Cladirea fost editata cu succes!"
                                });
                            }
                        }, function() {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Cladirea nu a putut fi adaugata!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Cladirea nu a putut fi editata!"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteBuilding(result.item).then(function(deleted) {
                                if (deleted) {
                                    //TODO optimizare cu WhereTo, indexOf si splice apoi
                                    for (var i = 0; i < $scope.buildings.length; i++) {
                                        if ($scope.buildings[i].id == result.item.id) {
                                            $scope.buildings.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Cladirea fost stersa cu succes!"
                                });
                            },
                            function() {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Cladirea nu a putut fi stersa cu succes!"
                                });
                            });
                        break;
                }
            }, function(reason) {
                //alert(reason);
            });
        }


    }]);
}())
