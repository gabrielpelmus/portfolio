(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('TransfersController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal",
        function($scope, $state, dataProvider, $rootScope, $modal) {

            $scope.itemsPerPage = 17;

            $scope.allTransferTypes = [{
                key: "",
                value: "Toate"
            }, {
                key: "IN",
                value: "Intrari"
            }, {
                key: "OUT",
                value: "Iesiri"
            }];

            $scope.searchTransfer = {
                type: $scope.allTransferTypes[0],
                text: ""
            }

            dataProvider.loadBankAccounts().then(function(bankAccounts) {
                $scope.bankAccounts = bankAccounts;

                // eliminare cont principal din selectorul de transfer To/From Bank Account
                for (var i = 0; i < bankAccounts.length; i++) {
                    if (bankAccounts[i].isMaster == 1) {
                        bankAccounts.splice(i, 1);
                        break;
                    }
                }

                dataProvider.loadTransfers().then(function(data) {
                    $scope.transfers = data;

                    $scope.transfers.forEach(function(transfer) {
                        transfer.extended = {};
                        transfer.extended.bankAccount = $.Enumerable.From($scope.bankAccounts).Where('bank => bank.id == ' + transfer.transferToOrFromBankAccountId).FirstOrDefault();
                    }, this);

                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Transferurile nu au putut fi incarcate!"
                    });
                });
            }, function() {
                $rootScope.extended.ngToast.create({
                    className: "danger",
                    dismissButton: true,
                    content: "Conturile bancare nu au putut fi incarcate!"
                });
            });

            $scope.openInfo = function() {
                alert('Functionalitate in lucru...');
            }

            $scope.sortKey = "name";
            $scope.reverse = false;

            $scope.sort = function(keyname) {
                $scope.sortKey = keyname;
                $scope.reverse = !$scope.reverse;
            }

            $scope.openModal = function(action, accounts, transfer) {

                $rootScope.extended.modalInstance = $modal.open({
                    keyboard: false,
                    animation: $scope.animationsEnabled,
                    templateUrl: 'app/views/transfers/edit.html',
                    controller: "ModalController",
                    backdrop: 'static',
                    resolve: {
                        title: function() {
                            switch (action) {
                                case "add":
                                    return "Adaugati un transfer bancar";
                                case "edit":
                                    return "Editati transferul bancar";
                            }
                            return "Etapa: unknown action";
                        },
                        showDelete: function() {
                            switch (action) {
                                case "add":
                                    return false;
                                case "edit":
                                    return true;
                            }
                            return true;
                        },
                        item: function() {
                            var item = {
                                extended: {}
                            };
                            item.extended.validCRT = 0;

                            item.extended.dateOptions = {
                                showWeeks: true,
                                startingDay: 1
                            };

                            item.extended.dateOptions = {
                                showWeeks: true
                            };

                            if (transfer) {
                                item.extended.operationDT = {
                                    value: Date(transfer.operationDT),
                                    isOpen: false
                                };

                            } else {
                                item.extended.operationDT = {
                                    value: new Date(),
                                    isOpen: false
                                };

                            }

                            switch (action) {
                                case "add":
                                    item.type = "IN";
                                    item.operationDT = item.extended.operationDT.value;
                                    break;
                                case "edit":
                                    item = angular.copy(transfer);
                                    item.extended.bankAccount = $.Enumerable.From(accounts).Where('bank => bank.id == ' + transfer.transferToOrFromBankAccountId).FirstOrDefault();
                                    break;
                            }

                            item.extended.bankAccounts = accounts;

                            item.extended.openCalendar = function(e, date) {
                                item.extended.isCalendarOpen = true;
                            };

                            item.filesToBeDeleted = new Array();

                            item.extended.removeFile = function(file) {
                                var index = item.files.indexOf(file);
                                item.files.splice(index, 1);
                                item.filesToBeDeleted.push(file);
                            };

                            item.extended.validateThisCRT = function(crt) {
                                dataProvider.validateCRT(crt).then(function(tof) {

                                    if (tof[0]) {
                                        item.extended.validCRT = 0;
                                        item.message = "CRT-ul introdus (" + crt + ") exista deja in baza de date!"
                                    } else {
                                        item.extended.validCRT = 1;
                                        item.message = "";
                                    }
                                });
                            };

                            return item;
                        }
                    }
                });

                $rootScope.extended.modalInstance.result.then(function(result) {
                    result.item.transferToOrFromBankAccountId = result.item.extended.bankAccount.id;
                    result.item.newFiles = $.Enumerable.From(result.item.newFiles).Where('newFile => newFile != null').ToArray();
                    switch (result.action) {
                        case "save":
                            dataProvider.saveTransfer(result.item).then(function(transferId) {
                                if (!(result.item.id)) {
                                    dataProvider.loadTransferById(transferId).then(function(data) {
                                        data.extended = {};
                                        data.extended.bankAccount = $.Enumerable.From($scope.bankAccounts).Where('bank => bank.id == ' + data.transferToOrFromBankAccountId).FirstOrDefault();
                                        $scope.transfers.push(data);
                                    }, function() {
                                        $rootScope.extended.ngToast.create({
                                            className: "danger",
                                            dismissButton: true,
                                            content: "Transferul bancar nu a putut fi incarcat!"
                                        });
                                    });
                                    $rootScope.extended.ngToast.create("Transferul bancar a fost introdus cu success!");
                                } else {
                                    //actiune de edit al unui serviciu
                                    var transfer = $.Enumerable.From($scope.transfers).Where('trans => trans.id == ' + result.item.id).FirstOrDefault();
                                    var index = $scope.transfers.indexOf(transfer);
                                    if (index >= 0) {
                                        dataProvider.loadTransferById(result.item.id).then(function(data) {
                                            $scope.transfers[index] = data;
                                            $scope.transfers[index].extended = {};
                                            $scope.transfers[index].extended.bankAccount = $.Enumerable.From($scope.bankAccounts).Where('bank => bank.id == ' + data.transferToOrFromBankAccountId).FirstOrDefault();
                                        }, function() {
                                            $rootScope.extended.ngToast.create({
                                                className: "danger",
                                                dismissButton: true,
                                                content: "Transferul bancar nu a putut fi incarcat!"
                                            });
                                        });
                                    }
                                    $rootScope.extended.ngToast.create("Transferul bancar a fost editat cu success!");
                                }
                            }, function(response) {
                                if (result.item.id == "") {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Transferul bancar nu a putut fi adaugat!"
                                    });
                                } else {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Transferul bancar nu a putut fi editat!"
                                    });
                                }
                                if (response && response.data && response.data.dbMessage && response.data.dbMessage.code == "ER_DUP_ENTRY") {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Numarul CRT exista deja"
                                    });
                                }
                            });
                            break;
                        case "delete":
                            dataProvider.deleteTransfer(result.item).then(function(deleted) {
                                    if (deleted) {
                                        //TODO optimizare cu WhereTo, indexOf si splice apoi
                                        for (var i = 0; i < $scope.transfers.length; i++) {
                                            if ($scope.transfers[i].id == result.item.id) {
                                                $scope.transfers.splice(i, 1);
                                                break;
                                            }
                                        }
                                    }
                                    $rootScope.extended.ngToast.create("Transferul bancar a fost sters cu succes!"); // de pus in modal
                                },
                                function() {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Transferul bancar nu a putut fi sters!"
                                    });
                                });
                            break;
                    }
                }, function(reason) {
                    //TODO
                    //alert(reason);
                });
            }
        }
    ]);
}())
