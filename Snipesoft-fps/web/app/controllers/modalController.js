(function() {
    "use strict";

    var fps = angular.module('fps');


    fps.controller('ModalController', ["$scope", "$modalInstance", "title", "item", "showDelete",
        function($scope, $modalInstance, title, item, showDelete) {
            $scope.title = title;
            $scope.item = item;
            $scope.isDirtyConfirmationPopup = {
                yes: function(){
                    $modalInstance.dismiss('modal closed ok');
                },
                no: function(){
                    $scope.isDirtyConfirmationPopup.visible = false;
                },
                visible: false
            };

            $scope.delete = {};
            $scope.delete.visible = showDelete;
            if ($scope.delete.visible) {
                $scope.delete.confirm = function() {
                    $modalInstance.close({
                        action: "delete",
                        item: $scope.item
                    });
                }
            }

            $scope.save = function() {
                $modalInstance.close({
                    action: "save",
                    item: $scope.item
                });
            };

            $scope.savePayment = function() {
                $modalInstance.close({
                    action: "savePayment",
                    item: $scope.item
                });
            };

            $scope.savePaymentHistory = function() {
                $modalInstance.close({
                    action: "savePaymentHistory",
                    item: $scope.item
                });
            };            

            $scope.cancel = function(isDirty) {
                if(isDirty){
                    $scope.isDirtyConfirmationPopup.visible = true;
                }
                else{
                    $modalInstance.dismiss('modal closed ok');
                } 
            };

            
        }
    ]);
}())
