(function() {
    "use strict";

    var fps = angular.module('fps');

    fps.controller('LoginController', ["$scope", "$state", "DataProvider", "jwtHelper", "$rootScope",
        function($scope, $state, dataProvider, jwtHelper, $rootScope) {
            $scope.username = "";
            $scope.password = "";
            $scope.message = "";

            var stateToLoadAfterLoginOK = null;

            $rootScope.$on('loadStateAfterLoginOK', function(event, state) {
                console.log("%c loadStateAfterLoginOK " + JSON.stringify(state), window.customConsoleCss.stateInfo);
                event.preventDefault();
                $scope.message = "Autentificare necesara pentru a accesa pagina dorita";
                stateToLoadAfterLoginOK = state;
            });

            $scope.login = function() {
                $scope.message = "";
                dataProvider.login($scope.username, $scope.password).then(function(data) {
                    localStorage["jwtToken"] = data;
                    if (stateToLoadAfterLoginOK != null) {
                        $state.go(stateToLoadAfterLoginOK.name);
                        stateToLoadAfterLoginOK = null;
                    } else {
                        $state.go("app.home");
                    }
                }, function(response) {
                    $scope.message = "Autentificare esuata";
                    if (response.status == 500) $scope.message = "Baza de date este inaccesibila";       
                });
            }
        }
    ]);

}())
