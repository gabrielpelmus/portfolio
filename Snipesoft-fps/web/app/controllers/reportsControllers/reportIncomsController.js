(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ReportIncomsController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal",
        function($scope, $state, dataProvider, $rootScope, $modal) {
            $scope.buttonDisabled = false;
            var currDate = new Date();
            $scope.dateFrom = new Date(currDate.getFullYear(), currDate.getMonth() - 1, 1);
            $scope.dateTo = new Date(currDate.getFullYear(), currDate.getMonth(), 0);

            //var vals = [];
            $scope.data = [{
                key: "Cumulative Return",
                values: []
            }];

            dataProvider.loadIncomsBundle().then(function(data) {


                if (data[0].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista blocuri."
                    });
                } else {
                    $scope.buildings = data[0];
                }

                if (data[1].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista tip apartament."
                    });
                } else {
                    $scope.apartments = data[1];
                }
            });

            $scope.clearAll = function() {

                if (this.dateFrom) {
                    this.dateFrom = "";
                }

                if (this.dateTo) {
                    this.dateTo = ""
                }

                if (this.reportName) {
                    this.reportName = "";
                }
                if (this.building) {
                    this.building = "";
                }
                if (this.apType) {
                    this.apType = "";
                }

                $("input:checkbox").removeAttr("checked");
                $scope.remCols();
                $("#mainForm").css("display", "none");
                $scope.api.hideChart();
            }

            $scope.getTotalAmount = function(incoms) {
                var sum = 0.00;

                angular.forEach(incoms, function(income) {
                    sum += parseFloat(income.amount);
                });

                return sum.toFixed(2);
            }

            $scope.item = function() {
                var item = {};
                item.extended = {};

                if (invoice) {
                    item.extended.supplier = $scope.suppliers.filter(function(obj) {
                        return obj.id == invoice.supplierId;
                    });
                }

                item.extended.dateOptions = {
                    showWeeks: true
                };

                item.extended.createDTIOpen = false;
                item.extended.dueDTIsOpen = false;

                item.extended.openCalendar = function(e, date) {
                    switch (date) {
                        case "createDT":
                            item.extended.createDTIsOpen = true;
                            break;
                        case "dueDT":
                            item.extended.dueDTIsOpen = true;
                            break;
                    }
                };



                item.filesToBeDeleted = new Array();

                item.extended.removeFile = function(file) {
                    var index = item.files.indexOf(file);
                    item.files.splice(index, 1);
                    item.filesToBeDeleted.push(file);
                };

                item.extended.createDTChanged = function(item) {
                    item.dueDT = new Date(item.createDT.toString());
                    item.dueDT.setDate(item.dueDT.getDate() + 1);
                };

                return item;
            }

            $scope.sortKey = "client.name";
            $scope.reverse = false;

            $scope.sort = function(keyname) {
                $scope.sortKey = keyname;
                $scope.reverse = !$scope.reverse;
            };

            $scope.isCollapsed = false;

            $scope.remCols = function() {

                $scope.isCollapsed = !$scope.isCollapsed;

                if ($('#remCols span').hasClass('glyphicon-chevron-down')) {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-up"></span>');
                } else {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-down"></span>');
                }

            };

            $scope.exportAction = function(tip) {
                var reportName = "Raport_Intrari";

                if (this.reportName) {
                    reportName = this.reportName;
                }
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var dateF = "";
                var dateT = d.getFullYear().toString() + "-" + month + "-" + day;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                switch (tip) {
                    case 'pdf':
                        $rootScope.$broadcast('export-pdf', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    case 'excel':
                        $rootScope.$broadcast('export-excel', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    default:
                        console.log('no event caught');
                }
            };

            $scope.generateReport = function() {
                $scope.buttonDisabled = true;
                var dateF = null;
                var dateT = null;
                var buildingName = null;
                var apType = null;

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }
                if (this.building) {
                    buildingName = this.building.name;
                }
                if (this.apType) {
                    apType = this.apType.apType;
                }

                dataProvider.loadIncomeByDataRange(dateF, dateT, buildingName, apType).then(function(data) {

                    $scope.incoms = data;

                    if ($scope.incoms.length > 0) {
                        $("#mainForm").css("display", "");
                    } else {
                        // $scope.clearAll();
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Selectia nu a returnat rezultate!"
                        });
                    }
                    
                    $scope.buttonDisabled = false;

                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare la citirea listei de intrari!"
                    });
                });
                
            }

            $scope.hide = function(clasa) {
                var state = $("." + clasa).css("display");
                if (state != "none")
                    $("." + clasa).css("display", "none");
                else
                    $("." + clasa).css("display", "");
            }

            $scope.nvd3Chart = function() {

                //$rootScope.$broadcast('show_hide_chart');

                var dateF = null;
                var dateT = null;
                var buildingName = null;
                var apType = null;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }
                if (this.building) {
                    buildingName = this.building.name;
                }
                if (this.apType) {
                    apType = this.apType.apType;
                }
                if (!$scope.api.isChartVisible()) {
                    dataProvider.loadIncomesChartData(dateF, dateT, buildingName, apType).then(function(data) {

                        $scope.incomesChart = data;
                        $scope.data = incomesToOrto();
                        $scope.api.updateWithData($scope.data);

                    }, function() {
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Eroare la citirea listei de intrari!"
                        });
                    });
                }
                $rootScope.$broadcast("show_hide_chart");
            }

            $scope.options = {
                "chart": {
                    "type": 'discreteBarChart',
                    "height": 600,
                    "margin": {
                        "top": 20,
                        "right": -1,
                        "bottom": 10,
                        "left": 100
                    },
                    "x": function(d) {
                        return d.x;
                    },
                    "y": function(d) {
                        return d.y;
                    },
                    "showValues": false,
                    "valueFormat": function(d) {
                        return d3.format(',.2f')(d);
                    },
                    "transitionDuration": 500,
                    "showXAxis": false,
                    "xAxis": {
                        "axisLabel": '',
                        "rotateLabels": -90,
                        "axisLabelDistance": 30
                    },
                    "yAxis": {
                        "axisLabel": '',
                        "axisLabelDistance": 30
                    },
                    "tooltip": {
                        "position": {
                            "left": 50,
                            "right": 50
                        },
                        "fixedTop": 300
                    }

                }
            };



            function incomesToOrto() {
                var values = [];

                angular.forEach($scope.incomesChart, function(income) {
                    values.push({
                        x: income.client,
                        y: parseFloat(income.amount)
                    });
                });

                //return values;
                return [{
                    values: values, //values - represents the array of {x,y} data points
                    key: 'Intrari' //key  - the name of the series.                    
                }];
            }

            // console.log(vals.length);

        }
    ]);
}());
