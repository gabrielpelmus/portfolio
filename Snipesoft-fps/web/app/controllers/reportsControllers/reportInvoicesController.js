(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ReportInvoicesController', ["$scope", "$state", "DataProvider", "$rootScope",
        function($scope, $state, dataProvider, $rootScope) {
            $scope.buttonDisabled = false;
            var currDate = new Date();
            $scope.dateFrom = new Date(currDate.getFullYear(), currDate.getMonth()-1, 1);
            $scope.dateTo = new Date(currDate.getFullYear(), currDate.getMonth(), 0);
            
            var vals = [];
            $scope.extended.dateOptions = {
                showWeeks: true
            };

            $scope.extended.dateFromIOpen = false;
            $scope.extended.dateToIsOpen = false;

            $scope.extended.openCalendar = function(e, date) {
                switch (date) {
                    case "dateFrom":
                        $scope.extended.dateFromIsOpen = true;
                        break;
                    case "dateTo":
                        $scope.extended.dateToIsOpen = true;
                        break;
                }
            };

            $scope.hideUnlinked = function() {
                $("#serviceGroupId").css("display", "none");
                $("#serviceGroupIdLinked").css("display", "");
            }

            dataProvider.loadInvoiceBundle().then(function(data) {
                if (data[0].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista furnizori."
                    });
                } else {
                    $scope.suppliers = data[0];
                    $scope.supplierServiceGroup = [];
                    angular.forEach($scope.suppliers, function(supplier) {
                        angular.forEach(supplier.services, function(service) {
                            $scope.supplierServiceGroup.push(service);
                        });
                    });
                }

                if (data[1].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista cladiri."
                    });
                } else {
                    $scope.buildings = data[2];
                }

                if (data[2].error) {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare citire lista tipuri facturi."
                    });
                } else {
                    $scope.invoiceTypes = data[4];
                }

                $scope.searchInvoices = ''; // set the default search/filter term
            });

            $scope.clearAll = function() {

                if (this.dateFrom) {
                    this.dateFrom = "";
                }

                if (this.dateTo) {
                    this.dateTo = ""
                }

                if (this.reportName) {
                    this.reportName = "";
                }

                if (this.supplier) {
                    this.supplier = "";
                }

                if (this.serviceGroup) {
                    this.serviceGroup = "";
                }

                if (this.building) {
                    this.building = "";
                }

                if (this.invoiceType) {
                    this.invoiceType = "";
                }

                $("#serviceGroupId").css("display", "");
                $("#serviceGroupIdLinked").css("display", "none");

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();
            }

            $scope.getTotalAmount = function(invoices, status) {
                var sum = 0.00;
                var processedInvoices = [];

                angular.forEach(invoices, function(invoice) {
                    if (status == 'TOTAL'&& !processedInvoices[invoice.idV]) {
                        sum += parseFloat(invoice.invoiceAmount);
                        processedInvoices[invoice.idV] = true;
                    }
                    if (status == 'PAID' && invoice.amount) {
                        sum += parseFloat(invoice.amount);
                    }
                    if (status == 'REST') {
                        sum += parseFloat(invoice.remainingAmount);
                    }
                });

                return sum.toFixed(2);
            }

            $scope.isCollapsed = false;

            $scope.remCols = function() {

                $scope.isCollapsed = !$scope.isCollapsed;

                if ($('#remCols span').hasClass('glyphicon-chevron-down')) {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-up"></span>');
                } else {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-down"></span>');
                }

            };

            $scope.daysRemaining = function daysRemaining(createDT, dueDT) {
                var dateStart = new Date(createDT);
                var dateEnd = new Date(dueDT);
                var timeDiff = Math.abs(dateEnd.getTime() - dateStart.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            }

            $scope.setRowColor = function(statusId) {
                if (statusId == 15) {
                    return {
                        color: "grey"
                    }
                }
            }

            $scope.generateReport = function() {
                $scope.buttonDisabled = true;
                var dateF = null;
                var dateT = null;
                var supplier = null;
                var servicesGr = null;
                var building = null;
                var invoiceType = null;

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                if (this.supplier) {
                    supplier = this.supplier.name;
                }
                if (this.serviceGroup) {
                    servicesGr = this.serviceGroup.name;
                }
                if (this.building) {
                    building = this.building.aliasName;
                }
                if (this.invoiceType) {
                    invoiceType = this.invoiceType.name;
                }

                dataProvider.loadInvoicesByDataRange(dateF, dateT, supplier, servicesGr, building, invoiceType).then(function(data) {

                    $scope.invoices = data;

                    if ($scope.invoices.length > 0) {
                        $("#mainForm").css("display", "");
                    } else {
                        // $scope.clearAll();
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Selectia nu a returnat rezultate!"
                        });
                    }
                    $scope.buttonDisabled = false;
                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare la citirea listei de facturi!"
                    });
                });

                //$("#mainForm").css("display", "");
            }

            var invoicesToPay = new Array();
            var invoiceToPay = {};
            $scope.invoicesToPayCounter = 0;

            $scope.sendInvoiceForPayment = function(invoiceId, includeInvoice) {

                if (includeInvoice == true) {

                    invoiceToPay = $.Enumerable.From($scope.invoices).Where('inv => inv.id == ' + invoiceId).FirstOrDefault();
                    invoicesToPay.push(invoiceToPay);

                } else {
                    var index = invoicesToPay.indexOf(invoiceId);
                    invoicesToPay.splice(index, 1);
                }

                $scope.invoicesToPay = invoicesToPay;
                $scope.invoicesToPayCounter = invoicesToPay.length;

            }

            $scope.sortKey = "name";
            $scope.reverse = false;

            $scope.sort = function(keyname) {
                $scope.sortKey = keyname;
                $scope.reverse = !$scope.reverse;
            }

            $scope.exportAction = function(tip) {
                var reportName = "Raport_Facturi";

                if (this.reportName) {
                    reportName = this.reportName;
                }
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var dateF = "";
                var dateT = d.getFullYear().toString() + "-" + month + "-" + day;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                switch (tip) {
                    case 'pdf':
                        $rootScope.$broadcast('export-pdf', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    case 'excel':
                        $rootScope.$broadcast('export-excel', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    default:
                        console.log('no event caught');
                }
            };

            $scope.hide = function(clasa) {
                var state = $("." + clasa).css("display");
                if (state != "none") {
                    $("." + clasa).css("display", "none");
                    $("." + clasa + "Selector").css("background-color", "#FF4E49");
                } else {
                    $("." + clasa).css("display", "");
                    $("." + clasa + "Selector").css("background-color", "");
                }
            }

            $scope.nvd3Chart = function() {

                $rootScope.$broadcast("show_hide_chart");

                if (!$scope.api.isChartVisible()) {
                    
                    $scope.data = overviewToOrto();
                    $scope.api.updateWithData($scope.data);
                }
                

            }


            $scope.options = {
                chart: {
                    "type": 'pieChart',
                    "height": 450,
                    "x": function(d) {
                        return d.key;
                    },
                    "y": function(d) {
                        return d.y;
                    },
                    "showLabels": true,
                    "labelType": "value",
                    "labelTreshold": 0.01,
                    "legend": {
                        "margin": {
                            "top": 5,
                            "right": 35,
                            "bottom": 5,
                            "left": 0
                        }
                    },
                    "transitionDuration": 250
                }
            };



            function overviewToOrto() {
                var values = [];

                values = [{
                    key: "Total platit",
                    y: $scope.getTotalAmount($scope.invoices, "PAID")
                }, {
                    key: "Rest de plata",
                    y: $scope.getTotalAmount($scope.invoices, "REST")
                }];

                return values;
            }
        }
    ]);
}())
