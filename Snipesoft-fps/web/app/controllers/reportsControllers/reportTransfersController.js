(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ReportTransfersController', ["$scope", "$state", "DataProvider", "$rootScope",
        function($scope, $state, dataProvider, $rootScope) {
            $scope.buttonDisabled = false;
            var currDate = new Date();
            $scope.dateFrom = new Date(currDate.getFullYear(), currDate.getMonth()-1, 1);
            $scope.dateTo = new Date(currDate.getFullYear(), currDate.getMonth(), 0);

            var vals = [];
            $scope.extended.dateOptions = {
                showWeeks: true
            };

            $scope.extended.dateFromIOpen = false;
            $scope.extended.dateToIsOpen = false;


            $scope.extended.openCalendar = function(e, date) {
                switch (date) {
                    case "dateFrom":
                        $scope.extended.dateFromIsOpen = true;
                        break;
                    case "dateTo":
                        $scope.extended.dateToIsOpen = true;
                        break;
                }
            };

            $scope.getTotalAmount = function(transfers, trsType) {
                var sum = 0.00;

                angular.forEach(transfers, function(trs) {
                    if (trs.amount && trs.type == trsType)
                        sum += parseFloat(trs.amount);
                });

                return sum.toFixed(2);
            }

            $scope.isCollapsed = false;

            $scope.remCols = function() {

                $scope.isCollapsed = !$scope.isCollapsed;

                if ($('#remCols span').hasClass('glyphicon-chevron-down')) {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-up"></span>');
                } else {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-down"></span>');
                }

            };

            $scope.allTransferTypes = [{
                key: "''",
                value: "Toate"
            }, {
                key: "IN",
                value: "Intrari"
            }, {
                key: "OUT",
                value: "Iesiri"
            }];

            $scope.searchTransfersType = $scope.allTransferTypes[0];
            $scope.searchTransfer = "";

            $scope.clearAll = function() {

                if (this.dateFrom) {
                    this.dateFrom = "";
                }

                if (this.dateTo) {
                    this.dateTo = ""
                }

                if (this.reportName) {
                    this.reportName = "";
                }

                this.searchTransfersType.key = "";
                this.searchTransfersType.value = "Toate";
                $("#mainForm").css("display", "none");
                $scope.api.hideChart();
            }

            $scope.generateReport = function() {
                $scope.buttonDisabled = true;
                var dateF = null;
                var dateT = null;
                var ttype = null;

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }
                if (this.searchTransfersType.key != "''") {
                    ttype = this.searchTransfersType.key;
                }

                dataProvider.loadTransferByDataRange(ttype, dateF, dateT).then(function(data) {
                    $scope.transfers = data;

                    if ($scope.transfers.length > 0) {
                        $("#mainForm").css("display", "");
                    } else {
                        // $scope.clearAll();
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Selectia nu a returnat rezultate!"
                        });
                    }
                    $scope.buttonDisabled = false;
                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Transferurile nu au putut fi incarcate!"
                    });
                });
            }



            $scope.openInfo = function() {
                alert('Functionalitate in lucru...');
            }

            $scope.sortKey = "name";
            $scope.reverse = false;

            $scope.sort = function(keyname) {
                $scope.sortKey = keyname;
                $scope.reverse = !$scope.reverse;
            }

            $scope.exportAction = function(tip) {
                var reportName = "Raport_transfer";

                if (this.reportName) {
                    reportName = this.reportName;
                }

                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var dateF = "";
                var dateT = d.getFullYear().toString() + "-" + month + "-" + day;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                switch (tip) {
                    case 'pdf':
                        $rootScope.$broadcast('export-pdf', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    case 'excel':
                        $rootScope.$broadcast('export-excel', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    default:
                        console.log('no event caught');
                }
            };

            $scope.hide = function(clasa) {
                var state = $("." + clasa).css("display");
                if (state != "none") {
                    $("." + clasa).css("display", "none");
                    $("." + clasa + "Selector").css("background-color", "#FF4E49");
                } else {
                    $("." + clasa).css("display", "");
                    $("." + clasa + "Selector").css("background-color", "");
                }
            }



            $scope.nvd3Chart = function() {

                //$rootScope.$broadcast('show_hide_chart');

                var dateF = null;
                var dateT = null;
                var ttype = null;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }
                if (this.searchTransfersType.key != "''") {
                    ttype = this.searchTransfersType.key;
                }
                if (!$scope.api.isChartVisible()) {
                    var dateF = null;
                    var dateT = null;
                    var ttype = null;

                    if (this.dateFrom) {
                        dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                    }
                    if (this.dateTo) {
                        dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                    }
                    if (this.searchTransfersType.key != "''") {
                        ttype = this.searchTransfersType.key;
                    }

                    dataProvider.loadTransfersChartData(ttype, dateF, dateT).then(function(data) {

                        $scope.transfersChart = data;
                        $scope.data = transfersToOrto();
                        $scope.api.updateWithData($scope.data);

                    }, function() {
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Eroare la citirea listei de intrari!"
                        });
                    });
                }
                $rootScope.$broadcast("show_hide_chart");
            }

            $scope.options = {
                "chart": {
                    "type": 'discreteBarChart',
                    "height": 600,
                    "margin": {
                        "top": 20,
                        "right": -1,
                        "bottom": 60,
                        "left": 100
                    },
                    "x": function(d) {
                        return d3.time.format("%Y-%m-%d")(new Date(d.x));
                    },
                    "y": function(d) {
                        return d.y;
                    },
                    "showValues": false,
                    "rotateValues": -90,
                    "valueFormat": function(d) {
                        return d3.format(',.2f')(d);
                    },
                    "transitionDuration": 500,
                    "xAxis": {
                        "axisLabel": 'Data',
                        "rotateLabels": -45
                    },
                    "yAxis": {
                        "axisLabel": 'Suma',
                        "axisLabelDistance": 30
                    }
                }
            };



            function transfersToOrto() {
                var values = [];

                angular.forEach($scope.transfersChart, function(transfer) {
                    values.push({
                        x: transfer.operationDT,
                        y: parseFloat(transfer.correctAmount)
                    });
                });

                return [{
                    values: values, //values - represents the array of {x,y} data points
                    key: 'Transferuri' //key  - the name of the series.                    
                }];
            }
        }
    ]);
}())
