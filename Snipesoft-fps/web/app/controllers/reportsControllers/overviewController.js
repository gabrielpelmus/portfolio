(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ReportsController', ["$scope", "$state", "DataProvider", "$rootScope",
        function($scope, $state, dataProvider, $rootScope) {

            $scope.buttonDisabled = false;

            var currDate = new Date();
            $scope.dateFrom = new Date(currDate.getFullYear(), currDate.getMonth()-1, 1);
            $scope.dateTo = new Date(currDate.getFullYear(), currDate.getMonth(), 0);

            $scope.extended.dateOptions = {
                showWeeks: true
            };

            $scope.toPositive = function (val){
                var outVal = Math.abs(val);

                return outVal;
            }

            $scope.extended.dateFromIOpen = false;
            $scope.extended.dateToIsOpen = false;

            $scope.extended.openCalendar = function(e, date) {
                switch (date) {
                    case "dateFrom":
                        $scope.extended.dateFromIsOpen = true;
                        break;
                    case "dateTo":
                        $scope.extended.dateToIsOpen = true;
                        break;
                }
            };

            $scope.getTotalAmount = function(overview, ovrType) {
                var sum = parseFloat(0.00);

                angular.forEach(overview, function(ovr, i) {

                    if (ovrType == "IN" && ovr.type == "IN") {
                        sum += parseFloat(ovr.inAmount);
                    }

                    if (ovrType == "OUT" && ovr.type == "OUT") {
                        sum += parseFloat(ovr.outAmount);
                    }
                });

                return Math.abs(sum.toFixed(2));
            };

            $scope.getInitSold = function(ovr){
                var val = parseFloat(0.00);
                val = ovr.sold;
                if(ovr.type == "IN"){
                    val -= ovr.inAmount;
                }
                if(ovr.type == "OUT"){
                    val += ovr.outAmount;
                }

                return val.toFixed(2);
            };

            $scope.clearAll = function() {

                if (this.dateFrom) {
                    this.dateFrom = "";
                }

                if (this.dateTo) {
                    this.dateTo = ""
                }

                if (this.reportName) {
                    this.reportName = "";
                }

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();
            }

            $scope.isCollapsed = false;

            $scope.remCols = function() {

                // $scope.isCollapsed = !$scope.isCollapsed;
                if($(".topbar").css("display")!="none"){
                    $(".topbar").css("display", "none");
                }else{
                    $(".topbar").css("display","");
                }

                if ($('#remCols span').hasClass('glyphicon-chevron-down')) {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-up"></span>');
                } else {
                    $('#remCols').html('Elimina coloane din raport <span class="glyphicon glyphicon-chevron-down"></span>');
                }

            };

            $scope.exportAction = function(tip) {
                var reportName = "Raport_General";

                if (this.reportName) {
                    reportName = this.reportName;
                }
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var dateF = "";
                var dateT = d.getFullYear().toString() + "-" + month + "-" + day;

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                switch (tip) {
                    case 'pdf':
                        $rootScope.$broadcast('export-pdf', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    case 'excel':
                        $rootScope.$broadcast('export-excel', {
                            filename: reportName + "_" + dateF + "_" + dateT,
                            reportName: reportName
                        });
                        $scope.clearAll();
                        break;
                    default:
                        console.log('no event caught');
                }
            };

            $scope.generateReport = function() {

                $scope.buttonDisabled = true;

                var dateF = null;
                var dateT = null;

                $("#mainForm").css("display", "none");
                $scope.api.hideChart();

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }

                dataProvider.loadOverviewByDataRange(dateF, dateT).then(function(data) {

                    $scope.overview = data;

                    if ($scope.overview.length > 0) {
                        $("#mainForm").css("display", "");
                        
                    } else {
                        //$scope.clearAll();
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Selectia nu a returnat rezultate!"
                        });
                    }
                    $scope.buttonDisabled = false;

                }, function() {
                    $rootScope.extended.ngToast.create({
                        className: "danger",
                        dismissButton: true,
                        content: "Eroare la citirea listei de intrari!"
                    });
                });

            }

            $scope.hide = function(clasa) {
                var state = $("." + clasa).css("display");
                if (state != "none")
                    $("." + clasa).css("display", "none");
                else
                    $("." + clasa).css("display", "");
            }

            $scope.nvd3Chart = function() {

                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var dateF = "";
                //var dateT = d.getFullYear().toString() + "-" + month + "-" + day;
                var dateT = "";

                if (this.dateFrom) {
                    dateF = this.dateFrom.getFullYear().toString() + "-" + (this.dateFrom.getMonth() + 1).toString() + "-" + this.dateFrom.getDate().toString();
                }
                if (this.dateTo) {
                    dateT = this.dateTo.getFullYear().toString() + "-" + (this.dateTo.getMonth() + 1).toString() + "-" + this.dateTo.getDate().toString();
                }


                if (!$scope.api.isChartVisible()) {
                    dataProvider.loadOverviewChartData(dateF, dateT).then(function(data) {

                        $scope.overviewChart = data;
                        $scope.data = overviewToOrto();
                        $scope.api.updateWithData($scope.data);


                    }, function() {
                        $rootScope.extended.ngToast.create({
                            className: "danger",
                            dismissButton: true,
                            content: "Eroare la citirea listei de intrari!"
                        });
                    });
                }
                $rootScope.$broadcast("show_hide_chart");
            }

            $scope.options = {
                chart: {
                    type: 'stackedAreaChart',
                    height: 450,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 50,
                        left: 165
                    },
                    x: function(d) {
                        return d[0];
                    },
                    y: function(d) {
                        return d[1];
                    },
                    useVoronoi: false,
                    clipEdge: true,
                    useInteractiveGuideline: true,
                    xAxis: {
                        showMaxMin: false,
                        tickFormat: function(d) {
                            return d3.time.format("%Y-%m-%d")(new Date(d))
                        }
                    },
                    yAxis: {
                        tickFormat: function(d) {
                            return d3.format('.,2f')(d);
                        }
                    },
                    transitionDuration: 250
                }
            };



            function overviewToOrto() {
                var vals = [];
                var key = "Sold";
                $.each($scope.overviewChart, function(i, ovr) {
                    vals.push([new Date(ovr.operationDT).getTime(), parseFloat(ovr.sold)]);
                });

                var val = {
                    "key": key,
                    "values": vals
                };
                var values = [];
                values.push(val);

                return values;
            }
        }
    ]);
}())
