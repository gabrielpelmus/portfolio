(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('UsersController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal",
     function($scope, $state, dataProvider, $rootScope, $modal) {
        dataProvider.loadUsers().then(function(data) {
            $scope.users = data;
            $scope.searchUsers = ''; // set the default search/filter term
        }, function() {

        });
    }]);
}())
