(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('AccountsController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function($scope, $state, dataProvider, $rootScope, $modal) {
        dataProvider.loadBankAccounts().then(function(data) {
            $scope.bankAccounts = data;
            $scope.sortType = 'accountName'; // set the default sort type
            $scope.sortReverse = false; // set the default sort order
            $scope.searchAccounts = '';
        }, function() {

        });

        $scope.sortKey = "accountName";
        $scope.reverse = false;

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openModal = function(action, account) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/settings/accounts/edit.html',
                controller: "ModalController",
                backdrop: 'static',
                resolve: {
                    title: function() {
                        switch (action) {
                            case "add":
                                return "Adaugati un cont bancar";
                            case "edit":
                                return "Editati contul bancar";
                        }
                        return "Etapa: unknown action";
                    },
                    showDelete: function() {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function() {
                        var item = {};

                        switch (action) {
                            case "add":
                                item.id = "";
                                item.accountName = "";
                                item.owner = "";
                                item.currency = "";
                                item.bankName = "";
                                item.description = "";
                                item.details = "";

                                break;
                            case "edit":
                                item.id = account.id;
                                item.accountName = account.accountName;
                                item.owner = account.owner;
                    
                                item.currency = account.currency;
                                item.bankName = account.bankName;
                                item.description = account.description;
                                item.details = account.details;
                                break;
                        }
                        return item;
                    }
                }
            });

            $rootScope.extended.modalInstance.result.then(function(result) { //add function(ngToast)
                switch (result.action) {
                    case "save":
                        dataProvider.saveBankAccount(result.item).then(function(accountId) {
                            if (result.item.id == "") {
                                //actiune de add al unui serviciu
                                $scope.bankAccounts.push({
                                    id: accountId,
                                    accountName: result.item.accountName,
                                    owner: result.item.owner,
                    
                                    currency: result.item.currency,
                                    bankName: result.item.bankName,
                                    description: result.item.description,
                                    details: result.item.details
                                });
                                $rootScope.extended.ngToast.create({
                                    content: "Contul bancar a fost introdus cu succes!"
                                });
                            } else {
                                //actiune de edit al unui serviciu
                                var bankAccount = $.Enumerable.From($scope.bankAccounts).Where('bka => bka.id == ' + result.item.id).FirstOrDefault();
                                if (bankAccount != null) {
                                    bankAccount.accountName = result.item.accountName;
                                    bankAccount.owner = result.item.owner;
                        
                                    bankAccount.currency = result.item.currency;
                                    bankAccount.bankName = result.item.bankName;
                                    bankAccount.description = result.item.description;
                                    bankAccount.details = result.item.details;
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Contul bancar a fost editat cu succes!"
                                });
                            }
                        }, function() {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Contul bancar nu a putut fi adaugat!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Contul bancar nu a putut fi editat!"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteBankAccount(result.item).then(function(deleted) {
                                if (deleted) {
                                    //TODO optimizare cu WhereTo, indexOf si splice apoi
                                    for (var i = 0; i < $scope.bankAccounts.length; i++) {
                                        if ($scope.bankAccounts[i].id == result.item.id) {
                                            $scope.bankAccounts.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Contul bancar a fost sters cu succes!"
                                });
                            },
                            function() {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Contul bancar nu a putut fi sters!"
                                });
                            });
                        break;
                }
            }, function(reason) {
                //alert(reason);
            });
        }


    }]);
}())
