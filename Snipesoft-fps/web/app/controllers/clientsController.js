(function () {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('ClientsController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function ($scope, $state, dataProvider, $rootScope, $modal) {

        dataProvider.loadClients().then(function (data) {
            $scope.clients = data;
        }, function () {

        });

        $scope.sortKey = "name";
        $scope.reverse = false;

        $scope.itemsPerPage = 17;

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openModal = function (action, client) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/settings/clients/edit.html',
                controller: "ModalController",
                size: "lg",
                backdrop: 'static',
                resolve: {
                    title: function () {
                        switch (action) {
                            case "add":
                                return "Adaugati un client";
                            case "edit":
                                return "Editati clientul";
                        }
                        return "Etapa: unknown action";
                    },
                    showDelete: function () {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function () {
                        var caseLocationChange = function (caseFile, what) {
                            switch (what) {
                                case "building":
                                    caseFile.extended.stairs = null;
                                    caseFile.extended.location.stair = null;
                                    caseFile.extended.floors = null;
                                    caseFile.extended.location.floor = null;
                                    caseFile.extended.apartments = null;
                                    caseFile.extended.location.apartment = null;
                                                
                                    //reincarc lista de stairs
                                    dataProvider.loadLocation({ buildingId: caseFile.extended.location.building.id, stairs: "all" }).then(function (data) {
                                        caseFile.extended.stairs = data;
                                    });
                                    break;
                                case "stair":
                                    caseFile.extended.location.floor = null;
                                    caseFile.extended.location.apartment = null;
                                            
                                    //reincarc lista de floors
                                    dataProvider.loadLocation({ buildingId: caseFile.extended.location.building.id, stairs: caseFile.extended.location.stair.name, floor: "all" }).then(function (data) {
                                        caseFile.extended.floors = data;
                                    });
                                    break;
                                case "floor":
                                    caseFile.extended.location.apartment = null;
                                                
                                    //reincarc lista de apartments
                                    dataProvider.loadLocation({ buildingId: caseFile.extended.location.building.id, stairs: caseFile.extended.location.stair.name, floor: caseFile.extended.location.floor.name }).then(function (data) {
                                        caseFile.extended.apartments = data;
                                    });
                                    break;
                            }
                        }

                        var initializeNewCaseFile = function (item) {
                            item.extended.newCaseFile = {
                                id: "",
                                caseNumber: "",
                                caseDescription: "",
                                apartment: {
                                    stairs: "",
                                    floor: "",
                                    type: "",
                                    number: "",
                                    buildingId: ""
                                },
                                files: [],
                                extended: {
                                    filesToBeDeleted: [],
                                    newFiles: []
                                }
                            };
                            item.extended.expandCreateNewCaseFile = false;
                            item.extended.newCaseFile.extended.locationChange = caseLocationChange;
                            //incarc lista de blocuri
                            dataProvider.loadLocation({ buildingId: "all" }).then(function (data) {
                                item.extended.newCaseFile.extended.buildings = data;
                            });
                        }

                        var item = {};

                        switch (action) {
                            case "add":
                                item.contactPerson = {};

                                item.id = "";
                                item.name = "";
                                item.type = "Fizic";
                                item.RegNo = "";
                                item.mobile = "";
                                item.address = "";
                                item.email = "";
                                item.websiteOrFaceBook = "";
                                item.contactPerson.name = "";
                                item.contactPerson.mobile = "";
                                item.contactPerson.email = "";
                                item.details = "";
                                item.cases = [];

                                break;
                            case "edit":
                                item = angular.copy(client);
                                angular.forEach(item.cases, function (caseFile) {
                                    caseFile.extended = {};
                                    caseFile.extended.newFiles = [];
                                    caseFile.extended.filesToBeDeleted = [];

                                    caseFile.extended.location = {}
                                    //pentru fiecare dosar incarc lista de blocuri si setez blocul curent
                                    dataProvider.loadLocation({ buildingId: "all" }).then(function (data) {
                                        caseFile.extended.buildings = data;
                                        caseFile.extended.location.building = $.Enumerable.From(caseFile.extended.buildings).Where('b => b.id == ' + caseFile.apartment.buildingId).FirstOrDefault();
                                    });
                                    
                                    //pentru fiecare dosar incarc lista de stairs si setez stair curent
                                    dataProvider.loadLocation({ buildingId: caseFile.apartment.buildingId, stairs: "all" }).then(function (data) {
                                        caseFile.extended.stairs = data;
                                        caseFile.extended.location.stair = $.Enumerable.From(caseFile.extended.stairs).Where('s => s.name == "' + caseFile.apartment.stairs + '"').FirstOrDefault();
                                    });
                                    
                                    //pentru fiecare dosar incarc lista de floors si setez floor curent
                                    dataProvider.loadLocation({ buildingId: caseFile.apartment.buildingId, stairs: caseFile.apartment.stairs, floor: "all" }).then(function (data) {
                                        caseFile.extended.floors = data;
                                        caseFile.extended.location.floor = $.Enumerable.From(caseFile.extended.floors).Where('f => f.name == "' + caseFile.apartment.floor + '"').FirstOrDefault();
                                    });
                                    
                                    
                                    //pentru fiecare dosar incarc lista de apartments si setez apt curent
                                    dataProvider.loadLocation({ buildingId: caseFile.apartment.buildingId, stairs: caseFile.apartment.stairs, floor: caseFile.apartment.floor }).then(function (data) {
                                        caseFile.extended.apartments = data;
                                        caseFile.extended.location.apartment = $.Enumerable.From(caseFile.extended.apartments).Where('f => f.number == "' + caseFile.apartment.number + '"').FirstOrDefault();
                                    });

                                    caseFile.extended.locationChange = caseLocationChange;

                                    caseFile.extended.removeFile = function (caseFile, file) {
                                        var index = caseFile.files.indexOf(file);
                                        caseFile.files.splice(index, 1);
                                        caseFile.extended.filesToBeDeleted.push(file);
                                    };
                                });
                                break;
                        }

                        item.extended = {};

                        initializeNewCaseFile(item);

                        item.extended.addNewCaseFile = function (item, newCaseFile) {
                            var aux = angular.copy(newCaseFile);
                            item.cases.push(aux);

                            initializeNewCaseFile(item);
                        };

                        item.extended.cancelAddingNewCaseFile = function (item) {
                            initializeNewCaseFile(item);
                        }

                        return item;
                    }
                }
            });

            $rootScope.extended.modalInstance.result.then(function (result) {
                switch (result.action) {
                    case "save":
                        dataProvider.saveClient(result.item).then(function (insertId) {
                            //actiune de add al unui client
                            if (!(result.item.id)) {
                                dataProvider.loadClientById(insertId).then(function (data) {
                                    $scope.clients.push(data);
                                }, function () {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Clientul nu a putut fi incarcat!"
                                    });
                                });
                                $rootScope.extended.ngToast.create("Clientul a fost introdus cu success!");
                            } else {
                                //actiune de edit al unui client
                                dataProvider.loadClientById(result.item.id).then(function (data) {
                                    
                                    var client = $.Enumerable.From($scope.clients).Where('c => c.id == ' + result.item.id).FirstOrDefault();
                                    var index = $scope.clients.indexOf(client);

                                    if (index > -1) {
                                        $scope.clients[index] = data;
                                        $rootScope.extended.ngToast.create("Clientul a fost editat cu success!");
                                    }
                                }, function () {
                                    $rootScope.extended.ngToast.create({
                                        className: "danger",
                                        dismissButton: true,
                                        content: "Clientul nu a putut fi incarcat!"
                                    });
                                });
                            }
                        }, function () {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Clientul nu a putut fi adaugat!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Clientul nu a putut fi editat!"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteClient(result.item).then(function (deleted) {
                            if (deleted) {
                                //TODO optimizare cu WhereTo, indexOf si splice apoi
                                for (var i = 0; i < $scope.clients.length; i++) {
                                    if ($scope.clients[i].id == result.item.id) {
                                        $scope.clients.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                            $rootScope.extended.ngToast.create({
                                content: "Clientul a fost sters cu succes!"
                            });
                        },
                            function () {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Clientul nu a putut fi sters!"
                                });
                            });
                        break;
                }
            }, function (reason) {
                //alert(reason);
            });
        }


    }]);
} ())
