(function() {
    "use strict";

    var fps = angular.module('fps');
    fps.controller('SuppliersController', ["$scope", "$state", "DataProvider", "$rootScope", "$modal", function($scope, $state, dataProvider, $rootScope, $modal) {

        dataProvider.loadServiceGroups().then(function(data) {
            $scope.services = data;

            dataProvider.loadSuppliers().then(function(data) {
                $scope.suppliers = data;
            }, function() {
                $rootScope.extended.ngToast.create({
                    className: "danger",
                    dismissButton: true,
                    content: "Eroare la citirea listei de furnizori!"
                });
            });

        }, function() {
            $rootScope.extended.ngToast.create({
                className: "danger",
                dismissButton: true,
                content: "Eroare la citirea listei de servicii!"
            });
        });

        $scope.sortKey = "name";
        $scope.reverse = false;

        $scope.itemsPerPage = 17;

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        }

        $scope.openModal = function(action, supplier) {

            $rootScope.extended.modalInstance = $modal.open({
                keyboard: false,
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/settings/suppliers/edit.html',
                controller: "ModalController",
                backdrop: 'static',
                resolve: {
                    title: function() {
                        switch (action) {
                            case "add":
                                return "Adaugati un furnizor";
                            case "edit":
                                return "Editati furnizorul";
                        }
                        return "Furnizor: unknown action";
                    },
                    showDelete: function() {
                        switch (action) {
                            case "add":
                                return false;
                            case "edit":
                                return true;
                        }
                        return true;
                    },
                    item: function() {
                        var item = {};
                        item.extended = {};
                        item.extended.allServices = $scope.services;

                        item.extended.addService = function($item, item) {
                            var srv = $.Enumerable.From(item.extended.services).Where('srv => srv.id == ' + $item.id).FirstOrDefault();
                            if (srv == null) {
                                item.extended.services.push($item);
                            }

                            item.extended.newService = null;
                        }

                        item.extended.deleteService = function(service, item) {
                            for (var i = 0; i < item.extended.services.length; i++) {
                                if (item.extended.services[i].id == service.id) {
                                    item.extended.services.splice(i, 1);
                                    break;
                                }
                            }
                        }

                        switch (action) {
                            case "add":
                                item.extended.services = [];

                                break;
                            case "edit":
                                item.id = supplier.id;
                                item.name = supplier.name;
                                item.address = supplier.address;
                                item.CUI = supplier.CUI;
                                item.J = supplier.J;
                                item.telephoneFax = supplier.J;
                                item.mobile1 = supplier.mobile1;
                                item.mobile2 = supplier.mobile2;
                                item.email1 = supplier.email1;
                                item.email2 = supplier.email2;
                                item.website = supplier.website;
                                item.contactPerson = supplier.contactPerson;
                                item.details = supplier.details;

                                item.extended.services = $.map(supplier.services, function(ss) {
                                    var srv = $.Enumerable.From($scope.services).Where('srv => srv.id == ' + ss.id).FirstOrDefault();
                                    return srv;
                                });

                                break;
                        }

                        return item;
                    }
                }
            });

            $rootScope.extended.modalInstance.result.then(function(result) {
                switch (result.action) {
                    case "save":

                        result.item.services = $.map(result.item.extended.services, function(ies) {
                            return {
                                id: ies.id
                            }
                        });

                        dataProvider.saveSupplier(result.item).then(function(supplierId) {
                            if (!(result.item.id)) {
                                //actiune de add al unui serviciu
                                $scope.suppliers.push({
                                    id: supplierId,
                                    name: result.item.name,
                                    address: result.item.address,
                                    CUI: result.item.CUI,
                                    J: result.item.J,
                                    telephoneFax: result.item.telephoneFax,
                                    mobile1: result.item.mobile1,
                                    mobile2: result.item.mobile2,
                                    email1: result.item.email1,
                                    email2: result.item.email2,
                                    website: result.item.website,
                                    services: result.item.extended.services,
                                    contactPerson: result.item.contactPerson,
                                    details: result.item.details
                                });

                                $rootScope.extended.ngToast.create({
                                    content: "Furnizorul a fost introdus cu succes!"
                                });
                            } else {
                                //actiune de edit al unui serviciu
                                var supplier = $.Enumerable.From($scope.suppliers).Where('srv => srv.id == ' + result.item.id).FirstOrDefault();
                                if (supplier != null) {
                                    supplier.name = result.item.name;
                                    supplier.address = result.item.address;
                                    supplier.CUI = result.item.CUI;
                                    supplier.J = result.item.J;
                                    supplier.telephoneFax = result.item.telephoneFax;
                                    supplier.mobile1 = result.item.mobile1;
                                    supplier.mobile2 = result.item.mobile2;
                                    supplier.email1 = result.item.email1;
                                    supplier.email2 = result.item.email2;
                                    supplier.website = result.item.website;
                                    supplier.services = result.item.extended.services;
                                    supplier.contactPerson = result.item.contactPerson;
                                    supplier.details = result.item.details;

                                }
                                $rootScope.extended.ngToast.create({
                                    content: "Furnizorul a fost editat cu succes!"
                                });
                            }
                            //$scope.status.busy = false;
                        }, function() {
                            if (result.item.id == "") {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "furnizorul nu a putut fi adaugat!"
                                });
                            } else {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Furnizorul nu a putut fi editat!"
                                });
                            }
                        });
                        break;
                    case "delete":
                        dataProvider.deleteSupplier(result.item).then(function(deleted) {
                                if (deleted) {
                                    //TODO optimizare cu WhereTo, indexOf si splice apoi
                                    for (var i = 0; i < $scope.suppliers.length; i++) {
                                        if ($scope.suppliers[i].id == result.item.id) {
                                            $scope.suppliers.splice(i, 1);
                                            break;
                                        }
                                    }
                                }
                                $rootScope.extended.ngToast.create("Furnizorul a fost sters cu succes!");
                            },
                            function() {
                                $rootScope.extended.ngToast.create({
                                    className: "danger",
                                    dismissButton: true,
                                    content: "Furnizorul nu a putut fi sters!"
                                });
                            });
                        break;
                }
            }, function(reason) {
                //alert(reason);
            });
        }


    }]);
}())
