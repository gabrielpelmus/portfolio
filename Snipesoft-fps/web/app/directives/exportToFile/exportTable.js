(function() {
    //export html table to pdf, excel and doc format directive
    var fps = angular.module('fps');

    fps.directive('exportTable', function() {

            return {
                restrict: 'C',
                link: function($scope, elm, attr) {

                    $scope.$on('export-pdf', function(e, d) {
                        $scope.fontSize=d.fontSize;
                        $scope.theme=d.theme;
                        $scope.filename=d.filename;
                        $scope.reportName=d.reportName;
                        elm.tableExport({
                            type: 'pdf',
                            pdfFontSize: $scope.fontSize,
                            pdfTheme: $scope.theme,
                            escape: 'false',
                            filename: $scope.filename,
                            reportName: $scope.reportName
                        });
                    });

                    $scope.$on('export-excel', function(e, d) {
                        elm.tableExport({
                            type: 'excel',
                            escape: false,
                            filename: $scope.filename,
                            reportName: $scope.reportName
                        });
                    });

                }
            }
        }

    )
}());
