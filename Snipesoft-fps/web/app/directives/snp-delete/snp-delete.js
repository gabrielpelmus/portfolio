(function() {

    var fps = angular.module('fps');
    fps.directive('snpDelete', function() {
        return {
            templateUrl: "app/directives/snp-delete/template.html",
            restrict: 'E',
            transclude: false,
            replace: true,
            scope: {
                confirm : '&',
                size: "@size"
            },
            controller: ['$scope', function($scope){
                $scope.confirmationGroupVisible = false;
                if(!$scope.size) $scope.size = "btn-lg";
            }]
        };
    });
}())
