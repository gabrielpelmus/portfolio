var fps = angular.module("fps");


fps.directive('snpUpload', function () {
    return {
        restrict: 'E',
        templateUrl: "app/directives/snp-upload/template.html",
        replace: true,
        transclude: false,
        scope: {
            __userFiles: '=files',
            __root: '=rootScope'
        },
        link: function (scope, el, attr) {
            var fileName,
                uri;

            fileName = "uploadedFile";
            uri = attr.uri || "api/media";

            function uploadFile(file, uri, index) {
                var xhr = new XMLHttpRequest(),
                    fd = new FormData(),
                    progress = 0;

                xhr.open('POST', uri, true);
                xhr.onreadystatechange = function () {
                    if (xhr.status == 200) {
                        scope.__userFiles[index].status = {
                            code: xhr.status,
                            statusText: xhr.statusText,
                            response: xhr.response
                        };
                    } else {
                        if (scope.__userFiles[index]) {
                            var errMessage = "Upload esuat.";
                            if (xhr.status == 400) errMessage += "Fisierul este prea mare.";
                            scope.$root.extended.ngToast.create({ className: "danger", content: errMessage, animation: "slide" });                            
                            scope.__userFiles[index] = null;
                        }
                    }

                    scope.$apply();
                };
                xhr.upload.addEventListener("progress", function (e) {
                    progress = parseInt(e.loaded / e.total * 100);
                    scope.__userFiles[index].percent = progress;
                    scope.$apply();
                }, false);
                fd.append(fileName, file);                
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('jwtToken'));                
                xhr.send(fd);
                return {
                    name: file.name,
                    size: file.size,
                    type: file.type,
                    status: {},
                    percent: 0
                }
            }

            $(el.find('button')[0]).bind('click', function () {
                scope.$eval(el.find('input')[0].click());
            });

            scope.removeFile = function (removedFile) {
                var index = scope.__userFiles.indexOf(removedFile);
                scope.__userFiles.splice(index, 1);
            }

            angular.element(el.find('input')[0]).bind('change', function (e) {
                var files = e.target.files;
                if (!scope.__userFiles) scope.__userFiles = [];
                var initialLength = scope.__userFiles.length;
                for (var i = 0, f; f = files[i]; i++) {
                    scope.__userFiles.push(uploadFile(f, uri, i + initialLength));
                }

                e.target.files = [];
                e.target.value = '';
            })
        }
    }
});
