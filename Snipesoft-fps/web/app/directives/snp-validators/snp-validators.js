(function() {

    var fps = angular.module('fps');
    fps.directive('snpIsObject', function() {

        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(value) {
                    if (!value || value.length == 0) return;

                    ngModel.$setValidity('snpIsObject', typeof value === "object");
                    return value;
                })
            }
        }
    });

    fps.directive('snpLowerThan', [
        function() {
            var link = function($scope, $element, $attrs, ctrl) {
                var validate = function(viewValue) {
                    var comparisonModel = $attrs.snpLowerThan;
                    if (!viewValue || !comparisonModel) {
                        // It's valid because we have nothing to compare against
                        ctrl.$setValidity('snpLowerThan', true);
                    }
                    // It's valid if model is lower than the model we're comparing against
                    ctrl.$setValidity('snpLowerThan', parseFloat(viewValue).toFixed(10) <= parseFloat(comparisonModel, 10).toFixed(10));
                    return viewValue;
                };
                ctrl.$parsers.unshift(validate);
                ctrl.$formatters.push(validate);

                $attrs.$observe('snpLowerThan', function(comparisonModel) {
                    // Whenever the comparison model changes we'll re-validate
                    return validate(ctrl.$viewValue);
                });
            };
            return {
                require: 'ngModel',
                link: link
            };
        }
    ]);
}())
