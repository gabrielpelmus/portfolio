(function() {

    var fps = angular.module('fps');
    fps.directive('snpEsc', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 27) {
                    scope.$apply(function() {
                        scope.$eval(attrs.snpEsc);
                    });

                    event.preventDefault();
                }
            });
        };
    });
}())
