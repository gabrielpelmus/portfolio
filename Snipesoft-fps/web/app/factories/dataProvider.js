(function () {
    "use strict";
    var server = "api/";
    var batchEndpoint = "../batch";
    var fps = angular.module("fps");
    
    var dataProvider = function ($http, $q) {

        /*BEGIN Login*/
        var login = function (username, password) {
            var data = {};
            data.username = username;
            data.password = password;

            var def = $q.defer();
            $http.post(server + "login", data).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(response);
            });
            return def.promise;
        }
        /*END Login*/

        var validateCRT = function (crt) {
            var def = $q.defer();
            $http.get(server + "validateCRT/" + crt).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        /*start invoices*/
        var loadInvoiceBundle = function () {
            var def = $q.defer();
            var batchPayload = { "requests": [
                    {"method": "get", "path": "/fps/api/suppliers"},
                    {"method": "get", "path": "/fps/api/paymentTypes"},
                    {"method": "get", "path": "/fps/api/buildings"},
                    {"method": "get", "path": "/fps/api/invoiceStatuses"},
                    {"method": "get", "path": "/fps/api/invoiceTypes"},
                    {"method": "get", "path": "/fps/api/invoices"}
                ] };
            $http.post(batchEndpoint, batchPayload).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadInvoices = function () {
            var def = $q.defer();
            $http.get(server + "invoices").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        
        var loadInvoiceById = function (invoiceId) {
            var def = $q.defer();
            $http.get(server + "invoices/" + invoiceId).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveInvoice = function (invoice) {
            var def = $q.defer();

            var data = {};

            var sgId = "null";

            if (invoice.extended.supplierServiceGroup) {
                sgId = invoice.extended.supplierServiceGroup.id;
            }

            data.id = invoice.id;
            data.supplierId = invoice.extended.supplier.id;
            data.serviceGroupId = sgId;
            data.buildingId = invoice.extended.building.id;
            data.externalNumber = invoice.externalNumber;
            data.amount = invoice.amount;
            data.currency = invoice.currency;
            data.createDT = invoice.createDT;
            data.dueDT = invoice.dueDT;
            data.paymentDT = invoice.paymentDT;
            data.details = invoice.details;
            data.statusId = invoice.statusId;
            data.invoiceTypeId = invoice.extended.invoiceType.id;
            data.hasIssue = invoice.hasIssue;
            data.issueDescription = invoice.issueDescription;
            data.lastUpdateUserId = invoice.lastUpdateUserId;
            data.itemTypeName = invoice.itemTypeName;
            data.statusName = invoice.statusName;
            data.supplierName = invoice.supplierName;
            data.buildingName = invoice.buildingName;
            data.serviceGroupName = invoice.serviceGroupName;
            data.files = invoice.files;
            data.filesToBeDeleted = invoice.filesToBeDeleted;
            data.newFiles = invoice.newFiles;

            if (invoice.id === "") {
                $http.post(server + "invoices", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "invoices/" + invoice.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteInvoice = function (invoice) {
            var def = $q.defer();
            $http.delete(server + "invoices/" + invoice.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*end invoices*/
        /* start paymentTypes*/
        var loadPaymentTypes = function () {
            var def = $q.defer();
            $http.get(server + "paymentTypes").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*end paymentTypes*/

        /* start payments */
        var loadInvoicePaymentsHistory = function (invoiceId) {
            if (!invoiceId) return;
            var def = $q.defer();
            $http.get(server + "payments/" + invoiceId).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var savePayments = function (payment) {
            var def = $q.defer();

            var data = angular.copy(payment);

            if (payment.id === "") {
                $http.post(server + "payments", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });

            } else {
                $http.put(server + "payments/" + payment.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });
            }
            return def.promise;
        };


        var deletePayments = function (payment) {
            var def = $q.defer();

            var data = angular.copy(payment);

            $http.put(server + "payments", data).then(function (response) {
                def.resolve();
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(response);
            });
            
            return def.promise;
        };
        /* end payments*/
        
        /* start invoice statuses*/
        var loadInvoiceStatuses = function () {
            var def = $q.defer();
            $http.get(server + "invoiceStatuses").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*end invoice statuses*/
        
        /* start invoice types*/
        var loadInvoiceTypes = function () {
            var def = $q.defer();
            $http.get(server + "invoiceTypes").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*end invoice types*/
        
        /*start cases*/
        var loadCases = function () {
            var def = $q.defer();
            $http.get(server + "cases").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*end cases*/
        
        /*start incoms*/
        var loadIncoms = function () {
            var def = $q.defer();
            $http.get(server + "incoms").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadIncomsBundle = function () {
            var def = $q.defer();
            var batchPayload = { "requests": [
                    {"method": "get", "path": "/fps/api/buildings"},
                    {"method": "get", "path": "/fps/api/apartments"}
                ] };
            $http.post(batchEndpoint, batchPayload).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadIncomeById = function (id) {
            if (!id) return;
            var def = $q.defer();
            $http.get(server + "incoms/" + id).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveIncoms = function (income) {
            var def = $q.defer();

            var data = angular.copy(income);
            delete data.extended;

            if (income.id === "") {
                $http.post(server + "incoms", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });

            } else {
                $http.put(server + "incoms/" + income.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });
            }
            return def.promise;
        };

        var deleteIncoms = function (ins) {
            var def = $q.defer();
            $http.delete(server + "incoms/" + ins.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*end incoms*/
        
        /*start transfer*/
        var loadTransferById = function (id) {
            var def = $q.defer();
            $http.get(server + "transfers/" + id).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadTransfers = function () {
            var def = $q.defer();
            $http.get(server + "transfers").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveTransfer = function (trans) {
            var def = $q.defer();

            var data = angular.copy(trans);
            delete data.extended;

            if (!(trans.id)) {
                $http.post(server + "transfers", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });

            } else {
                $http.put(server + "transfers/" + trans.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject(response);
                });
            }
            return def.promise;
        };

        var deleteTransfer = function (trans) {
            var def = $q.defer();
            $http.delete(server + "transfers/" + trans.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*end transfer*/
        
        /*BEGIN services*/
        var loadServiceGroups = function () {
            var def = $q.defer();
            $http.get(server + "serviceGroups").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveServiceGroup = function (service) {
            var def = $q.defer();

            var data = {};
            data.name = service.name;
            data.description = service.description;
            data.details = service.details;

            if (!(service.id)) {
                $http.post(server + "serviceGroups", data).then(function (response) {
                    def.resolve(response);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "serviceGroups/" + service.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteService = function (service) {
            var def = $q.defer();
            $http.delete(server + "serviceGroups/" + service.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*END services*/

        /*BEGIN suppliers*/
        var loadSuppliers = function () {
            var def = $q.defer();
            $http.get(server + "suppliers").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveSupplier = function (supplier) {
            var def = $q.defer();

            var data = {};
            data.name = supplier.name;
            data.address = supplier.address;
            data.CUI = supplier.CUI;
            data.J = supplier.J;
            data.telephoneFax = supplier.telephoneFax;
            data.mobile1 = supplier.mobile1;
            data.mobile2 = supplier.mobile2;
            data.email1 = supplier.email1;
            data.email2 = supplier.email2;
            data.website = supplier.website;
            data.services = supplier.services;
            data.contactPerson = supplier.contactPerson;
            data.details = supplier.details;

            if (!(supplier.id)) {
                $http.post(server + "suppliers", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);//response.data[0].insertId
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "suppliers/" + supplier.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteSupplier = function (supplier) {
            var def = $q.defer();
            $http.delete(server + "suppliers/" + supplier.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*END suppliers*/
        
        /*BEGIN users*/
        var loadUsers = function () {
            var def = $q.defer();
            $http.get(server + "users").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*END users*/

        /*BEGIN buildings*/
        var loadBuildings = function () {
            var def = $q.defer();
            $http.get(server + "buildings").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveBuilding = function (building) {
            var def = $q.defer();

            var data = {};
            data.name = building.name;
            data.address = building.address;
            data.details = building.details;

            if (building.id === "") {
                $http.post(server + "buildings", data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "buildings/" + building.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteBuilding = function (building) {
            var def = $q.defer();
            $http.delete(server + "buildings/" + building.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*END buildings*/

        /*BEGIN bankAccounts*/
        var loadBankAccounts = function () {
            var def = $q.defer();
            $http.get(server + "bankaccounts").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveBankAccount = function (bankAccount) {
            var def = $q.defer();

            var data = {};
            data.accountName = bankAccount.accountName;
            data.owner = bankAccount.owner;
            data.IBAN = bankAccount.IBAN;
            data.currency = saveBankAccount.currency;
            data.bankName = bankAccount.bankName;
            data.description = bankAccount.description;
            data.details = bankAccount.details;

            if (bankAccount.id === "") {
                $http.post(server + "bankaccounts", data).then(function (response) {
                    def.resolve(); //response.data.insertId
                    console.log(response.data);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "bankaccounts/" + bankAccount.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteBankAccount = function (bankAccount) {
            var def = $q.defer();
            $http.delete(server + "bankaccounts/" + bankAccount.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*END bankAccounts*/

        /*BEGIN clients*/
        var loadClients = function () {
            var def = $q.defer();
            $http.get(server + "clients").then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        
         var loadClientById = function (id) {
             if(!id) return;
            var def = $q.defer();
            $http.get(server + "clients/" + id).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var saveClient = function (client) {
            var def = $q.defer();

            var data = angular.copy(client);
            angular.forEach(data.cases, function (caseFile) {
                caseFile.apartment = {
                        id: caseFile.extended.location.apartment.id,
                        stairs: caseFile.extended.location.stair.name,
                        floor: caseFile.extended.location.floor.name,
                        type: caseFile.extended.location.apartment.type,
                        number: caseFile.extended.location.apartment.number,
                        buildingId: caseFile.extended.location.building.id
                }
                caseFile.newFiles = angular.copy(caseFile.extended.newFiles);
                caseFile.filesToBeDeleted = angular.copy(caseFile.extended.filesToBeDeleted);
                delete caseFile.extended;
            });
            delete data.extended;

            if (data.id === "") {
                $http.post(server + "clients", data).then(function (response) {
                    def.resolve(response.data[0][1][0].insertId);
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });

            } else {
                $http.put(server + "clients/" + data.id, data).then(function (response) {
                    def.resolve();
                }, function (response) {
                    console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                    def.reject();
                });
            }
            return def.promise;
        };

        var deleteClient = function (client) {
            var def = $q.defer();
            $http.delete(server + "clients/" + client.id).then(function (response) {
                def.resolve(true);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        };
        /*END clients*/

        /*START reports*/
        var loadIncomeByDataRange = function (startDate, endDate, buildingName, apType) {
            //if (!startDate||!endDate) return;
            var def = $q.defer();
            var multiFilter = false;
            var serverReq = server+"reports/incomes?";

            if(startDate){
                serverReq += "startDate="+startDate;
                multiFilter = true;
            }
            if(endDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "endDate="+endDate;
                multiFilter = true;
            }
            if(buildingName){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "buildingName="+buildingName;
                multiFilter = true;
            }
            if(apType){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "apType="+apType;
                multiFilter = true;
            }

            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadTransferByDataRange = function (type, startDate, endDate) {
            var def = $q.defer();
            var multiFilter = false;
            var serverReq = server+"reports/transfers?";
            if(type){
                serverReq += "type="+type;
                multiFilter = true;
            }
            if(startDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "startDate="+startDate;
                multiFilter = true;
            }
            if(endDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "endDate="+endDate;
            }
            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadOverviewByDataRange = function (startDate, endDate) {
            //if (!startDate||!endDate) return;
            var def = $q.defer();
            var serverReq = server+"reports/overview?";
            if(startDate){
                serverReq += "startDate="+startDate;
            }
            if(startDate&&endDate){
                serverReq+="&";
            }
            if(endDate){
                serverReq+="endDate="+endDate;
            }
            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadInvoicesByDataRange = function (startDate, endDate, supplierName, serviceGroups, building, invoiceType) {
            var def = $q.defer();
            var multiFilter = false;
            var serverReq = server + "reports/invoices?";
            if(startDate){
                serverReq += "startDate="+startDate;
                multiFilter = true;
            }
            
            if(endDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq+="endDate="+endDate;
                multiFilter = true
            }

            if(supplierName){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq+="supplierName="+supplierName;
                multiFilter=true;
            }

            if(serviceGroups){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq+="serviceGroups="+serviceGroups;
                multiFilter=true;
            }

            if(building){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq+="building="+building;
                multiFilter=true;
            }

            if(invoiceType){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq+="invoiceType="+invoiceType;
                multiFilter=true;
            }
            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*END reports*/

        /*START charts*/
        var loadOverviewChartData = function (startDate, endDate) {
            //if (!startDate||!endDate) return;
            var def = $q.defer();
            var serverReq = server+"charts/overview?";
            if(startDate){
                serverReq += "startDate="+startDate;
            }
            if(startDate&&endDate){
                serverReq+="&";
            }
            if(endDate){
                serverReq+="endDate="+endDate;
            }
            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadTransfersChartData = function (type, startDate, endDate) {
            var def = $q.defer();
            var multiFilter = false;
            var serverReq = server+"charts/transfers?";
            if(type){
                serverReq += "type="+type;
                multiFilter = true;
            }
            if(startDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "startDate="+startDate;
            }
            if(endDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "endDate="+endDate;
            }
            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };

        var loadIncomesChartData = function (startDate, endDate, buildingName, apType) {
            //if (!startDate||!endDate) return;
            var def = $q.defer();
            var multiFilter = false;
            var serverReq = server+"charts/incomes?";

            if(startDate){
                serverReq += "startDate="+startDate;
                multiFilter = true;
            }
            if(endDate){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "endDate="+endDate;
                multiFilter = true;
            }
            if(buildingName){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "buildingName="+buildingName;
                multiFilter = true;
            }
            if(apType){
                if(multiFilter){
                    serverReq+="&";
                }
                serverReq += "apType="+apType;
                multiFilter = true;
            }

            $http.get(serverReq).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject();
            });
            return def.promise;
        };
        /*END charts*/

        var loadLocation = function (queryString) {
            var def = $q.defer();
            $http.get(server + "location?" + $.param(queryString)).then(function (response) {
                def.resolve(response.data);
            }, function (response) {
                console.log("%c" + JSON.stringify(response), window.customConsoleCss.error);
                def.reject(false);
            });
            return def.promise;
        }

        return {
            login: login,
            validateCRT: validateCRT,
            loadInvoiceBundle: loadInvoiceBundle,
            loadInvoices: loadInvoices,
            loadInvoiceById: loadInvoiceById,
            loadInvoicesByDataRange: loadInvoicesByDataRange,
            saveInvoice: saveInvoice,
            deleteInvoice: deleteInvoice,
            loadPaymentTypes: loadPaymentTypes,
            savePayments: savePayments,
            deletePayments: deletePayments,
            loadInvoicePaymentsHistory: loadInvoicePaymentsHistory,
            loadInvoiceStatuses: loadInvoiceStatuses,
            loadInvoiceTypes: loadInvoiceTypes,
            loadCases: loadCases,
            loadIncoms: loadIncoms,
            loadIncomsBundle: loadIncomsBundle,
            loadIncomeById: loadIncomeById,
            loadIncomeByDataRange: loadIncomeByDataRange,
            saveIncoms: saveIncoms,
            deleteIncoms: deleteIncoms,
            loadTransferById: loadTransferById,
            loadTransferByDataRange: loadTransferByDataRange,
            loadTransfers: loadTransfers,
            saveTransfer: saveTransfer,
            deleteTransfer: deleteTransfer,
            loadServiceGroups: loadServiceGroups,
            saveServiceGroup: saveServiceGroup,
            deleteService: deleteService,
            loadSuppliers: loadSuppliers,
            saveSupplier: saveSupplier,
            deleteSupplier: deleteSupplier,
            loadUsers: loadUsers,
            loadBuildings: loadBuildings,
            saveBuilding: saveBuilding,
            deleteBuilding: deleteBuilding,
            loadBankAccounts: loadBankAccounts,
            saveBankAccount: saveBankAccount,
            deleteBankAccount: deleteBankAccount,
            loadClients: loadClients,
            loadClientById : loadClientById,
            saveClient: saveClient,
            deleteClient: deleteClient,
            loadOverviewByDataRange: loadOverviewByDataRange,
            loadOverviewChartData: loadOverviewChartData,
            loadTransfersChartData: loadTransfersChartData,
            loadIncomesChartData: loadIncomesChartData,
            loadLocation: loadLocation
        }
    }

    fps.factory("DataProvider", ["$http", "$q", dataProvider]);
} ());
