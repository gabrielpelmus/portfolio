window.customConsoleCss = {
    "stateInfo": 'background: #222; color: #bada55',
    "error": 'background: #F00; color: #FFF'
};

var fps = angular.module('fps', ["ui.router", "ui.bootstrap", "ngAnimate", "ncy-angular-breadcrumb", "ui.bootstrap.datetimepicker", "ngToast", "angularUtils.directives.dirPagination", "ngMessages", "angular-jwt", "nvd3", "ui.filters"]);

fps.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$breadcrumbProvider', 'jwtInterceptorProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $breadcrumbProvider, jwtInterceptorProvider) {

    jwtInterceptorProvider.tokenGetter = ['config', function (config) {
        if (config.url.substr(config.url.length - 5) == '.html') {
            return null;
        }
        return localStorage.getItem('jwtToken');
    }];
    $httpProvider.interceptors.push('jwtInterceptor');

    $breadcrumbProvider.setOptions({
        prefixStateName: 'app.home',
        // includeAbstract : true
    });

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('app', {
            url: '',
            abstract: true,
            templateUrl: 'app/views/shell.html',
            controller: "AppController",
            data: {
                requireLogin: true
            },
            ncyBreadcrumb: {
                label: 'app'
            }
        })
        .state('app.home', {
            url: '/home',
            templateUrl: 'app/views/home.html',
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Home'
            }
        })
        .state('app.invoices', {
            url: '/facturi',
            templateUrl: 'app/views/invoices/overview.html',
            controller: "InvoicesController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Facturi'
            }
        })
        .state('app.incoms', {
            url: '/intrari',
            templateUrl: 'app/views/incoms/overview.html',
            controller: "IncomsController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Intrari clienti'
            }
        })
        .state('app.transfers', {
            url: '/transferuri',
            templateUrl: 'app/views/transfers/overview.html',
            controller: "TransfersController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Transferuri'
            }
        })
        .state('app.reports', {
            url: '/rapoarte',
            templateUrl: 'app/views/reports/reports.html',
            controller: "ReportsController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Raportari'
            }
        })
        .state('app.reports.overview', {
            url: '/raportare-generala',
            templateUrl: 'app/views/reports/reportOverview.html',
            controller: "ReportsController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'General'
            }
        })
        .state('app.reports.transfers', {
            url: '/rapoarte-transferuri',
            templateUrl: 'app/views/reports/reportTransfers.html',
            controller: "ReportTransfersController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Transferuri'
            }
        })
        .state('app.reports.incoms', {
            url: '/rapoarte-intrari-clienti',
            templateUrl: 'app/views/reports/reportIncoms.html',
            controller: "ReportIncomsController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Intrari de la clienti'
            }
        })
        .state('app.reports.invoices', {
            url: '/rapoarte-plati',
            templateUrl: 'app/views/reports/reportInvoices.html',
            controller: "ReportInvoicesController",
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Facturi si plati'
            }
        })
        .state('app.settings', {
            url: '/setari',
            templateUrl: 'app/views/settings.html',
            // abstract:true,
            data: {
                roles: [1, 2, 3, 4]
            },
            ncyBreadcrumb: {
                label: 'Setari'
            }
        })
        .state('app.settings.suppliers', {
            url: '/furnizori',
            templateUrl: 'app/views/settings/suppliers/overview.html',
            controller: "SuppliersController",
            ncyBreadcrumb: {
                label: 'Configurare furnizori'
            }
        })
        .state('app.settings.services', {
            url: '/servicii',
            templateUrl: 'app/views/settings/services/overview.html',
            controller: "ServicesController",
            ncyBreadcrumb: {
                label: 'Configurare servicii'
            }
        })
        .state('app.settings.clients', {
            url: '/clienti',
            templateUrl: 'app/views/settings/clients/overview.html',
            controller: "ClientsController",
            ncyBreadcrumb: {
                label: 'Configurare clienti'
            }
        })
        .state('app.settings.accounts', {
            url: '/conturi',
            templateUrl: 'app/views/settings/accounts/overview.html',
            controller: "AccountsController",
            ncyBreadcrumb: {
                label: 'Configurare conturi bancare'
            }
        })
        .state('app.settings.buildings', {
            url: '/blocuri',
            templateUrl: 'app/views/settings/buildings/overview.html',
            controller: "BuildingsController",
            ncyBreadcrumb: {
                label: 'Configurare cladiri'
            }
        })
        .state('app.settings.users', {
            url: '/utilizatori',
            templateUrl: 'app/views/settings/users/overview.html',
            controller: "UsersController",
            ncyBreadcrumb: {
                label: 'Configurare utilizatori'
            }
        })


        .state('login', {
            url: '/autentificare',
            controller: "LoginController",
            templateUrl: 'app/views/login/overview.html',
            data: {
                requireLogin: false,
                roles: [1, 2, 3, 4]
            }
        })
        .state('unauthorized', {
            url: '/unauthorized',
            controller: "LoginController",
            templateUrl: 'app/views/login/unauthorized.html',
            data: {
                requireLogin: false,
                roles: [1, 2, 3, 4]
            }
        });

}]);

fps.run(["$state", "$stateParams", "$rootScope", "jwtHelper", "$http", function ($state, $stateParams, $rootScope, jwtHelper, $http) {
    $rootScope.$stateParams = $stateParams;
    $rootScope.extended = {};

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        //close the modal if opened
        if ($rootScope.extended && $rootScope.extended.modalInstance) {
            $rootScope.extended.modalInstance.dismiss('cancel');
        }

        var token = localStorage.getItem("jwtToken");

        if (toState.data.requireLogin) {
            if (token == undefined) {
                event.preventDefault();
                $rootScope.$broadcast('loadStateAfterLoginOK', toState);
                $state.go("login");
                return;
            }
            if (jwtHelper.isTokenExpired(token)) {
                event.preventDefault();
                localStorage.removeItem("jwtToken");
                $rootScope.$broadcast('loadStateAfterLoginOK', toState);
                $state.go("login");
                return;
            }
            var user = jwtHelper.getUserData(token);
            if (user.role != undefined && toState.data.roles.indexOf(user.role) == -1) {
                event.preventDefault();
                localStorage.removeItem("jwtToken");
                $state.go("unauthorized");
                return;
            }
        }
        //TODO daca e deja logat si acceseaza login, redirecteaza pe home
        if (toState.name == "login" && token != undefined && !jwtHelper.isTokenExpired(token)) {
            event.preventDefault();
            $state.go("app.home");
        }


    });
    
    $rootScope.snpUtils = {};
    $rootScope.snpUtils.download = function (arg) {
        console.log(arg);

        var requestConfig = {
            method: 'Get',
            url: arg,
            responseType: 'arraybuffer',
            cache: 'true'
        };

        $http(requestConfig)
            .success(function (response, status, headers, config) {
                var fileName = "download";
                if (headers()["content-disposition"]) {
                    fileName = headers()["content-disposition"].match(/filename=(\S+)/)[1];    
                };
                
                var arr = new Uint8Array(response);
                
                var blob = new Blob([arr], {type:"application/octet-stream"});
                var url = window.URL.createObjectURL(blob);
                var a = document.createElement("a");
                document.body.appendChild(a);                
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
                document.body.removeChild(a);
            })
            .error(function (data, status) {
                console.log("eroare");
                $rootScope.extended.ngToast.create({
                    className: "danger",
                    dismissButton: true,
                    content: "Fisierul nu a putut fi descarcat!"
                });
            });
    }

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        console.log('%c Navigate to state "' + toState.name + '" params(' + JSON.stringify(toParams) + '), Controller: ' + toState.controller, window.customConsoleCss.stateInfo);

    });

    $rootScope.$on('unauthenticated', function (event, response) {
        console.log("%c NEAUTENTIFICAT", window.customConsoleCss.stateInfo);
        event.preventDefault();
        $state.go("login");
    });

}]);
