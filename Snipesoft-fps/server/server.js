var Hapi = require('hapi');
var fs = require('fs');

var server = new Hapi.Server();
var httpOptions = {
    host: "localhost",
    port: 80,
    routes: {
        validate: {
            options: {
                stripUnknown: false,
                allowUnknown: true
            }
        }
    }
};
server.connection(httpOptions);

server.register(require('hapi-auth-jwt'), (err) => {
    if (err) {
        console.error('Plug-in-ul JWT nu a putut fi incarcat:', err);
    }
});
server.register({
        register: require('bassmaster'),
        options: { batchEndpoint: "/batch" }
    }, (err) => {
        if (err) {
            console.error('Plug-in-ul JWT nu a putut fi incarcat:', err);
        }
    });

server.register(require('./lib/fps'), (err) => {
    if (err) {
        console.error('Plug-in-ul FPS nu a putut fi incarcat:', err);
    }
});

server.start(function () {
    console.log('Server running at:', server.info.uri);
});