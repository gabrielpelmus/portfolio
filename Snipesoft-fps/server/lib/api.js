var mysql = require('mysql');
var db = require('../db');
var _ = require('underscore');
var constants = require('../config');
var jwt = require('jsonwebtoken');

var tempPath = __dirname + "/../temp/";
var mediaPath = __dirname + "/../media/";

var dbCallback = function(err, data, reply, query) {
    if (err) {
        var error = {
            dbMessage: err,
            details: {
                warning: "[DISABLE IN PRODUCTION!] - to be replaced with app-wide db error log ",
                query: query
            },
            //operationCode: 999
        };
        reply(error).code(500);
        return;
    } else
        reply(data).code(200);
}

exports.location = {
    all: function(request, reply) {
        var params = request.query;
        var query = {};

        if (params.buildingId == "all") {
            query = {
                statement: "SELECT id, name FROM building where isDeleted = 0;",
                values: []
            };
        } else {
            if (params.stairs == "all") {
                query = {
                    statement: "select stairs as name from apartment where buildingId = ? group by stairs;",
                    values: [params.buildingId]
                };
            } else {
                if (params.floor == "all") {
                    query = {
                        statement: "select floor as name from apartment where buildingId = ? and stairs = ? group by floor order by convert(floor, unsigned);",
                        values: [params.buildingId, params.stairs]
                    };
                } else {
                    query = {
                        statement: "select id, apNumber as number, apType as type from apartment where buildingId = ? and stairs = ? and floor = ? order by convert(apNumber, unsigned);",
                        values: [params.buildingId, params.stairs, params.floor]
                    };
                }
            }

        }

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

var authenticate = function(username, password, onSuccess, onError) {
    var query = {
        statement: "SELECT * FROM vuser where userName = ? and password = ? LIMIT 1;",
        values: [username, password]
    };

    db.query({
        sql: query.statement,
        values: query.values,
        callback: function(err, data) {
            if (err == null) {
                if (data.length > 0) {
                    onSuccess(data[0]);
                } else {
                    onError("Invalid credentials");
                }
            } else
                onError(err.code);
        }
    });
}

exports.auth = {
    login: function(request, reply) {
        var jsonUser = request.payload;

        if (jsonUser.username && jsonUser.password) {
            authenticate(jsonUser.username, jsonUser.password, function(data) {

                var token = jwt.sign({
                    username: data.userName,
                    id: data.id,
                    firstName: data.firstName,
                    role: data.roleId
                }, constants.security.privateKey, {
                    expiresIn: constants.security.tokenExpiry
                });

                reply(token).code(200);
            }, function(err) {
                var errCode = 401;                
                if (err == "ECONNREFUSED") errCode = 500;
                
                reply({
                    reason: err
                }).code(errCode);
            });
        } else {
            reply("Username or password: invalid format").code(401);
        }
    }
}

exports.apartments = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vapartment;"
        };

        db.query({
            sql: query.statement,

            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {

                    var dataSet = {
                        id: group[0].id,
                        stairs: group[0].stairs,
                        floor: group[0].floor,
                        apType: group[0].apType,
                        apNumber: group[0].apNumber,

                        buildings: _.map(group, function(building) {
                            return {
                                id: building.buildingId,
                                name: building.buildingName
                            }
                        })
                    }

                    if (dataSet.buildings[0].id == null)
                        dataSet.buildings = [];

                    return dataSet;
                });
                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vapartment where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL insertApartment(@buildingId := ?, @caseId := ?, @stairs := ?, @floor := ?, @apType := ?, @apNumber := ?, @lastUpdateUserId := ?, @insertId);",
            values: [params.buildingId,
                params.caseId,
                params.stairs,
                params.floor,
                params.apType,
                params.apNumber,
                10
            ]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL updateApartment(@id :=?, @buildingId := ?, @caseId := ?, @stairs := ?, @floor := ?, @apType := ?, @apNumber := ?, @lastUpdateUserId := ?);",
            values: [request.params.id,
                params.buildingId,
                params.caseId,
                params.stairs,
                params.floor,
                params.apType,
                params.apNumber,
                10
            ]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteApartment(@id := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.serviceGroups = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vservicegroup;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vservicegroup where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL insertServiceGroup(@name := ?, @description := ?, @details := ?, @lastUpdateUserId := ?, @insertServiceId); SELECT @insertServiceId AS insertServiceId",
            values: [params.name, params.description, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL updateServiceGroup(@id := ?, @name := ?, @description := ?, @details := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, params.name, params.description, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteServiceGroup(@id := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.suppliers = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vsupplier;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {

                    var dataSet = {
                        id: group[0].id,
                        name: group[0].name,
                        address: group[0].address,
                        CUI: group[0].CUI,
                        J: group[0].J,
                        telephoneFax: group[0].telephoneFax,
                        mobile1: group[0].mobile1,
                        mobile2: group[0].mobile2,
                        email1: group[0].email1,
                        email2: group[0].email2,
                        website: group[0].website,
                        details: group[0].details,

                        services: _.map(group, function(service) {
                            return {
                                id: service.serviceId,
                                name: service.serviceName
                            }
                        })
                    }
                    if (dataSet.services[0].id == null)
                        dataSet.services = [];

                    return dataSet;
                });
                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vsupplier where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        queries.push({
            statement: "CALL insertSupplier(@name := ?," + " @address := ?," + " @CUI := ?," + " @J := ?," + " @telephoneFax := ?," + " @mobile1 := ?," + " @mobile2 := ?," + " @email1 := ?," + " @email2 := ?," + " @website :=?," + " @details :=?," + " @lastUpdateUserId := ?," + " @insertId); " + " SELECT @insertId AS insertId;",
            values: [params.name,
                params.address,
                params.CUI,
                params.J,
                params.telephoneFax,
                params.mobile1,
                params.mobile2,
                params.email1,
                params.email2,
                params.website,
                params.details,
                10
            ]
        });

        params.services.forEach(function(obj) {
            queries.push({
                statement: " CALL insertSupplierServiceGroup( " + "@supplierId := @insertId, " + "@serviceGroupId := ?, " + "@lastUpdateUserId :=?); ",

                values: [
                    obj.id,
                    10
                ]
            });
        });

        db.transaction({ //request.auth.credentials.db
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
                //console.log(data);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);
        var supplierId = request.params.id;

        var updateSupplier = {
            statement: "CALL updateSupplier(" + " @id := ?," + " @name := ?," + " @address := ?," + " @CUI := ?," + " @J := ?," + " @telephoneFax := ?," + " @mobile1 := ?," + " @mobile2 := ?," + " @email1 := ?," + " @mail2 := ?," + " @website := ?," + " @details :=?," + " @lastUpdateUserId := ?); ",

            values: [supplierId,
                params.name,
                params.address,
                params.CUI,
                params.J,
                params.telephoneFax,
                params.mobile1,
                params.mobile2,
                params.email1,
                params.email2,
                params.website,
                params.details,
                10
            ]
        };

        var queries = [updateSupplier];

        if (params.services.length > 0) {

            var deleteSupplierServices = {
                statement: " CALL deleteSupplierServiceGroup(@supplierId := ?); ",
                values: [supplierId]
            };

            queries.push(deleteSupplierServices);

            params.services.forEach(function(obj) {

                queries.push({
                    statement: " CALL insertSupplierServiceGroup( " + "@supplierId := ?, " + "@serviceGroupId := ?, " + "@lastUpdateUserId :=?); ",

                    values: [
                        supplierId,
                        obj.id,
                        10
                    ]
                });
            });
        }

        db.transaction({ //request.auth.credentials.db
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
                //console.log(data);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteSupplier(@id := ?, @lastUpdateUserId :=?); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.users = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vuser;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vuser where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.bankAccounts = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vbankaccount;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vbankaccount where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL insertBankAccount(@accountName := ?, @owner := ?, @currency :=?, @bankName := ?, @description := ?, @details := ?, @lastUpdateUserId := ?, @insertId);",
            values: [params.accountName, params.owner, null, params.bankName, params.description, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
                console.log(data);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL updateBankAccount(@id := ?, @accountName := ?, @owner := ?, @currency :=?, @bankName := ?, @description := ?, @details := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, params.accountName, params.owner, params.currency, params.bankName, params.description, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteBankAccount(@id := ?, @lastUpdateUserId := ?);",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.buildings = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vbuilding;"
        };

        db.query({
            sql: query.statement,

            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {

                    var dataSet = {
                        id: group[0].id,
                        name: group[0].name,
                        aliasName: group[0].aliasName,
                        address: group[0].address,
                        details: group[0].details,

                        apartments: _.map(group, function(apartment) {
                            return {
                                id: apartment.apartmentId,
                                stairs: apartment.stairs,
                                floor: apartment.floor,
                                apType: apartment.apType,
                                apNumber: apartment.apNumber,
                            }
                        })
                    }
                    if (dataSet.apartments[0].id == null)
                        dataSet.apartments = [];

                    return dataSet;
                });
                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vbuilding where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL insertBuilding(@name := ?, @address := ?, @details :=?, @lastUpdateUserId := ?, @insertId); ",
            values: [params.name, params.address, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);

        var query = {
            statement: "CALL updateBuilding(@id := ?, @name := ?, @address := ?, @details :=?, @lastUpdateUserId := ?); ",
            values: [request.params.id, params.name, params.address, params.details, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteBuilding(@id := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.clients = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vclient"
        };

        db.query({
            sql: query.statement,

            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id
                });

                var mapped = _.map(groups, function(group, key) {

                    var dataSet = {
                        id: group[0].id,
                        name: group[0].name,
                        type: group[0].type,
                        RegNo: group[0].RegNo,
                        mobile: group[0].mobile,
                        address: group[0].address,
                        email: group[0].email,
                        websiteOrFaceBook: group[0].websiteOrFaceBook,
                        details: group[0].details,
                        contactPerson: {
                            name: group[0].contactPerson,
                            mobile: group[0].contactPersonMobile,
                            email: group[0].contactPersonEmail
                        },
                        cases: []
                    }

                    var groupsByCases = _.groupBy(group, function(groupCase) {
                        return groupCase.caseId;
                    });

                    _.each(groupsByCases, function(groupCase) {
                        dataSet.cases.push({
                            id: groupCase[0].caseId,
                            clientCaseId: groupCase[0].clientCaseId,
                            caseNumber: groupCase[0].caseNumber,
                            caseDescription: groupCase[0].caseDescription,
                            apartment: {
                                id: groupCase[0].apartmentId,
                                stairs: groupCase[0].apartmentStairs,
                                floor: groupCase[0].apartmentFloor,
                                type: groupCase[0].apartmentType,
                                number: groupCase[0].apartmentNumber,
                                buildingId: groupCase[0].buildingId
                            },
                            files: []
                        });
                        var idx = dataSet.cases.length - 1;

                        _.each(groupCase, function(file) {
                            if (file.fileId && file.fileName && file.pathToFile) {
                                dataSet.cases[idx].files.push({
                                    id: file.fileId,
                                    name: file.fileName,
                                    path: file.pathToFile
                                });
                            }
                        })
                    })

                    if (dataSet.cases == null)
                        dataSet.cases = [];

                    return dataSet;
                });
                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vclient where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                var dataSet = {
                    id: data[0].id,
                    name: data[0].name,
                    type: data[0].type,
                    RegNo: data[0].RegNo,
                    mobile: data[0].mobile,
                    address: data[0].address,
                    email: data[0].email,
                    websiteOrFaceBook: data[0].websiteOrFaceBook,
                    details: data[0].details,
                    contactPerson: {
                        name: data[0].contactPerson,
                        mobile: data[0].contactPersonMobile,
                        email: data[0].contactPersonEmail
                    },
                    cases: []
                }

                var groupsByCases = _.groupBy(data, function(groupCase) {
                    return groupCase.caseId;
                });

                _.each(groupsByCases, function(groupCase) {
                    dataSet.cases.push({
                        id: groupCase[0].caseId,
                        clientCaseId: groupCase[0].clientCaseId,
                        caseNumber: groupCase[0].caseNumber,
                        caseDescription: groupCase[0].caseDescription,
                        apartment: {
                            id: groupCase[0].apartmentId,
                            stairs: groupCase[0].apartmentStairs,
                            floor: groupCase[0].apartmentFloor,
                            type: groupCase[0].apartmentType,
                            number: groupCase[0].apartmentNumber,
                            buildingId: groupCase[0].buildingId
                        },
                        files: []
                    });
                    var idx = dataSet.cases.length - 1;

                    _.each(groupCase, function(file) {
                        if (file.fileId && file.fileName && file.pathToFile) {
                            dataSet.cases[idx].files.push({
                                id: file.fileId,
                                name: file.fileName,
                                path: file.pathToFile
                            });
                        }
                    })
                })

                if (dataSet.cases == null)
                    dataSet.cases = [];

                dbCallback(err, dataSet, reply, query);
            }
        });
    },

    insert: function(request, reply) {
        var params = _.clone(request.payload);
        var queries = [];
        queries.push({
            statement: "CALL insertClient(" + " @name := ?," + " @type := ?," + " @RegNo := ?," + " @mobile :=?," + " @address := ?," + " @email :=?," + " @websiteOrFaceBook :=?," + " @contactPerson :=?," + " @contactPersonMobile :=?," + " @contactPersonEmail :=?," + " @details := ?," + " @lastUpdateUserId := ?," + " @clientInsertId); " + "SELECT @clientInsertId AS insertId;",
            values: [params.name,
                params.type,
                params.RegNo,
                params.mobile,
                params.address,
                params.email,
                params.websiteOrFaceBook,
                params.contactPerson.name,
                params.contactPerson.mobile,
                params.contactPerson.email,
                params.details,
                10
            ]
        });

        if (params.cases) {
            params.cases.forEach(function(currentCase) {
                var apartmentId = '';
                var buildingId = '';

                if (currentCase.apartment.id) {
                    apartmentId = currentCase.apartment.id;
                    buildingId = currentCase.apartment.buildingId;
                }
                //insert or update case
                queries.push({
                    statement: "CALL insertOrUpdateCase(" + "@id := ?, " + "@externalNumber := ?, " + "@description := ?, " + "@lastUpdateUserId :=?," + "@outCaseId); ",
                    values: [
                        currentCase.id,
                        currentCase.caseNumber,
                        currentCase.caseDescription,
                        10
                    ]

                });

                queries.push({
                    statement: "CALL insertOrUpdateClientCase(" + "@clientId := @clientInsertId, " + "@caseId := @outCaseId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                    values: [
                        10
                    ]

                });

                queries.push({
                    statement: "CALL insertOrUpdateApartment(" + "@id := ?, " + "@buildingId := ?, " + "@caseId := @outCaseId, " + "@stairs := ?, " + "@floor := ?, " + "@apType := ?, " + "@apNumber := ?, " + "@details := ?, " + "@lastUpdateUserId :=?," + "@outId); ",
                    values: [
                        apartmentId,
                        buildingId,
                        currentCase.apartment.stairs,
                        currentCase.apartment.floor,
                        currentCase.apartment.type,
                        currentCase.apartment.number,
                        "", //TODO: nu ne trebuie details la nivel de apartament
                        10
                    ]

                });


                if (currentCase.newFiles) {
                    var fs = require('fs');
                    currentCase.newFiles.forEach(function(newFile) {
                        if (newFile) {
                            var path = tempPath + newFile.status.response;
                            var file = fs.readFileSync(path);
                            fs.writeFileSync(mediaPath + "casefiles/" + newFile.status.response, file);
                            fs.unlinkSync(path);

                            var pathToFile = "media\\casefiles\\" + newFile.status.response;

                            queries.push({
                                statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertFileId); ",
                                values: [
                                    newFile.name,
                                    newFile.name.split('.').pop(),
                                    newFile.size,
                                    pathToFile,
                                    10,
                                    10
                                ]
                            });

                            queries.push({
                                statement: "CALL insertCaseFile(" + "@caseId := @outCaseId, " + "@fileId := @insertFileId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                                values: [10]
                            });
                        }
                    });
                }
            });
        }


        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    update: function(request, reply) {
        var params = _.clone(request.payload);
        var queries = [];

        var updateClient = {
            statement: "CALL updateClient(" + " @id := ?," + " @name := ?," + " @type := ?," + " @RegNo := ?," + " @mobile :=?," + " @address := ?," + " @email :=?," + " @websiteOrFaceBook :=?," + " @contactPerson :=?," + " @contactPersonMobile :=?," + " @contactPersonEmail :=?," + " @details := ?," + " @lastUpdateUserId := ?);",
            values: [request.params.id,
                params.name,
                params.type,
                params.RegNo,
                params.mobile,
                params.address,
                params.email,
                params.websiteOrFaceBook,
                params.contactPerson.name,
                params.contactPerson.mobile,
                params.contactPerson.email,
                params.details,
                10
            ]
        };
        queries.push(updateClient);

        if (params.cases) {
            params.cases.forEach(function(currentCase) {
                var apartmentId = '';
                var buildingId = '';

                if (currentCase.apartment.id) {
                    apartmentId = currentCase.apartment.id;
                    buildingId = currentCase.apartment.buildingId;
                }

                //insert or update case
                queries.push({

                    statement: "CALL insertOrUpdateCase(" + "@id := ?, " + "@externalNumber := ?, " + "@description := ?, " + "@lastUpdateUserId :=?," + "@outCaseId); ",
                    values: [
                        currentCase.id,
                        currentCase.caseNumber,
                        currentCase.caseDescription,
                        10
                    ]
                });

                queries.push({

                    statement: "CALL insertOrUpdateClientCase(" + "@clientId := ?, " + "@caseId := @outCaseId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                    values: [
                        request.params.id,
                        10
                    ]
                });

                queries.push({

                    statement: "CALL insertOrUpdateApartment(" + "@id := ?, " + "@buildingId := ?, " + "@caseId := @outCaseId, " + "@stairs := ?, " + "@floor := ?, " + "@apType := ?, " + "@apNumber := ?, " + "@details := ?, " + "@lastUpdateUserId :=?," + "@outId); ",
                    values: [
                        apartmentId,
                        buildingId,
                        currentCase.apartment.stairs,
                        currentCase.apartment.floor,
                        currentCase.apartment.type,
                        currentCase.apartment.number,
                        currentCase.apartment.details,
                        10
                    ]

                });

                if (currentCase.filesToBeDeleted) {
                    currentCase.filesToBeDeleted.forEach(function(fileToBeDeleted) {
                        var fileIdTBD = fileToBeDeleted.id;

                        queries.push({
                            statement: "CALL removeCaseFile(" + "@caseId := ?, " + "@fileId := ?); ",
                            values: [
                                currentCase.id,
                                fileIdTBD
                            ]
                        });

                        queries.push({
                            statement: "CALL removeFile(@fileTBD :=?); ",
                            values: [
                                fileIdTBD
                            ]
                        });
                    });
                }

                if (currentCase.newFiles) {
                    var fs = require('fs');
                    currentCase.newFiles.forEach(function(newFile) {
                        if (newFile) {
                            var path = tempPath + newFile.status.response;
                            var file = fs.readFileSync(path);
                            fs.writeFileSync(mediaPath + "casefiles/" + newFile.status.response, file);
                            fs.unlinkSync(path);

                            var pathToFile = "media\\casefiles\\" + newFile.status.response;

                            queries.push({
                                statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertFileId); ",
                                values: [
                                    newFile.name,
                                    newFile.name.split('.').pop(),
                                    newFile.size,
                                    pathToFile,
                                    10,
                                    10
                                ]
                            });

                            queries.push({
                                statement: "CALL insertCaseFile(" + "@caseId := @outCaseId, " + "@fileId := @insertFileId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                                values: [10]
                            });
                        }
                    });
                }
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteClient(@id := ?, @lastUpdateUserId := ?); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.invoices = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vinvoice;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {
                    var invoice = _.clone(group[0]);
                    delete invoice.fileId;
                    delete invoice.fileName;
                    delete invoice.pathToFile;

                    delete invoice.paymentId;
                    delete invoice.paymentOperationDT;
                    delete invoice.paymentCRT;
                    delete invoice.paymentLinkNumber;
                    delete invoice.paymentDetails;
                    delete invoice.paymentAmount;

                    invoice.files = [];
                    var groupByFiles = _.groupBy(group, function(value) {
                        return value.fileId;
                    });
                    _.each(groupByFiles, function(i) {
                        if (i[0].fileId) {
                            invoice.files.push({
                                "id": i[0].fileId,
                                "name": i[0].fileName,
                                "path": i[0].pathToFile
                            })
                        };
                    });
                    
                    invoice.payments = [];
                    var groupByPayments = _.groupBy(group, function(value) {
                        return value.paymentId;
                    });
                    _.each(groupByPayments, function(i) {
                        if (i[0].paymentId) {
                            invoice.payments.push({
                                "paymentId": i[0].paymentId,
                                "paymentOperationDT": i[0].paymentOperationDT,
                                "paymentCRT": i[0].paymentCRT,
                                "paymentLinkNumber": i[0].paymentLinkNumber,
                                "paymentDetails": i[0].paymentDetails,
                                "paymentAmount": i[0].paymentAmount,
                            })
                        };
                    });
                    
                    return invoice;
                });

                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vinvoice where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                
                var invoice = _.clone(data[0]);
                delete invoice.fileId;
                delete invoice.fileName;
                delete invoice.pathToFile;

                delete invoice.paymentId;
                delete invoice.paymentOperationDT;
                delete invoice.paymentCRT;
                delete invoice.paymentLinkNumber;
                delete invoice.paymentDetails;
                delete invoice.paymentAmount;

                invoice.files = [];
                var groupByFiles = _.groupBy(data, function(value) {
                    return value.fileId;
                });
                _.each(groupByFiles, function(i) {
                    if (i[0].fileId) {
                        invoice.files.push({
                            "id": i[0].fileId,
                            "name": i[0].fileName,
                            "path": i[0].pathToFile
                        })
                    };
                });
                
                invoice.payments = [];
                var groupByPayments = _.groupBy(data, function(value) {
                    return value.paymentId;
                });
                _.each(groupByPayments, function(i) {
                    if (i[0].paymentId) {
                        invoice.payments.push({
                            "paymentId": i[0].paymentId,
                            "paymentOperationDT": i[0].paymentOperationDT,
                            "paymentCRT": i[0].paymentCRT,
                            "paymentLinkNumber": i[0].paymentLinkNumber,
                            "paymentDetails": i[0].paymentDetails,
                            "paymentAmount": i[0].paymentAmount,
                        })
                    };
                });
               
                dbCallback(err, invoice, reply, query);
            }
        });
    },

    byDateRange: function(request, reply) {

        var query = {
            statement: "SELECT DISTINCT baa.id "
                        +",baa.operationDT "
                        +",baa.crt "
                        +",vi.supplierName "
                        +",vi.serviceGroupName "
                        +",baa.linkNumber "
                        +",vi.buildingAliasName "
                        +",vi.invoiceTypeName "
                        +",baa.details "
                        +",baa.amount as outAmount "
                        +",( "
                            +"SELECT sum(vb.amount)  "
                            +"FROM vbankaccountactivity vb "
                            +"INNER JOIN vinvoice vi "
                            +"ON vb.invoiceId = vi.id "
                            +"WHERE isTransfer = 0 "
                            +"AND vb.operationDT between ? AND baa.operationDT "
                            +"AND vi.invoiceTypeName <> 'Storno' "
                        +") as masterAccTotalAmount "
                        +"FROM vbankaccountactivity baa "
                        +"INNER JOIN vinvoice vi "
                        +"ON baa.invoiceId = vi.id "
                        +"WHERE baa.isTransfer = 0 "
                        +"AND baa.operationDT BETWEEN ? AND ? "
                        +"AND vi.invoiceTypeName <> 'Storno' "
                        +"ORDER BY baa.crt",
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: [request.params.startDate, request.params.startDate, request.params.endDate]
        };
        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.id;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        supplierName: group[0].supplierName,
                        serviceGroupName: group[0].serviceGroupName,
                        linkNumber: group[0].linkNumber,
                        buildingAliasName: group[0].buildingAliasName,
                        invoiceTypeName: group[0].invoiceTypeName,
                        outAmount: group[0].outAmount,
                        sold: group[0].masterAccTotalAmount
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        var serviceGroupId = '';

        if (params.serviceGroupId) {
            serviceGroupId = params.serviceGroupId;
        }

        var insertInvoice = {
            statement: "CALL insertInvoice( " + "@supplierId := ?," + "@serviceGroupId := ?," + "@buildingId := ?," + "@externalNumber := ?," + "@amount := ?," + "@currency := ?," + "@paymentTypeId := ?," + "@createDT := ?," + "@dueDT := ?," + "@paymentDT := ?," + "@details := ?," + "@statusId := ?," + "@invoiceTypeId := ?," + "@hasIssue := ?," + "@issueDescription := ?," + "@lastUpdateUserId := ?," + "@outInvoiceId); " + " SELECT @outInvoiceId AS insertId;",

            values: [
                params.supplierId,
                serviceGroupId,
                params.buildingId,
                params.externalNumber,
                params.amount,
                'LEI', //params.currency
                1, //params.paymentTypeId (Op bancara)
                params.createDT,
                params.dueDT,
                '', //params.paymentDT,
                params.details,
                params.statusId,
                params.invoiceTypeId,
                params.hasIssue,
                '', //params.issueDescription,
                10 //lastUpdateUserId
            ]
        };

        queries.push(insertInvoice);

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                var path = tempPath + newFile.status.response;
                var file = fs.readFileSync(path);
                fs.writeFileSync(mediaPath + "invoices/" + newFile.status.response, file);
                fs.unlinkSync(path);

                var pathToFile = "media\\invoices\\" + newFile.status.response;

                queries.push({
                    statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@outFileId); ",
                    values: [
                        newFile.name,
                        newFile.name.split('.').pop(),
                        newFile.size,
                        pathToFile,
                        10,
                        10
                    ]
                });

                queries.push({
                    statement: "CALL insertInvoiceFile(" + "@invoiceId := @outInvoiceId, " + "@fileId := @outFileId, " + "@lastUpdateUserId := ?," + "@insertId); ",
                    values: [
                        10
                    ]
                });
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);
        var serviceGroupId = '';

        var queries = [];

        if (params.serviceGroupId) {
            serviceGroupId = params.serviceGroupId;
        }

        var updateInvoice = {
            statement: "CALL updateInvoice(@id := ?, " + "@supplierId := ?," + "@serviceGroupId := ?," + "@buildingId := ?," + "@externalNumber := ?," + "@amount := ?," + "@currency := ?," + "@paymentTypeId := ?," + "@createDT := ?," + "@dueDT := ?," + "@paymentDT := ?," + "@details := ?," + "@statusId := ?," + "@invoiceTypeId := ?," + "@hasIssue := ?," + "@issueDescription := ?," + "@lastUpdateUserId := ?);",

            values: [request.params.id,
                params.supplierId,
                serviceGroupId,
                params.buildingId,
                params.externalNumber,
                params.amount,
                'LEI', //params.currency
                1, //params.paymentTypeId, (Op bancara)
                params.createDT,
                params.dueDT,
                '', //params.paymentDT,
                params.details,
                params.statusId,
                params.invoiceTypeId,
                params.hasIssue,
                '', //params.issueDescription,
                10 //lastUpdateUserId
            ]
        };

        queries.push(updateInvoice);
 
        if (params.filesToBeDeleted) {

            params.filesToBeDeleted.forEach(function(fileToBeDeleted) {
                var fileIdTBD = fileToBeDeleted.id;

                queries.push({
                    statement: "CALL removeInvoiceFile(" + "@invoiceId := ?, " + "@fileId :=?); ",
                    values: [
                        request.params.id,
                        fileIdTBD
                    ]
                });

                queries.push({
                    statement: "CALL removeFile(@fileTBD :=?); ",
                    values: [
                        fileIdTBD
                    ]
                });
            });
        }

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                var path = tempPath + newFile.status.response;
                var file = fs.readFileSync(path);
                fs.writeFileSync(mediaPath + "invoices/" + newFile.status.response, file);
                fs.unlinkSync(path);

                var pathToFile = "media\\invoices\\" + newFile.status.response;

                queries.push({
                    statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertId); ",
                    values: [
                        newFile.name,
                        newFile.name.split('.').pop(),
                        newFile.size,
                        pathToFile,
                        10,
                        10
                    ]
                });

                queries.push({
                    statement: "CALL insertInvoiceFile(" + "@invoiceId := ?, " + "@fileId := @insertId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                    values: [
                        request.params.id,
                        10
                    ]
                });
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL deleteInvoice(@id := ?, @lastUpdateUserId :=? ); ",
            values: [request.params.id, 10]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.payments = {

    getHistory: function(request, reply){

        var query = {
            statement: "SELECT baa.operationDT, baa.crt, baa.linkNumber, baa.details, baa.amount as amount "
                        + " FROM vbankaccountactivity baa "
                        + " INNER JOIN vinvoice vi "
                        + " ON baa.invoiceId = vi.id "
                        + " WHERE baa.isTransfer = 0; "
                        + " AND vi.id = ? "
                        + " ORDER BY baa.operationDT; ",
            values: [request.params.invoiceId]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    },
    insert: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];
        params.invoicesToPay.forEach(function(invoice) {
            queries.push({
                statement: "CALL insertBankAccountActivity(@crt := ?, "
                                + " @invoiceId := ?,"
                                +" @clientCaseId := ?,"
                                +" @bankAccountId := ?,"
                                +" @amount := ?,"
                                +" @currency := ?,"
                                +" @type := ?,"
                                +" @isTransfer := ?,"
                                +" @transferToBankAccountId := ?,"
                                +" @description := ?,"
                                +" @details := ?,"
                                +" @linkNumber := ?,"
                                +" @operationDT := ?,"
                                +" @operatorId := ?,"
                                +" @lastUpdateUserId := ?,"
                                +" @insertId); "
                                +"SELECT @insertId AS insertId;",

                values: [
                    params.crt,
                    invoice.id,
                    null, //params.clientCaseId,
                    1, // cont master
                    invoice.partialAmount,
                    null,
                    'OUT',
                    0,
                    null,
                    null, //params.description,
                    params.details,
                    params.linkNumber,
                    params.operationDT,
                    10,
                    10
                ]
            });

            if (invoice.remainingAmount == invoice.partialAmount) {
                // setez factura/ile ca si achitata/e complet
                queries.push({
                    statement: "CALL updateInvoiceAfterPayment(@id := ?, "
                                        + "@paymentDT := ?,"
                                        +" @statusId := ?,"
                                        +" @lastUpdateUserId := ?);",

                    values: [
                        invoice.id,
                        params.operationDT,
                        13,  // Achitata Complet
                        10
                    ]
                });                
             
            } else if (invoice.remainingAmount > invoice.partialAmount) { 
                // setez factura ca si achitata partial
                queries.push({
                    statement: "CALL updateInvoiceAfterPayment(@id := ?, "
                                        + "@paymentDT := ?,"
                                        +" @statusId := ?,"
                                        +" @lastUpdateUserId := ?);",

                    values: [
                        invoice.id,
                        null, //params.operationDT
                        14,  // Achitata Partial
                        10
                    ]
                });                  
            }

        })

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    delete: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        params.paymentsToBeDeleted.forEach(function(payment) {

            //console.log(payment);
            queries.push({
                statement: "CALL deletePaymentAndResetInvoiceStatus(@paymentCRT := ?, @lastUpdateUserId := ?, @deletedInvoiceIds); ",
                values: [
                    payment.paymentCRT,
                    10
                ]
            });
        })

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    }
}

exports.paymentTypes = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vpaymenttype;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.invoiceStatuses = {
    all: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vstatus WHERE appliesto = ?;"
        };

        db.query({
            sql: query.statement,
            values: ["invoice"],
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.invoiceTypes = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vinvoicetype;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.cases = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vcase;"
        }

        db.query({
            sql: query.statement,

            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {

                    var dataSet = {
                        id: group[0].id,
                        externalNumber: group[0].externalNumber,
                        description: group[0].description,

                        apartments: _.map(group, function(apartment) {
                            return {
                                id: apartment.apartmentId,
                                stairs: apartment.apartmentStairs,
                                floor: apartment.apartmentFloor,
                                apType: apartment.apartmentType,
                                apNumber: apartment.apartmentNumber,
                                buildings: _.map(group, function(building) {
                                    return {
                                        id: building.buildingId,
                                        aliasName: building.buildingName
                                    }
                                })
                            }
                        }),

                        clients: _.map(group, function(client) {
                            return {
                                id: client.clientId,
                                name: client.clientName
                            }
                        })
                    }
                    if (dataSet.apartments[0].id == null)
                        dataSet.apartments = [];

                    return dataSet;
                });
                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vcase where id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.incoms = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vbankaccountactivity WHERE type='IN' AND isTransfer = 0;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                var groups = _.groupBy(data, function(group) {
                    return group.id;
                });

                var incomeList = [];
                _.each(groups, function(group) {
                    var item = {
                        id: group[0].id,
                        crt: group[0].crt,
                        amount: group[0].amount,
                        currency: group[0].currency,
                        description: group[0].description,
                        details: group[0].details,
                        linkNumber: group[0].linkNumber,
                        operationDT: group[0].operationDT,
                        clientCaseId: group[0].clientCaseId,
                        files: _.map(group, function(income) {
                            return {
                                id: income.fileId,
                                name: income.fileName,
                                pathToFile: income.pathToFile
                            }
                        })
                    }
                    if (group[0].fileId == null) item.files = [];
                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vbankaccountactivity WHERE type='IN' AND isTransfer = 0 AND id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                var item = {
                    id: data[0].id,
                    crt: data[0].crt,
                    amount: data[0].amount,
                    currency: data[0].currency,
                    description: data[0].description,
                    details: data[0].details,
                    linkNumber: data[0].linkNumber,
                    operationDT: data[0].operationDT,
                    clientCaseId: data[0].clientCaseId,
                    files: _.map(data, function(income) {
                        return {
                            id: income.fileId,
                            name: income.fileName,
                            pathToFile: income.pathToFile
                        }
                    })
                }
                if (data[0].fileId == null) item.files = [];

                dbCallback(err, item, reply, query);
            }
        });
    },

    byDateRange: function(request, reply) {

        var query = {
            statement: "SELECT baa.id, "
            +"baa.crt, " 
            + "baa.operationDT, " 
            + "baa.clientName, " 
            + "baa.caseNumber, " 
            + "baa.linkNumber, " 
            + "baa.amount as inAmount, " 
            + "( " 
                + "SELECT sum(amount) FROM vbankaccountactivity WHERE type = 'IN' " 
                + "AND isTransfer = 0 AND operationDT between ? AND baa.operationDT "
                + ") as masterAccTotalAmount " 
                + "FROM vbankaccountactivity baa " 
                + "WHERE baa.type = 'IN' " 
                + "AND baa.isTransfer = 0 " 
                + "AND baa.operationDT BETWEEN ? AND ? " 
                + "ORDER BY baa.crt",
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: [request.params.startDate, request.params.startDate, request.params.endDate]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.id;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        client: group[0].clientName,
                        caseNumber: group[0].caseNumber,
                        linkNumber: group[0].linkNumber,
                        amount: group[0].inAmount,
                        sold: group[0].masterAccTotalAmount
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        var insertTransferQuery = {

                        statement: "CALL insertBankAccountActivity(@crt := ?, "
                                + " @invoiceId := ?,"
                                +" @clientCaseId := ?,"
                                +" @bankAccountId := ?,"
                                +" @amount := ?,"
                                +" @currency := ?,"
                                +" @type := ?,"
                                +" @isTransfer := ?,"
                                +" @transferToBankAccountId := ?,"
                                +" @description := ?,"
                                +" @details := ?,"
                                +" @linkNumber := ?,"
                                +" @operationDT := ?,"
                                +" @operatorId := ?,"
                                +" @lastUpdateUserId := ?,"
                                +" @insertId); "
                                +"SELECT @insertId AS insertId;",

            values: [
                params.crt,
                null,
                params.clientCaseId,
                1,
                params.amount,
                null,
                'IN',
                0,
                null,
                params.description,
                params.details,
                params.linkNumber,
                params.operationDT,
                10,
                10
            ]
        };
        queries.push(insertTransferQuery);

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                if (newFile) {
                    var path = tempPath + newFile.status.response;
                    var file = fs.readFileSync(path);
                    fs.writeFileSync(mediaPath + "incomes/" + newFile.status.response, file);
                    fs.unlinkSync(path);

                    var pathToFile = "media\\incomes\\" + newFile.status.response;

                    queries.push({
                        statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertFileId); ",
                        values: [
                            newFile.name,
                            newFile.name.split('.').pop(),
                            newFile.size,
                            pathToFile,
                            10,
                            10
                        ]
                    });

                    queries.push({
                        statement: "CALL insertBankAccountActivityFile(" + "@bankAccountActivityId :=  @insertId, " + "@fileId := @insertFileId, " + "@lastUpdateUserId :=?," + "@insertActivityFileId); ",
                        values: [10]
                    });
                }
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        var updateTransferQuery = {

            statement: "CALL updateBankAccountActivity(@id := ?,"
                +" @crt := ?,"
                +" @invoiceId := ?,"
                +" @clientCaseId := ?,"
                +" @bankAccountId := ?,"
                +" @amount := ?,"
                +" @currency := ?,"
                +" @type := ?,"
                +" @isTransfer := ?,"
                +" @transferToBankAccountId := ?,"
                +" @description := ?,"
                +" @details := ?,"
                +" @linkNumber := ?,"
                +" @operationDT := ?,"
                +" @operatorId := ?,"
                +" @lastUpdateUserId := ?); ",

            values: [request.params.id,
                params.crt,
                null,
                params.clientCaseId,
                1,
                params.amount,
                null,
                'IN',
                0,
                null,
                params.description,
                params.details,
                params.linkNumber,
                params.operationDT,
                10,
                10
            ]
        };
        queries.push(updateTransferQuery);

        if (params.filesToBeDeleted) {

            params.filesToBeDeleted.forEach(function(fileToBeDeleted) {
                var fileIdTBD = fileToBeDeleted.id;

                queries.push({
                    statement: "CALL removeBankAccountActivityFile(" + "@bankAccountActivityId := ?, " + "@fileId :=?); ",
                    values: [
                        request.params.id,
                        fileIdTBD
                    ]
                });

                queries.push({
                    statement: "CALL removeFile(@fileTBD :=?); ",
                    values: [
                        fileIdTBD
                    ]
                });
            });
        }

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                if (newFile) {
                    var path = tempPath + newFile.status.response;
                    var file = fs.readFileSync(path);
                    fs.writeFileSync(mediaPath + "incomes/" + newFile.status.response, file);
                    fs.unlinkSync(path);

                    var pathToFile = "media\\incomes\\" + newFile.status.response;

                    queries.push({
                        statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertId); ",
                        values: [
                            newFile.name,
                            newFile.name.split('.').pop(),
                            newFile.size,
                            pathToFile,
                            10,
                            10
                        ]
                    });

                    queries.push({
                        statement: "CALL insertBankAccountActivityFile(" + "@bankAccountActivityId := ?, " + "@fileId := @insertId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                        values: [
                            request.params.id,
                            10
                        ]
                    });
                }
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL removeBankAccountActivity(@bankAccountActivityId := ?, @filesToBeDeleted); SELECT @filesToBeDeleted AS filesToBeDeleted;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                // if (data[0][0][Object.keys(data[0][0])[0]]) {
                //     var fs = require('fs');
                //     var paths = data[0][0][Object.keys(data[0][0])[0]].split(",");
                //     paths.forEach(function (path) {
                //         fs.unlinkSync(path);
                //     });
                // }
                dbCallback(err, data, reply, query);
            }
        });
    }
}
exports.transfers = {
    all: function(request, reply) {
        var query = {
            statement: "SELECT * FROM vtransfer;"
        };

        db.query({
            sql: query.statement,
            callback: function(err, data) {
                var groups = _.groupBy(data, function(value) {
                    return value.id;
                });

                var mapped = _.map(groups, function(group) {
                    var transfer = _.clone(group[0]);
                    delete transfer.fileId;
                    delete transfer.fileName;
                    delete transfer.pathToFile;
                    transfer.files = [];
                    _.each(group, function(t) {
                        if (t.fileId) {
                            transfer.files.push({
                                "id": t.fileId,
                                "name": t.fileName,
                                "path": t.pathToFile
                            })
                        };
                    });
                    return transfer;
                });

                dbCallback(err, mapped, reply, query);
            }
        });
    },

    byId: function(request, reply) {

        var query = {
            statement: "SELECT * FROM vtransfer WHERE id = ?;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                var transfer = _.clone(data[0]);
                delete transfer.fileId;
                delete transfer.fileName;
                delete transfer.pathToFile;
                transfer.files = [];
                _.each(data, function(t) {
                    if (t.fileId) {
                        transfer.files.push({
                            "id": t.fileId,
                            "name": t.fileName,
                            "path": t.pathToFile
                        })
                    };
                });

                dbCallback(err, transfer, reply, query);
            }
        });
    },

    byDateRange: function(request, reply) {

        var query = {
            statement: "SELECT baa.id " 
                            +",baa.crt "
                            +",baa.operationDT "
                            +",baa.description "
                            +",baa.details "
                            +",baa.type "
                            +",baa.linkNumber "
                            +",baa.amount as inAmount "
                            +",( "
                                +"SELECT sum(amount) FROM vbankaccountactivity "
                                +"WHERE isTransfer = 1 "
                                +"AND operationDT between ? AND baa.operationDT "
                                +"AND baa.type <> ? "        
                            +") as masterAccTotalAmount "
                            +"FROM vbankaccountactivity baa "
                            +"WHERE baa.isTransfer = 1 "
                            +"AND baa.operationDT BETWEEN ? AND ? "
                            +"AND baa.type <> ? "
                            +"ORDER BY baa.crt",
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: [request.params.startDate, request.params.type, request.params.startDate, request.params.endDate, request.params.type]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.id;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        description: group[0].description,
                        type: group[0].type,
                        linkNumber: group[0].linkNumber,
                        amount: group[0].inAmount,
                        sold: group[0].masterAccTotalAmount
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    insert: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        var insertTransferQuery = {

            statement: "CALL insertBankAccountActivity(@crt :=?, @invoiceId := ?, @clientId := ?, @bankAccountId := ?, " +
                        "@amount := ?, @currency := ?, @type := ?, @isTransfer := ?, @transferToOrFromBankAccountId := ?, @description := ?, " + 
                        "@details := ?, @linkNumber := ?, @operationDT := ?, @operatorId := ?, @lastUpdateUserId := ?, @insertId); " +
                        "SELECT @insertId AS insertId;",

            values: [params.crt, null, null, 1, params.amount, null, params.type, 1, params.transferToOrFromBankAccountId, params.description,
                params.details, params.linkNumber, params.operationDT, 10, 10
            ]
        };
        queries.push(insertTransferQuery);

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                if (newFile) {
                    var path = tempPath + newFile.status.response;
                    var file = fs.readFileSync(path);
                    fs.writeFileSync(mediaPath + "transfers/" + newFile.status.response, file);
                    fs.unlinkSync(path);

                    var pathToFile = "media\\transfers\\" + newFile.status.response;

                    queries.push({
                        statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertFileId); ",
                        values: [
                            newFile.name,
                            newFile.name.split('.').pop(),
                            newFile.size,
                            pathToFile,
                            10,
                            10
                        ]
                    });

                    queries.push({
                        statement: "CALL insertBankAccountActivityFile(" + "@bankAccountActivityId :=  @insertId, " + "@fileId := @insertFileId, " + "@lastUpdateUserId :=?," + "@insertActivityFileId); ",
                        values: [10]
                    });
                }
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    update: function(request, reply) {

        var params = _.clone(request.payload);
        var queries = [];

        var updateTransferQuery = {

            statement: "CALL updateBankAccountActivity(@id := ?, @crt := ?, @invoiceId := ?, @clientId := ?, @bankAccountId := ?, " +
                        "@amount := ?, @currency := ?, @type := ?, @isTransfer := ?, @transferToOrFromBankAccountId := ?, @description := ?, " + 
                        "@details := ?, @linkNumber := ?, @operationDT := ?, @operatorId := ?, @lastUpdateUserId := ?);",

            values: [request.params.id, params.crt, null, null, 1, params.amount, null, params.type, 1, params.transferToOrFromBankAccountId, params.description,
                params.details, params.linkNumber, params.operationDT, 10, 10
            ]
        };
        queries.push(updateTransferQuery);

        if (params.filesToBeDeleted) {

            params.filesToBeDeleted.forEach(function(fileToBeDeleted) {
                var fileIdTBD = fileToBeDeleted.id;

                queries.push({
                    statement: "CALL removeBankAccountActivityFile(" + "@bankAccountActivityId := ?, " + "@fileId :=?); ",
                    values: [
                        request.params.id,
                        fileIdTBD
                    ]
                });

                queries.push({
                    statement: "CALL removeFile(@fileTBD :=?); ",
                    values: [
                        fileIdTBD
                    ]
                });
            });
        }

        if (params.newFiles) {
            var fs = require('fs');
            params.newFiles.forEach(function(newFile) {
                if (newFile) {
                    var path = tempPath + newFile.status.response;
                    var file = fs.readFileSync(path);
                    fs.writeFileSync(mediaPath + "transfers/" + newFile.status.response, file);
                    fs.unlinkSync(path);

                    var pathToFile = "media\\transfers\\" + newFile.status.response;

                    queries.push({
                        statement: "CALL insertFile(" + "@name := ?, " + "@ext := ?, " + "@size := ?, " + "@pathToFile := ?, " + "@createUserId := ?, " + "@lastUpdateUserId :=?," + "@insertId); ",
                        values: [
                            newFile.name,
                            newFile.name.split('.').pop(),
                            newFile.size,
                            pathToFile,
                            10,
                            10
                        ]
                    });

                    queries.push({
                        statement: "CALL insertBankAccountActivityFile(" + "@bankAccountActivityId := ?, " + "@fileId := @insertId, " + "@lastUpdateUserId :=?," + "@insertId); ",
                        values: [
                            request.params.id,
                            10
                        ]
                    });
                }
            });
        }

        db.transaction({
            queries: queries,
            callback: function(err, data) {
                dbCallback(err, data, reply, queries);
            }
        });
    },

    delete: function(request, reply) {

        var query = {
            statement: "CALL removeBankAccountActivity(@bankAccountActivityId := ?, @filesToBeDeleted); SELECT @filesToBeDeleted AS filesToBeDeleted;",
            values: [request.params.id]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                // if (data[0][0][Object.keys(data[0][0])[0]]) {
                //     var fs = require('fs');
                //     var paths = data[0][0][Object.keys(data[0][0])[0]].split(",");
                //     paths.forEach(function (path) {
                //         fs.unlinkSync(path);
                //     });
                // }
                dbCallback(err, data, reply, query);
            }
        });
    }
}

exports.reports = {
    transfers : function(request, reply) {
        if (request.query.type && !((request.query.type == "IN") || (request.query.type == "OUT") || (request.query.type == ""))) {
            reply("Invalid Call").code(404);        
        };
        
        var transferType = request.query.type ? request.query.type : "";
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() + 1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";
        
        var startFrom = startYear + "-" + startMonth + "-" + startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";
        
        var endIn = endYear + "-" + endMonth + "-" + endDay+""+endTime;
        
        var nonType = null;
        switch (transferType) {
            case "IN":
                nonType = "OUT";
                break;
            case "OUT":
                nonType = "IN";
                break;
            default:
                nonType = "";
        }
        
        var query = {
            statement: "SELECT baa.id " 
                            +",baa.crt "
                            +",baa.operationDT "
                            +",baa.toAccountName "
                            +",baa.description "
                            +",baa.details "
                            +",baa.type "
                            +",baa.linkNumber "
                            +",baa.amount as inAmount "
                            +",( "
                                +"SELECT sum(amount) FROM vbankaccountactivity "
                                +"WHERE isTransfer = 1 "
                                +"AND operationDT between ? AND baa.operationDT "
                                +"AND baa.type <> ? "   
                                +"ORDER BY baa.crt"     
                            +") as masterAccTotalAmount "
                            +"FROM vbankaccountactivity baa "
                            +"WHERE baa.isTransfer = 1 "
                            +"AND baa.operationDT BETWEEN ? AND ? "
                            +"AND baa.type <> ? "
                            +"ORDER BY baa.crt",
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            
            values: [startFrom, nonType, startFrom, endIn, nonType]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.crt;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        account: group[0].toAccountName,
                        description: group[0].description,
                        type: group[0].type,
                        linkNumber: group[0].linkNumber,
                        amount: group[0].inAmount.toFixed(2),
                        sold: group[0].masterAccTotalAmount.toFixed(2)
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });        
    },

    incomes: function(request, reply){
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        var buildingName = null;
        var apType = null;
        var valuesArr = [];
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }

        var statementStr = "SELECT baa.id, "
            +"baa.crt, " 
            + "baa.operationDT, " 
            + "baa.clientName, " 
            + "baa.caseNumber, " 
            + "baa.linkNumber, " 
            + "baa.amount as inAmount, "
            + "baa.apartmentType as apType, "
            + "baa.buildingName as buildingName, "
            + "( " 
                + "SELECT sum(amount) FROM vbankaccountactivity WHERE type = 'IN' " 
                + "AND isTransfer = 0 AND operationDT between ? AND baa.operationDT "
                +"ORDER BY baa.crt"
                + ") as masterAccTotalAmount " 
                + "FROM vbankaccountactivity baa " 
                + "WHERE baa.type = 'IN' " 
                + "AND baa.isTransfer = 0 " 
                + "AND baa.operationDT BETWEEN ? AND ? ";
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() +1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        valuesArr.push(startFrom);
        valuesArr.push(startFrom);
        valuesArr.push(endIn);

        if(request.query.buildingName){
            buildingName = request.query.buildingName;
            statementStr += "AND baa.buildingName = ? ";
            valuesArr.push(buildingName);
        }

        if(request.query.apType){
            apType = request.query.apType;
            statementStr += "AND baa.apartmentType = ? ";
            valuesArr.push(apType);
        }
        
        statementStr += "ORDER BY baa.crt ASC";

        var query = {
            statement: statementStr,
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: valuesArr
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.crt;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        client: group[0].clientName,
                        caseNumber: group[0].caseNumber,
                        linkNumber: group[0].linkNumber,
                        amount: group[0].inAmount.toFixed(2),
                        sold: group[0].masterAccTotalAmount.toFixed(2),
                        buildingName: group[0].buildingName,
                        apType: group[0].apType
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    overview: function(request, reply){
        var sold = 0.00;
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() + 1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        var query = {
            statement: "SELECT * FROM vbankaccountactivity baa WHERE baa.operationDT BETWEEN ? AND ? ORDER BY baa.crt;"
            
            /*statement: " SELECT  baa.crt, baa.operationDT, baa.clientName, baa.supplierName, baa.description, baa.type, baa.linkNumber, "
                        +"        COALESCE(SUM(CASE   WHEN baa.type = 'IN' THEN baa.amount "
                        +"            END ),0) as inAmount, "
                        +"        COALESCE(SUM(CASE   WHEN baa.type = 'OUT' then baa.amount * (-1)"
                        +"            END ),0) as outAmount, "
                        +"        COALESCE(SUM(CASE WHEN baa.type = 'IN' THEN baa.amount "
                        +"                    WHEN baa.type = 'OUT' THEN baa.amount * (-1) "
                        +"            END),0) as totalAmount, "
                        +"        (SELECT SUM(CASE WHEN type = 'IN' then amount "
                        +"                WHEN type = 'OUT' THEN amount * (-1) "
                        +"            END) FROM vbankaccountactivity "
                        +"        WHERE crt <= baa.crt "
                        +"        ORDER BY crt "
                        +"        ) as sold  "
                        +" FROM vbankaccountactivity baa "
                        +" WHERE baa.operationDT BETWEEN ? AND ? "
                        +" GROUP BY baa.crt, baa.operationDT, baa.clientName, baa.supplierName, baa.description, baa.type, baa.linkNumber "
                        +" ORDER BY baa.crt;"

                        +"SELECT SUM(CASE WHEN type = 'IN' then amount WHEN type = 'OUT' THEN amount * (-1) END) as sold "
                        +"FROM vbankaccountactivity baa WHERE baa.operationDT BETWEEN ? AND ? ORDER BY baa.crt ;"*/

                        +" SELECT  COALESCE(SUM(CASE    WHEN baa.type = 'IN' THEN baa.amount "
                        +"                     END ),0) as inTotalAmount, "
                        +"         COALESCE(SUM(CASE    WHEN baa.type = 'OUT' then baa.amount * (-1) "
                        +"                     END ),0) as outTotalAmount, "
                        +"         COALESCE(SUM(CASE     WHEN type = 'IN' then amount "
                        +"                      WHEN type = 'OUT' THEN amount * (-1) "
                        +"            END ),0) as grandTotalAmount "
                        +" FROM vbankaccountactivity baa "
                        +" WHERE baa.operationDT BETWEEN ? AND ?; "

                        +" SELECT   COALESCE(SUM(CASE     WHEN type = 'IN' then amount "
                        +"                      WHEN type = 'OUT' THEN amount * (-1) "
                        +"            END ), 0) as grandInitialTotalAmount "
                        +" FROM vbankaccountactivity baa "
                        +" WHERE baa.operationDT < ?; ",


            values: [startFrom, endIn, startFrom, endIn, startFrom, endIn, startFrom]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data[0], function(group) {
                     return group.crt;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }
                sold = parseFloat(data[2][0].grandInitialTotalAmount.toFixed(2));
                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        type: group[0].type,
                        client: group[0].clientName,
                        supplier: group[0].supplierName,
                        description: group[0].description,
                        linkNumber: group[0].linkNumber,
                        inAmount: null,
                        outAmount: null,
                        inTotalAmount: data[1][0].inTotalAmount.toFixed(2),
                        outTotalAmount: data[1][0].outTotalAmount.toFixed(2),
                        grandTotalAmount: data[1][0].grandTotalAmount.toFixed(2),
                        grandInitialTotalAmount: data[2][0].grandInitialTotalAmount.toFixed(2),
                        totalAmount: null,
                        sold: null
                    }
                    var totalGrAmount = 0.00;
                    if(item.type == 'IN'){
                        item.inAmount = group[0].amount.toFixed(2);
                        if(group.length>1){
                            
                            _.each(group, function(gr){
                                totalGrAmount += parseFloat(gr.amount.toFixed(2));
                            });
                            item.inAmount = totalGrAmount.toFixed(2);
                        }
                        
                        sold += parseFloat(item.inAmount);
                    }else{
                        item.outAmount = -1*group[0].amount.toFixed(2);
                        if(group.length>1){
                            _.each(group, function(gr){
                                totalGrAmount += parseFloat(-1*gr.amount.toFixed(2));
                            });
                            item.outAmount = totalGrAmount.toFixed(2);
                        }
                        
                        sold += parseFloat(item.outAmount);
                    }
                    
                    item.sold = sold.toFixed(2);

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    invoices: function(request, reply){
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        var supplierName = null;
        var serviceGroups = null;
        var building = null;
        var invoiceType = null;

        var valuesArr = [];

        var statementString = "SELECT DISTINCT "
                                +"baa.id "
                                +",vi.id as idV "
                                +",baa.operationDT "
                                +",vi.createDT "
                                +",baa.crt "
                                +",vi.supplierName "
                                +",vi.serviceGroupName "
                                +",baa.linkNumber "
                                +",vi.buildingAliasName "
                                +",vi.invoiceTypeName "
                                +",vi.externalNumber "
                                +",vi.statusId "
                                +",vi.amount as invoiceAmount "
                                +",vi.remainingAmount "
                                +",baa.details "
                                +",baa.amount as outAmount "
                                +"FROM vinvoice vi "
                                +"LEFT JOIN vbankaccountactivity baa "
                                +"ON (vi.id = baa.invoiceId) "                                
                                +"WHERE vi.createDT BETWEEN ? AND ? "
                                
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);
            if (request.query.supplierName) supplierName = request.query.supplierName;
            if (request.query.serviceGroups) serviceGroups = request.query.serviceGroups;
            if (request.query.building) building = request.query.building;
            if (request.query.invoiceType) invoiceType = request.query.invoiceType;
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() +1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        valuesArr.push(startFrom);
        valuesArr.push(endIn);

        if(supplierName){
            statementString += "AND vi.supplierName = ? ";
            valuesArr.push(supplierName);
        }

        if(serviceGroups){
            statementString += "AND vi.serviceGroupName = ? ";
            valuesArr.push(serviceGroups);
        }

        if(building){
            statementString += "AND vi.buildingAliasName = ? ";
            valuesArr.push(building);
        }

        if(invoiceType){
            statementString += "AND vi.invoiceTypeName = ? ";
            valuesArr.push(invoiceType);
        }
        statementString += "ORDER BY baa.crt ASC ";
        var query = {
            statement:  statementString ,
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: valuesArr
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 // var groups = _.groupBy(data, function(group) {
                 //     return group.idV;
                 // });

                 var groups = data;

                var invoiceList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        id: group.id,
                        idV: group.idV,
                        operationDT: group.operationDT,
                        createDT: group.createDT,
                        crt: group.crt,
                        supplierName: group.supplierName,
                        serviceGroupName: group.serviceGroupName,
                        linkNumber: group.linkNumber,
                        buildingAliasName: group.buildingAliasName,
                        invoiceTypeName: group.invoiceTypeName,
                        externalNumber: group.externalNumber,
                        statusId: group.statusId,
                        invoiceAmount: group.invoiceAmount,
                        remainingAmount: group.remainingAmount,
                        details: group.details,
                        amount: group.outAmount
                    }

                    invoiceList.push(item);
                });
                
                /*_.each(data, function(dat) {

                    var item = {
                        id: dat.id,
                        operationDT: dat.operationDT,
                        createDT: dat.createDT,
                        crt: dat.crt,
                        supplierName: dat.supplierName,
                        serviceGroupName: dat.serviceGroupName,
                        linkNumber: dat.linkNumber,
                        buildingAliasName: dat.buildingAliasName,
                        invoiceTypeName: dat.invoiceTypeName,
                        statusId: dat.statusId,
                        invoiceAmount: dat.invoiceAmount,
                        remainingAmount: dat.remainingAmount,
                        details: dat.details,
                        amount: dat.outAmount
                    }

                    invoiceList.push(item);
                });*/

                dbCallback(err, invoiceList, reply, query);
            }
        });
    }
}

exports.charts = {
    overview: function(request, reply){
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() + 1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        var query = {
            statement: "SELECT * FROM (SELECT baa.operationDT, YEARWEEK(baa.operationDT) as yweek,  "
                        + "baa.correctAmount, @balance := @balance + baa.correctAmount as sold "
                        + "FROM "
                        + "(select vba.operationDT, vba.type, vba.amount,"
                        + "(SELECT case when type='OUT' then -1*amount else amount end as amt)as correctAmount from "
                        + "vbankaccountactivity vba WHERE vba.operationDT BETWEEN ? AND ? ORDER BY vba.operationDT ASC) as baa,"
                        + "(select @balance := 0.00) as SqlVars) as baaa "
                        + "group by yweek" ,

            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: [startFrom, endIn]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.operationDT;
                 });

                var overviewList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        sold: group[0].sold.toFixed(2)
                    }

                    overviewList.push(item);
                });

                dbCallback(err, overviewList, reply, query);
            }
        });
    },
    transfers: function(request, reply){
        if (request.query.type && !((request.query.type == "IN") || (request.query.type == "OUT") || (request.query.type == ""))) {
            reply("Invalid Call").code(404);        
        };
        
        var transferType = request.query.type ? request.query.type : "";
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() + 1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";
        
        var startFrom = startYear + "-" + startMonth + "-" + startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";
        
        var endIn = endYear + "-" + endMonth + "-" + endDay+""+endTime;
        
        var nonType = null;
        switch (transferType) {
            case "IN":
                nonType = "OUT";
                break;
            case "OUT":
                nonType = "IN";
                break;
            default:
                nonType = "";
        }
        
        var query = {
            statement: "SELECT baa.crt ,baa.operationDT "
                            +",baa.amount as inAmount "
                            +",(SELECT case when type='OUT' then -1*amount else amount end as amt)as correctAmount "
                            +"FROM vbankaccountactivity baa "
                            +"WHERE baa.isTransfer = 1 "
                            +"AND baa.operationDT BETWEEN ? AND ? "
                            +"AND baa.type <> ? "
                            +"ORDER BY baa.crt",
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            
            values: [startFrom, endIn, nonType]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.crt;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,                        
                        amount: group[0].inAmount.toFixed(2),
                        correctAmount: group[0].correctAmount.toFixed(2)
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    incomes: function(request, reply){
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        var buildingName = null;
        var apType = null;
        var valuesArr = [];
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);            
        } catch (error) {
            reply(error).code(404);
        }

        var statementStr = "SELECT "
            +"baa.crt, " 
            + "baa.operationDT, " 
            + "baa.clientName, " 
            + "baa.amount as inAmount "             
            + "FROM vbankaccountactivity baa " 
            + "WHERE baa.type = 'IN' " 
            + "AND baa.isTransfer = 0 " 
            + "AND baa.operationDT BETWEEN ? AND ? ";
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() +1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        valuesArr.push(startFrom);
        valuesArr.push(endIn);

        if(request.query.buildingName){
            buildingName = request.query.buildingName;
            statementStr += "AND baa.buildingName = ? ";
            valuesArr.push(buildingName);
        }

        if(request.query.apType){
            apType = request.query.apType;
            statementStr += "AND baa.apartmentType = ? ";
            valuesArr.push(apType);
        }
        
        statementStr += "ORDER BY baa.crt ASC";

        var query = {
            statement: statementStr,
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: valuesArr
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.crt;
                 });

                var incomeList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        client: group[0].clientName,
                        amount: group[0].inAmount.toFixed(2)                        
                    }

                    incomeList.push(item);
                });

                dbCallback(err, incomeList, reply, query);
            }
        });
    },

    invoices: function(request, reply){
        var startDate = new Date(1900, 1, 1);
        var endDate = new Date(2099, 12, 31);
        
        var supplierName = null;
        var serviceGroups = null;
        var building = null;
        var invoiceType = null;

        var valuesArr = [];

        var statementString = "SELECT DISTINCT "
                                +"baa.id "
                                +",baa.operationDT "
                                +",baa.crt "
                                +",vi.supplierName "
                                +",vi.serviceGroupName "
                                +",baa.linkNumber "
                                +",vi.buildingAliasName "
                                +",vi.invoiceTypeName "
                                +",vi.statusId "
                                +",baa.details "
                                +",baa.amount as outAmount "
                                +"FROM vinvoice vi "
                                +"LEFT JOIN vbankaccountactivity baa "
                                +"ON baa.invoiceId = vi.id "                                
                                +"WHERE baa.operationDT BETWEEN ? AND ? "
        
        try {
            if (request.query.startDate) startDate = new Date(request.query.startDate);
            if (request.query.endDate) endDate = new Date(request.query.endDate);
            if (request.query.supplierName) supplierName = request.query.supplierName;
            if (request.query.serviceGroups) serviceGroups = request.query.serviceGroups;
            if (request.query.building) building = request.query.building;
            if (request.query.invoiceType) invoiceType = request.query.invoiceType;
        } catch (error) {
            reply(error).code(404);
        }
        
        var startYear = startDate.getFullYear();
        var startMonth = startDate.getMonth() +1;
        var startDay = startDate.getDate();
        var startTime = "T00:00:00.000Z";

        var startFrom = startYear+"-"+startMonth+"-"+startDay+""+startTime;
        
        var endYear = endDate.getFullYear();
        var endMonth = endDate.getMonth() + 1;
        var endDay = endDate.getDate();
        var endTime = "T23:59:59.000Z";

        var endIn = endYear+"-"+endMonth+"-"+endDay+""+endTime;

        valuesArr.push(startFrom);
        valuesArr.push(endIn);

        if(supplierName){
            statementString += "AND vi.supplierName = ? ";
            valuesArr.push(supplierName);
        }

        if(serviceGroups){
            statementString += "AND vi.serviceGroupName = ? ";
            valuesArr.push(serviceGroups);
        }

        if(building){
            statementString += "AND vi.buildingAliasName = ? ";
            valuesArr.push(building);
        }

        if(invoiceType){
            statementString += "AND vi.invoiceTypeName = ? ";
            valuesArr.push(invoiceType);
        }

        statementString += "ORDER BY baa.crt ASC";
        
        var query = {
            statement:  statementString ,
            // statement: "CALL getAllIncomsInRange (@startDate:=?, @endDate:=?)",
            values: valuesArr
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                 var groups = _.groupBy(data, function(group) {
                     return group.crt;
                 });

                var invoiceList = [];

                if (data.length == 0) {
                    dbCallback(err, {}, reply, query);
                    return;
                }

                _.each(groups, function(group) {

                    var item = {
                        id: group[0].id,
                        operationDT: group[0].operationDT,
                        crt: group[0].crt,
                        supplierName: group[0].supplierName,
                        serviceGroupName: group[0].serviceGroupName,
                        linkNumber: group[0].linkNumber,
                        buildingAliasName: group[0].buildingAliasName,
                        invoiceTypeName: group[0].invoiceTypeName,
                        statusId: group[0].statusId,
                        details: group[0].details,
                        amount: group[0].outAmount.toFixed(2)
                    }

                    invoiceList.push(item);
                });

                dbCallback(err, invoiceList, reply, query);
            }
        });
    }
}

exports.media = {
    upload: function(request, reply) {
        var temporaryFileName = request.payload.uploadedFile.path.replace(/^.*[\\\/]/, '')
        reply(temporaryFileName).code(200);
    },
    download: function (request, reply) {
        var query = {
            statement: "SELECT name FROM file WHERE pathToFile = ?",
            values: ['media\\' + request.params.path.replace('/', '\\')]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                if (err) {
                    dbCallback(err, data, reply, query);
                } else {
                    reply.file(mediaPath + request.params.path, { 
                        filename: data[0].name,
                        mode: 'attachment', 
                        lookupCompressed: true // allow looking for *.gz if the request allows it
                    });
                }
            }
        });
    }
}

exports.validateCRT = {
    trueOrFalse: function(request, reply) {
        var query = {
            statement: "SELECT id FROM bankaccountactivity WHERE crt = ? AND isDeleted = 0;",
            values: [request.params.crt]
        };

        db.query({
            sql: query.statement,
            values: query.values,
            callback: function(err, data) {
                dbCallback(err, data, reply, query);
            }
        });
    }
}