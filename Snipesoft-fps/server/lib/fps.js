exports.register = function (server, options, next) {

    var validate = function (decodedToken, callback) {    
        /* token was successfully verified, roleId can be trusted and passed as scope */

        return callback(null, true, {
            scope: [decodedToken.role],
            id: decodedToken.id
        })
    };

    var constants = require('.././config');
    server.auth.strategy('fpsToken', 'jwt', false, {
        key: constants.security.privateKey,
        validateFunc: validate
    });

    server.route({
        method: 'GET',
        path: '/fps/{param*}',
        handler: {
            directory: {
                path: __dirname + '/../../web',
                index: true
            }
        },
        config: {
            auth: false
        }
    });

    var api = require('./api');

    server.route({
        method: 'POST',
        path: '/fps/api/media',
        handler: api.media.upload,
        config: {
            payload: {
                maxBytes: 52428800,
                output: 'file',
                parse: true,
                uploads: __dirname + '/../temp',
                allow: 'multipart/form-data'
            },
            auth: 'fpsToken'
        }
    });

    server.route({
        method: 'GET',
        path: '/fps/media/{path*}',
        handler: api.media.download,
        config: { auth: 'fpsToken' }
    });

    var routes = require('./routes');
    routes.map(function (route) {
        if (typeof route.config === 'undefined' || typeof route.config.auth === 'undefined') {
            route.config = { auth: 'fpsToken' };
        };
    });
    server.route(routes);

    next();
}

exports.register.attributes = {
    name: 'FPS',
    version: '1.0.0'
};