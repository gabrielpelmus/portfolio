var api = require('./api');
var Joi = require('joi');

module.exports = [
{
    method: 'POST',
    path: '/fps/api/login',
    handler: api.auth.login,
    config: {
        auth: false
    }
}, 
{
    method: 'GET',
    path: '/fps/api/location',
    handler: api.location.all,
    config: {
        
    }
}, 
// grupe servicii
{
    method: 'GET',
    path: '/fps/api/apartments',
    handler: api.apartments.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/apartments/{id}',
    handler: api.serviceGroups.byId,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/apartments',
    handler: api.apartments.insert,
    config: {
        validate: {
            payload: {
                buildingId: Joi.any().required(),
                caseId: Joi.any().optional(),
                stairs: Joi.any().required(),
                floor: Joi.any().required(),
                apType: Joi.any().required(),
                apNumber: Joi.any().required()
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/apartments/{id}',
    handler: api.apartments.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                buildingId: Joi.any().required(),
                caseId: Joi.any().optional(),
                stairs: Joi.any().required(),
                floor: Joi.any().required(),
                apType: Joi.any().required(),
                apNumber: Joi.any().required()
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/apartments/{id}',
    handler: api.apartments.delete,
    config: {
        
    }
},
// end apartamente
// grupe servicii
{
    method: 'GET',
    path: '/fps/api/serviceGroups',
    handler: api.serviceGroups.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/serviceGroups/{id}',
    handler: api.serviceGroups.byId,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/serviceGroups',
    handler: api.serviceGroups.insert,
    config: {
    	validate: {
            payload: {
                name: Joi.string().required(),
                description: Joi.string().optional(),
                details: Joi.string().optional().allow('', null)
            }
        }
    }
},	
{
    method: 'PUT',
    path: '/fps/api/serviceGroups/{id}',
    handler: api.serviceGroups.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                name: Joi.any().required(),
                description: Joi.any().optional(),
                details: Joi.string().optional().allow('', null)
            }
        }
	}
}, 
{
    method: 'DELETE',
    path: '/fps/api/serviceGroups/{id}',
    handler: api.serviceGroups.delete,
    config: {
        
    }
},
// end grupe servicii

// furnizori
{
    method: 'GET',
    path: '/fps/api/suppliers',
    handler: api.suppliers.all
}, 
{
    method: 'GET',
    path: '/fps/api/suppliers/{id}',
    handler: api.suppliers.byId,
    config: {
        
    }
}, 
{
    method: 'POST',
    path: '/fps/api/suppliers',
    handler: api.suppliers.insert,
    config: {
        validate: {
            payload: {
                name: Joi.string().required(),
                address: Joi.string().optional(),
                CUI: Joi.any().optional(), 
                J: Joi.any().optional(),          
                telephoneFax: Joi.string().optional(), 
                mobile1: Joi.any().optional(), 
                mobile2: Joi.any().optional(), 
                email1: Joi.string().optional(),
                email2: Joi.string().optional(), 
                website: Joi.string().optional(),
                details: Joi.string().optional().allow('', null),
                services: Joi.array().items(Joi.object().keys({
                    id: Joi.number().required()
                }))
            }
        }
    }
},
{
    method: 'PUT',
    path: '/fps/api/suppliers/{id}',
    handler: api.suppliers.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                name: Joi.any().required(),
                address: Joi.any().optional(),
				CUI: Joi.any().optional(), 
                J: Joi.any().optional(),          
             	telephoneFax: Joi.any().optional(), 
                mobile1: Joi.any().optional(), 
                mobile2: Joi.any().optional(), 
                email1: Joi.any().optional(),
                email2: Joi.any().optional(), 
                website: Joi.any().optional(),
                details: Joi.string().optional().allow('', null),
                services: Joi.any().optional()
            }
        }
	}
}, 
{
    method: 'DELETE',
    path: '/fps/api/suppliers/{id}',
    handler: api.suppliers.delete,
    config: {
        
    }
},
// end furnizori

// start users
{
    method: 'GET',
    path: '/fps/api/users',
    handler: api.users.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/users/{id}',
    handler: api.users.byId,
    config: {
        
    }
},
// end users
// start buildings
{
    method: 'GET',
    path: '/fps/api/buildings',
    handler: api.buildings.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/buildings/{id}',
    handler: api.buildings.byId,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/buildings',
    handler: api.buildings.insert,
    config: {
        validate: {
            payload: {
                name: Joi.string().required(),
                address: Joi.any().optional(),
                details: Joi.any().optional().allow('', null)
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/buildings/{id}',
    handler: api.buildings.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                name: Joi.any().required(),
                address: Joi.any().optional(),
                details: Joi.any().optional().allow('', null)
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/buildings/{id}',
    handler: api.buildings.delete,
    config: {
        
    }
},
// end buildings
// start bankaccounts
{
    method: 'GET',
    path: '/fps/api/bankaccounts',
    handler: api.bankAccounts.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/bankaccounts/{id}',
    handler: api.bankAccounts.byId,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/bankaccounts',
    handler: api.bankAccounts.insert,
    config: {
        validate: {
            payload: {
                accountName: Joi.string().required(),
                owner: Joi.string().optional(),
                currency: Joi.string().optional(),
                bankName: Joi.string().optional(),
                description: Joi.string().optional().valid(''),
                details: Joi.any().optional().allow('', null)
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/bankaccounts/{id}',
    handler: api.bankAccounts.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                accountName: Joi.string().required(),
                owner: Joi.string().optional(),
                currency: Joi.string().optional(),
                bankName: Joi.string().optional(),
                description: Joi.string().optional().valid(''),
                details: Joi.any().optional().allow('', null)
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/bankaccounts/{id}',
    handler: api.bankAccounts.delete,
    config: {
        
    }
},
//end bankaccounts

//start clients
{
    method: 'GET',
    path: '/fps/api/clients',
    handler: api.clients.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/clients/{id}',
    handler: api.clients.byId,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/clients',
    handler: api.clients.insert,
    config: {
        validate: {
            payload: {
                name: Joi.string().required(),
                RegNo: Joi.string().required(),
                mobile: Joi.any().required(),
                id : Joi.any().optional(),
                type: Joi.string().optional(),
                contactPerson: Joi.any().optional(),
                address: Joi.any().optional(),
                email: Joi.any().optional(),
                websiteOrFaceBook: Joi.any().optional(),
                details: Joi.any().optional().allow('', null),
                cases: Joi.array()
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/clients/{id}',
    handler: api.clients.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.required(),
                name: Joi.string().required(),
                RegNo: Joi.string().required(),
                mobile: Joi.any().required(),
                type: Joi.string().optional(),
                contactPerson: Joi.any().optional(),
                address: Joi.any().optional(),
                email: Joi.any().optional(),
                websiteOrFaceBook: Joi.any().optional(),
                details: Joi.any().optional().allow('', null),
                cases: Joi.array()
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/clients/{id}',
    handler: api.clients.delete,
    config: {
        
    }
},
//end clients

//start invoices
{
    method: 'GET',
    path: '/fps/api/invoices',
    handler: api.invoices.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/invoices/{id}',
    handler: api.invoices.byId,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/invoices/{startDate},{endDate}',
    handler: api.invoices.byDateRange,
    config: {
        
    }
}, 
{
    method: 'POST',
    path: '/fps/api/invoices',
    handler: api.invoices.insert,
    config: {
        validate: {
            payload: {
                supplierId: Joi.required(),
                serviceGroupId: Joi.optional().allow('', null),
                buildingId: Joi.optional(),
                externalNumber: Joi.optional(),
                amount: Joi.optional(),
                currency: Joi.optional(),
                createDT: Joi.optional(),
                dueDT: Joi.optional(),
                paymentDT: Joi.optional(),
                details: Joi.optional().allow('', null),
                statusId: Joi.optional(),
                invoiceTypeId: Joi.optional(),
                hasIssue: Joi.optional(),
                issueDescription: Joi.optional(),
                lastUpdateUserId: Joi.optional(),
                files: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                filesToBeDeleted: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/invoices/{id}',
    handler: api.invoices.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                supplierId: Joi.optional(), 
                serviceGroupId: Joi.optional().allow('', null), 
                buildingId: Joi.optional(),
                externalNumber: Joi.optional(),
                amount: Joi.optional(),
                currency: Joi.optional(),
                createDT: Joi.optional(),
                dueDT: Joi.optional(),
                paymentDT: Joi.optional(),
                details: Joi.optional().allow('', null),
                statusId: Joi.optional(),
                invoiceTypeId: Joi.optional(),
                hasIssue: Joi.optional(),
                issueDescription: Joi.optional(),
                lastUpdateUserId: Joi.optional(),
                files: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                filesToBeDeleted: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/invoices/{id}',
    handler: api.invoices.delete,
    config: {
        
    }
},
//end invoices
//start paymentTypes
{
    method: 'GET',
    path: '/fps/api/paymentTypes',
    handler: api.paymentTypes.all,
    config: {
        
    }
}, 

//end paymentTypes

// begin payments

{
    method: 'GET',
    path: '/fps/api/payments/{invoiceId}',
    handler: api.payments.getHistory,
    config: {
        
    }
}, 

{
    method: 'POST',
    path: '/fps/api/payments',
    handler: api.payments.insert,
    config: {
        validate: {
            payload: {
                operationDT: Joi.required(),
                crt: Joi.number().optional(),
                linkNumber: Joi.number().optional().allow('', null),
                partialAmount: Joi.number().optional(),
                details: Joi.string().optional().allow('', null),
                invoicesToPay: Joi.required(),
                lastUpdateUserId: Joi.optional()
            }
        }
    }
}, 
{
    method: 'PUT',
    path: '/fps/api/payments',
    handler: api.payments.delete,
    config: {
        validate: {
            payload: {
                paymentsToBeDeleted: Joi.array().items(Joi.object().keys({
                    paymentCrt: Joi.any().optional()
                }))
            }
        }
    }
},
// end payments

//start invoiceStatuses

{
    method: 'GET',
    path: '/fps/api/invoiceStatuses',
    handler: api.invoiceStatuses.all,
    config: {
        
    }
}, 
//end invoice statuses
//start invoiceTypes

{
    method: 'GET',
    path: '/fps/api/invoiceTypes',
    handler: api.invoiceTypes.all,
    config: {
        
    }
}, 

//end invoiceTypes

// start cases
{
    method: 'GET',
    path: '/fps/api/cases',
    handler: api.cases.all,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/cases/{id}',
    handler: api.cases.byId,
    config: {
        
    }
},
// end cases
//start incoms
{
    method: 'GET',
    path: '/fps/api/incoms',
    handler: api.incoms.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/incoms/{id}',
    handler: api.incoms.byId,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/incoms/{startDate},{endDate}',
    handler: api.incoms.byDateRange,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/incoms',
    handler: api.incoms.insert,
    config: {
        validate: {
            payload: {
                crt: Joi.number().optional(),
                amount: Joi.number().optional(),
                clientCaseId: Joi.required(),
                description: Joi.string().optional().allow('', null),
                details: Joi.string().optional().allow('', null),
                linkNumber: Joi.number().optional().allow('', null),
                operationDT: Joi.required(),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/incoms/{id}',
    handler: api.incoms.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                crt: Joi.number().optional(),
                amount: Joi.number().required(),
                clientCaseId: Joi.required(),
                description: Joi.string().optional().allow('', null),
                details: Joi.string().optional().allow('', null),
                linkNumber: Joi.number().optional().allow('', null),
                operationDT: Joi.required(),
                files: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                filesToBeDeleted: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/incoms/{id}',
    handler: api.incoms.delete,
    config: {
        
    }
},
//end incoms
//start transfer
{
    method: 'GET',
    path: '/fps/api/transfers',
    handler: api.transfers.all,
    config: {
        
    }
}, 
{
    method: 'GET',
    path: '/fps/api/transfers/{id}',
    handler: api.transfers.byId,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/transfers/{type},{startDate},{endDate}',
    handler: api.transfers.byDateRange,
    config: {
        
    }
},  
{
    method: 'POST',
    path: '/fps/api/transfers',
    handler: api.transfers.insert,
    config: {
        validate: {
            payload: {
                crt: Joi.number().optional(),
                amount: Joi.number().required(),
                operationDT: Joi.required(),
                description: Joi.any().optional().allow('', null),
                details: Joi.string().optional().allow('', null),
                linkNumber: Joi.number().optional().allow('', null),
                transferToOrFromBankAccountId: Joi.any().required(),
                type: Joi.any().valid('IN', 'OUT'),
                files: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
},  
{
    method: 'PUT',
    path: '/fps/api/transfers/{id}',
    handler: api.transfers.update,
    config: {
        validate: {
            params: {
                id: Joi.required()
            },
            payload: {
                id: Joi.optional(),
                crt: Joi.number().optional(),
                amount: Joi.number().required(),
                operationDT: Joi.required(),
                description: Joi.any().optional().allow('', null),
                details: Joi.string().optional().allow('', null),
                linkNumber: Joi.number().optional().allow('', null),
                transferToOrFromBankAccountId: Joi.any().required(),
                type: Joi.any().valid('IN', 'OUT'),
                files: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                filesToBeDeleted: Joi.array().items(Joi.object().keys({
                    id: Joi.any().required()
                })),
                newFiles: Joi.array().items(Joi.object().keys({
                    status: Joi.any().required()
                }))
            }
        }
    }
}, 
{
    method: 'DELETE',
    path: '/fps/api/transfers/{id}',
    handler: api.transfers.delete,
    config: {
        
    }
},
//end transfer

//start reports
{
    method: 'GET',
    path: '/fps/api/reports/transfers',
    handler: api.reports.transfers,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/reports/incomes',
    handler: api.reports.incomes,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/reports/overview',
    handler: api.reports.overview,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/reports/invoices',
    handler: api.reports.invoices,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/charts/overview',
    handler: api.charts.overview,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/charts/transfers',
    handler: api.charts.transfers,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/charts/incomes',
    handler: api.charts.incomes,
    config: {
        
    }
},
{
    method: 'GET',
    path: '/fps/api/validateCRT/{crt}',
    handler: api.validateCRT.trueOrFalse,
    config: {
        
    }
}
];