"use strict";

var mysql = require('mysql');

module.exports = function () {
    var internals = {};
    var externals = {};

    var options = {
        multipleStatements: true,
        host: "localhost",
        user: "root",
        password: "BvBzDbPh4",
        database: "fps",
        timezone: "utc"
    };
    
    // var options = {
    //     multipleStatements: true,
    //     host: "infinitymedia.ro",
    //     user: "infinity_fps",
    //     password: "BvBzDbPh4",
    //     database: "infinity_fps"
    // };

    var pool = mysql.createPool(options);

    internals.connect = function (connectHandler) {
        pool.getConnection(function (err, connection) {
            if (err) return connectHandler(err, null);
            return connectHandler(null, connection);
        });
    };

    externals.query = function (params) {
        var sql = params.sql;
        var values = params.values;
        var queryHandler = params.callback;
        internals.connect(function (err, connection) {
            if (err) return queryHandler(err, null);
            connection.query(sql, values, function (err, rows, fields) {
                queryHandler(err, rows);
                connection.release();
            });
        });
    };

    externals.transaction = function (params) {

        var queries = params.queries;
        var queryHandler = params.callback;
        var results = [];

        var callTransactionQuery = function (connection, index, result) {
            var query = queries[index];

            if (query) {
                var sql = query.statement;
                var vals = query.values;

                /* replace "!" wildcard with previous result */
                if (result) {
                    sql = sql.replace("!", result.insertId);
                }

                connection.query(sql, vals, function (err, rows, fields) {
                    if (err) {
                        connection.rollback(function () { throw err; });
                        connection.release();
                        return queryHandler(err, null);
                    };

                    results.push(rows);
                    callTransactionQuery(connection, ++index, rows);
                });
            } else {
                connection.commit(function (err) {
                    if (err) {
                        connection.rollback(function () { throw err; });
                    }
                    queryHandler(err, results);
                    connection.release();
                });
            }
        }

        internals.connect(function (err, connection) {
            if (err) return queryHandler(err, null);

            connection.beginTransaction(function (err) {
                if (err) {
                    connection.release();
                    return queryHandler(err, null);
                }

                callTransactionQuery(connection, 0, null);
            });
        });
    }

    return externals;
}();