module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            prebuild: {
                src: ['build']
            },
            postbuild: {
                src: ['.tmp']
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            main: {
                files: [{
                    expand: true,
                    cwd: 'web/',
                    src: ['app/**/*.js', 'resources/js/*.js'],
                    dest: 'web/'
                }]
            }
        },
        copy: {
            main: {
                src: 'web/index.html',
                dest: 'build/web/index.html'
            },
            views: {
                files: [
                    { expand: true, cwd: 'web/app/views/', src: ['**/*.html'], dest: 'build/web/views/' },
                    { expand: true, cwd: 'web/app/directives/', src: ['**/*.html'], dest: 'build/web/directives/' },            
                ],
            },
            libsAndMedia: {
                files: [
                    { expand: true, cwd: 'web/resources/libs/', src: ['**/*.*'], dest: 'build/web/resources/libs' },
                    { expand: true, cwd: 'web/resources/img/', src: ['**/*.*'], dest: 'build/web/resources/img' }
                ],
            },
            server: {
                expand: true,
                src: ['server/**', '!**/package.json', '!**/node_modules/**', '!**/media/**', '!**/temp/**'],
                dest: 'build/'
            }
        },
        useminPrepare: {
            html: 'web/index.html',
            options: {
                dest: 'build/web'
            }
        },
        usemin: {
            html: ['build/web/index.html']
        },
        uglify: {
            server: {
                expand: true,
                cwd: 'build/server/',
                src: ['**/*.js'],
                dest: 'build/server/',
                options: {
                    report: 'min',
                    mangle: true
                }
            }
        },
        es6transpiler: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'build/server',
                        src: ['**/*.js'],
                        dest: 'build/server'
                    }
                ]
            }
        },
        concat: {
            options: {
                separator: ';'
            }
        },

        replace: {
            viewPath: {
                src: ['build/web/js/*.js', 'build/web/views/**/*.html'],
                replacements: [{
                    from: 'app/views',
                    to: 'views'
                }, {
                        from: 'app/directives',
                        to: 'directives'
                    }],
                overwrite: true
            },
            dbPath: {
                src: ['build/server/db.js'],
                replacements: [{
                    from: 'host: "localhost",',
                    to: 'host: "isaranapps.ro",'
                }],
                overwrite: true
            },
            host: {
                src: ['build/server/server.js'],
                replacements: [{
                    from: 'host: "localhost"',
                    to: 'host: "isaranapps.ro"',
                }],
                overwrite: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-es6-transpiler');    
 
    grunt.registerTask('default', [
    'clean:prebuild', 'ngAnnotate',
    'copy:main', 'useminPrepare', 'cssmin:generated', 'concat:generated', 'uglify:generated', 'usemin',
    'copy:views', 'copy:libsAndMedia', 'replace:viewPath','clean:postbuild',		
    'copy:server', 'es6transpiler:dist', 'uglify:server'
    ]);
};