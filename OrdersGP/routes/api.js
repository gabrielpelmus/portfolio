var passport = require('passport');
var config = require('../config/database').default;
require('../config/passport').default(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();

var Table = require("../src/app/models/table").default;
var Category = require("../src/app/models/category").default;
var Product = require("../src/app/models/product").default;
var Order = require("../src/app/models/order").default;
var Area = require("../src/app/models/area").default;
var User = require("../src/app/models/user").default;

router.post('/signup', function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({success: false, msg: 'Please pass username and password.'});
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    // save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Username already exists.'});
      }
      res.json({success: true, msg: 'Successful created new user.'});
    });
  }
});

router.post('/signin', function(req, res) {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.sign(user.toJSON(), config.secret);
          // return the information including token as JSON
          res.json({success: true, token: 'JWT ' + token});
        } else {
          res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
});



/* SAVE Order */
router.post('/order', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var newOrder = new Order({
      /*name: req.body.name,
      group: req.body.group*/
    });

    newOrder.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Save order failed.'});
      }
      res.json({success: true, msg: 'Successful created new order.'});
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* UPDATE Order */
router.put('/order/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newOrder = new Order({
      /*name: req.body.name,
      group: req.body.group*/
    });

    Order.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* DELETE Order */
router.delete('/order/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newOrder = new Order({
      /*name: req.body.name,
      group: req.body.group*/
    });

    Order.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* GET Order */
router.get('/order', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Order.find(function (err, orders) {
      if (err) return next(err);
      res.json(orders);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* SAVE Area */
router.post('/area', passport.authenticate('jwt', { session: false}), function(req, res) {
  
  var token = getToken(req.headers);
  if (token) {
    var newArea = new Area({
      display_name: req.body.display_name,
      description: req.body.description,
      observations: req.body.observations
    });

    newArea.save(function(err, doc) {
      if (err) {
        return res.json({success: false, msg: 'Save area failed.'});
      }
      res.json({success: true, msg: 'Successful created new area.', area_id: doc.id});
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* GET Area */
router.get('/area', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Area.find(function (err, areas) {
      if (err) return next(err);
      res.json(areas);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* UPDATE Area */
router.put('/area/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newOrder = new Area({
      display_name: req.body.display_name,
      description: req.body.description,
      seats: req.body.seats,
      observations: req.body.observations
    });

    Area.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* DELETE Area */
router.delete('/area/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newArea = new Area({
      display_name: req.body.display_name,
      description: req.body.description,
      seats: req.body.seats,
      observations: req.body.observations
    });

    Area.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* CREATE Table */
router.post('/table', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  console.log(req.body);
  if (token) {
    var newTable = new Table({
      number: req.body.number,
      areaId: req.body.areaId,
      seats: req.body.seats,
      x_pos: req.body.x_pos,
      y_pos: req.body.y_pos,
      observations: req.body.observations
    });

    newTable.save(function(err, doc) {
      if (err) {
        return res.json({success: false, msg: 'Save table failed.'});
      }
      res.json({success: true, msg: 'Successful created new table.', table_id: doc.id});
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* READ Table */
router.get('/table', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Table.find(function (err, tables) {
      if (err) return next(err);
      res.json(tables);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

router.get('/area/:areaId/tables', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Table.find({areaId: req.params.areaId}, (err, tables) => {
      let tablesArray = [];
      
      if(err){
        return res.status(500).send({message: err.message});
      }

      if(tables){
        tables.forEach(table => {
          tablesArray.push(table);
        });
      }

      res.send(tablesArray);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* CREATE Category */
router.post('/category', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var newCategory = new Category({
      display_name: req.body.display_name,
      description: req.body.description,
      keywords: req.body.keywords,
      root: req.body.root,
      long_description: req.body.long_description
    });

    newCategory.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Save category failed.'});
      }
      res.json({success: true, msg: 'Successful created new category.'});
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

// READ Category
router.get('/category', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Category.find(function (err, categories) {
      if (err) return next(err);
      res.json(categories);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

router.get('/category/:id', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Category.findById(req.params.id, function (err, categories) {
      if (err) return next(err);
      res.json(categories);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* UPDATE Category */
router.put('/category/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newCategory = new Category({
      name: req.body.name,
      group: req.body.group
    });

    Category.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* DELETE Category */
router.delete('/category/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newCategory = new Category({
      name: req.body.name,
      group: req.body.group
    });

    Category.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

// CREATE Product
router.post('/product', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var newProduct = new Product({
      brand: req.body.brand,
      display_name: req.body.display_name,
      price: req.body.price,
      tax: req.body.tax,
      discount: req.body.discount,
      description: req.body.description,
      long_description: req.body.long_description,
      category_id: req.body.category_id,
      keywords: req.body.keywords
    });

    newProduct.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Save product failed.'});
      }
      res.json({success: true, msg: 'Successful created new product.'});
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

//  READ Products
router.get('/product', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    Product.find(function (err, products) {
      if (err) return next(err);
      res.json(products);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* UPDATE Product */
router.put('/product/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newProduct = new Product({
      brand: req.body.brand,
      display_name: req.body.display_name,
      price: req.body.price,
      tax: req.body.tax,
      discount: req.body.discount,
      description: req.body.description,
      long_description: req.body.long_description,
      keywords: req.body.keywords
    });

    Product.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

/* DELETE Product */
router.delete('/product/:id', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  var token = getToken(req.headers);
  if (token) {
    var newProduct = new Product({
      name: req.body.name,
      group: req.body.group
    });

    Product.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});


const getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;