import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryEditComponent implements OnInit {

  category = {}

  constructor(private http:HttpClient, private router:Router, private route:ActivatedRoute) { }

  ngOnInit() {
  	this.getCategory(this.route.snapshot.params['id']);
  }

  getCategory(id){
  	this.http.get('/category/'+id).subscribe(data=>{
  		this.category = data;
  	});
  }

  updateCategory(id, data){
  	this.http.put('/category/'+id, data)
  		.subscribe(res=>{
  			let id = res['_id'];
  			this.router.navigate(['/category-details', id]);
  		}, (err)=>{
  			console.log(err);
  		});
  }

}
