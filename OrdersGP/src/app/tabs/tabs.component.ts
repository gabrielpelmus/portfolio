import { Component, Input, AfterViewChecked, OnInit, ElementRef, ChangeDetectorRef, OnChanges } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AreaCreateComponent } from '../area-create/area-create.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterViewChecked, OnChanges {
  @Input() tabs: Array<any>;
  @Input() closeTabsAllowed: boolean;

  activeTab = 0;
  wrapper: any;
  wrapperWidth: number;
  tabsWidth: number;
  minimumTabs = 1;
  tables: any;

  constructor (private elementRef: ElementRef, private changeDetectorRef: ChangeDetectorRef,
     private modalService: NgbModal, private http: HttpClient, private router: Router) {}

  ngOnInit() {
    this.wrapper = this.elementRef.nativeElement.querySelector('.wrapper');
    this.tables = [];
  }

  open() {
    let modalRef = this.modalService.open(AreaCreateComponent);
    modalRef.componentInstance.passNewArea.subscribe((receivedArea) => {
      this.tabs.push(receivedArea);
      this.setTabSize();
    });
  }

  // On resize, recalculate the size of tabs
  ngAfterViewChecked() {
    if (this.wrapper.offsetWidth !== 0 && this.wrapper.offsetWidth !== this.wrapperWidth) {
      this.wrapperWidth = this.wrapper.offsetWidth;
      this.setTabSize();
    }
    if(this.tables.length == 0 && this.tabs.length>0){
      this.updateTables(this.tabs[this.activeTab]._id);
    }
  }

  ngOnChanges() {
    if (this.tabs.length > 0){
      this.setTabSize();      
    } 
  }

  selectTab = (index: number) => {
    this.activeTab = index;
    this.updateTables(this.tabs[index]._id);
  }

  updateTables(areaId: string){
    this.tables = [];
    let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get('/api/area/' + areaId + '/tables', httpOptions).subscribe(data => {
      this.tables = data;
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
  }

  closeTab = (index: number) => {
    this.tabs.splice(index, 1); // Remove the tab.

    // Change in activeTab
    if (index === this.activeTab) this.activeTab = 0;
    if (index < this.activeTab) this.activeTab--;

    this.setTabSize(); // Resize the tabs
  }

  setTabSize = () => {
    const numberTab = this.tabs.length > this.minimumTabs ? this.tabs.length : this.minimumTabs;
    this.tabsWidth = (this.wrapperWidth-40) / numberTab;
    this.changeDetectorRef.detectChanges();
  }
}
