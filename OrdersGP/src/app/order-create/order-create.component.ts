import { Component , OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.scss']
})
export class OrderCreateComponent implements OnInit {

  @Input() table: any;
  order: any;
  products: any;
  selected_product: any;
  total_price: any;
  total_tax: any;
  total_discount: any;
  categories: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  	this.total_price = 0;
    this.total_tax = 0;
    this.total_discount = 0;
    this.products = [];

    //this.categoryService.currentCategories.subscribe(categories => this.categories = categories);

    let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get('/api/category', httpOptions).subscribe(data => {
      this.categories = data;
      console.log(this.categories);
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });

    // let httpOptions = {
    //   headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    // };
    // this.http.get('/api/product', httpOptions).subscribe(data => {
    //   this.products = data;
    //   console.log(this.products);
    // }, err => {
    //   if(err.status === 401) {
    //     this.router.navigate(['login']);
    //   }
    // });

  }

  onChange(selected_value){
    let selected_object = this.products.find(function(element){
      return element._id === selected_value;
    });
    this.selected_product = selected_object;
  }

  sendToOrder(){
    this.order.push(this.selected_product);
    this.total_price += this.selected_product.price;
    this.total_tax += this.selected_product.tax;
    this.total_discount += this.selected_product.discount;
  }

  saveOrder(){
  	let httpOptions = {
      		headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    	};
		this.http.post('/api/order', this.order, httpOptions)
			.subscribe(res=>{
				let id = res['_id'];
				this.router.navigate(['/orders']);
			}, (err)=>{
				if(err.status === 401) {
			    	this.router.navigate(['login']);
			    }
			});
  }

}
