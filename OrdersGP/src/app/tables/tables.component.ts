import { Component, OnInit, Input, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableCreateComponent } from '../table-create/table-create.component';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {

  @Input() tables: Array<any>;
  @Input() areas: Array<any>;

  activeTable = 0;
  wrapper: any;
  wrapperWidth: number;
  tablesWidth: number;
  minTablesWidth = 80;
  minimumTables = 1;

  constructor(private elementRef: ElementRef, private changeDetectorRef: ChangeDetectorRef, private modalService: NgbModal) { }

  ngOnInit() {
    this.wrapper = this.elementRef.nativeElement.querySelector('.wrapper');
  }

  open() {
    const modalRef = this.modalService.open(TableCreateComponent);
    modalRef.componentInstance.areas = this.areas;
    modalRef.componentInstance.passNewTable.subscribe((receivedTable) => {
      this.tables.push(receivedTable);
      this.setTableSize();
    });
  }

  selectTable = (index: number) => {
    this.activeTable = index;
  }

  ngAfterViewChecked() {
    if (this.wrapper.offsetWidth !== 0 && this.wrapper.offsetWidth !== this.wrapperWidth) {
      this.wrapperWidth = this.wrapper.offsetWidth;
      this.setTableSize();
    }
  }

  ngOnChanges() {
    if (this.tables.length > 0){
      this.setTableSize();
      
    } 
  }

  setTableSize = () => {
    /*const tablesCount = this.tables.length > this.minimumTables ? this.tables.length : this.minimumTables;
    this.tablesWidth = this.wrapperWidth / tablesCount - 14;
    this.tablesWidth = this.tablesWidth > this.minTablesWidth ? this.tablesWidth : this.minTablesWidth;*/
    this.tablesWidth = this.minTablesWidth;
    this.changeDetectorRef.detectChanges();
  }

}
