import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var CategorySchema = new mongoose.Schema({
  	display_name: {
        type: String,
        unique: true,
        required: true
    },
   	description: {type: String},
	creation_date: {type:Date, default:Date.now},
	end_date: {type:Date, default:Date.now},	
	keywords: [{type:String}],
	large_image: {data:Buffer, contentType:String},
	long_description: {type: String},
	root: {type: Boolean},
	parent_category: {type: Schema.Types.ObjectId, ref: 'Category'},
	small_image: {data:Buffer, contentType:String},
	start_date: {type:Date, default:Date.now},
	thumbnail_image: {data:Buffer, contentType:String},
	version: {type: Number},
	update_date: {type:Date, default: Date.now }
});


export default mongoose.model('Category', CategorySchema);