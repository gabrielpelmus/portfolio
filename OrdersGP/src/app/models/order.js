import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var OrderSchema = new mongoose.Schema({
  created_by: {type: Schema.Types.ObjectId, ref: 'User'},
  updated_by: {type: Schema.Types.ObjectId, ref: 'User'},
  table_id: {type: Schema.Types.ObjectId, ref: 'Table'},
  order_type: {type: String},
  state: {type: String},
  price: {type: Number},
  tax_price: {type: Number},
  discount: {type: Number},
  amount_paid: {type: Number},
  payment_type: {type: String},
  creation_date: {type: Date, default: Date.now},
  update_date: {type:Date, default: Date.now }
});


export default mongoose.model('Order', OrderSchema);
