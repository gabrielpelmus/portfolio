import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var TableSchema = new mongoose.Schema({
	number: {type: Number},
	seats: {type: Number, default: 4},
   	areaId: {type: Schema.Types.ObjectId, ref: 'Area'},
   	x_pos: {type: Number, default: 0},
   	y_pos: {type: Number, default: 0},
	observations: {type: String},
	is_occupied: {type: Boolean, default: false},
	is_reserved: {type: Boolean, default: false},
	is_clean: {type: Boolean, default: true},
	creation_date: {type:Date, default:Date.now},
	update_date: {type:Date, default: Date.now },
	created_by: {type: Schema.Types.ObjectId, ref: 'User'},
  	updated_by: {type: Schema.Types.ObjectId, ref: 'User'},
});


export default mongoose.model('Table', TableSchema);