import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var ProductSchema = new mongoose.Schema({
	brand: {
		type: String,
		required: true
	},
  	display_name: {
        type: String,
        unique: true,
        required: true
    },
   	description: {type: String},
   	price: {type: Number},
   	tax: {type: Number},
   	discount: {type: Number},
	creation_date: {type:Date, default:Date.now},
	end_date: {type:Date, default:Date.now},
	date_available: {type:Date, default:Date.now},
	days_available: {type: Number},
	keywords: [{type:String}],
	large_image: {data:Buffer, contentType:String},
	long_description: {type: String},
	category_id: {type: Schema.Types.ObjectId, ref: 'Category'},
	small_image: {data:Buffer, contentType:String},
	start_date: {type:Date, default:Date.now},
	thumbnail_image: {data:Buffer, contentType:String},
	version: {type: Number},
	update_date: {type:Date, default: Date.now }
});


export default mongoose.model('Product', ProductSchema);
