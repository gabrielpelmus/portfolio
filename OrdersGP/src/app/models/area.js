import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var AreaSchema = new mongoose.Schema({
	display_name: {type: String},
   	observations: {type: String},
	creation_date: {type:Date, default:Date.now},
	update_date: {type:Date, default: Date.now },
	created_by: {type: Schema.Types.ObjectId, ref: 'User'},
  	updated_by: {type: Schema.Types.ObjectId, ref: 'User'},
});


export default mongoose.model('Area', AreaSchema);