import { Component, Input, Output, EventEmitter, OnChanges, ElementRef } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnChanges {
  @Input() tab: any;
  @Input() active: boolean;
  @Input() closeAllowed: boolean;
  @Input() width: number;
  @Output() closeEvent: EventEmitter<any> = new EventEmitter();

  constructor (
    private elementRef: ElementRef,
  ) {}

  ngOnChanges() {
    const tabElement = this.elementRef.nativeElement.querySelector('.tab');
    const textElement = this.elementRef.nativeElement.querySelector('.text');

    if (tabElement) {
      tabElement.style.width = this.width + 'px';
      textElement.style.width = this.width - 28 + 'px';
      tabElement.style.transition = 'width 400ms';
    }
  }

  close = (event: { stopPropagation: () => void; }) => {
    event.stopPropagation();
    this.closeEvent.emit();
  }
}
