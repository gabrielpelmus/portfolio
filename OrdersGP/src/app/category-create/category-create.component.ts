import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-category-create',
	templateUrl: './category-create.component.html',
	styleUrls: ['./category-create.component.scss']
})
export class CategoryCreateComponent implements OnInit {
	category = {};

	constructor(public activeModal: NgbActiveModal, private http: HttpClient, private router: Router){}

	ngOnInit(){	}

	saveCategory(){
		let httpOptions = {
      		headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    	};
		this.http.post('/api/category', this.category, httpOptions)
			.subscribe(res=>{
				let id = res['_id'];
				this.router.navigate(['/homepage']);
			}, (err)=>{
				if(err.status === 401) {
			    	this.router.navigate(['login']);
		    }
		});
		this.activeModal.close();
	}
}