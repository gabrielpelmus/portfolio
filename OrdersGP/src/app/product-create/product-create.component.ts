import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-product-create',
	templateUrl: './product-create.component.html',
	styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {
	product = {};
	categories: any;
	constructor(public activeModal: NgbActiveModal, private http: HttpClient, private router: Router){}

	ngOnInit(){	}

	saveProduct(){
		let httpOptions = {
      		headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    	};
		this.http.post('/api/product', this.product, httpOptions)
			.subscribe(res=>{
				let id = res['_id'];
				this.router.navigate(['/homepage']);
			}, (err)=>{
				if(err.status === 401) {
			    	this.router.navigate(['login']);
			    }
			});
		
		this.activeModal.close();
	}
}