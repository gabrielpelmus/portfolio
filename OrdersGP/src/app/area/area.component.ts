import { Component , OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Area } from '../models/Area';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})

export class AreaComponent implements OnInit{

  areas: Area;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.areas = [];
  	let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get<Area[]>('/api/area', httpOptions).subscribe(data => {
      this.areas = data;
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
  }
}
