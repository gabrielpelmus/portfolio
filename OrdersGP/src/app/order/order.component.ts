import { Component , OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
	styleUrls: [
		'../../assets/css/bootstrap.min.css'
	]
})

export class OrderComponent implements OnInit {

  orders: any;
  products: any;
  selected_product: any;
  total_price: any;
  total_tax: any;
  total_discount: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {

    this.total_price = 0;
    this.total_tax = 0;
    this.total_discount = 0;

    let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get('/api/order', httpOptions).subscribe(data => {
      this.orders = data;
      console.log(this.orders);
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
    this.http.get('/api/product', httpOptions).subscribe(data => {
      this.products = data;
      console.log(this.products);
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });


  }

  onChange(selected_value){
    let selected_object = this.products.find(function(element){
      return element._id === selected_value;
    });
    this.selected_product = selected_object;
  }

  sendToOrder(){
    this.orders.push(this.selected_product);
    this.total_price += this.selected_product.price;
    this.total_tax += this.selected_product.tax;
    this.total_discount += this.selected_product.discount;
  }

  logout() {
    localStorage.removeItem('jwtToken');
    this.router.navigate(['login']);
  }

}