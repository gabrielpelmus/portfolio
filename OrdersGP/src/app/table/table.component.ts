import { Component, Input, ElementRef, OnChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderCreateComponent } from '../order-create/order-create.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnChanges {
  @Input() table: any;
  @Input() index: number;
  @Input() active: boolean;
  @Input() width: number;

  constructor (
    private elementRef: ElementRef, private modalService: NgbModal
  ) {
    if(this.table == null){
      this.table = {};
    }
  }

  order(){
    const modalRef = this.modalService.open(OrderCreateComponent);
    modalRef.componentInstance.table = this.table;
  }

  ngOnChanges() {
    const tableElement = this.elementRef.nativeElement.querySelector('.table');
    const textElement = this.elementRef.nativeElement.querySelector('.text');

    if (tableElement) {
      tableElement.style.width = this.width + 'px';
      textElement.style.width = this.width - 28 + 'px';
      tableElement.style.transition = 'width 400ms';
    }
  }
}
