import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryDetailComponent implements OnInit {

  category = {};

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getCategoryDetail(this.route.snapshot.params['id']);
  }

  getCategoryDetail(id) {
    this.http.get('/category/'+id).subscribe(data => {
      this.category = data;
    });
  }

  deleteCategory(id) {
    this.http.delete('/category/'+id).subscribe(res => {
          this.router.navigate(['/categories']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}