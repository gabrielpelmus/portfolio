import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Table } from '../models/Table';

@Component({
  selector: 'app-table-create',
  templateUrl: '../table-create/table-create.component.html',
  styleUrls: ['../table-create/table-create.component.scss']
})
export class TableCreateComponent {

	@Input() display_name: string;
	@Output() passNewTable: EventEmitter<any> = new EventEmitter();

	table: Table;
	areas: any;
  
  constructor(public activeModal: NgbActiveModal, private http: HttpClient, private router: Router) {
		this.table = {};
	}
	
  saveTable(){
	  console.log(this.table);
		let httpOptions = {
      		headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    	};
		this.http.post('/api/table', this.table, httpOptions)
			.subscribe(res=>{
				let id = res['table_id'];
				this.table._id=id;				
				this.passNewTable.emit(this.table);
				this.router.navigate(['/homepage']);
			}, (err)=>{
				if(err.status === 401) {
			    	this.router.navigate(['login']);
				}
				console.log(err);
			});
		this.activeModal.close();
	}
}