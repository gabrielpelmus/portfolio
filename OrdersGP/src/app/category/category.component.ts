import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { CategoryCreateComponent } from '../category-create/category-create.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { CategoryService } from './category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categories: any;

  constructor(private http: HttpClient, private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get('/api/category', httpOptions).subscribe(data => {
      this.categories = data;
      console.log(this.categories);
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
    // this.categoryService.currentCategories.subscribe(categories => this.categories = categories)
  }

  open() {
    let modalRef = this.modalService.open(CategoryCreateComponent);
    // modalRef.componentInstance.passNewArea.subscribe((receivedArea) => {
    //   this.tabs.push(receivedArea);
    //   this.setTabSize();
    // });
  }

  logout() {
    localStorage.removeItem('jwtToken');
    this.router.navigate(['login']);
  }

}