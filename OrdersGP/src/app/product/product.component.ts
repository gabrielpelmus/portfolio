import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { ProductCreateComponent } from '../product-create/product-create.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryComponent } from '../category/category.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  categories: any;
  products: any;

  constructor(private http: HttpClient, private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    let httpOptions = {
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    };
    this.http.get('/api/product', httpOptions).subscribe(data => {
      this.products = data;
      this.products.forEach(product => {
        let category_id = product.category_id;
        this.http.get('/api/category/' + category_id, httpOptions).subscribe(data => {
          let category: any;
          category = data;
          product.category_name = category.display_name;
        });
      });
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
    this.http.get('/api/category', httpOptions).subscribe(data => {
      this.categories = data;
    }, err => {
      if(err.status === 401) {
        this.router.navigate(['login']);
      }
    });
  }

  open() {
    let modalRef = this.modalService.open(ProductCreateComponent);
    modalRef.componentInstance.categories = this.categories;
    // modalRef.componentInstance.passNewArea.subscribe((receivedArea) => {
    //   this.tabs.push(receivedArea);
    //   this.setTabSize();
    // });
  }

}