import { Component,
   OnInit,
   ViewChild, 
   ViewContainerRef, 
   ComponentFactoryResolver
  } from '@angular/core';

import { AreaComponent } from '../area/area.component';
import { AdminZoneComponent } from '../admin-zone/admin-zone.component';
import { ClientZoneComponent } from '../client-zone/client-zone.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  // Setup custom viewChild here
  @ViewChild('componentContainer', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
  }

  openAdminZone(){
    this.viewContainerRef.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(AdminZoneComponent);
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

  openEmployeeZone(){
    this.viewContainerRef.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(AreaComponent);
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

  openClientZone(){
    this.viewContainerRef.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(ClientZoneComponent);
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

}
