import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { CategoryComponent } from '../category/category.component';
import { ProductComponent } from '../product/product.component';

@Component({
  selector: 'app-admin-zone',
  templateUrl: './admin-zone.component.html',
  styleUrls: ['./admin-zone.component.scss']
})
export class AdminZoneComponent implements OnInit {
  @ViewChild('componentContainer', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
  }

  openUsersManagement(){
    console.log("User management");
    this.viewContainerRef.clear();
  }

  openCategoriesManagement(){
    this.viewContainerRef.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(CategoryComponent);
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

  openProductsManagement(){
    this.viewContainerRef.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(ProductComponent);
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

}
