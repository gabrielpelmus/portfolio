import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { CategoryComponent } from './category/category.component';
import { CategoryDetailComponent } from './category-detail/category-detail.component';
import { CategoryCreateComponent } from './category-create/category-create.component';
import { CategoryEditComponent } from './category-edit/category-edit.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NgbdDatepickerPopup } from './datepicker/datepicker.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { TableComponent } from './table/table.component';
import { AreaComponent } from './area/area.component';
import { AreaCreateComponent } from './area-create/area-create.component';
import { AreaDetailComponent } from './area-detail/area-detail.component';
import { AreaEditComponent } from './area-edit/area-edit.component';
import { HomepageComponent } from './homepage/homepage.component';
import { TabComponent } from './tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';
import { TablesComponent } from './tables/tables.component';
import { TableCreateComponent } from './table-create/table-create.component';
import { AdminZoneComponent } from './admin-zone/admin-zone.component';
import { ClientZoneComponent } from './client-zone/client-zone.component'

const appRoutes: Routes = [
  {
    path: 'products',
    component: ProductComponent,
    data: { title: 'Product List' }
  },
  {
    path: 'product-details/:id',
    component: ProductDetailComponent,
    data: { title: 'Product Details' }
  },
  {
    path: 'product-create',
    component: ProductCreateComponent,
    data: { title: 'Create Product' }
  },
  {
    path: 'product-edit',
    component: ProductEditComponent,
    data: { title: 'Edit Product' }
  },
  {
    path: 'categories',
    component: CategoryComponent,
    data: { title: 'Category List' }
  },
  {
    path: 'category-details/:id',
    component: CategoryDetailComponent,
    data: { title: 'Category Details' }
  },
  {
    path: 'category-create',
    component: CategoryCreateComponent,
    data: { title: 'Create Category' }
  },
  {
    path: 'category-edit',
    component: CategoryEditComponent,
    data: { title: 'Edit Category' }
  },
  {
    path: 'orders',
    component: OrderComponent,
    data: { title: 'Orders List' }
  },
  {
    path: 'order-details/:id',
    component: OrderDetailComponent,
    data: { title: 'Order Details' }
  },
  {
    path: 'order-create',
    component: OrderCreateComponent,
    data: { title: 'Create Order' }
  },
  {
    path: 'order-edit',
    component: OrderEditComponent,
    data: { title: 'Edit Order' }
  },
  {
    path: 'areas',
    component: AreaComponent,
    data: { title: 'Areas List' }
  },
  {
    path: 'area-details/:id',
    component: AreaDetailComponent,
    data: { title: 'Area Details' }
  },
  {
    path: 'area-create',
    component: AreaCreateComponent,
    data: { title: 'Create Area' }
  },
  {
    path: 'area-edit',
    component: AreaEditComponent,
    data: { title: 'Edit Area' }
  },
  {
    path: 'tables',
    component: TableComponent,
    data: { title: 'Tables List' }
  },
  {
    path: 'table-create',
    component: TableCreateComponent,
    data: { title: 'Create Table' }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: 'signup',
    component: SignupComponent,
    data: { title: 'Sign Up' }
  },
  {
    path: 'homepage',
    component: HomepageComponent,
    data: { title: 'OrdersGP' }
  },
  {
    path: 'adminzone',
    component: AdminZoneComponent,
    data: { title: 'Admin Zone' }
  },
  {
    path: 'clientzone',
    component: ClientZoneComponent,
    data: { title: 'Client Zone' }
  },
  { path: '',
    redirectTo: '/homepage',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ProductDetailComponent,
    ProductCreateComponent,
    ProductEditComponent,
    CategoryComponent,
    CategoryDetailComponent,
    CategoryCreateComponent,
    CategoryEditComponent,
    LoginComponent,
    SignupComponent,
    NgbdDatepickerPopup,
    OrderComponent,
    OrderDetailComponent,
    OrderCreateComponent,
    OrderEditComponent,
    TableComponent,
    AreaComponent,
    AreaCreateComponent,
    AreaDetailComponent,
    AreaEditComponent,
    HomepageComponent,
    TabComponent,
    TabsComponent,
    TablesComponent,
    TableCreateComponent,
    AdminZoneComponent,
    ClientZoneComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }