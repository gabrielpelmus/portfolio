import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Area } from '../models/Area';
import { Table } from '../models/Table';

@Component({
  selector: 'app-area-create',
  templateUrl: '../area-create/area-create.component.html',
  styleUrls: ['../area-create/area-create.component.scss']
})
export class AreaCreateComponent {

	@Input() display_name: string;
	@Output() passNewArea: EventEmitter<any> = new EventEmitter();

	area: Area;
	table: Table;
  
  constructor(public activeModal: NgbActiveModal, private http: HttpClient, private router: Router) {
		this.area = {};
	}

  saveArea(){
		let httpOptions = {
      		headers: new HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken')?localStorage.getItem('jwtToken'):"" })
    	};
		this.http.post('/api/area', this.area, httpOptions)
			.subscribe(res=>{
				let id = res['area_id'];
				this.area._id = id;
				for(let i=1; i<=this.area.tables; i++){
					this.createTable(i, id, httpOptions);
				}
				this.passNewArea.emit(this.area);
				this.router.navigate(['/homepage']);
			}, (err)=>{
				if(err.status === 401) {
			    	this.router.navigate(['login']);
				}
				console.log(err);
			});		
		this.activeModal.close();
	}

	createTable(tableNumber: Number, areaId: String, httpOptions: any){
		this.table = {};
		this.table.number = tableNumber;
		this.table.areaId = areaId;
		this.http.post('/api/table', this.table, httpOptions)
		.subscribe(res=>{
			this.router.navigate(['/homepage']);
		}, (err)=>{
			if(err.status === 401) {
					this.router.navigate(['login']);
			}
			console.log(err);
		});
	}
}
